<?php

namespace Surefire\Microsites;

/**
 * Plugin Name: SFL Microsites
 * Plugin URI: #
 * Description: SFL Microsites is a plugin that used to have a control panel that creates a microsite pages and more other features.
 * Version: 2.0.0
 * Author: suremak
 * Author URI: #
 * Text Domain: sfl-microsites
**/

define('SFL_MICROSITES_DIR', plugin_dir_path(__FILE__));
define('SFL_MICROSITES_URL', plugin_dir_url(__FILE__));

require_once 'lib/install.php';
require_once 'lib/roles.php';

$helpers = [
    'helpers/debug_helper.php',
    'helpers/ci_helper.php',
    'helpers/script_helper.php',
    'helpers/microsite_helper.php',
    'helpers/yoast_helper.php',
    'helpers/events_helper.php',
];

foreach ($helpers as $helper) {
    if ( ! file_exists(SFL_MICROSITES_DIR . '/lib/' . $helper) ) {
        trigger_error(sprintf(__('Error locating %s for inclusion', 'sfl_microsites'), $helper), E_USER_ERROR);
    } else {
        require_once 'lib/' . $helper;
    }
}
unset($helper);



use Surefire\Microsites\Lib\Install;
use Surefire\Microsites\Lib\Bootstrap;
use Surefire\Microsites\Lib\Sfmu\Posttypes;

register_activation_hook( __FILE__,  [Bootstrap::class, 'create_dashboard_placeholder']);
register_activation_hook( __FILE__, '\\Surefire\\Microsites\\Lib\\Install\\tables' );
register_activation_hook( __FILE__, '\\Surefire\\Microsites\\Lib\\Roles\\setup_roles' );
register_activation_hook( __FILE__, function(){
    $sites = get_sites();

    foreach ($sites as $site) {
        switch_to_blog($site->blog_id);
          \Surefire\Microsites\Lib\Sfmu\Posttypes\register();
          
          flush_rewrite_rules();
        restore_current_blog();
    }
});

register_deactivation_hook( __FILE__, '\\Surefire\\Microsites\\Lib\\Roles\\remove_roles' );

require 'lib/shortcodes.php';
require 'lib/bootstrap.php';
require 'lib/contactform.php';

// Plugin will only load the files for main site
if ( is_main_site() !== FALSE ) {
    require 'lib/locations.php';
    require 'lib/helpers.php';
}

require 'lib/assets.php';
require 'lib/hooks.php';

$sfmu_includes = [
    'sfmu/hooks.php',
    'sfmu/microsite.php',
    'sfmu/posttypes.php',
    'sfmu/cf7_submit.php',
    'sfmu/wp_overrides.php',
];

foreach ($sfmu_includes as $file) {
  if (!$filepath = locate_script('lib/' . $file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sfl_microsites'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);
