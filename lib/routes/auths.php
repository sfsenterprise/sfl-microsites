<?php

namespace Surefire\Microsite\Lib\Routes;

/**
 *
 */
class Auths extends \WP_REST_Controller
{
    /**
    * Register the routes for the objects of the controller.
    */
    public function register_routes()
    {
        $version = '2';
        $namespace = 'surefire/v' . $version;
        $base = 'auth';

        register_rest_route( $namespace, '/' . $base, array(
            array(
                'methods' => 'POST',
                'callback' => array( $this, 'auth_login' ),
                'args' => array()
            ),
        ) );
    }

    /**
    * Callback function for route 'auth'
    * User login process
    */
    public function auth_login ( $request )
    {
        $user = wp_signon( [
            'user_login' => $_POST['username'],
            'user_password' => $_POST['password'],
            'remember' => true,
        ], false );

        if ( is_wp_error( $user ) ) {
            
            return new \WP_REST_Response( preg_replace('#(<a.*?>).*?(</a>)#', '$1$2', $user->get_error_message()), 401);
        }

        return new \WP_REST_Response( $user, 200 );
    }
}
