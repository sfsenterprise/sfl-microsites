<?php

namespace Surefire\Microsite\Lib\Routes;

/**
*
*/
class Pages extends \WP_REST_Controller{

    public $sfl_table;

    public function __construct(){
        $this->sfl_table = 'sfl_microsites';
    }

    public function page_endpoints(){
        $version = '2';
        $namespace = 'surefire/v' . $version;
        $base = 'page';

        register_rest_route( $namespace, '/' . $base , array(
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'pages_view_response' )
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/cities/', array(
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'cities_view_response' )
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/states/', array(
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'states_view_response' )
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/edit/(?P<post_id>\d+)', array(
            array(
                'methods'             => 'GET, POST',
                'callback'            => array( $this, 'page_edit_response' )
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/get_attachment_image_url/', array(
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'get_image_url' )
            ),
        ) );

    }

    public function pages_view_response( $request )
    {
        global $wpdb;
        $microsites = $wpdb->get_results( "SELECT microsite_post_id FROM {$this->sfl_table}", ARRAY_A );

        $data = array();
        foreach ( $microsites as $microsite ) {

            $microsite_id = $microsite['microsite_post_id'];

            // insert to array the microsite
            $data[][$microsite_id] = get_post( $microsite['microsite_post_id'], ARRAY_A );


            $args = array(
                    'post_parent'   => $microsite_id,
                    'post_type'     => SFL_MICROSITE_CPT,
                    'numberposts'   => -1,
                    'post_status'   => 'any'
                );
            $sub_pages = get_children( $args, ARRAY_A );

            // insert to array the sub pages of microsite
            foreach ($sub_pages as $sub_page) {
                $data[][$microsite_id] = $sub_page;
            }

        }

        return new \WP_REST_Response( $data, 200 );
    }

    public function cities_view_response( $request )
    {
        global $wpdb;
        $microsites = $wpdb->get_results( "SELECT microsite_post_id FROM {$this->sfl_table}", ARRAY_A );

        $parents = array();
        foreach ( $microsites as $microsite ) {
            if( ! in_array(wp_get_post_parent_id($microsite['microsite_post_id']), $parents) ) {
                $parents[] = wp_get_post_parent_id($microsite['microsite_post_id']);
            }
        }

        $cities = array();
        foreach ( $parents as $parent ) {
            $cities[] = get_post( $parent, ARRAY_A );
        }

        return new \WP_REST_Response( $cities, 200 );
    }

    public function states_view_response( $request )
    {

        global $wpdb;
        $microsites = $wpdb->get_results( "SELECT microsite_post_id FROM {$this->sfl_table}", ARRAY_A );

        $parents = array();
        foreach ( $microsites as $microsite ) {
            if( ! in_array(wp_get_post_parent_id($microsite['microsite_post_id']), $parents) ) {
                $parents[] = wp_get_post_parent_id($microsite['microsite_post_id']);
            }
        }

        $cities = array();
        foreach ( $parents as $parent ) {
            $cities[] = get_post( $parent, ARRAY_A )['post_parent'];
        }

        $cities = array_unique($cities);

        $states = array();
        foreach ($cities as $city) {
            $states[] = get_post( $city, ARRAY_A );
        }

        return new \WP_REST_Response( $states, 200 );

    }

    public function page_edit_response( $request )
    {

        if( isset ($_POST['update']) ) {

            $post = array(
                    'ID'            => $request['post_id'],
                    'post_content'  => $_POST['post_content'],
                );

            $data = wp_update_post( $post );

            unset($_POST['post_content']);
            unset($_POST['update']);

            if( $_POST && count($_POST) > 0 ) {
                foreach ( $_POST as $name => $value ) {

                    if( $name == 'sfl_microsite_page_title' ) {

                        $microsite_page = wp_update_post(
                            array(
                                'ID'            => $request['post_id'],
                                'post_title'    => $_POST['sfl_microsite_page_title'],
                                'post_modified' => current_time( 'mysql' )
                            )
                        );

                    } else {
                        update_post_meta( $request['post_id'], $name, $value);
                    }

                }
            }

        } else {
            $data = get_post( $request['post_id'], ARRAY_A );
            $data['featured_image'] = get_the_post_thumbnail_url( $request['post_id'] );
            $data['featured_image_id'] = get_post_meta( $request['post_id'], '_thumbnail_id', true );
            $data['bios'] = get_post_meta( $request['post_id'], 'sfl_microsite_bios', true );
            $data['welcome_title'] = get_post_meta( $request['post_id'], 'sfl_microsite_welcome_title', true );
        }

        return new \WP_REST_Response( $data, 200 );

    }

    public function get_image_url( $request ){

        //$data = wp_get_attachment_url( $_POST['attachment_id'] );
$data = array($_GET['attachment_id']);
        return new \WP_REST_Response( $data, 200 );
    }


}
