<?php

namespace Surefire\Microsite\Lib\Routes;

/**
 *
 */
class Microsite extends \WP_REST_Controller{

    public $sfl_table;
    public $sfl_termstable;
    public $wp_terms;

    public function __construct(){
        global $wpdb;
        $this->sfl_table = 'sfl_microsites';
        $this->sfl_termstable = 'sfl_micrositeterms';
        $this->wp_terms = $wpdb->prefix . 'terms';
    }

    public function microsites(){
        $version = '2';
        $namespace = 'surefire/v' . $version;
        $base = 'microsite';

        register_rest_route( $namespace, '/' . $base, array(
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'microsite_view_response' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/add/', array(
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'microsite_add_response' ),
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/add/(?P<id>\d+)', array(
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'microsite_add_response' ),
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/edit/(?P<id>\d+)', array(
            array(
                'methods'             => 'GET',
                'callback'            => array( $this, 'microsite_edit_response' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/delete/(?P<id>\d+)', array(
            array(
                'methods'             => 'DELETE',
                'callback'            => array( $this, 'microsite_delete_response' ),
                'args' => array(
                        'filter' => array(
                                    'description' => esc_html__( 'The filter parameter is used to filter the collection of colors', 'my-text-domain' ),
                                    'type'        => 'string',
                                    'enum'        => array( 'red', 'green', 'blue' )
                            )
                    )
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/addUser/', array(
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'microsite_addUser_response' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/auto_locate/', array(
            array(
                'methods'             => 'POST',
                'callback'            => array( $this, 'microsite_auto_locate' ),
                'args' => array()
            ),
        ) );
    }

    /**
    * Callback function for route microsite 'view'
    */
    public function microsite_view_response( $request )
    {

        global $wpdb;
        $microsites = $wpdb->get_results( "SELECT * FROM {$this->sfl_table}", ARRAY_A );

        return new \WP_REST_Response( $microsites, 200 );
    }

    /**
    * Callback function for route microsite 'add'
    */
    public function microsite_add_response( $request )
    {
        global $wpdb, $sfl_microsite_config;

        if ( !$_POST['microsite_name'] || !$_POST['microsite_city'] || !$_POST['microsite_state_province'] ) {

            $data = array(
                'code'      => 'bad_request',
                'message'   => 'Microsite Name, City and State are required.',
                'data'      => array(
                                'status'    => 500
                            )
            );

            return new \WP_REST_Response( $data, 500 );
        }

        if ( ! isset( $request['id'] ) && $existing = get_page_by_path( sanitize_title($_POST['microsite_name']), OBJECT, $sfl_microsite_config['post_type'] ) ) {
            $data = array(
                'code' => 'microsite_already_exists',
                'message' => 'The microsite you are trying to add already exist.',
                'data'    => array(
                    'status' => 500,
                )
            );

            return new \WP_REST_Response( $data, 500 );
        }

        $others = array(
            'microsite_franchise_id' => @$_POST['microsite_franchise_id'],
            'microsite_trust_id' => @$_POST['microsite_trust_id'],
            'microsite_service_areas' => @$_POST['microsite_service_areas'],
            'microsite_hours' => @$_POST['microsite_hours'],
        );

        $data = array(
            'microsite_name'            => sanitize_text_field( @$_POST['microsite_name'] ) ?: '',
            'microsite_slug'            => sanitize_text_field( @$_POST['microsite_slug'] ),
            'microsite_status'          => 'active',
            'microsite_street'          => sanitize_text_field( @$_POST['microsite_street'] ) ?: '',
            'microsite_street2'         => sanitize_text_field( @$_POST['microsite_street2'] ) ?: '',
            'microsite_city'            => sanitize_text_field( @$_POST['microsite_city'] ) ?: '',
            'microsite_state_province'  => sanitize_text_field( @$_POST['microsite_state_province'] ) ?: '',
            'microsite_fax_number'      => sanitize_text_field( @$_POST['microsite_fax_number'] ) ?: '',
            'microsite_zip_postal'      => sanitize_text_field( @$_POST['microsite_zip_postal'] ) ?: '',
            'microsite_telephone'       => sanitize_text_field( @$_POST['microsite_telephone'] ) ?: '',
            'microsite_email'           => sanitize_text_field( @$_POST['microsite_email'] ) ?: '',
            'microsite_lat'             => @$_POST['microsite_lat'] ?: 0,
            'microsite_lng'             => @$_POST['microsite_lng'] ?: 0,
            'microsite_fields'          => serialize($others),
        );

        if (! @$_POST['microsite_lat'] || ! @$_POST['microsite_lng']) {
            $street = ( @$_POST['microsite_street'] !== '' ) ? @$_POST['microsite_street'] : @$_POST['microsite_street2'];


            $location =  $street . ',' . @$_POST['microsite_city'] . ',' . @$_POST['microsite_state_province'] . ' ' . @$_POST['locator_zip_postal'];
            $location = preg_replace('/\s*C\.?P\.?+\s*?[0-9]{1,4}\s*+/i', "", $location);
            $location = preg_replace("/\s*P\.?O\.?+\s*+B?O?X?+\s*+\d+/i", "", $location);
            $location = preg_replace("/(\d+)\s*?(ROUTE)\s*?(\d+)/i", '$1 $3', $location);
            $location = preg_replace("/([a-z]+[0-9]+[a-z])+([0-9]+[a-z]+[0-9])/i", '$1 $2', $location);
            $location = preg_replace('/\s/', '+', $location);

            $response = wp_remote_get("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCwMiSj9oskXL-3QwUF7sa7GnVxDVMSD-M&address={$location}", array('timeout' => 20, 'redirect' => 20));
            $map = json_decode(wp_remote_retrieve_body($response), true);

            if (isset($map['error_message']))
                return false;

            $latlng = array(
                'lat' => @$map['results'][0]['geometry']['location']['lat'] ?: 0,
                'lng' => @$map['results'][0]['geometry']['location']['lng'] ?: 0
            );

            $data['microsite_lat'] = $latlng['lat'];
            $data['microsite_lng'] = $latlng['lng'];

        }

        if ( ! isset( $request['id'] ) ) { // add
            $data['microsite_date'] = current_time( 'mysql' );
            $data['microsite_modified'] = $data['microsite_date' ];

            $microsite = wp_insert_post(
                array(
                    'post_type'     => $sfl_microsite_config['post_type'],
                    'post_date'     => $data['microsite_date'],
                    'post_title'    => $data['microsite_name'],
                    'post_status'   => 'publish'
                )
            );

            $subpages = array('How It Works', 'Products', 'Blog', 'Contact');

            foreach( $subpages as $page ) {
                wp_insert_post(
                    array(
                        'post_type'   => $sfl_microsite_config['post_type'],
                        'post_title'  => $page,
                        'post_parent' => $microsite,
                        'post_status' => 'publish'
                    )
                );
            }

            if ( ! is_wp_error($microsite) && $microsite > 0 ){
                $headers = array();
                $data['microsite_link'] = get_permalink($microsite);
                $to = 'danemmf@surefiresocial.com';
                $subject = 'Lendio - Microsite Add';
                $body = $data; // microsite's details/data
                $headers[] = "Content-Type: text/html; charset=UTF-8";
                $headers[] = "From: Lendio - Microsite Add <noreply@formleads.surepulse.com>";
                $send = wp_mail( $to, $subject, $body, $headers );

                $data['microsite_post_id'] = $microsite; // add to data array value
                $wpdb->insert( $this->sfl_table, $data ); // save data to microsite table

                $data['microsite_id'] = $wpdb->insert_id; // add to data array value
                $microsite_id = $wpdb->insert_id;
            } else {
                $data = array(
                    'code' => 'microsite_create_error',
                    'message' => $microsite->get_error_message(),
                    'data'    => array(
                        'status' => 500,
                    )
                );

                return new \WP_REST_Response( $data, 500 );
            }

        } else { //edit
            $microsite_id = $request['id'];
            $old_microsite_object = $wpdb->get_row("SELECT * FROM {$this->sfl_table} WHERE ID = '{$request['id']}'");

            /*$microsite = wp_update_post(
                array(
                    'ID'            => $_POST['microsite_post_id'],
                    'post_title'    => $data['microsite_name'],
                    'post_name'     => $data['microsite_slug'],
                    'post_modified' => current_time( 'mysql' )
                )
            );*/

            $wpdb->update( $this->sfl_table, $data, array( 'ID' => $request['id'] ) );

        //update microsite GTM/GA HEADER
        update_option( 'lendio_microsite_gta_head_'.@$microsite_id, stripslashes_deep(@$_POST['microsite_gta_head']) );

        //update microsite GTM/GA BODY
        update_option( 'lendio_microsite_gta_body_'.@$microsite_id, stripslashes_deep(@$_POST['microsite_gta_body']) );

        $category = $wpdb->get_row("SELECT * FROM {$this->sfl_termstable} WHERE microsite_id = {$microsite_id}");

        $new_slug = str_replace(array('-', '*', '.', '@', '&', ' '), '_', $data['microsite_slug']);

        if ( ! $category ) {
            if ( ! $term = term_exists($data['microsite_name'], 'category') )
                $term = wp_insert_term($new_slug . '_' . $microsite_id, 'category', ['slug' => $new_slug . '_' . $microsite_id]);

            $wpdb->insert( $this->sfl_termstable, ['microsite_id' => $microsite_id, 'term_id' => $term['term_id']]);

            if ( isset( $request['id'] ) ) {
                if ( $old_microsite_object->microsite_name !== $data['microsite_name'])
                    wp_update_term($term['term_id'], 'category', ['name' => $data['microsite_name'], 'slug' => $data['microsite_slug'] . '_' . $microsite_id]);
            }

        } elseif( isset( $request['id'] ) ) {
            $old_slug = str_replace('-', '_', $old_microsite_object->microsite_slug) . '_' . $old_microsite_object->ID;
            $term = $wpdb->get_row("SELECT * FROM {$this->wp_terms} WHERE slug = '{$old_slug}' ");
            wp_update_term($term->term_id, 'category', ['name' => $new_slug . '_' . $microsite_id, 'slug' => $new_slug . '_' . $microsite_id]);
        }

        return new \WP_REST_Response( $data, 200 );
    }

    /**
    * Callback function for route 'addUser'
    */
    public function microsite_addUser_response( $request )
    {

        // adding users
        if( count($_POST['addedUsers']) > 0 ) {
            $addedUsers = array_unique($_POST['addedUsers']);

            $add_counter = array();
            foreach ( $addedUsers as $user_id ) {
                $usermeta = get_user_meta( $user_id, 'sfl_microsite_id', true );

                if( $usermeta != $_POST['microsite_id'] ) {
                    $add_counter[] = add_user_meta( $user_id, 'sfl_microsite_id', $_POST['microsite_id'] );
                }

            }
        }

        // remove users
        if( count($_POST['removedUsers']) > 0 ) {

            $remove_counter = array();
            foreach ( $_POST['removedUsers'] as $user_remove_id ) {
                $remove_counter[] = delete_user_meta( $user_remove_id, 'sfl_microsite_id', $_POST['microsite_id'] );
            }
        }

        return new \WP_REST_Response( $data, 200 );

    }


    /**
    * Callback function for route microsite 'edit'
    */
    public function microsite_edit_response( $request )
    {
        global $wpdb, $sfl_loggedin_user;

        // get user data to get the role
        $user = get_userdata($sfl_loggedin_user->ID);

        if( $sfl_loggedin_user->ID ) {
          $user_role = implode(', ', $user->roles);
        }

        if( $user_role === 'author' ) {

            $microsite_users = $wpdb->get_results( "SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'sfl_microsite_id' AND user_id = '{$sfl_loggedin_user->ID}' ", ARRAY_A );

            $users = array();
            foreach ($microsite_users as $microsite_user) {
              $users[] = $microsite_user['meta_value'];
            }

            $all_users = implode(',', $users);

            // get all microsites assoiated with loggedin user
            $microsites = $wpdb->get_results( "SELECT ID FROM sfl_microsites WHERE ID in ({$all_users})", ARRAY_A );

            // check here if microsite was assigned to this logged in user
            foreach ($microsites as $microsite) {
                if ( in_array($request['id'], $microsite ) ) {
                    $data = $wpdb->get_row( "SELECT * FROM {$this->sfl_table} WHERE ID = {$request['id']}", ARRAY_A );

                    $franchise_id = unserialize($data['microsite_fields']);
                    $data['microsite_franchise_id'] = $franchise_id['microsite_franchise_id'];
                    $data['microsite_trust_id'] = $franchise_id['microsite_trust_id'];

                } else {
                    $data = array(
                        'code'      => 'bad_request',
                        'message'   => 'Microsite not found.',
                        'data'      => array(
                                        'status'    => 400
                                    )
                    );
                }
            }

        } else {
            $data = $wpdb->get_row( "SELECT * FROM {$this->sfl_table} WHERE ID = {$request['id']}", ARRAY_A );
            $franchise_id = unserialize($data['microsite_fields']);
            $data['microsite_franchise_id'] = $franchise_id['microsite_franchise_id'];
            $data['microsite_trust_id'] = $franchise_id['microsite_trust_id'];
        }

        return new \WP_REST_Response( $data, 200 );
    }


    /**
    * Callback function for route microsite 'delete'
    */
    public function microsite_delete_response( $request )
    {
        /************************************************
        ****** $data = $request->get_attributes(); ******
        ****** $data['args']['filter']['enum'];    ******
        ************************************************/

        global $wpdb, $city_id, $state_id, $sfl_loggedin_user;
        $microsite = $wpdb->get_row( "SELECT * FROM {$this->sfl_table} WHERE ID = {$request['id']}" );
        $city_id = wp_get_post_parent_id( $microsite->microsite_post_id );
        $state_id = wp_get_post_parent_id( wp_get_post_parent_id( $microsite->microsite_post_id ) );
        $get_micrositeterms = $wpdb->get_row( "SELECT * FROM sfl_micrositeterms WHERE microsite_id = {$request['id']}" );


        if( $microsite ) {

            $args = array(
                'post_parent' => $microsite->microsite_post_id,
                'post_type'   => SFL_MICROSITE_CPT,
                'numberposts' => -1,
                'post_status' => 'any'
            );

            $microsite_children = get_children( $args );

            foreach ( $microsite_children as $child ) {
                wp_delete_post($child->ID);
            }

            // delete post from post type 'microsite'
            $data = wp_delete_post($microsite->microsite_post_id);

            // delete microsite from 'sfl_microsites' table
            $wpdb->delete( $this->sfl_table, array( 'ID' => $request['id'] ) );

            /**
            * Get city children, if returns a null - delete microsite parent(city)
            */
            $microsite_parent_args = array(
                'post_parent' => $city_id,
                'post_type'   => SFL_MICROSITE_CPT,
                'numberposts' => -1,
                'post_status' => 'any'
            );
            $microsite_parent_children = get_children( $microsite_parent_args );

            if( count($microsite_parent_children) < 1 ) { // if city has no microsite children
                wp_delete_post( $city_id );
            }

            /**
            * Get state children, if returns a null - delete city parent(state/province)
            */
            $city_parent_args = array(
                'post_parent' => $state_id,
                'post_type'   => SFL_MICROSITE_CPT,
                'numberposts' => -1,
                'post_status' => 'any'
            );
            $city_parent_children = get_children( $city_parent_args );

            if( count($city_parent_children) < 1 ) { // if state has no city children
                wp_delete_post( $state_id );
            }

            /**
            * delete microsite category and terms
            * update posts status under the category into draft
            */
            if( $get_micrositeterms->term_id ) {
                //wp_delete_category( $get_micrositeterms->term_id );
                //$wpdb->delete( 'sfl_micrositeterms', array( 'microsite_id' => $request['id'] ) );
                $args = array(
                    'posts_per_page'   => -1,
                    'category'         => $get_micrositeterms->term_id,
                    'post_type'        => 'post',
                    'post_status'      => 'publish',
                );
                $posts_array = get_posts( $args );

                foreach ($posts_array as $post_array) {
                    $my_post = array(
                        'ID'            => $post_array->ID,
                        'post_status'   => 'draft',
                    );
                    wp_update_post( $my_post );
                }
            }

            /*
            * process delete email here
            */
            $headers = array();
            unset($microsite->microsite_fields);
            $microsite->loggedin_user = $sfl_loggedin_user->data->display_name;
            $to = 'danemmf@surefiresocial.com';
            $subject = 'Lendio - Microsite Delete';
            $body = $microsite; // microsite's details/data
            $headers[] = "Content-Type: text/html; charset=UTF-8";
            $headers[] = "From: Lendio - Microsite Delete <noreply@formleads.surepulse.com>";
            $send = wp_mail( $to, $subject, $body, $headers );

        } else {
            $data = array(
                'code'      => 'bad_request',
                'message'   => 'Microsite not found.',
                'data'      => array(
                                'status'    => 400
                            )
            );
        }

        return new \WP_REST_Response( $data, 200 );
    }

    public function microsite_auto_locate( $request ){

        $street = ( $_POST['microsite_street'] !== '' ) ? $_POST['microsite_street'] : $_POST['microsite_street2'];

        $location =  $street . ',' . $_POST['microsite_city'] . ',' . $_POST['microsite_state_province'] . ' ' . $_POST['locator_zip_postal'];
        $location = preg_replace('/\s*C\.?P\.?+\s*?[0-9]{1,4}\s*+/i', "", $location);
        $location = preg_replace("/\s*P\.?O\.?+\s*+B?O?X?+\s*+\d+/i", "", $location);
        $location = preg_replace("/(\d+)\s*?(ROUTE)\s*?(\d+)/i", '$1 $3', $location);
        $location = preg_replace("/([a-z]+[0-9]+[a-z])+([0-9]+[a-z]+[0-9])/i", '$1 $2', $location);
        $location = preg_replace('/\s/', '+', $location);

        $response = wp_remote_get("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCwMiSj9oskXL-3QwUF7sa7GnVxDVMSD-M&address={$location}", array('timeout' => 20, 'redirect' => 20));
        $map = json_decode(wp_remote_retrieve_body($response), true);

        if (isset($map['error_message']))
            return false;

        $latlng = array(
            'lat' => @$map['results'][0]['geometry']['location']['lat'] ?: 0,
            'lng' => @$map['results'][0]['geometry']['location']['lng'] ?: 0
        );

        $data = array(
            'microsite_lat'     => $latlng['lat'],
            'microsite_lng'     => $latlng['lng'],
        );

        return new \WP_REST_Response( $data, 200 );
    }

}
