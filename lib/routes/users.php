<?php

namespace Surefire\Microsite\Lib\Routes;

/**
 *
 */
class Users extends \WP_REST_Controller
{

    public $users_table;

    public function __construct(){
        global $wpdb;
        $this->users_table = $wpdb->prefix . 'users';
    }

    /**
    * Register the routes for the objects of the controller.
    */
    public function register_routes()
    {
        $version = '2';
        $namespace = 'surefire/v' . $version;
        $base = 'users';

        register_rest_route( $namespace, '/' . $base, array(
            array(
                'methods' => 'GET',
                'callback' => array( $this, 'list_users' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/user', array(
            array(
                'methods' => 'GET',
                'callback' => array( $this, 'get_current_user' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/edit/(?P<user_id>\d+)', array(
            array(
                'methods' => 'GET',
                'callback' => array( $this, 'user_edit_response' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/add/', array(
            array(
                'methods' => 'POST',
                'callback' => array( $this, 'user_add_response' ),
                'args' => array()
            ),
        ) );

        register_rest_route( $namespace, '/' . $base . '/add/(?P<user_id>\d+)', array(
            array(
                'methods' => 'POST',
                'callback' => array( $this, 'user_add_response' ),
                'args' => array()
            ),
        ) );

    }

    /**
    * Callback function for route 'users'
    * List users
    */
    public function list_users ( $request )
    {
        global $wpdb;
        $args = array(
                'role__in'     => array('author', 'editor'),
                'fields'       => array( 'ID', 'user_login', 'user_nicename', 'user_email', 'user_registered', 'display_name' ),
                'orderby'      => 'display_name',
                'order'        => 'ASC',
             ); 
        $users = get_users($args);

        $microsite_user = array();
        $counter = 0;
        foreach ($users as $user) {

            $user_role = get_user_meta( $user->ID, $wpdb->prefix.'capabilities', true );

            foreach ($user_role as $role => $value) {
                $microsite_user[$counter]['user_role'] = $role;
            }

            $microsite_user[$counter]['ID'] = $user->ID;
            $microsite_user[$counter]['user_login'] = $user->user_login;
            $microsite_user[$counter]['user_nicename'] = $user->user_nicename;
            $microsite_user[$counter]['user_email'] = $user->user_email;
            $microsite_user[$counter]['user_registered'] = date('M j, F', strtotime($user->user_registered));
            $microsite_user[$counter]['display_name'] = $user->display_name;
            $microsite_user[$counter]['first_name'] = get_user_meta( $user->ID, 'first_name', true );
            $microsite_user[$counter]['last_name'] = get_user_meta( $user->ID, 'last_name', true );
            $counter++;

        }

        return new \WP_REST_Response( $microsite_user, 200 );
    }

    /**
    * getCurrentUser
    */
    public function get_current_user ( $request )
    {
        $user = wp_get_current_user();
        if ( !$user->ID ) {
            return new \WP_REST_Response(array(), 403);
        }

        return new \WP_REST_Response( json_encode($user), 201 );
    }

    /**
    *
    */
    public function user_edit_response( $request ){

        $data = get_userdata( $request['user_id'] );

        $user = array();

        $avatar = get_user_meta($request['user_id'], 'sp_avatar', true);

        $user['ID']                 = $data->ID;
        $user['username']           = $data->user_login;
        $user['user_nicename']      = $data->user_nicename;
        $user['user_email']         = $data->user_email;
        $user['user_date']          = date('M j, Y', strtotime($data->user_registered) );
        $user['display_name']       = $data->display_name;
        $user['user_first_name']    = $data->first_name;
        $user['user_last_name']     = $data->last_name;
        $user['user_role']          = $data->roles[0];
        $user['sp_avatar']          = $avatar;
        $user['sp_avatar_img']      = wp_get_attachment_url( absint($avatar), 'avatar' );

        return new \WP_REST_Response( $user, 200 );
    }

    /**
    *
    */
    public function user_add_response( $request ){

        $data = array( 
            'first_name'        => sanitize_text_field( $_POST['user_first_name'] ),
            'last_name'         => sanitize_text_field( $_POST['user_last_name'] ),
            'user_email'        => sanitize_text_field( $_POST['user_email'] ),
            'user_login'        => sanitize_text_field( $_POST['username'] ),
            'user_pass'         => sanitize_text_field( $_POST['user_password'] ),
            'user_registered'   => current_time( 'mysql' ),
            'user_nicename'     => sanitize_text_field($_POST['user_first_name']) . '_' . sanitize_text_field($_POST['user_last_name']),
            'role'              => $_POST['user_role'] ?: 'author',
            'display_name'      => sanitize_text_field( $_POST['user_first_name'] ) . ' ' . sanitize_text_field( $_POST['user_last_name'] ),
        );

        if( $request['user_id'] ) {

            unset($data['user_registered']);

            $data['ID'] = $request['user_id'];
            $user_id = wp_update_user( $data );

            update_user_meta( $request['user_id'], 'sp_avatar', $_POST['sp_avatar']);

        } else {

            $data['user_id'] = wp_insert_user( $data );

            if( $_POST['sp_avatar'] ) {
                $data['sp_avatar'] = add_user_meta( $data['user_id'], 'sp_avatar', $_POST['sp_avatar'] );
            }
        
        }

        return new \WP_REST_Response( $data, 200 );

    }

}
