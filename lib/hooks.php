<?php
namespace Surefire\Microsites\Lib\Hooks;

use Surefire\Microsites\Lib\Bootstrap;
use Surefire\Microsites\Lib\Locations;
use Surefire\Microsites\Lib\Assets;
use Surefire\Microsites\Lib\Contactform;

/**
* Redirect the vanity url to the correct microsite url.
*/
add_action('template_redirect', function () {
    global $wp, $wpdb;

    if ( is_404() === FALSE ) return;

    if ( is_main_site() === FALSE ) return;

    $basedirs = explode('/', trim($_SERVER['REQUEST_URI'], '/'));

    if ( count($basedirs) > 0 ) {
        $vanity = str_replace(['http://', 'https://', 'www'], ['', '', $basedirs[0]], rtrim(home_url('/'), '/'));
        $microsite = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_vanity_url = '{$vanity}'");

        if ( ! $microsite ) return;

        $redirect_to = rtrim(get_site_url($microsite->microsite_blog_id), '/') . '/';
        if ( count($basedirs) > 1 ) {
            array_shift($basedirs);
            $redirect_to = $redirect_to . implode('/', $basedirs) . '/';
        }
        if ( $microsite ) wp_safe_redirect($redirect_to, 301); exit;
    }

    return;
});

/**
* Redirect the page to setup page on plugin first activation.
*/
add_action('init', function () {

    if ( strpos($_SERVER['REQUEST_URI'], '/surefire/auth/') === FALSE && strpos($_SERVER['REQUEST_URI'], '/wp-admin/') === FALSE  && $_SERVER['PHP_SELF'] !== '/wp-login.php' ) {
        if ( strpos($_SERVER['REQUEST_URI'], '/surefire/setup/') === FALSE) {
            switch_to_blog(1);
            $sfmu_setup = absint(get_option('_sfmu_setup', 1));
            restore_current_blog();
            if ( $sfmu_setup !== 0) {
                wp_redirect('/surefire/setup/');
                exit;
            }
        }
    }

});

add_action('init',  [Bootstrap::class, '_sfmu_global_pages'], 99);
add_action('pre_get_posts',  [Bootstrap::class, '_sfmu_update_global_page'], 99, 1);
add_action('pre_get_posts',  [Bootstrap::class, '_sfmu_update_global_form'], 99, 1);

/**
* FAQ and STAFFS post_type hooks
* - Sorting
* - Posts Per Page
*/
add_action('pre_get_posts',  function ( $query) {
    if ( is_admin() ) return $query;

    if ( ! $query->is_main_query() ) return $query;

    if ( $query->get('post_type') === 'ff_faqs' ) {
        $query->set('posts_per_page', -1);
        $query->set('orderby', 'menu_order');
        $query->set('order', 'ASC');
    }

    if ( $query->get('post_type') === 's8_staff_member' ) {
        $query->set('posts_per_page', -1);
        $query->set('orderby', 'menu_order');
        $query->set('order', 'ASC');
    }

    return $query;
});

/**
* FAQ and STAFFS posts
* Modify the ordering.
* Put menu_order 0 below the posts list
*/
add_action( 'the_posts', function( $posts, $query) {
    if ( $query->get('post_type') !== 'ff_faqs' && $query->get('post_type') !== 's8_staff_member' ) return $posts;

    if ( is_admin() ) return $posts;

    $reposts = array();
    $reposts_no_order = array();
    foreach ( $posts as $index => $post ) {
        if ( $post->menu_order === 0 ) {
            $reposts_no_order[] = $post;
        } else {
            $reposts[] = $post;
        }
    }

    usort($reposts, function( $a, $b) {
        return $a->menu_order - $b->menu_order;
    });

    $posts = array_merge($reposts, $reposts_no_order);

    return $posts;
}, 10, 2 );

add_action('wp_enqueue_scripts', function() {

    wp_dequeue_script('google-maps');
    wp_deregister_script('google-maps');

    wp_enqueue_script('google-maps', 'https://maps.google.com/maps/api/js?key=AIzaSyDAZzkjfo5i1kHQUok7i7jsTsceTGlWyN8&libraries=places', ['jquery'], null, true);
    wp_enqueue_script('sfl-locator-map', Assets\asset_path('scripts/sfl-locator-map.js'), ['jquery', 'google-maps'], null, true);
}, 100);

add_action('sfmu_before_send_mail', [Contactform::class, 'before_email_send'], 99, 2);

if ( is_main_site() === FALSE ) return;

add_action('init',  [Bootstrap::class, 'create_dashboard_placeholder']);
add_action('init',  [Locations::class, 'create_locations_post_type']);

// add_action('wpmu_new_blog', [Locations::class, '_ref_location'], 10, 1);
// add_action('refresh_blog_details', [Locations::class, '_ref_location'], 10, 1);
add_action('pre_get_posts', [Locations::class, 'query_sites'], 10, 1);
// add_action('found_posts', [Locations::class, 'found_search_sites'], 10, 1); // This is override for the ?s= search arg to attach site from the location post.
add_filter('posts_results', [Locations::class, 'attach_nap']);

add_filter('template_include', [Bootstrap::class, 'app_init'], 199, 1);

add_action('parse_request', [Bootstrap::class, 'app_loader'], 99, 1);
add_action('pre_get_posts', [Bootstrap::class, 'exclude_dashboard_action'], 1);
add_action('get_blogs_of_user', [Bootstrap::class, 'exclude_main_site_below_franchisor'], 10, 2);

/**
* Hooks that are only needed under Surefire Microsites Dashboard
*/

add_filter('pre_get_document_title', __NAMESPACE__ . '\\surefire_microsites_dashboard_title', 10, 2);

add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 101);

/**
* Removes WordPress Frontend Bloats
*/
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\remove_from_head', 999);

/**
* Used to include media upload override
* 'controller' => array('method')
*/
$include_media_override = array(
    'pages' => true,
    'sitehero' => true,
    'events' => true,
    'jobs' => true,
    'announcements' => true,
    'promos' => true,
    'staff' => true,
    'news' => true,
);

if ( isset($_COOKIE['selected_microsite']) ) {
    $selected_blog_id = $_COOKIE['selected_microsite'];
    $request = explode('/', $_SERVER['REQUEST_URI']);
    $route = isset($request[2]) ? $request[2] : null;
    $method = isset($request[3]) ? $request[3] : null;

    $override = false;
    if ( array_key_exists($route, $include_media_override) ) {
        if ( $include_media_override[$route] === true ) {
            $override = true;
        } elseif (  in_array($method, $this->include_media_override[$route]) ) {
            $override = true;
        }
    }

    if ( absint($selected_blog_id) === 1 ) $override = false;

    if ( $override ) {
        add_action('admin_url', __NAMESPACE__ . '\\modify_wp_media_url', 999, 2);

        add_filter('plupload_default_settings', function( $defaults) use ($selected_blog_id) {
            switch_to_blog($selected_blog_id);
            $defaults['url'] = admin_url('async-upload.php', 'relative');
            restore_current_blog();

            return $defaults;
        }, 999, 1);
    }
}

function modify_wp_media_url($url, $path) {
    if ( get_current_blog_id() !== 1 ) return $url;

    $request = explode('/', $_SERVER['REQUEST_URI']);
    $selected_blog_id = $_COOKIE['selected_microsite'];

    if ( isset( $request[1] ) && $request[1] === 'surefire' ) {
        if (strpos($path, 'admin-ajax.php') !== FALSE)  {
            remove_action('admin_url', __NAMESPACE__ . '\\modify_wp_media_url');
            switch_to_blog($selected_blog_id);
            $url = admin_url('admin-ajax.php', 'relative');
            restore_current_blog();
            add_action('admin_url', __NAMESPACE__ . '\\modify_wp_media_url', 999, 2);
        }
    }

    return $url;
}

/**
* Set Surefire Dashboard title
*/
function surefire_microsites_dashboard_title($title) {
    if ( ! has_ci() )
        return;

    $ci =& get_instance();
    $title = '';

    if ( $ci->router->fetch_class() === 'auth' ) {
        if ( $ci->router->fetch_method() === 'login' ) {
            $title = 'Login | ';
        }
    }

    $title .= 'Goldfish Swim School - Franchising';

    return $title;
}

/**
* Dashboard assets
*/
function assets() {

    if ( ! has_ci() )
        return;

    wp_dequeue_style('surepress/css');
    wp_dequeue_script('surepress/js');

    wp_enqueue_style('sfl-microsites/css', Assets\asset_path('styles/app.css'), false, null);

    wp_enqueue_script('sfl-microsites/js', Assets\asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_enqueue_script('sfl-microsites/angular', Assets\asset_path('scripts/angular.js'), ['jquery'], null, false);
    wp_enqueue_script('sfl-microsites/app', Assets\asset_path('scripts/app.js'), ['sfl-microsites/angular'], null, true);

    wp_enqueue_media();
    wp_enqueue_script( 'suggest' );
    wp_enqueue_script('jquery-ui-sortable');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script('thickbox');
}

function remove_from_head()
{
    remove_action('wp_head', 'rest_output_link_wp_head');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wp_shortlink_wp_head');
    remove_action('wp_head', 'feed_links', 2 );
    remove_action('wp_head', 'feed_links_extra', 3 );
    remove_action('wp_head', 'wp_oembed_add_discovery_links' );
    remove_action('wp_head', 'wp_oembed_add_host_js');
    remove_action('wp_head', 'rel_canonical');
}
