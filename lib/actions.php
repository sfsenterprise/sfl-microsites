<?php

namespace Surefire\Microsite\Lib\Actions;

use Surefire\Microsite\Lib\Wrapper;
use Surefire\Microsite\Lib\Routes\Auths;
use Surefire\Microsite\Lib\Routes\Users;
use Surefire\Microsite\Lib\Routes\Microsite;
use Surefire\Microsite\Lib\Routes\Pages;
use Surefire\Microsite\Lib\Assets;

/**
* So many magic happening here.
* If you know what you are doing then modify this file.
* By: Derik Seno
*/

/**
* Create the Surefire Dashboard Placeholder page
*/
add_action( 'init', function() {
    global $wpdb, $sfl_microsite_config;

    $appview = $wpdb->get_row("SELECT ID FROM {$wpdb->prefix}posts WHERE post_parent = 0 AND post_name = '{$sfl_microsite_config['dashboard_slug']}' AND post_type = 'page'");
    if (!$appview) {
        wp_insert_post(
            array(
                'post_parent' => 0,
                'post_type' => 'page',
                'post_title' => 'Surefire Local Microsite Dashboard',
                'post_name' => $sfl_microsite_config['dashboard_slug'],
                'post_content' => __('This page is just a placeholder for Surefire Local Microsite Dashboard. Whatever you do in this page will not be visible on the frontpage. This should always stay private.', 'sfl-microsite'),
                'post_status' => 'publish',
            )
        );
    }

    add_rewrite_rule('^'.$sfl_microsite_config['dashboard_slug'].'/(.+)/(.+)/?$', 'index.php?pagename='.$sfl_microsite_config['dashboard_slug'].'&route=$matches[1]&method=$matches[2]', 'top');
    add_rewrite_rule('^'.$sfl_microsite_config['dashboard_slug'].'/(.+)/?$', 'index.php?pagename='.$sfl_microsite_config['dashboard_slug'].'&route=$matches[1]', 'top');
    add_rewrite_rule('^'.$sfl_microsite_config['dashboard_slug'].'/?$', 'index.php?pagename='.$sfl_microsite_config['dashboard_slug'], 'top');
}, 100 );

function sflm_pre_get_posts ( $query ) {
    global $wp, $wpdb, $sfl_microsite_config, $microsite_object;

    if ( is_admin() || ! $query->is_main_query() )
        return;

    if ( $query->is_posts_page ) {
        return;
    }

    $microsite_slug = explode('/', $wp->request);
    $microsite_object = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_slug = '{$microsite_slug[0]}'");

    // For archive query
    if ( $query->is_archive('post') ) {
        if ( ! $microsite_object ) {
            $query->set_404();
            status_header(404);
        }

        if ( $query->get('category_name') ) {
            $query->set('category_name', $query->get('category_name') . '+' . microsite_cat() );
        } else {
            $query->set('category_name', microsite_cat());
        }
    }

    if ( $query->is_category() ) {
        if ( $query->get( 'cat' ) ) {
            $term = get_term( $query->get( 'cat' ), 'category' );
        } elseif ( $query->get( 'category_name' ) ) {
            $term = get_term_by( 'slug', $query->query['category_name'], 'category' );
        }

        if ( ! empty( $term ) && ! is_wp_error( $term ) )  {
            $query->queried_object = $term;
            $query->queried_object_id = (int) $term->term_id;

            if ( $query->is_category && 'category' === $query->queried_object->taxonomy )
                _make_cat_compat( $query->queried_object );
        }
    }

    if ( $query->is_singular() && ! $query->get( 'post_type' ) && ! $query->is_page() && ! $query->is_attachment() ) {
        if ( $microsite_object ) {
            $query->set( 'microsite_object', $microsite_object );
        } else {
            $query->set_404();
            status_header(404);
        }
    }

    if ( ! empty( $query->query['page'] ) )
        return;

    if ( ! isset( $query->query['name']) )
        return;

    if ( ! $microsite_object )
        return;

    $query->set( 'microsite_object', $microsite_object );

    if (
        (isset( $microsite_slug[1] ) && $microsite_slug[1] === 'blog' && count($microsite_slug) < 3) ||
        (isset( $microsite_slug[1] ) && $microsite_slug[1] === 'blog' && $microsite_slug[2] === 'page')
    ) {
        $page_for_posts = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts WHERE post_name = 'blog' AND post_parent = {$microsite_object->microsite_post_id}");
        $query->queried_object = get_post( $page_for_posts );
        $query->queried_object_id = (int) $query->queried_object->ID;
        $query->is_single = false;
        $query->is_singular = false;
        $query->is_home = 1;
        $query->is_posts_page = 1;
        $cat = $wpdb->get_row("SELECT * FROM sfl_micrositeterms WHERE microsite_id = {$microsite_object->ID}");
        $query->set('cat', $cat->term_id);

        if ( count($microsite_slug) < 3) {
            $query->set('posts_per_page', 7);
        } else {
            if ( $query->get('paged') === 2 ) {
                $query->set('offset', 7);
            }
        }
    }
    return;
}
add_action( 'pre_get_posts', __NAMESPACE__ . '\\sflm_pre_get_posts' );

/**
* Routes Hooks
*/
add_action( 'rest_api_init', function() {
    $auth_routes = new Auths;
    $auth_routes->register_routes();

    $microsite_routes = new Microsite;
    $microsite_routes->microsites();

    $users_routes = new Users;
    $users_routes->register_routes();

    $pages_routes = new Pages;
    $pages_routes->page_endpoints();
}, 100 );

/**
* Localize the WP REST api url to surefire
*/
add_action( 'wp_enqueue_scripts', function() {
    global $post, $wp_scripts, $wp_styles, $sfl_microsite_config;

    if ( !isset($post) )
        return;

    if ( $post->post_name !== $sfl_microsite_config['dashboard_slug'] )
        return;

    if ( $wp_scripts || $wp_styles ) {
        foreach($wp_scripts->queue as $key => $script) {
            wp_deregister_script( $script );
            unset($wp_scripts->queue[$key]);
        }
        $wp_scripts->queue = array_values($wp_scripts->queue);

        foreach($wp_styles->queue as $key => $style) {
            wp_deregister_style( $style );
            unset($wp_styles->queue[$key]);
        }
        $wp_styles->queue = array_values($wp_styles->queue);
    }

    wp_register_script( 'angularjs', Assets\asset_path('scripts/angular.js'), array(), false);
    wp_register_script( 'sfl-microsite',  Assets\asset_path('scripts/app.js'), array('angularjs'), false);

    wp_localize_script( 'sfl-microsite', 'SFL', array(
        'home_url' => home_url('/'.$sfl_microsite_config['dashboard_slug'].'/'),
        'apiurl' => sprintf('%1$s%2$s', home_url('/'), 'wp-json/surefire/v2/'),
        'appview' => SFL_MICROSITE_APP_DIR . 'views/',
        'appasset' => SFL_MICROSITE_APP_DIR . 'dist/',
        )
    );

    wp_register_script( 'sfl-googlemaps', '//maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyCwMiSj9oskXL-3QwUF7sa7GnVxDVMSD-M', array(), false);
    wp_enqueue_script( 'sfl-googlemaps' );

    wp_enqueue_script( 'sfl-microsite-script',  Assets\asset_path('scripts/main.js'), array('jquery'), true );
    wp_enqueue_style( 'sfl-microsite-style',  Assets\asset_path('styles/app.css'), null);

    wp_enqueue_script( 'angularjs' );
    wp_enqueue_script( 'sfl-microsite' );


    wp_enqueue_media();

}, 110 );

/**
* Load the view template on WP Page placeholder
*/
add_action( 'template_include', function($template) {
    global $post, $sfl_microsite_config;

    if (!$post)
        return $template;

    if ($post->post_parent || $post->post_name !== $sfl_microsite_config['dashboard_slug'] || $post->post_type !== 'page')
        return $template;

    $appview = new Wrapper\Wrapping();

    if ( !($appview) )
        return $template;

    // Remove the WP Admin bar in Surefire Dashboard
    show_admin_bar(false);

    // Remove unnecessary header codes
    remove_theme_support( 'automatic-feed-links' );
    remove_action( 'wp_head', 'wp_resource_hints', 2 );
    remove_action ('wp_head', 'rsd_link' );
    remove_action ('wp_head', 'wlwmanifest_link' );
    remove_action ('wp_head', 'wp_generator' );
    remove_action ('wp_head', 'wp_shortlink_wp_head' );

    return $appview;
}, 110, 1 );

add_action( 'template_include', function($template) {
    global $sfl_microsite_config, $microsite_object, $wp, $wp_query;
    if ( ! $microsite_object )
        return $template;

    if ( isset( $wp->query_vars['post_type'] ) && $wp->query_vars['post_type'] === $sfl_microsite_config['post_type'] ) {
        $request = explode('/', $wp->request);

        if ( in_array('blog', $request) ) {
            if ( count($request) < 3 ) {
                // blog template
            } else {
                if ( in_array('category', $request ) ) {
                    // category template
                } else {
                    // Single template
                }
            }
        }
    } else {
        // sfld($wp_query);
    }


    return $template;

}, 100, 1);

function auth_redirect( $query ) {
    global $sfl_microsite_config;

    if( !is_user_logged_in() && $query->request !== $sfl_microsite_config['dashboard_slug'] . '/auth' ) {
        $request = $query->request;
        $segment = explode('/', $request);

        if( count($segment) >= 1 && $segment[0] === $sfl_microsite_config['dashboard_slug'] ) {
            $auth = home_url($sfl_microsite_config['dashboard_slug'].'/auth');
            header("Refresh: 0; $auth");
            exit;
        }
    } else {
        global $wpdb, $sfl_get_microsites, $sfl_user_role, $sfl_loggedin_user;
        $sfl_loggedin_user = wp_get_current_user();

        $user = get_userdata($sfl_loggedin_user->ID);

        if( $sfl_loggedin_user->ID ) {
          $sfl_user_role = implode(', ', $user->roles);
        }

        if( $sfl_user_role === 'author' ) {

          $microsite_users = $wpdb->get_results( "SELECT meta_value FROM $wpdb->usermeta WHERE meta_key = 'sfl_microsite_id' AND user_id = '{$sfl_loggedin_user->ID}' ", ARRAY_A );

          if ( ! $microsite_users )
            return;

          $users = array();
          foreach ($microsite_users as $microsite_user) {
            $users[] = $microsite_user['meta_value'];
          }

          $all_users = implode(',', $users);
          $sfl_get_microsites = $wpdb->get_results( "SELECT * FROM sfl_microsites WHERE ID in ({$all_users})", OBJECT );

        } else {

          $sfl_get_microsites = $wpdb->get_results( 'SELECT * FROM sfl_microsites', OBJECT );

        }
    }

}
add_action( 'parse_request', __NAMESPACE__ .'\\auth_redirect', 9, 1 );

function sfl_wpadmin_panel(){
    global $sfl_microsite_config;
    $user = wp_get_current_user();

    if( is_user_logged_in() && is_admin() ){
        if( $user->roles[0] !== 'administrator' ) {
            $auth = home_url( $sfl_microsite_config['dashboard_slug'] );
            header("Refresh: 0; $auth");
            //exit;
        }
    }

    $role = get_role( 'author' );
    $role->add_cap( 'edit_others_posts' );
    $role->add_cap( 'edit_pages' );
    $role->add_cap( 'edit_others_pages' );
    $role->add_cap( 'edit_published_pages' );
}
add_action( 'admin_init', __NAMESPACE__ .'\\sfl_wpadmin_panel', 8 );

function sflm_post_type() {
    global $sfl_microsite_config;

    // post type for Microsites
    $labels_microsites = array(
        'name'               => _x( 'Microsites', 'post type general name', 'sfl-microsites' ),
        'singular_name'      => _x( 'Microsite', 'post type singular name', 'sfl-microsites' ),
        'menu_name'          => _x( 'Microsites', 'admin menu', 'sfl-microsites' ),
        'name_admin_bar'     => _x( 'Microsite', 'add new on admin bar', 'sfl-microsites' ),
        'add_new'            => _x( 'Add New', 'microsite', 'sfl-microsites' ),
        'add_new_item'       => __( 'Add New Microsite', 'sfl-microsites' ),
        'new_item'           => __( 'New Microsite', 'sfl-microsites' ),
        'edit_item'          => __( 'Edit Microsite', 'sfl-microsites' ),
        'view_item'          => __( 'View Microsite', 'sfl-microsites' ),
        'all_items'          => __( 'All Microsite', 'sfl-microsites' ),
        'search_items'       => __( 'Search Microsite', 'sfl-microsites' ),
        'parent_item_colon'  => __( 'Parent Microsite:', 'sfl-microsites' ),
        'not_found'          => __( 'No microsite found.', 'sfl-microsites' ),
        'not_found_in_trash' => __( 'No microsite found in Trash.', 'sfl-microsites' )
    );

    $args_microsites = array(
        'labels'             => $labels_microsites,
        'description'        => __( 'Creating a page for microsite.', 'sfl-microsites' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => $sfl_microsite_config['slug'], 'with_front' => $sfl_microsite_config['with_front'] ),
        'capability_type'    => 'page',
        'has_archive'        => $sfl_microsite_config['has_archive'],
        'hierarchical'       => true,
        'menu_position'      => 5,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'page-attributes' )
    );
    register_post_type( $sfl_microsite_config['post_type'], $args_microsites );
}
add_action( 'init', __NAMESPACE__ . '\\sflm_post_type', 99 );
