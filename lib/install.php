<?php

namespace Surefire\Microsites\Lib\Install;

function tables()
{
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();

    $sql[] = "CREATE TABLE sfl_microsites (
      ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
      microsite_blog_id bigint(20) unsigned NOT NULL,
      microsite_post_id bigint(20) unsigned NOT NULL,
      microsite_date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      microsite_updated datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
      microsite_name varchar(250) NOT NULL,
      microsite_slug varchar(250) NOT NULL,
      microsite_vanity_url varchar(250) NOT NULL,
      microsite_street varchar(250) NOT NULL,
      microsite_street2 varchar(250) DEFAULT NULL,
      microsite_city varchar(50) NOT NULL,
      microsite_state_province varchar(50) NOT NULL,
      microsite_zip_postal varchar(10) NOT NULL,
      microsite_phone varchar(100) DEFAULT NULL,
      microsite_general_manager text,
      microsite_lat decimal(10,6) NOT NULL,
      microsite_lng decimal(10,6) NOT NULL,
      microsite_gtm_ga longtext,
      microsite_fields longtext,
      KEY microsite_blog_id (microsite_blog_id),
      KEY microsite_post_id (microsite_post_id),
      KEY microsite_slug (microsite_slug),
      KEY microsite_state_province (microsite_state_province),
      KEY microsite_city (microsite_city),
      KEY microsite_zip_postal (microsite_zip_postal),
      KEY microsite_state_province_microsite_city (microsite_state_province, microsite_city),
      KEY microsite_state_province_microsite_city_microsite_zip_postal (microsite_state_province, microsite_city, microsite_zip_postal),
      PRIMARY KEY  (ID)
    ) $charset_collate;";

    $sql[] = "CREATE TABLE sfl_logs (
        ID bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        microsite bigint(20) unsigned NOT NULL,
        module varchar(100) NOT NULL,
        user bigint(20) unsigned NOT NULL,
        action varchar(50) NOT NULL,
        variables longtext,
        date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
        PRIMARY KEY  (ID),
        KEY date (date)
    ) $charset_collate;";

    $sql[] = "CREATE TABLE {$wpdb->prefix}sfl_lead_detail (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        lead_id bigint(20) unsigned NOT NULL,
        form_id bigint(20) unsigned NOT NULL,
        field_name varchar(100) NOT NULL,
        value longtext,
        PRIMARY KEY  (id),
        KEY lead_id (lead_id),
        KEY form_id (form_id)
    ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

    foreach ( $sql as $table ) {
        dbDelta( $table );
    }

    flush_rewrite_rules();
}
