<?php

return 'Goldfish Swim School in Carmel is the place for children to build character and make new friendships while having a ton of fun! Swim lessons at Goldfish are carefully curated to include swim techniques and water safety at each level, but your kids will forget they’re learning thanks to our friendly staff and fun environment. No matter the weather outside, it’s always a warm, tropical day at Goldfish. Stop by to register for swimming lessons for your kids today!';
