<?php

return 'Goldfish Swim School in Fishers is the best place for your child to cultivate a love for swimming. Our compassionate instructors utilize a proprietary curriculum and small class sizes to build confidence and teach potentially life-saving swim skills. Our Perpetual Lessons model allows your child to continue to progress year-round, so you don’t waste time and money on relearning lost skills. Come check out our state-of-the-art facility and sign up for a convenient time for family swimming lessons. Let’s go swimming!';
