<?php

return 'At Goldfish Swim School, swimming is so much more than a fun activity. We are passionate about teaching Chicago children the fundamentals of great swim technique while building confidence and character with every stroke. In addition, our emphasis on water safety gives your child potentially life-saving skills that last a lifetime. Kids and parents alike love our colorful, tropical-themed décor, shiver-free water, caring instructors and flexible scheduling for swimming lessons. Stop in today and check out our WOW! customer service at your nearest Chicago Goldfish location!';
