<?php

return 'When it’s time to learn how to swim, it’s time for Goldfish Swim School of Mundelein! Our facility joins a growing roster of Goldfish locations in the Chicago suburbs where experienced, caring swim instructors build character while teaching child and infant swim classes. You’ll love watching your child’s skills and confidence grow with each lesson, and your little swimmers will love the fun atmosphere, colorful décor and warm 90-degree water. Let us treat your family to the “Golden Experience” – register today at a time that’s convenient for you!';
