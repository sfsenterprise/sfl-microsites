<?php

return 'Come have a “Golden Experience” at Goldfish Swim School! Families on Northbrook’s west side love our brand new, state-of-the-art swimming facility, and we love teaching kids of all ages and skill levels how to swim and be safer while doing it. We also offer weekly Family Swims and party packages in our colorful, tropical-themed facility. Splash on over for a tour and choose a convenient time for child or toddler swimming lessons. Come swim with us!';
