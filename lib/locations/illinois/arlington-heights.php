<?php

return 'Goldfish Swim School – Johns Creek is the premier learn-to-swim facility in Fulton County! Trained and certified instructors offer indoor swimming lessons for toddlers and kids of all ages, from 4 months to 12 years. Our carefully developed curriculum simultaneously teaches swim skills and water safety, so your little swimmers will understand how to be safer in the water while having tons of fun. Stop by and visit our state-of-the-art facility in Johns Creek, and let’s make a splash together!';
