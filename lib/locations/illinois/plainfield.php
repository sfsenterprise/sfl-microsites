<?php

return 'The staff of Goldfish Swim School combines a passion for kids and a passion for swimming to create a swim experience that’s unlike any other in Plainfield. We strive to develop a personal relationship with each child; building trust, confidence and character with every lesson. Just as importantly, your swimmer will learn potentially life-saving swim safety skills that encourage safety in and around the water. Stop by our state-of-the-art facility and enjoy the fun atmosphere and tropical décor, then register for indoor swimming lessons for your kids today!';
