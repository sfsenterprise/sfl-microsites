<?php

return 'Swim on over to Goldfish Swim School in Glen Ellyn, where we make learning a life-saving skill totally fun! We’re passionate about teaching children ages 4 months to 12 years to swim while building their character and confidence. Highly trained instructors, small class sizes, a state-of-the-art water purification system and a shiver-free 90-degree pool are just a few reasons why kids love learning to swim at Goldfish. Walk through our doors to see and feel the difference, then register your little swimmers today!';
