<?php

return 'We’re ready to treat you to the “Golden Experience” at Goldfish Swim School in St. Charles! An experienced and certified staff, small class sizes, state-of-the-art heated pool and a proprietary curriculum make swimming classes for children fun and stress-free. Parents love our air-conditioned viewing gallery that allows for relaxing or catching up on work while watching kids swim. Plus, all family members can enjoy our beautiful facilities during Family Swim time! Stop by our St. Charles location and let’s make a splash together!';
