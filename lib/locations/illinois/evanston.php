<?php

return 'When it’s time for swimming classes for toddlers and kids, bring your little ones to Goldfish Swim School in Evanston! Your child won’t be locked into one swim level and lesson time for an entire session. Our Perpetual Lessons model ensures your child progresses steadily while having a blast year-round in our tropical 90-degree pool – all at a time that works with your family’s busy schedule. Goldfish is the best place in town for kids to make new friends and develop a love for swimming. Swim on over and let’s make a splash together!';
