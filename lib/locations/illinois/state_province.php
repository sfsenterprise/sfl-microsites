<?php

return 'At Goldfish Swim School in Arlington Heights, we’re passionate about teaching kids how to have fun and be safer in and around the water, while building confidence with every lesson! You will love the extraordinary results we’re able to achieve thanks to a proprietary curriculum and colorful, state-of-the-art facility. Whether you sign up for swimming classes for children, toddlers or babies, your family will have a “Golden Experience” at Goldfish. Stop by today for a tour and choose a lesson time that’s convenient for you!';
