<?php

return 'When it’s time to swim in Naperville, it’s time for Goldfish Swim School! We are a one-of-a-kind swim facility that focuses not only on indoor swimming lessons for toddlers and kids, but also on water safety skills and building character – and we do it all while having a ton of fun! Our crew strives to provide a “Golden Experience” for every family with caring instructors, flexible class scheduling and a state-of-the-art environment. Stop by our Naperville facility and you’ll quickly see why Goldfish is a splash above the rest!';
