<?php

return 'Welcome to Goldfish Swim School of Elmhurst, where learning to swim is an exciting adventure for babies and children. Our proprietary curriculum emphasizes confidence building while teaching children to swim and be safer in and around the water. You’ll be amazed at how quickly your swimmer progresses with our year-round perpetual lesson schedule that’s always tailored around your family’s busy schedule. From our state-of-the-art facilities to our certified staff and beyond, come find out what makes Goldfish a splash above the rest. Register today and let’s get swimming!';
