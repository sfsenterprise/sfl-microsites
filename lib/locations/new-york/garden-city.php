<?php

return 'Goldfish Swim School in Garden City is the newest and best way for kids to learn to swim! Our 7,100-square-foot, state-of-the-art facility is the first Goldfish school in New York. Our friendly staff is passionate not only about providing indoor swimming lessons for kids in a fun atmosphere, but also about teaching children how to be safer in and around the water. Stop by our beautiful facility, located immediately south of the Roosevelt Field mall, to register for a convenient lesson time!';
