<?php

return 'From the moment you enter the doors of Goldfish Swim School on Cleveland\'s East Side, you\'ll see and feel the difference. Our aquatic facility was designed especially with kids and families in mind! We\'re passionate about changing lives by teaching swimming to kids, which means we strive to give them a "Golden Experience" every time. Our pool is always a warm 90 degrees, and our bright, tropical-themed décor will transport you straight to the beach. Swim on over to Warrensville Heights and register for lessons today!';
