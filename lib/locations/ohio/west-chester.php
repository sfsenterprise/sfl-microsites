<?php

return 'Welcome to Goldfish Swim School, West Chester\'s premier location for child and toddler swimming classes. We understand how important swim and water safety is to Cincinnati-area families, and our proprietary curriculum allows children ages 4 months to 12 years to learn these vital skills at a pace that\'s comfortable. Our state-of-the-art, tropical-themed facility makes learning fun! For even more fun in the water, check out our weekly Family Swims and party packages. We can\'t wait to treat you to the "Golden Experience" at Goldfish Swim School!';
