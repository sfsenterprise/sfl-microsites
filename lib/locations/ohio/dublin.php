<?php

return 'Learning to swim is always fun at Goldfish Swim School of Dublin! We take the anxiety out of swimming lessons, starting with colorful, kid-friendly décor and a pool that\'s heated to a comfortable 90 degrees year-round. Your child will be taught crucial swim and water safety skills by a trained and certified instructor who follows the proprietary Goldfish curriculum. We\'re more than just a swim school for kids – we\'re changing lives and building confidence with every lesson! Stop in or call today to register for a convenient lesson time!';
