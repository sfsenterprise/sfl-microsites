<?php

return 'Not all local swim facilities are child-friendly – but Goldfish Swim School has changed all that! Our beautiful Hudson location joins several thriving Ohio locations that offer a proven curriculum for teaching crucial swim and water safety skills, all in an environment designed especially for kids. Teaching kids swimming lessons is all we do, and we\'re passionate about it! Your entire family will love our bright, tropical-themed facility and water that\'s always a shiver-free 90 degrees. Register today and start seeing extraordinary results at Goldfish Swim School!';
