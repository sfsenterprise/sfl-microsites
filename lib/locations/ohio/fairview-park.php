<?php

return 'Looking for swimming classes for kids on Cleveland\'s West Side? You\'ll get more than just swim lessons at Goldfish Swim School in Fairview Park! Our state-of-the-art, 8,900-square-foot indoor aquatic facility has been specially designed to make learning to swim a "Golden Experience" for children and their parents. In addition to high-quality swim instruction, the proprietary Goldfish curriculum also teaches life-saving water safety skills every child needs to know. Register today and let\'s make a splash together at Goldfish Swim School!';
