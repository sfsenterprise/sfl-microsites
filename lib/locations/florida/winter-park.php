<?php

return 'Our Winter Park location is Florida’s first Goldfish Swim School! Learning how to be safer in and around the water is something every child benefits from, but for Florida kids, it’s even more of a necessity. At Goldfish, our trained and certified swim instructors incorporate age-appropriate water safety skills into every lesson. Your child will learn not only how to swim, but also potentially life-saving water safety guidelines. Stop by our fun-filled facility in Winter Park and discover how Goldfish Swim School has revolutionized swimming classes for kids!';
