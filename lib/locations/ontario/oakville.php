<?php

return 'At Goldfish Swim School, we don\'t just give swimming lessons. We build character and instill self-confidence. in every child we teach. Small class sizes and caring instructors ensure that every child has a fantastic time on land and in the water. If you want your child to learn not only to swim, but also potentially life-saving safety skills, let us treat you to the GOLDEN Experience at the Goldfish Swim School!';
