<?php

return 'Goldfish Swim School of Wexford has been making a splash in the Pittsburgh area since 2014! Our beautiful, 8,700-square-foot facility is conveniently located on Lake Drive just north of the Wexford Plaza. Trained and certified instructors offer swimming lessons for toddlers, babies and children, including those as young as 4 months, in a fun and family-friendly atmosphere. We\'re passionate about teaching children to be confident and comfortable in the water. Stop by for a tour and sign up for lessons today!';
