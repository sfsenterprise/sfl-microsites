<?php

return 'Goldfish Swim School of Malvern offers the best swimming lessons for kids, flippers down! If your family has tried many seasonal swim programs with little success, we know you will love our year-round Perpetual Lessons. The Goldfish model ensures your child\'s development stays on track with no interruptions and no wasted time and money. Shiver-free 90-degree water, small class sizes, caring instructors and WOW! customer service make every swim experience "Golden"! Stop by our state-of-the-art Malvern location today and see the Goldfish difference for yourself!';
