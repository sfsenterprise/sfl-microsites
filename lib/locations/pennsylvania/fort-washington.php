<?php

return 'Welcome to Goldfish Swim School in Fort Washington, the best place for Philadelphia-area children to learn how to swim! We offer swimming programs for toddlers and kids of all ages and skill levels, with a focus on proper swim technique and water safety. Even when summer comes to an end, your child can continue to progress and develop swim skills because Goldfish is open year-round! Stop by for a tour of our state-of-the-art facility and register for a convenient lesson time today.'; 
