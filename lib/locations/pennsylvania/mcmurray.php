<?php

return 'It\'s always summer at Goldfish Swim School of Peters Township, the premier swimming school for toddlers and kids in the South Hills! Our beautiful, tropical-themed facility creates an inviting atmosphere where kids learn vital swim and water safety skills while having fun and building character. Parents can watch their children make extraordinary progress from the comfort of an air-conditioned viewing gallery. We can\'t wait to treat your family to the "Golden Experience"! Stop by and register for lessons today!';
