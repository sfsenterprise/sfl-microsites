<?php

return 'Why are West Houston parents raving about Goldfish Swim School? Because we\'re a fin-tastic place for children ages 4 months to 12 years to learn how to swim! Swimming lessons at Goldfish feature certified instructors, small class sizes and a patented curriculum that covers essential swim and water safety skills at an individualized pace. Our tropical décor is bright, fun and entirely kid-friendly, so your family will feel right at home, on land and at sea. Register for lessons today and let us treat you to the "Golden Experience"!'; 
