<?php

return 'When you walk through the door of Goldfish Swim School in Westford, you\'ll feel the difference immediately. You\'ll notice the bright, tropical décor and the curriculum designed to build character while teaching children to swim and be safer in the water. You\'ll notice that our swimmers love the shiver-free, 90-degree heated pool. But the difference is more than that – it\'s the passion we have for changing lives with swimming lessons for kids. Come find out for yourself what makes Goldfish Swim School a splash above the rest!';
