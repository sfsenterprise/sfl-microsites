<?php

return 'Welcome to Goldfish Swim School in Brookline! We are a state-of-the-art learn to swim center, and the area\'s premier swim school for kids! We believe that our perpetual, year-round approach is the best way to teach children how to swim and be safer in the water. Kids that swim at Goldfish learn invaluable life skills and build the confidence they need to succeed. Come see our Golden Experience for yourself! Stop by our colorful, tropical-themed facility today and register for a convenient class time. Let\'s make a splash together!';
