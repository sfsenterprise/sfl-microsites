<?php

return 'At Goldfish Swim School in Braintree, our staff has a special passion for changing lives by teaching children how to swim. Our curriculum isn\'t just about swim lessons; it\'s about building character while we teach vital swim and water safety skills. Our year-round swim instruction is complemented by a colorful, state-of-the-art facility and shiver-free, 90-degree water, so kids look forward to every lesson. Register today for child or baby swimming lessons and let\'s make a splash together at Goldfish Swim School!';
