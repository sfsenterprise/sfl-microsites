<?php

return 'Our Brookfield location is the debut Goldfish Swim School in Wisconsin! We believe that in addition to developing proper swim technique, every child should know the basics of water safety. Our 9,000-square-foot facility is dedicated to helping kids ages 4 months to 12 years have fun and learn how to be safe in the water! The pool is always a shiver-free 90 degrees and fun is always on tap at Goldfish Swim School. Swim over and reserve your spot for the best kids swimming lessons in Brookfield today!';
