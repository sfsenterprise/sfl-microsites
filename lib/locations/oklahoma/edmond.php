<?php

return 'It\'s always time for wet water fun at Goldfish Swim School of Edmond! We offer year-round swimming lessons for children ages 4 months to 12 years in a tropical-themed, family-friendly environment. Not only does our proven curriculum teach kids how to swim, our trained and certified instructors also emphasize life-saving water safety skills during every lesson. The water is always a shiver-free 90 degrees and fun is always on tap at Goldfish Swim School. Swim on over to Edmond and register for a convenient lesson time today!';
