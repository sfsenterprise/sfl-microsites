<?php

/*
* Retrieve categories of specific post type
* @param int|array - blog_id, get_categories parameters
*/
function gss_get_categories($blog_id, $arr)
{
    switch_to_blog( $blog_id );

        $arr['hide_empty'] = false;
        $categories = get_categories($arr);

    restore_current_blog();

    return $categories;
}

/*
* Retrieve categories of specific post
* @param array
*/
function gss_get_post_categories($arr)
{
    if( is_array($arr) ){
        extract($arr);
    }

    switch_to_blog( $blog_id );

        $categories = get_the_terms( $post_id, $taxonomy );

    restore_current_blog();

    return $categories;
}

/*
* Retrieve posts|post_types of blog
* @param int|array - blog_id, get_posts parameters
*/
function gss_get_posts($blog_id, $arr)
{

    switch_to_blog( $blog_id );
        $posts = get_posts($arr);
    restore_current_blog();

    return $posts;
}

/*
* Retrieve single post of blog
* @param int - blog_id, post ID
*/
function gss_get_post($blog_id, $post_id)
{
    switch_to_blog( $blog_id );

        if( $post_id ){
            $post = get_post( $post_id );
         } else {
            $post = array();
         }

    restore_current_blog();

    return $post;
}

/*
* Retrieve post meta of blog
* @param int|string - blog_id, post ID, meta key
*/
function gss_get_post_meta( $blog_id, $post_id, $key = null, $single = true )
{
    switch_to_blog( $blog_id );
        $postmeta = get_post_meta( $post_id, $key, $single );

        if( ! $postmeta )
            $postmeta = '';

    restore_current_blog();

    return $postmeta;
}

/*
* Retrieve blog user
* @param array - get_users parameters
*/
function gss_get_users($args=null)
{
    if( is_array($args) )
        extract($args);

    $args = array(
                'blog_id'      => isset($blog_id) ? $blog_id : '',
                'role'         => '',
                'role__in'     => array(),
                'role__not_in' => array(),
                'meta_key'     => '',
                'meta_value'   => '',
                'meta_compare' => '',
                'meta_query'   => array(),
                'date_query'   => array(),
                'include'      => array(),
                'exclude'      => array(),
                'orderby'      => 'email',
                'order'        => 'ASC',
                'offset'       => '',
                'search'       => '',
                'number'       => '',
                'count_total'  => false,
                'fields'       => 'all',
                'who'          => '',
            );
    $users = get_users( $args );

    return $users;
}

/*
* Get microsite/blog details
* @param int|string - blog id, data to return
*/
function gss_get_blog_details($blog_id, $details)
{
    $blog_details = get_blog_details( $blog_id, true );

    if( $blog_details ){
        return $blog_details->$details;
    }
    else {
        return false;
    }

}

function get_microsite_nap($microsite_blog_id) {
    global $wpdb;

    return $wpdb->get_row("SELECT * FROM `sfl_microsites` WHERE `microsite_blog_id` = {$microsite_blog_id}");

}

/*
* Retrieve term
* @param int|string - blog_id, post ID, meta key
*/
function gss_get_term( $blog_id, $term_id )
{
    switch_to_blog( $blog_id );

        $term = get_term( $term_id );

    restore_current_blog();

    return $term;
}

/*
* Retrieve term meta of blog
* @param int|string - blog_id, term ID, meta key
*/
function gss_get_term_meta( $blog_id, $term_id, $key )
{
    switch_to_blog( $blog_id );

        $termmeta = get_term_meta( $term_id, $key, true );

    restore_current_blog();

    return $termmeta;
}


/*
* Retrieve all assigned categories to a post
* @param int|int|string - blog_id, post ID, taxonomy
*/
function gss_wp_get_post_categories( $blog_id, $post_id, $tax )
{
    switch_to_blog( $blog_id );

        $post_cats = wp_get_object_terms( $post_id, $tax );

    restore_current_blog();

    return $post_cats;
}


/*
* Retrieve all pages
* @param int|array - blog_id, args
*/
function gss_get_pages( $blog_id, $args )
{
    switch_to_blog( $blog_id );

        $pages = get_pages($args);

    restore_current_blog();

    return $pages;
}

function gss_get_blog_option( $blog_id, $option )
{
    switch_to_blog( $blog_id );

        $output = maybe_unserialize(get_blog_option( $blog_id, $option ));

    restore_current_blog();

    return $output;
}

function gss_update_post($blog_id, $post){
    switch_to_blog( $blog_id );
        $update = wp_update_post($post);
    restore_current_blog();

    return $update;
}

function gss_wp_get_attachment_image($blog_id, $post){

    switch_to_blog( $blog_id );
        $thumbnail = wp_get_attachment_image_src($post, array('250','250'));
    restore_current_blog();

    return $thumbnail;
}

function get_form_leads_count($microsite=1, $form_id, $cf7_form){
    global $wpdb;

    switch_to_blog($microsite);
        $rg_count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}rg_lead WHERE form_id = $form_id");

        $sfl_count = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}rg_lead WHERE form_id = $cf7_form");
    restore_current_blog();

    $count = $rg_count + $sfl_count;

    return $count;
}
