<?php

namespace Surefire\Microsites\Lib\Roles;

function setup_roles() {

    $roles = array();
    __NAMESPACE__ . '\\' . remove_roles();

    $ZOR['main'] = include_once(SFL_MICROSITES_DIR . '/lib/config/franchisor.php');
    $ZEE['main'] = include_once(SFL_MICROSITES_DIR . '/lib/config/franchisee.php');
    $ZOR['microsite'] = include_once(SFL_MICROSITES_DIR . '/lib/config/franchisor-site.php');
    $ZEE['microsite'] = include_once(SFL_MICROSITES_DIR . '/lib/config/franchisee-site.php');

    $roles['franchisor'] = add_role(
        'franchisor',
        __( 'Franchisor', 'sfl-microsites' ),
        $ZOR['main']
    );

    $roles['franchisee'] = add_role(
        'franchisee',
        __( 'Franchisee', 'sfl-microsites' ),
        $ZEE['main']
    );

    $sites = get_sites([ 'site__not_in'=>[1] ]);

    foreach ($sites as $site) {
        switch_to_blog($site->blog_id);
            $roles['franchisor'] = add_role(
                'franchisor',
                __( 'Franchisor', 'sfl-microsites' ),
                $ZOR['microsite']
            );

            $roles['franchisee'] = add_role(
                'franchisee',
                __( 'Franchisee', 'sfl-microsites' ),
                $ZEE['microsite']
            );
        restore_current_blog();
    }
}

function remove_roles() {
    remove_role('franchisor');
    remove_role('franchisee');
    $sites = get_sites([ 'site__not_in'=>[1] ]);

    foreach ($sites as $site) {
        switch_to_blog($site->blog_id);
        remove_role('franchisor');
        remove_role('franchisee');
        restore_current_blog();
    }
}
