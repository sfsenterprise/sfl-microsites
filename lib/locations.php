<?php

namespace Surefire\Microsites\Lib;

class Locations
{
    public static function create_locations_post_type()
    {
        $labels = array(
            'name'               => 'Locations',
            'singular_name'      => 'Location',
            'menu_name'          => 'Locations',
            'name_admin_bar'     => 'Locations',
            'add_new'            => _x( 'Add New', 'location', '' ),
            'add_new_item'       => __( 'Add New Location', '' ),
            'new_item'           => __( 'New Location', '' ),
            'edit_item'          => __( 'Edit Location', '' ),
            'view_item'          => __( 'View Location', '' ),
            'all_items'          => __( 'All Locations', '' ),
            'search_items'       => __( 'Search Locations', '' ),
            'parent_item_colon'  => __( 'Parent Locations:', '' ),
            'not_found'          => __( 'No Location found.', '' ),
            'not_found_in_trash' => __( 'No Location found in Trash.', '' )
        );

        $capabilities = array(
                'create_posts'      => 'do_not_allow',
                'delete_post'       => 'do_not_allow',
                'delete_posts'      => 'do_not_allow',
            );

        $args =  [
            'labels'          => $labels,
            'has_archive'     => true,
            'hierarchical'    => true,
            'public'          => true,
            'supports'        => array( 'title', 'editor', 'page-attributes'),
            'menu_position'   => 20,
            'capabilities'    => $capabilities,
            'map_meta_cap'    => false,
            'rewrite'         => array( 'slug' => 'locations', 'with_front' => false )
        ];

        register_post_type( 'location', $args );
    }

    public static function found_search_sites($found_posts)
    {
        global $wp_query;

        if ( is_admin() ) return $found_posts;

        // if ( !is_admin() && is_search() && $wp_query->get('post_type', 'location') ) {
        //     if ( isset($wp_query->search_sites) ) $wp_query->posts = $wp_query->search_sites;
        // }

        return $found_posts;
    }

    public static function query_sites($query) {
        global $wpdb;

        if ( is_admin() ) return $query;

        if ( $query->get('post_type') !== 'location' ) return $query;
        $locations__not_in = $wpdb->get_col("SELECT microsite_post_id FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs as b ON b.blog_id = m.microsite_blog_id WHERE m.microsite_post_id > 0 AND b.archived = 1");
        $states__not_in = array();
        $cities__not_in = array();
        $children_in = array();

        foreach( $locations__not_in as $archived ) {
            $location__not_in = $wpdb->get_row("SELECT * FROM {$wpdb->posts} WHERE ID = {$archived}");

            $siblings = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_parent = {$location__not_in->post_parent}");
            if (count($siblings) <= 1) {
                $cities__not_in[] = $location__not_in->post_parent;
                $state__not_in = $wpdb->get_row("SELECT post_parent AS state FROM {$wpdb->posts} WHERE ID = {$location__not_in->post_parent}");
                $children = $wpdb->get_results("SELECT * FROM {$wpdb->posts} WHERE post_parent = {$state__not_in->state}");

                foreach ($children as $child) {
                    $children_in[] = $child->ID;
                }

                if( count(array_diff($children_in, $cities__not_in)) <= 0 ){
                    $states__not_in[] = $state__not_in->state;
                }
            }
        }

        $locations__not_in = array_merge($locations__not_in, $states__not_in, $cities__not_in);

        if ( $query->is_archive() ) {
            if ( $query->is_main_query() ) {
                $post__in = $wpdb->get_col("SELECT microsite_post_id FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs as b ON b.blog_id = m.microsite_blog_id  WHERE m.microsite_post_id > 0 AND b.archived = 0");

                if ( isset($_REQUEST['address']) && !empty($_REQUEST['address']) ) {

                    // $query->set('s', $_REQUEST['address']);
                    // $query->is_search = 1;
                    // $search = $query->get('s');
                    // $search_sites = $wpdb->get_col($wpdb->prepare("SELECT microsite_post_id FROM sfl_microsites as m LEFT JOIN {$wpdb->blogs} as b ON m.microsite_blog_id = b.blog_id
                    //     WHERE b.archived = 0 AND microsite_post_id > 0 AND (m.microsite_zip_postal = '%s'
                    //     OR (
                    //         m.microsite_name LIKE '%s'
                    //         OR m.microsite_state_province LIKE '%s'
                    //         OR m.microsite_city LIKE '%s'
                    //         OR m.microsite_street LIKE '%s'
                    //         OR m.microsite_street2 LIKE '%s'
                    //     ))",
                    //     $search,
                    //     '%' . $search . '%',
                    //     '%' . $search . '%',
                    //     '%' . $search . '%',
                    //     '%' . $search . '%',
                    //     '%' . $search . '%'
                    // ));
                    //
                    //
                    // $query->search_sites = $search_sites;
                    // $sites = $wpdb->get_results("SELECT * FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs as b ON b.blog_id = m.microsite_blog_id  WHERE m.microsite_post_id > 0 AND b.archived = 0");
                    // $query->sites_count =  count($sites);
                    $query->set('post__in', $post__in);
                } else {
                    $query->set('post__in', $post__in);

                }
                $query->set('posts_per_page', -1);
            } else {

                $query->set('post__not_in', $states__not_in);
                $query->set('posts_per_page', -1);
            }
        } else {

            if ( $query->is_single() ) {
                $area = explode('/', $query->get('name'));

                if ( count( $area ) === 1 ) {
                    $state = str_replace('-', ' ', $area[0]);
                    $sites = $wpdb->get_results("SELECT * FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs as b ON b.blog_id = m.microsite_blog_id  WHERE m.microsite_post_id > 0 AND b.archived = 0 AND m.microsite_state_province = '{$state}'");

                    $query->sites =  $sites;
                    $query->sites_count =  count($sites);
                } elseif ( count ( $area ) === 2 ) {
                    $state = str_replace('-', ' ', $area[0]);
                    $city = str_replace('-', ' ', $area[1]);
                    $sites = $wpdb->get_results("SELECT * FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs as b ON b.blog_id = m.microsite_blog_id  WHERE m.microsite_post_id > 0 AND b.archived = 0 AND m.microsite_state_province = '{$state}' AND m.microsite_city = '{$city}' GROUP BY m.microsite_post_id");
                    $query->sites =  $sites;

                    $query->sites_count =  count($sites);
                } elseif ( count ( $area ) === 3 ) {
                    $state = str_replace('-', ' ', $area[0]);
                    $city = str_replace('-', ' ', $area[1]);
                    $site = $wpdb->get_row("SELECT * FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs as b ON b.blog_id = m.microsite_blog_id  WHERE m.microsite_post_id > 0 AND b.archived = 0 AND m.microsite_state_province = '{$state}' AND m.microsite_city = '{$city}'");
                    if ( $site ) {
                        status_header(302);
                        wp_safe_redirect($site->path, 302);
                        exit;
                    }
                }

                if ( !$sites ) {
                    $query->set_404();
                    status_header( 302 );
                    wp_safe_redirect(home_url('/locations/'), 302);
                    exit;
                }

            }
        }

        $query->set('post__not_in', $locations__not_in);

        return $query;
    }

    public static function attach_nap($posts) {
        global $wpdb, $wp_query;

        if ( isset($posts[0]) && $posts[0]->post_type !== 'location' ) return $posts;

        $posts2nap = array();
        foreach ( $posts as $post ) {
            $posts2nap[] = $post->ID;
        }

        $post__in = implode(',', $posts2nap);
        $naps = $wpdb->get_results("SELECT * FROM sfl_microsites WHERE microsite_post_id IN ({$post__in})");

        if ( ! $naps ) return $posts;

        foreach ( $naps as $nap ) {
            foreach( $posts as $key => $post ) {
                if ( absint($post->ID) === absint($nap->microsite_post_id) ) {
                    $posts[$key]->microsite_blog_id = $nap->microsite_blog_id;
                    $posts[$key]->microsite_date = $nap->microsite_date;
                    $posts[$key]->microsite_updated = $nap->microsite_updated;
                    $posts[$key]->microsite_name = $nap->microsite_name;
                    $posts[$key]->microsite_street = $nap->microsite_street;
                    $posts[$key]->microsite_street2 = $nap->microsite_street2;
                    $posts[$key]->microsite_city = $nap->microsite_city;
                    $posts[$key]->microsite_state_province = $nap->microsite_state_province;
                    $posts[$key]->microsite_zip_postal = $nap->microsite_zip_postal;
                    $posts[$key]->microsite_phone = $nap->microsite_phone;
                    $posts[$key]->microsite_general_manager = $nap->microsite_general_manager;
                    $posts[$key]->microsite_lat = $nap->microsite_lat;
                    $posts[$key]->microsite_lng = $nap->microsite_lng;
                    $posts[$key]->microsite_gtm_ga = $nap->microsite_gtm_ga;
                    $posts[$key]->microsite_fields = $nap->microsite_fields;
                }
            }
        }

        return $posts;
    }
}
