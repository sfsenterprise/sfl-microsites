Thank you for your interest in our Goldfish Holiday Packages! Please fill out the form below, and a Goldfish Swim School representative will reach out to you and confirm your order and payment information.

<div class="form-error-container">
  <p class="row"><span class="label col-md-8">[form_location_dropdown required="yes"]</span></p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">[checkbox* reserve use_label_element exclusive 'Reserve my gift package today!']</label></p>
</div>
<hr>
<h2>Purchaser Contact Information</h2>

<div>
<p class="row">
  <label class="col-md-15">Name <em>*</em></label>
  <span class="label col-md-7 form-error-container">[text* first_name class:form-control placeholder "First Name"]</span>
  <span class="label col-md-7 form-error-container">[text* last_name class:form-control placeholder "Last Name"]</span>
</p>
</div>

<div
<p class="row">
  <label class="col-md-15">Address <em>*</em></label>
  <span class="label col-md-7 form-error-container">[text* address1 class:form-control placeholder "Address line 1"]</span>
  <span class="label col-md-7">[text address2 class:form-control placeholder "Address Line 2"]</span>
</p>
</div>

<div class="">
<p class="row">
  <label class="col-md-15">State/City <em>*</em></label>
  <span class="label col-md-7 form-error-container">[text* city class:form-control placeholder "City"]</span>
  <span class="label col-md-7 form-error-container">[select* state class:form-control " " "Alabama" "Alaska" "Arizona" "Arkansas" "California" "Colorado" "Connecticut" "Delaware" "District of Columbia" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Kansas" "Kentucky" "Louisiana" "Maine" "Maryland" "Massachusetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York" "North Carolina" "North Dakota" "Ohio" "Oklahoma" "Oregon" "Pennsylvania" "Rhode Island" "South Carolina" "South Dakota" "Tennessee" "Texas" "Utah" "Vermont" "Virginia" "Washington" "West Virginia" "Wisconsin" "Wyoming" "Armed Forces Americas" "Armed Forces Europe" "Armed Forces Pacific"]</span>  
  <span class="label col-md-7 form-error-container">[text* zip class:form-control placeholder "Zip/Postal Code"]</span>
  <span class="label col-md-7 form-error-container">[select* country include_blank class:form-control 'USA' 'Canada']</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Primary Phone <em>*</em></label>
  <span class="label col-md-8">[tel* phone class:form-control]</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Email <em>*</em></label>
  <span class="label col-md-8">[email* email class:form-control]</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">How did you hear about us? <em>*</em></label>
  <span class="label col-md-8">[select*  heard_on include_blank class:form-control 'Direct Mail' 'Event' 'Facebook' 'Magazine' 'School Communication' 'Search Engine' 'TV' 'Word of Mouth' 'W.A.T.E.R. Safety Presentation' 'Other']</span>
</p>
</div>
<hr>
<h2>Recipient Contact Information<h2>

<div class="form-error-container">
<p class="row">
  <label class="col-md-15">Child's Name <em>*</em></label>
  <span class="label col-md-7">[text* first_name_child class:form-control placeholder "First Name"]</span>
  <span class="label col-md-7">[text* last_name_child class:form-control placeholder "Last Name"]</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Child's Date of Birth <em>*</em></label>
  <span class="label col-md-8">[text* birth_child class:form-control class:datepicker]</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Child's Gender <em>*</em></label>
  <span class="label col-md-8">[select*  gender_child include_blank class:form-control 'Male' 'Female']</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Phone Number <em>*</em></label>
  <span class="label col-md-8">[tel* recipient_phone class:form-control]</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Parent/Guardian Email <em>*</em></label>
  <span class="label col-md-8">[email* email_recipient class:form-control]</span>
</p>
</div>

<div class="form-error-container">
<p class="row"><label class="col-md-15">Delivery Method <em>*</em></label>
  <span class="label col-md-8">[select* delivery_method include_blank class:form-control 'In School Pick-up' 'Ship']</span>
</p>
</div>

<div id="ship_to" class="hidden">
  <hr>
  <h2>Ship To</h2>

  <div class="form-error-container">
  <p class="row">
    <label class="col-md-15">Name <em>*</em></label>
    <span class="label col-md-7">[text* f_name_ship class:form-control placeholder "First Name"]</span>
    <span class="label col-md-7">[text* l_name_ship class:form-control placeholder "Last Name"]</span>
  </p>
  </div>

  <div class="form-error-container">
  <p class="row">
    <label class="col-md-15">Address <em>*</em></label>
    <span class="label col-md-7">[text* address1_ship class:form-control placeholder "Address line 1"]</span>
    <span class="label col-md-7">[text address2_ship class:form-control placeholder "Address Line 2"]</span>
  </p>
  </div>

  <div class="form-error-container">
  <p class="row">
    <label class="col-md-15">State/City <em>*</em></label>
    <span class="label col-md-7">[text* city_ship class:form-control placeholder "City"]</span>
    <span class="label col-md-7">[select* state_ship class:form-control " " "Alabama" "Alaska" "Arizona" "Arkansas" "California" "Colorado" "Connecticut" "Delaware" "District of Columbia" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Kansas" "Kentucky" "Louisiana" "Maine" "Maryland" "Massachusetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York" "North Carolina" "North Dakota" "Ohio" "Oklahoma" "Oregon" "Pennsylvania" "Rhode Island" "South Carolina" "South Dakota" "Tennessee" "Texas" "Utah" "Vermont" "Virginia" "Washington" "West Virginia" "Wisconsin" "Wyoming" "Armed Forces Americas" "Armed Forces Europe" "Armed Forces Pacific"]</span>     
    <span class="label col-md-7">[text* zip_ship class:form-control placeholder "Zip/Postal Code"]</span>
    <span class="label col-md-7">[select* country_ship include_blank class:form-control 'USA' 'Canada']</span>
  </p>
  </div>
</div>

<p class="row">[misc_hidden_fields]<span class="col-md-15">[submit class:btn class:btn-blue "Submit"]</span></p>