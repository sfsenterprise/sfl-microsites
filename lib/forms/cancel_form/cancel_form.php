<br>
<div class="single-class-cancellation-form">
    <p class="row">
        <span class="col-md-8">[form_location_dropdown required="yes"]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Student's First Name:</label>
        <span class="col-md-8">[text student-first-name-2 class:form-control]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Student's Last Name:</label>
        <span class="col-md-8">[text student-last-name-3 class:form-control]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Lesson Day and Time:</label>
        <span class="col-md-8">[text lesson-day-time-4 class:form-control]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Date you will be cancelling for:</label>
        <span class="col-md-8">[text date-of-cancellation-5 class:form-control class:datepicker]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Your Email <em>*</em></label>
        <span class="col-md-8">[email* student-email-6 class:form-control]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Your Phone #:</label>
        <span class="col-md-8">[tel student-phone-7 class:form-control]</span>
    </p>
    <p class="row">[misc_hidden_fields]<span class="col-md-15">[submit class:btn class:btn-blue "Submit"]</span></p>
</div>