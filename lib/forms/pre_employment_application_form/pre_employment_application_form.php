<br>
<div class="pre-job-application-form">
    <label class="title">Pre Employment Application</label>
    <div class="step-1">
        <div class="progress-number">Step 1 of 4</div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-info" role="progressbar" aria-valuenow="25"  aria-valuemin="0" aria-valuemax="100" style="width:25%">
            25%
          </div>
        </div>
        <hr class="progress-divider">
        <div class=" ">
            <p class="row">
                <label class="col-md-15">Name:<em>*</em></label>
                <span class="col-md-7 form-error-container">[text* applicant-first-name-15_3 class:form-control] First</span>
                <span class="col-md-7 form-error-container">[text* applicant-last-name-15_6 class:form-control] Last</span>
            </p>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Phone:<em>*</em></label>
                <span class="col-md-7">[tel* applicant-phone-14 class:form-control]</span>
            </p>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Email:<em>*</em></label>
                <span class="col-md-7">[email* applicant-email-13 class:form-control]</span>
            </p>
        </div>
        <div class="form-error-container">
            <p class="row">
                <label class="col-md-15">Are you under 18 years of age?<em>*</em></label>
                <span class="col-md-7 radio">[radio under-age-4 class:form-control class:wpcf7-validates-as-required "Yes" "No"]</span>
            </p>
        </div>
        <div class="form-error-container ">
        <p class="row">
            <label class="col-md-15">Are you legally eligible to work in the United States?<em>*</em></label>
            <span class="col-md-7 radio"> [radio legally-eligible-120 class:form-control  class:wpcf7-validates-as-required "Yes" "No"]</span>
        </p>
        </div>

        <div>
            <p class="row">
                <label class="col-md-15">Address <em>*</em></label>
                <span class="col-md-15 form-error-container">[text* applicant-street-address-12_1 class:form-control]Street Address</span>
                <span class="col-md-15 inputlabel">[text applicant-address-line2-12_2 class:form-control]Address Line 2</span>
                <span class="col-md-8 form-error-container inputlabel">[text* applicant-city-12_3 class:form-control]City</span>
                <span class="col-md-7 form-error-container inputlabel">[select* applicant-state-province-12_4 class:form-control class:wpcf7-validates-as-required " " "Alabama" "Alaska" "Arizona" "Arkansas" "California" "Colorado" "Connecticut" "Delaware" "District of Columbia" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Kansas" "Kentucky" "Louisiana" "Maine" "Maryland" "Massachusetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York" "North Carolina" "North Dakota" "Ohio" "Oklahoma" "Oregon" "Pennsylvania" "Rhode Island" "South Carolina" "South Dakota" "Tennessee" "Texas" "Utah" "Vermont" "Virginia" "Washington" "West Virginia" "Wisconsin" "Wyoming" "Armed Forces Americas" "Armed Forces Europe" "Armed Forces Pacific"]State / Province</span>
                <span class="col-md-8 inputlabel form-error-container">[text* applicant-zip-12_5 class:form-control]ZIP / Postal Code</span>
                <span class="col-md-7 inputlabel"></span>
            </p>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Date you can start<em>*</em></label>
                <span class="col-md-7">[text* applicant-start-date-16 class:form-control class:datepicker]</span>
            </p>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Availability<em>*</em></label>
                <span class="col-md-7 radio">[radio applicant-availability-17 class:form-control class:wpcf7-validates-as-required "Full-time" "Part-time"]</span>
            </p>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Days Available<em>*</em></label>
                <span class="col-md-7 checkbox">[checkbox* days-available-123 class:form-control "Sunday AM" "Sunday PM" "Monday AM" "Monday PM" "Tuesday AM" "Tuesday PM" "Wednesday AM" "Wednesday PM" "Thursday AM" "Thursday PM" "Friday AM" "Friday PM" "Saturday AM" "Saturday PM"]</span>
            </p>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Position Desired<em>*</em></label>
                <span class="col-md-7">[select* position-desired-18 class:form-control class:wpcf7-validates-as-required " " "Swim Teacher" "Member Services Rep" "Manager"]</span>
            </p>
        </div>
        <p class="row">
            <label class="col-md-15">Referred by</label>
            <span class="col-md-7">[text applicant-referrer-20 class:form-control]</span>
        </p>
        <hr>
        <p class="row"><span class="col-md-15"><a href="#" class="btn btn-blue step-1-btn multistep-btn" page="1">Next</a></span></p>
    </div>
    <div class="step-2 job-app-sub-form">
        <div class="progress-number">Step 2 of 4</div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="50"  aria-valuemin="0" aria-valuemax="100" style="width:50%">
            50%
          </div>
        </div>
        </br>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Have you been employed at one of our Goldfish Swim School locations before?<em>*</em></label>
                <span class="col-md-7 radio"> [radio employed-once-29 class:form-control class:hid-employed-details class:add-child class:wpcf7-validates-as-required   "Yes" "No"]</span>
            </p>
        </div>
        <div class="hid-employed-details sub-form">
            <div class="form-error-container "><p class="row"><label class="col-md-15"></label><span class="col-md-7">[form_location_dropdown label="Which location did you work at *" required="yes" mainsite="false"]</span></p></div>
        </div>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Are you physically able to perform the skills necessary to complete the duties of the job for which you are applying?<em>*</em></label>
                <span class="col-md-7 radio"> [radio physically-able-33 class:form-control class:hid-physically-enable-details class:add-child class:wpcf7-validates-as-required  "Yes" "No"]</span>
            </p>
        </div>
        <div class="hid-physically-enable-details sub-form">
            <p class="row">
                <label class="col-md-15">Please Explain</label>
                <span class="col-md-15">[textarea physically-enable-details-34 class:form-control]</span>
            </p>
        </div>
        <p class="row">
            <label class="col-md-15">Relevant Job Experience</label>
            <span class="col-md-15">[textarea job-experience-124 class:form-control]</span>
        </p>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">Have you ever been fired from a job or asked to resign?<em>*</em></label>
                <span class="col-md-7 radio"> [radio fired-or-resigned-35 class:form-control class:hid-fired-or-resigned-details class:add-child class:wpcf7-validates-as-required "Yes" "No"]</span>
            </p>
        </div>
        <div class="hid-fired-or-resigned-details sub-form">
            <p class="row">
                <label class="col-md-15">Please give details:</label>
                <span class="col-md-15">[textarea fired-or-resigned-details-36 class:form-control]</span>
            </p>
        </div>
        <p class="row"><span class="col-md-15 row">
            <span class="col-md-2"><a href="#" class="btn btn-blue step-2-btn multistep-btn" page="2">Previous</a></span>
            <span class="col-md-2"><a href="#" class="btn btn-blue step-2-btn multistep-btn" page="2">Next</a></span>
        </span></p>
    </div>
    <div class="step-3 job-app-sub-form">
        <div class="progress-number">Step 3 of 4</div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="75"  aria-valuemin="0" aria-valuemax="100" style="width:75%">
            75%
          </div>
        </div>
        </br>
        <h2>Education Data</h2>
        <hr>
        <div class="form-error-container ">
        <p class="row">
            <label class="col-md-15">Graduated High School</label>
            <span class="col-md-7 radio"> [radio high-school-graduate-121 class:form-control class:wpcf7-validates-as-required  "Yes" "No"]</span>
        </p>
        </div>
        <div class="form-error-container ">
        <p class="row">
            <label class="col-md-15">Graduated College</label>
            <span class="col-md-7 radio"> [radio college-graduate-122 class:form-control class:wpcf7-validates-as-required "Yes" "No"]</span>
        </p>
        </div>
        <p class="row">
            <label class="col-md-15">Degree Obtained</label>
            <span class="col-md-7">[text degree-obtained-82 class:form-control]</span>
        </p>
        <p class="row">
                <label class="col-md-15">Special Activities (Civic, Athletics, Academics, etc. - exclude labor organizations, and memberships which reveal race, color, religion, national origin, sex, age, disability or other protected status)</label>
                <span class="col-md-15">[textarea special-activities-92 class:form-control]</span>
        </p>
        <p class="row"><span class="col-md-15 row">
            <span class="col-md-2"><a href="#" class="btn btn-blue step-3-btn multistep-btn" page="3">Previous</a></span>
            <span class="col-md-2"><a href="#" class="btn btn-blue step-3-btn multistep-btn" page="3">Next</a></span>
    </div>
    <div class="step-4 job-app-sub-form">
        <div class="progress-number">Step 4 of 4</div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="100"  aria-valuemin="0" aria-valuemax="100" style="width:100%">
            100%
          </div>
        </div>
        </br>
        <h2>Authorization</h2>
        <hr>

        <p>I understand and agree that Goldfish Swim School shall administer background checks on new employees having sensitive positions including but not limited to those positions which (1) handle cash; (2) are responsible for facility security (3) have occasion to be alone with children. Background checks may be waived if the person has had significant previous experience with Goldfish Swim School prior to acquiring a sensitive position. I understand and agree that employment is conditional upon the results of these checks. I certify that all information provided in this Application for Employment is true, correct and complete. I understand that any false or misleading information or omission may disqualify me from further consideration for employment and may result in my dismissal if discovered at a later date. I authorize the investigation of any and all statements contained in this application. I UNDERSTAND THAT NEITHER THIS APPLICATION NOR VERBAL STATEMENTS BY MANAGEMENT, OR SUBSEQUENT EMPLOYMENT DOES NOT CREATE AN EXPRESS OR IMPLIED CONTRACT OR EMPLOYMENT OR GUARANTEE EMPLOYMENT FOR ANY SPECIFIED PERIOD OF TIME AND THAT ANY SUCH AGREEMENT MUST BE IN WRITING, SIGNED BY THE DULY AUTHORIZED REPRESENTATIVE OF THE EMPLOYER AND THE EMPLOYEE, IF EMPLOYED. I hereby release and hold Goldfish Swim School from any and all claims whatsoever, including but not limited to personal injury, arising out of or relating to any non-work hour and/or non-work related recreational activity provided to employee by or on behalf of Goldfish Swim School. I further understand and agree that if employed, the employment will be "at will", which means that either party, the employee or Goldfish Swim School, may end the employment relationship at any time, for any reason, or for no reason.</p>
        <br>
        <div class="form-error-container ">
            <p class="row">
                <label class="col-md-15">I have read, understand, and consent to these statements<em>*</em></label>
                <span class="col-md-7 checkbox"> [checkbox* authorization-112 class:form-control "I Agree"]</span>
            </p>
        </div>
[misc_hidden_fields]
        <p class="row"><span class="col-md-15 row">
            <span class="col-md-2"><a href="#" class="btn btn-blue step-4-btn multistep-btn" page="4">Previous</a></span>
            <span class="col-md-2">[submit class:btn class:btn-blue class:step-4-btn class:last-btn "Submit"]</span>
        </span></p>
    </div>
</div>
