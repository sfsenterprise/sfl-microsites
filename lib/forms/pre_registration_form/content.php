<?php 
	return '<p>If you have additional questions or need more information before you book, please fill out the form on this page. We will follow up to ensure you are completely comfortable with South Jordan and our programs before finalizing enrollment for your little swimmer.</p>';
?>