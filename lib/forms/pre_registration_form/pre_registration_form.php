<br>
<div class="pre-registration-form">
    <h3 class="blue-h3">You Child's Information</h2>
    <hr>
    <div class="form-error-container">
      <p class="row">
          <label class="col-md-15">Child's First Name: <em>*</em></label>
          <span class="col-md-8">[text* child-first-name-1 class:form-control]</span>
      </p>
    </div>  
    <div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Child's Last Name: <em>*</em></label>
        <span class="col-md-8">[text* child-last-name-24 class:form-control]</span>
    </p>
  </div>  
  <div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Child's Date of Birth: <em>*</em></label>
        <span class="col-md-4">[text* child-dob-3 class:form-control class:datepicker]</span>
    </p>
  </div>  
  <div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Gender <em>*</em></label>
        <span class="col-md-7">[select* child-gender-4 class:form-control "-- CHOOSE ONE --" "Female" "Male"]</span>
    </p>
  </div>  
  <div class="form-error-container">
    <p class="row">
        <label class="col-md-15">T-Shirt Size <em>*</em></label>
        <span class="col-md-7">[select* child-tshirt-size-5 class:form-control "-- CHOOSE ONE --" "6 months" "18 months" "2T" "3T" "4T" "5/6" "7" "XS" "S" "M"]</span>
    </p>
  </div>  
  <div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Child's Swimming Experience Level</label>
        <span class="col-md-7">[select child-swimming-exp-47 class:form-control "-- CHOOSE ONE --" "No water experience" "Comfortable in water" "Comfortable putting their face in the water" "Can float independently" "Can swim 3-5 feet independently (dog paddle)" "Can swim 10 feet independently (windmill arms with roll over breath)" "Can swim the basic freestyle/backstroke" "Can swim the basic breastroke/butterfly"]</span>
    </p>
  </div>  
    
    <p class="row">
        <label class="col-md-15">Add a Second Child?</label>
        <span class="col-md-7 checkbox"> [checkbox add-second-child-23 class:form-control class:hid-second-child class:add-child "Yes"]</span>
    </p>
    <div class="hid-second-child sub-form">
        <div class="form-error-container">      
        <p class="row">
            <label class="col-md-15">Second Child's First Name: <em>*</em></label>
            <span class="col-md-8">[text* second-child-first-name-25 class:form-control]</span>
        </p>
        </div>  
        <div class="form-error-container">          
        <p class="row">
            <label class="col-md-15">Second Child's Last Name: <em>*</em></label>
            <span class="col-md-8">[text* second-child-last-name-2 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">              
        <p class="row">
            <label class="col-md-15">Second Child's Date of Birth: <em>*</em></label>
            <span class="col-md-2">[text* second-child-dob-26 class:form-control class:datepicker]</span>
        </p>
      </div>  
      <div class="form-error-container">              
        <p class="row">
            <label class="col-md-15">Gender <em>*</em></label>
            <span class="col-md-7">[select* second-child-gender-31 class:form-control "-- CHOOSE ONE --" "Female" "Male"]</span>
        </p>
      </div>  
      <div class="form-error-container">              
        <p class="row">
            <label class="col-md-15">T-Shirt Size <em>*</em></label>
            <span class="col-md-7">[select* second-child-tshirt-size-32 class:form-control "-- CHOOSE ONE --" "6 months" "18 months" "2T" "3T" "4T" "5/6" "7" "XS" "S" "M"]</span>
        </p>
      </div>  
      <div class="form-error-container">              
        <p class="row">
            <label class="col-md-15">Child's Swimming Experience Level</label>
            <span class="col-md-7">[select second-child-swimming-exp-48 class:form-control "-- CHOOSE ONE --" "No water experience" "Comfortable in water" "Comfortable putting their face in the water" "Can float independently" "Can swim 3-5 feet independently (dog paddle)" "Can swim 10 feet independently (windmill arms with roll over breath)" "Can swim the basic freestyle/backstroke" "Can swim the basic breastroke/butterfly"]</span>
        </p>
      </div>  
      <div class="form-error-container">              
        <p class="row">
            <label class="col-md-15">Add a Third Child?</label>
            <span class="col-md-7 checkbox"> [checkbox add-third-child-27 class:form-control class:hid-third-child class:add-child "Yes"]</span>
        </p>
      </div>        
    </div>
    <div class="hid-third-child sub-form">
      <div class="form-error-container">            
        <p class="row">
            <label class="col-md-15">Third Child's First Name: <em>*</em></label>
            <span class="col-md-8">[text* third-child-first-name-28 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">                    
        <p class="row">
            <label class="col-md-15">Third Child's Last Name: <em>*</em></label>
            <span class="col-md-8">[text* third-child-last-name-29 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">                    
        <p class="row">
            <label class="col-md-15">Third Child's Date of Birth: <em>*</em></label>
            <span class="col-md-2">[text* third-child-dob-30 class:form-control class:datepicker]</span>
        </p>
      </div>  
      <div class="form-error-container">                    
        <p class="row">
            <label class="col-md-15">Gender <em>*</em></label>
            <span class="col-md-7">[select* third-child-gender-33 class:form-control "-- CHOOSE ONE --" "Female" "Male"]</span>
        </p>
      </div>  
      <div class="form-error-container">                    
        <p class="row">
            <label class="col-md-15">T-Shirt Size <em>*</em></label>
            <span class="col-md-7">[select* third-child-tshirt-size-34 class:form-control "-- CHOOSE ONE --" "6 months" "18 months" "2T" "3T" "4T" "5/6" "7" "XS" "S" "M"]</span>
        </p>
      </div>  
      <div class="form-error-container">                    
        <p class="row">
            <label class="col-md-15">Child's Swimming Experience Level</label>
            <span class="col-md-7">[select third-child-swimming-exp-49 class:form-control "-- CHOOSE ONE --" "No water experience" "Comfortable in water" "Comfortable putting their face in the water" "Can float independently" "Can swim 3-5 feet independently (dog paddle)" "Can swim 10 feet independently (windmill arms with roll over breath)" "Can swim the basic freestyle/backstroke" "Can swim the basic breastroke/butterfly"]</span>
        </p>
      </div>  
      <div class="form-error-container">                    
        <p class="row">
            <label class="col-md-15">Add a Fourth Child?</label>
            <span class="col-md-7 checkbox"> [checkbox add-fourth-child-35 class:form-control class:hid-fourth-child class:add-child "Yes"]</span>
        </p>
      </div>        
    </div>
    <div class="hid-fourth-child sub-form">
      <div class="form-error-container">                  
        <p class="row">
            <label class="col-md-15">Fourth Child's First Name: <em>*</em></label>
            <span class="col-md-8">[text* fourth-child-first-name-36 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">                        
        <p class="row">
            <label class="col-md-15">Fourth Child's Last Name: <em>*</em></label>
            <span class="col-md-8">[text* fourth-child-last-name-38 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">                          
        <p class="row">
            <label class="col-md-15">Fourth Child's Date of Birth: <em>*</em></label>
            <span class="col-md-2">[text* fourth-child-dob-37 class:form-control class:datepicker]</span>
        </p>
      </div>  
      <div class="form-error-container">                          
        <p class="row">
            <label class="col-md-15">Gender <em>*</em></label>
            <span class="col-md-7">[select* fourth-child-gender-39 class:form-control "-- CHOOSE ONE --" "Female" "Male"]</span>
        </p>
      </div>  
      <div class="form-error-container">                          
        <p class="row">
            <label class="col-md-15">T-Shirt Size <em>*</em></label>
            <span class="col-md-7">[select* fourth-child-tshirt-size-40 class:form-control "-- CHOOSE ONE --" "6 months" "18 months" "2T" "3T" "4T" "5/6" "7" "XS" "S" "M"]</span>
        </p>
      </div>  
      <div class="form-error-container">                          
        <p class="row">
            <label class="col-md-15">Child's Swimming Experience Level</label>
            <span class="col-md-7">[select fourth-child-swimming-exp-50 class:form-control "-- CHOOSE ONE --" "No water experience" "Comfortable in water" "Comfortable putting their face in the water" "Can float independently" "Can swim 3-5 feet independently (dog paddle)" "Can swim 10 feet independently (windmill arms with roll over breath)" "Can swim the basic freestyle/backstroke" "Can swim the basic breastroke/butterfly"]</span>
        </p>
      </div>  
      <div class="form-error-container">                          
        <p class="row">
            <label class="col-md-15">Add a Fifth Child?</label>
            <span class="col-md-7 checkbox"> [checkbox add-fifth-child-41 class:form-control class:hid-fifth-child class:add-child "Yes"]</span>
        </p>
      </div>        
    </div>
    <div class="hid-fifth-child sub-form">
      <div class="form-error-container">                        
        <p class="row">
            <label class="col-md-15">Fifth Child's First Name: <em>*</em></label>
            <span class="col-md-8">[text* fifth-child-first-name-42 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">                              
        <p class="row">
            <label class="col-md-15">Fifth Child's Last Name: <em>*</em></label>
            <span class="col-md-8">[text* fifth-child-last-name-43 class:form-control]</span>
        </p>
      </div>  
      <div class="form-error-container">                                
        <p class="row">
            <label class="col-md-15">Fifth Child's Date of Birth: <em>*</em></label>
            <span class="col-md-2">[text* fifth-child-dob-44 class:form-control class:datepicker]</span>
        </p>
      </div>  
      <div class="form-error-container">                                
        <p class="row">
            <label class="col-md-15">Gender <em>*</em></label>
            <span class="col-md-7">[select* fifth-child-gender-45 class:form-control "-- CHOOSE ONE --" "Female" "Male"]</span>
        </p>
      </div>  
      <div class="form-error-container">                                
        <p class="row">
            <label class="col-md-15">T-Shirt Size <em>*</em></label>
            <span class="col-md-7">[select* fifth-child-tshirt-size-46 class:form-control "-- CHOOSE ONE --" "6 months" "18 months" "2T" "3T" "4T" "5/6" "7" "XS" "S" "M"]</span>
        </p>
      </div>  
      <div class="form-error-container">                                
        <p class="row">
            <label class="col-md-15">Child's Swimming Experience Level</label>
            <span class="col-md-7">[select fifth-child-swimming-exp-51 class:form-control "-- CHOOSE ONE --" "No water experience" "Comfortable in water" "Comfortable putting their face in the water" "Can float independently" "Can swim 3-5 feet independently (dog paddle)" "Can swim 10 feet independently (windmill arms with roll over breath)" "Can swim the basic freestyle/backstroke" "Can swim the basic breastroke/butterfly"]</span>
        </p>
      </div>      
    </div>
    <h3 class="blue-h3">Additional Information</h2>
    <hr>

<div class="form-error-container">
<p class="row">
<span class="label col-md-8">[form_location_dropdown required="yes"]</span></p>
</div>


  <div class="form-error-container">                        
    <p class="row">
        <label class="col-md-15">Program or Event <em>*</em></label>
        <span class="col-md-8">[select* program-event-8 class:form-control "-- CHOOSE ONE --" "Swim Lessons" "Swim Force" "Jump Start Clinic" "Technique Clinic" "Party"]</span>
    </p>
  </div>  
  
  <div class="form-error-container">                          
    <p>* Jump Start Clinics run 4 or 5 consecutive days depending on location. We will contact you to discuss scheduling.</p>
    <p class="row">
        <label class="col-md-15">Day Preference</label>
        <span class="col-md-7 checkbox">[checkbox day-preference-10 class:form-control "Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday"]</span>
    </p>
  </div>  
  <div class="form-error-container">                            
    <p class="row">
        <label class="col-md-15">Time Preference</label>
        <span class="col-md-7 checkbox">[checkbox time-preference-11 class:form-control "Morning" "Afternoon" "Evening"]</span>
    </p>
  </div>    
    <br>
    <h3 class="blue-h3">Parent Information</h3>
    <hr>
  <div class="form-error-container">                            
    <p class="row">
        <label class="col-md-15">Name <em>*</em></label>
        <span class="col-md-8 ">[text* parent-first-name-14_3 class:form-control]First</span>
        <span class="col-md-7 ">[text* parent-last-name-14_6 class:form-control]Last</span>
    </p>
  </div>  
                              
    <p class="row">
        <label class="col-md-15">Address <em>*</em></label>
        <span class="col-md-15 form-error-container">[text* parent-street-address-22_1 class:form-control]Street Address</span>
        <span class="col-md-15 inputlabel">[text parent-address-line2-22_2 class:form-control]Address Line 2</span>
        <span class="col-md-8 inputlabel  form-error-container">[text* parent-city-22_3 class:form-control]City</span>
        <span class="col-md-7 inputlabel">[select parent-state-province-22_4 class:form-control " " "Alabama" "Alaska" "Arizona" "Arkansas" "California" "Colorado" "Connecticut" "Delaware" "District of Columbia" "Florida" "Georgia" "Hawaii" "Idaho" "Illinois" "Indiana" "Iowa" "Kansas" "Kentucky" "Louisiana" "Maine" "Maryland" "Massachusetts" "Michigan" "Minnesota" "Mississippi" "Missouri" "Montana" "Nebraska" "Nevada" "New Hampshire" "New Jersey" "New Mexico" "New York" "North Carolina" "North Dakota" "Ohio" "Oklahoma" "Oregon" "Pennsylvania" "Rhode Island" "South Carolina" "South Dakota" "Tennessee" "Texas" "Utah" "Vermont" "Virginia" "Washington" "West Virginia" "Wisconsin" "Wyoming" "Armed Forces Americas" "Armed Forces Europe" "Armed Forces Pacific"]State / Province</span>
        <span class="col-md-8 inputlabel  form-error-container">[text* parent-zip-22_5 class:form-control]ZIP / Postal Code</span>
        <span class="col-md-7 inputlabel"></span>
    </p>
    

  <div class="form-error-container">                            
    <p class="row">
        <label class="col-md-15">Primary Phone <em>*</em></label>
        <span class="col-md-8">[tel* parent-primary-phone-16 class:form-control]</span>
    </p>
  </div>  
  <div class="form-error-container">                            
    <p class="row">
    <label class="col-md-15">Email <em>*</em></label>
    <span class="col-md-8">[email* parent-email-17 class:form-control]</span>
    </p>
  </div>  
  <div class="form-error-container">                            
    <p class="row">
    <label class="col-md-15">How did you hear about us? <em>*</em></label>
    <span class="col-md-8">[select* how-did-you-hear-about-us-18 class:form-control "-- CHOOSE ONE --" "Direct Mail" "Event" "Facebook" "Magazine" "School Communication" "Search Engine" "Word of Mouth" "W.A.T.E.R. Safety Presentation"]</span>
    </p>
  </div>  
    [misc_hidden_fields]
    <p class="row"><span class="col-md-15">[submit class:btn class:btn-blue "Submit"]</span></p>
</div>