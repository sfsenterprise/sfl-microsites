<table width="99%" border="0" cellpadding="1" cellspacing="0" bgcolor="#EAEAEA">
    <tr>
        <td>
            <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
              <tr><td colspan="2" style="font-size:14px; font-weight:bold; background-color:#EEE; border-bottom:1px solid #DFDFDF; padding:7px 7px">Parent Information</td></tr>
                <tr bgcolor="#EAF2FA" style="padding: 7px;">
                    <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>First Name</strong></font></td>
                <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                    <td><font style="font-family: sans-serif; font-size:12px;">{parent-first-name-14_3}</font></td>
                </tr>
                <tr bgcolor="#EAF2FA" style="padding: 7px;">
                    <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Last Name</strong></font></td>
                <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                    <td><font style="font-family: sans-serif; font-size:12px;">{parent-last-name-14_6}</font></td>
                </tr>
                <tr bgcolor="#EAF2FA" style="padding: 7px;">
                    <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Address</strong></font></td>
                <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                    <td><font style="font-family: sans-serif; font-size:12px;">{parent-street-address-22_1}<br> {parent-address-line2-22_2} <br>{parent-city-22_3}, {parent-state-province-22_4} {parent-zip-22_5}</font></td>
                </tr>
                <tr bgcolor="#EAF2FA" style="padding: 7px;">
                    <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Primary Phone</strong></font></td>
                <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                    <td><font style="font-family: sans-serif; font-size:12px;">{parent-primary-phone-16}</font></td>
                </tr>
                <tr bgcolor="#EAF2FA" style="padding: 7px;">
                    <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Email</strong></font></td>
                <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                    <td><font style="font-family: sans-serif; font-size:12px;">{parent-email-17}</font></td>
                </tr>
                <tr bgcolor="#EAF2FA" style="padding: 7px;">
                    <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>How did you hear about us?</strong></font></td>
                <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                    <td><font style="font-family: sans-serif; font-size:12px;">{how-did-you-hear-about-us-18}</font></td>
                </tr>
                <tr><td colspan="2" style="font-size:14px; font-weight:bold; background-color:#EEE; border-bottom:1px solid #DFDFDF; padding:7px 7px">Additional Information</td></tr>
                   <tr bgcolor="#EAF2FA" style="padding: 7px;">
                       <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Program or Event</strong></font></td>
                   <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                       <td><font style="font-family: sans-serif; font-size:12px;">{program-event-8}</font></td>
                   </tr>
                   <tr bgcolor="#EAF2FA" style="padding: 7px;">
                       <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Day Preference</strong></font></td>
                   <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                       <td><font style="font-family: sans-serif; font-size:12px;">{day-preference-10}</font></td>
                   </tr>
                   <tr bgcolor="#EAF2FA" style="padding: 7px;">
                       <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Time Preference</strong></font></td>
                   <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                       <td><font style="font-family: sans-serif; font-size:12px;">{time-preference-11}</font></td>
                   </tr>
              <tr><td colspan="2" style="font-size:14px; font-weight:bold; background-color:#EEE; border-bottom:1px solid #DFDFDF; padding:7px 7px">Your Child's Information</td></tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Child's First Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{child-first-name-1}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Child's Last Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{child-last-name-24}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Child's Date of Birth</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{child-dob-3}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Gender</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{child-gender-4}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>T-Shirt Size</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{child-tshirt-size-5}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Child's Swimming Experience Level</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{child-swimming-exp-47}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Second Child's First Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{second-child-first-name-25}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Second Child's Last Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{second-child-last-name-2}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Second Child's Date of Birth</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{second-child-dob-26}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Second Child's Gender</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{second-child-gender-31}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Second Child's T-Shirt Size</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{second-child-tshirt-size-32}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Second Child's Swimming Experience Level</strong></font></td>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{second-child-swimming-exp-48}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Third Child's First Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{third-child-first-name-28}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Third Child's Last Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{third-child-last-name-29}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Third Child's Date of Birth</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{third-child-dob-30}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Third Child's Gender</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{third-child-gender-33}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Third Child's T-Shirt Size</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{third-child-tshirt-size-34}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Third Child's Swimming Experience Level</strong></font></td>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{third-child-swimming-exp-49}</font></td>
                 </tr>

                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fourth Child's First Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fourth-child-first-name-36}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fourth Child's Last Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fourth-child-last-name-38}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fourth Child's Date of Birth</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fourth-child-dob-37}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fourth Child's Gender</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fourth-child-gender-39}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fourth Child's T-Shirt Size</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fourth-child-tshirt-size-40}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fourth Child's Swimming Experience Level</strong></font></td>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fourth-child-swimming-exp-50}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fifth Child's First Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fifth-child-first-name-42}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fifth Child's Last Name</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fifth-child-last-name-43}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fifth Child's Date of Birth</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fifth-child-dob-44}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fifth Child's Gender</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fifth-child-gender-45}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fifth Child's T-Shirt Size</strong></font></td>
                 </tr>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fifth-child-tshirt-size-46}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Fifth Child's Swimming Experience Level</strong></font></td>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{fifth-child-swimming-exp-51}</font></td>
                 </tr>
                 <tr><td colspan="2" style="font-size:14px; font-weight:bold; background-color:#EEE; border-bottom:1px solid #DFDFDF; padding:7px 7px">URLs</td></tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Embed URL</strong></font></td>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{embed_url}</font></td>
                 </tr>
                 <tr bgcolor="#EAF2FA" style="padding: 7px;">
                     <td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>Referer URL</strong></font></td>
                 <tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td>
                     <td><font style="font-family: sans-serif; font-size:12px;">{embed_referrer}</font></td>
                 </tr>
            </table>
        </td>
     </tr>
</table>
