<div class="registration-form">
    <div class="form-error-container message">
            There was a problem with your submission. Errors have been highlighted below.
    </div>
    <div class="form-error-container">
        <p class="row">
            <label class="col-md-15">Child's First Name:</label>
            <span class="label col-md-8">[text* child-first-name-1 class:form-control]</span>
        </p>
    </div>
    <div class="form-error-container">
        <p class="row">
            <label class="col-md-15">Child' Birthdate:</label>
            <span class="label col-md-7">[text registration-dob-29 class:form-control class:datepicker]</span>
        </p>
    </div>
    <div class="questions">
        <div class="question mini-1">
            <p class="row">
                <label class="col-md-15">Is your baby able to hold their head up independently?</label>
                <span class="label col-md-7">[select  head-independent-33  class:form-control '' 'Yes' 'No']</span>
            </p>
        </div>
        <div class="question mini-2">
            <p class="row">
                <label class="col-md-15">Would you like your child to swim independently, without a parent?</label>
                <span class="label col-md-7">[select  swim-independent-10  class:form-control '' 'Yes' 'No']</span>
            </p>
        </div>
        <div class="question other others-1">
            <p class="row">
                <label class="col-md-15">What is your child's swim experience?</label>
                <span class="label col-md-7">[select  swim-experience-11  class:form-control '' 'Has not taken swim lessons' 'Can swim, but has not taken formal swim lessons' 'Can swim and has taken formal swim lessons']</span>
            </p>
        </div>
        <div class="question other others-2">
            <p class="row">
                <label class="col-md-15">Can your child swim independently with their face in the water without wearing or using a flotation device?</label>
                <span class="label col-md-7">[select  no-floating-device-12  class:form-control '' 'Yes' 'No']</span>
            </p>
            <p class="row"><span class="col-md-15"><a  class="btn btn-blue" page="1">Go Back</a></span></p>
        </div>
        <div class="question other others-3">
            <p class="row">
                <label class="col-md-15">How far can your child swim without any assistance?</label>
                <span class="label col-md-7">[select  swim-assistance-13  class:form-control '' 'Short distance (5-10 feet)' '10 feet or further']</span>
            </p>
            <p class="row"><span class="col-md-15"><a  class="btn btn-blue" page="2">Go Back</a></span></p>
        </div>
        <div class="question other others-4">
            <p class="row">
                <label class="col-md-15">How does your child take a breath when they swim?</label>
                <span class="label col-md-7">[select  swim-breath-14  class:form-control '' 'Lift head straight up' 'Roll over onto their back independently and/or breath to the side']</span>
            </p>
            <p class="row"><span class="col-md-15"><a  class="btn btn-blue"  page="3">Go Back</a></span></p>
        </div>
        <div class="question other others-5">
            <p class="row">
                <label class="col-md-15">How does your child swim?</label>
                <span class="label col-md-7">[select  swim-style-19  class:form-control '' 'Dog paddle' 'Arms out of the water' 'Freestyle (bent elbows, side breath)']</span>
            </p>
            <p class="row"><span class="col-md-15"><a  class="btn btn-blue"  page="4">Go Back</a></span></p>
        </div>
        <div class="question other others-6">
            <p class="row">
                <label class="col-md-15">Can your child swim Backstroke?</label>
                <span class="label col-md-7">[select  swim-backstroke-20  class:form-control '' 'Yes' 'No']</span>
            </p>
            <p class="row"><span class="col-md-15"><a  class="btn btn-blue"  page="5">Go Back</a></span></p>
        </div>
        <div class="question other others-7">
            <p class="row">
                <label class="col-md-15">Can your child do the Breaststroke Kick (aka Frog Kick)?</label>
                <span class="label col-md-7">[select  frog-kick-21  class:form-control '' 'Yes' 'No']</span>
            </p>
            <p class="row"><span class="col-md-15"><a  class="btn btn-blue"  page="6">Go Back</a></span></p>
        </div>
    </div>
    <div class="results">
        <div class="result default">
            <a href="/forms/pre-registration-form/">Click here to pre-register now.</a>
            <p>Please fill out a Pre-Registration form and we will contact you when your baby is closer to 4 months old. We may also be offering a special class better suited for your child, so feel free to give us a call to learn more. In the meantime, please check out our Family Swim</p>
        </div>
        <div class="result mini-1">
            <h2><span class="name"></span> is in the Mini 1</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
        </div>
        <div class="result mini-2 v1">
            <h2><span class="name"></span> is in the Mini 2</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
        </div>
        <div class="result mini-2 v2">
            <h2><span class="name"></span> is in the Mini 2</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result mini-3">
            <h2><span class="name"></span> is in the Mini 3</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 3 class is for independent toddlers (who wish to swim without a parent in the water) with a 3:1 student to teacher ratio. Students work directly with a certified Swim Instructor in this 30-minute class. They learn basic swimming and safety skills while becoming more confident in the water.</p>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result mini-4">
            <h2><span class="name"></span> is in the Mini 4</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
        </div>
        <div class="result junior-1">
            <h2><span class="name"></span> is in the Junior 1</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result junior-2">
            <h2><span class="name"></span> is in the Junior 2</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result junior-3">
            <h2><span class="name"></span> is in the Junior 3</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 3 class is for independent toddlers (who wish to swim without a parent in the water) with a 3:1 student to teacher ratio. Students work directly with a certified Swim Instructor in this 30-minute class. They learn basic swimming and safety skills while becoming more confident in the water.</p>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>

        </div>
        <div class="result glider-1">
            <h2><span class="name"></span> is in the Glider 1</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result glider-2">
            <h2><span class="name"></span> is in the Glider 2</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result glider-3">
            <h2><span class="name"></span> is in the Glider 3</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result pro-1">
            <h2><span class="name"></span> is in the Pro 1</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
        <div class="result pro-2">
            <h2><span class="name"></span> is in the Pro 2</h2>
            <a href="[microsite loginurl]">Click here to book your lesson now.</a>
            <p>Our Mini 3 class is for independent toddlers (who wish to swim without a parent in the water) with a 3:1 student to teacher ratio. Students work directly with a certified Swim Instructor in this 30-minute class. They learn basic swimming and safety skills while becoming more confident in the water.</p>
            <p>Our Mini 2 class is a 30 minute parent-toddler class with a 6:1 student-to-teacher ratio. Students and parents are guided through the 30-minute class by a certified Swim Instructor. They work on basic swimming and safety skills while having fun singing songs and playing games.</p>
            <p>If you're unsure which class to select please give us a call and speak with one of our Front Desk representatives.</p>
        </div>
    </div>
    
</div>
