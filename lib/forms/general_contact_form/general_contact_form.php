<br>
<div class="contact-form">

<div class="form-error-container">
    <p class="row">
        <label class="col-md-15"></label>
        <span class="col-md-7">[form_location_dropdown label="Choose a location: *" required="yes"]</span>
    </p>
</div>
<div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Name:<em>*</em></label>
        <span class="col-md-7">[text* contact-first-name-1_3 class:form-control] First</span>
        <span class="col-md-7">[text* contact-last-name-1_6 class:form-control] Last</span>
    </p>
</div>
<div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Email:<em>*</em></label>
        <span class="col-md-7">[email* contact-email-2 class:form-control]</span>
    </p>
</div>
<div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Phone:<em>*</em></label>
        <span class="col-md-7">[tel* contact-phone-3 class:form-control]</span>
    </p>
</div>
<div class="form-error-container">
    <p class="row">
        <label class="col-md-15">Message/Comment:<em>*</em></label>
        <span class="col-md-14">[textarea* contact-message-4 class:form-control]</span>
    </p>
</div>
    <p class="row">[misc_hidden_fields]<span class="col-md-15">[submit class:btn class:btn-blue "Submit"]</span></p>
</div>