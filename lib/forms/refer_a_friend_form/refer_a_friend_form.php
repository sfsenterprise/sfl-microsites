<br>
<div class="refer-a-friend-form">
    <h3 class="blue-h3">Your Information</h3>
    <hr>
    <p class="row">
        <label class="col-md-15">Your Name <em>*</em></label>
        <span class="col-md-8">[text* your-first-name-2_3 class:form-control]First</span>
        <span class="col-md-7">[text* your-last-name-2_6 class:form-control]Last</span>
    </p>
    <p class="row">
        <label class="col-md-15">Your Email <em>*</em></label>
        <span class="col-md-8">[email* your-email-3 class:form-control]</span>
    </p>
    <br>
    <h3 class="blue-h3">Referral Information</h3>
    <hr>
    <p class="row">
        <label class="col-md-15">Referral Name <em>*</em></label>
        <span class="col-md-8">[text* referral-first-name-5_3 class:form-control]First</span>
        <span class="col-md-7">[text* referral-last-name-5_6 class:form-control]Last</span>
    </p>
    <p class="row">
        <label class="col-md-15">Referral Email <em>*</em></label>
        <span class="col-md-8">[email* referral-email-6 class:form-control]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Referral Phone (optional)</label>
        <span class="col-md-8">[tel referral-phone-8 class:form-control]</span>
    </p>
    <p class="row">
        <label class="col-md-15">Tell us a little about your referral!</label>
        <span class="col-md-15">[textarea referral-description-7 class:form-control placeholder "Provide us any information that will help us serve them well!"]</span>
    </p>
    <p class="row">[misc_hidden_fields]<span class="col-md-15">[submit class:btn class:btn-blue "Submit"]</span></p>
</div>