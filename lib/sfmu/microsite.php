<?php

namespace Surefire\Microsites\Lib\Sfmu;

/**
 *
 */
class Microsite
{

    public static function _sfmu_attach_nap( $wp_query )
    {
        global $wpdb, $nap;

        if ( $blog_id = get_current_blog_id() ) {

            $nap = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_blog_id = {$blog_id}");

            $wp_query->nap = $nap;
        }

        return $wp_query;

    }

    public static function _sfmu_default_page_in_queried_object( $wp_query )
    {
        global $wp;

        if ( is_main_site() !== FALSE )
            return $wp_query;

        if ( !isset($wp_query->queried_object) )
            return $wp_query;

        switch_to_blog(1);
        $main = get_page_by_path($wp->request ?: 'home', OBJECT, $wp_query->get('post_type', 'page'));
        if ( $main ) $is_parent = get_post_meta($main->ID, '_sfmu_default_page', true);
        restore_current_blog();

        if ( isset($is_parent) && $is_parent !== 'yes' ) return $wp_query;

        if ( $main && $main->post_name === $wp_query->queried_object->post_name && $main->post_title === $wp_query->queried_object->post_title && $main->post_date === $wp_query->queried_object->post_date ) {
            $wp_query->queried_object->post_content = $main->post_content;
        }

        return $wp_query;

    }

    public static function _sfmu_default_page_in_post( $post, $query )
    {
        global $wp;

        if ( is_main_site() !== FALSE )
            return $post;

        if ( $post->post_type !== 'page'  ) {
            return $post;
        }

        if ( $parent = get_post_meta($post->ID, '_sfmu_parent_id', true) ) {
            switch_to_blog(1);
            $main = get_page_by_path($wp->request ?: 'home', OBJECT, $post->post_type);
            if ( $main ) $is_parent = get_post_meta($main->ID, '_sfmu_default_page', true);
            restore_current_blog();

            if ( isset($is_parent) && $is_parent !== 'yes' ) return $post;
            $post = $main;
        }

        return $post;
    }

    public static function _sfmu_default_form_in_post( $post, $query )
    {
        if ( is_main_site() !== FALSE )
            return $post;

        if ( $post->post_type !== 'ff_forms'  ) {
            return $post;
        }

        $name = $query->get('name');
        switch_to_blog(1);
        $main = get_page_by_path($name, OBJECT, 'ff_forms');
        if ( $main ) $is_parent = get_post_meta($main->ID, '_sfmu_default_page', true);
        restore_current_blog();

        if ( isset($is_parent) && $is_parent !== 'yes' ) return $post;

        if ( $main && $main->post_name === $post->post_name && $main->post_title === $post->post_title && $main->post_date === $post->post_date ) {
            $post->post_content = $main->post_content;
        }

        return $post;
    }

    public static function _sfmu_default_page_in_posts( $posts, $query )
    {
        if ( is_main_site() !== FALSE )
            return $posts;

        if ( ! $posts )
            return $posts;

        if ( $posts[0]->post_type !== 'page') {
            return $posts;
        }

        foreach( $posts as $key => $post ) {
            if ( $parent_id = get_post_meta($post->ID, '_sfmu_parent_id', true) ) {
                switch_to_blog(1);
                $parent = get_post($parent_id);
                $is_parent = get_post_meta($parent->ID, '_sfmu_default_page', true);
                restore_current_blog();
                if ( $is_parent !== 'yes' ) {
                    continue;
                } else {
                    $posts[$key]->post_content = $parent->post_content;
                }
            }
        }

        return $posts;
    }

    public static function _sfmu_default_form_in_posts( $posts, $query )
    {
        global $wp;

        if ( is_main_site() !== FALSE )
            return $posts;

        if ( $query->is_archive() ) return $posts;

        if ( !$posts )
            return $posts;

        if ( $posts[0]->post_type !== 'ff_forms') {
            return $posts;
        }

        foreach( $posts as $key => $post ) {
            if ( $parent_id = get_post_meta($post->ID, '_sfmu_parent_id', true) ) {
                switch_to_blog(1);
                $parent = get_post($parent_id);
                $is_parent = get_post_meta($parent->ID, '_sfmu_default_page', true);
                restore_current_blog();
                if ( $is_parent !== 'yes' ) {
                    continue;
                } else {
                    $posts[$key]->post_content = $parent->post_content;
                }
            }
        }

        return $posts;
    }

    public static function _sfmu_default_template( $template ) {

        if ( is_page() ) {
            global $post;

            $parent = get_post_meta($post->ID, '_sfmu_parent_id', true);
            if ( $parent ) {
                switch_to_blog(1);
                $template = get_page_template_slug($parent) ?: $template;
                restore_current_blog();

                if ( is_front_page() ) {
                    $template = locate_template('templates/template-microsite-home.php');
                }
            }
        }

        return $template;
    }

    /**
    * Deprecated
    * 10-27-2017
    */
    public static function _sfmu_pre_event_posts( $query )
    {
        if ( $query->get('post_type') !== 'stellar_event' ) return $query;

        if ( is_admin() ) return $query;

        if ( strpos($_SERVER['REQUEST_URI'], '/surefire') !== FALSE ) return $query;

        if ( $query->is_main_query() ) return $query;

        global $wpdb;

        $query->set('suppress_filters', false);

        $year = $query->get('event_year', date('Y'));
        $month = $query->get('event_month', 'all');

        $query->set('event_year', $year);
        $query->set('event_month', $month);

        $meta_query = array(
            'relation' => 'AND',
            array(
                'key' => '_gss_post_hidden',
                'value' => '',
                'compare' => '='
            )
        );

        if ( $month === 'all' ) {
            $query_all = array(
                array(
                    'key' => '_stellar_event_repeating_ends_on',
                    'value' => current_time('mysql'),
                    'type' => 'DATETIME',
                    'compare' => '>=',
                )
            );

            $meta_query = array_merge($meta_query, $query_all);
        } else {
            $query_month = array(
                'relation' => 'AND',
                array(
                    'relation' => 'OR',
                    array(
                        'key' => '_stellar_event_repeating_ends_on',
                        'value' => current_time('mysql'),
                        'type' => 'DATETIME',
                        'compare' => '>=',
                    ),
                    array(
                        'key' => '_stellar_event_startDate',
                        'value' => array('2017-10-01', '2017-10-31'),
                        'type' => 'DATETIME',
                        'compare' => 'BETWEEN',
                    ),
                )
            );

            $meta_query = array_merge($meta_query, $query_month);
        }

        $query->set('meta_key', '_stellar_event_startDate');
        $query->set('orderby', 'meta_value_datetime');
        $query->set('order', 'ASC');

        $query->set('meta_query', $meta_query);

        return $query;
    }

    /**
    * Deprecated
    * 10-27-2017
    */
    public static function _sfmu_found_event_posts($posts, $query)
    {
        if ( $query->is_main_query() ) return $posts;
        if ( $query->get('post_type') !== 'stellar_event' ) return $posts;
        if ( strpos($_SERVER['REQUEST_URI'], '/surefire') !== FALSE ) return $posts;

        $new_posts = array();
        foreach( $posts as $post ) {
            $post->_stellar_event_startDate = date('Y-m-d H:i:s', strtotime(get_post_meta($post->ID, '_stellar_event_startDate', true)));
            $post->post_date = $post->_stellar_event_startDate;
            $post->_stellar_event_endDate = date('Y-m-d H:i:s', strtotime(get_post_meta($post->ID, '_stellar_event_endDate', true)));
            $post->_stellar_event_repeats = get_post_meta($post->ID, '_stellar_event_repeats', true);
            $post->_stellar_event_repeat_every = get_post_meta($post->ID, '_stellar_event_repeat_every', true);
            $post->_stellar_event_repeating_ends_on = date('Y-m-d H:i:s', strtotime(get_post_meta($post->ID, '_stellar_event_repeating_ends_on', true)));

            $new_posts[date('Y-m-d', strtotime($post->post_date))] = $post;

            if ( $post->_stellar_event_repeats === 'daily' ) {
                $day = date_diff(date_create($post->_stellar_event_startDate), date_create($post->_stellar_event_repeating_ends_on));
                for($i=1; $i<=$day->days; $i++) {
                    $next = date('Y-m-d', strtotime("+1 days", strtotime($post->post_date)));
                    $new_posts[$next] = $post;
                    $new_posts[$next]->post_date = $next;
                }
            } else if ( $post->_stellar_event_repeats === 'weekly' ) {
            }
        }

        if ( $query->get('posts_per_page', 3) !== -1 ) {
            $chunk_posts = array_chunk($new_posts, $query->get('posts_per_page', 3));
            if ( $chunk_posts ) {
                $posts = $chunk_posts[0];
            }

        } else {
            $posts = $new_posts;
        }

        return $posts;
    }
}
