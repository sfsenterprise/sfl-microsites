<?php

$categories_labels = array(
    'name'              => _x( 'Lesson Groups', 'Lesson Groups', 'sfl-microsites' ),
    'singular_name'     => _x( 'Lesson Group', 'Lesson Group', 'sfl-microsites' ),
    'search_items'      => __( 'Search Groups', 'sfl-microsites' ),
    'all_items'         => __( 'All Groups', 'sfl-microsites' ),
    'parent_item'       => __( 'Parent Groups', 'sfl-microsites' ),
    'parent_item_colon' => __( 'Parent Groups:', 'sfl-microsites' ),
    'edit_item'         => __( 'Edit Group', 'sfl-microsites' ),
    'update_item'       => __( 'Update Group', 'sfl-microsites' ),
    'add_new_item'      => __( 'Add New Group', 'sfl-microsites' ),
    'new_item_name'     => __( 'New Group Name', 'sfl-microsites' ),
    'menu_name'         => __( 'Groups', 'sfl-microsites' ),
);

return array(
    'name'              => 'gss_swim_lesson_groups',
    'hierarchical'      => true,
    'labels'            => $categories_labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'lesson-groups', 'with_front' => false ),
    'post_type'         => array('gss_swim_lessons')
);
