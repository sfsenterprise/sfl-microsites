<?php

$categories_labels = array(
    'name'              => _x( 'Tags', 'GSS News Tags', 'sfl-microsites' ),
    'singular_name'     => _x( 'Tag', 'GSS News Tag', 'sfl-microsites' ),
    'search_items'      => __( 'Search Tag', 'sfl-microsites' ),
    'all_items'         => __( 'All Tags', 'sfl-microsites' ),
    'parent_item'       => __( 'Parent Tags', 'sfl-microsites' ),
    'parent_item_colon' => __( 'Parent Tags:', 'sfl-microsites' ),
    'edit_item'         => __( 'Edit Tag', 'sfl-microsites' ),
    'update_item'       => __( 'Update Tag', 'sfl-microsites' ),
    'add_new_item'      => __( 'Add New Tag', 'sfl-microsites' ),
    'new_item_name'     => __( 'New Tag Name', 'sfl-microsites' ),
    'menu_name'         => __( 'Tags', 'sfl-microsites' ),
);

return array(
    'name'              => 'ff_news_tag',
    'hierarchical'      => true,
    'labels'            => $categories_labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'news/tags', 'with_front' => false ),
    'post_type'         => array('ff_news')
);
