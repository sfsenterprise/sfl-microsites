<?php

$categories_labels = array(
    'name'              => _x( 'Categories', 'Event Categories', 'sfl-microsites' ),
    'singular_name'     => _x( 'Category', 'Event Category', 'sfl-microsites' ),
    'search_items'      => __( 'Search Categories', 'sfl-microsites' ),
    'all_items'         => __( 'All Categories', 'sfl-microsites' ),
    'parent_item'       => __( 'Parent Categories', 'sfl-microsites' ),
    'parent_item_colon' => __( 'Parent Categories:', 'sfl-microsites' ),
    'edit_item'         => __( 'Edit Category', 'sfl-microsites' ),
    'update_item'       => __( 'Update Category', 'sfl-microsites' ),
    'add_new_item'      => __( 'Add New Category', 'sfl-microsites' ),
    'new_item_name'     => __( 'New Category Name', 'sfl-microsites' ),
    'menu_name'         => __( 'Categories', 'sfl-microsites' ),
);

return array(
    'name'              => 'stellar_event_categories',
    'hierarchical'      => true,
    'labels'            => $categories_labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'event-category', 'with_front' => false ),
    'post_type'         => array('stellar_event')
);
