<?php

namespace Surefire\Microsites\Lib\Sfmu\Hooks;

use Surefire\Microsites\Lib\Bootstrap;
use Surefire\Microsites\Lib\Sfmu\Microsite;
use Surefire\Microsites\Lib\Sfmu\Posttypes;
use Surefire\Microsites\Lib\Sfmu\CF7_Submit;
use Surefire\Microsites\Lib\Sfmu\WP_Overrides;

add_action('plugins_loaded', function() {
    CF7_Submit::get_instance();
    WP_Overrides::get_instance();

    show_admin_bar( false );
});
add_action( 'parse_query', [Microsite::class, '_sfmu_attach_nap'], 10, 1);
add_action( 'parse_query', [Microsite::class, '_sfmu_default_page_in_queried_object'], 99, 1);
add_action( 'the_post', [Microsite::class, '_sfmu_default_page_in_post'], 99, 2);
add_action( 'the_post', [Microsite::class, '_sfmu_default_form_in_post'], 99, 2);
add_action( 'the_posts', [Microsite::class, '_sfmu_default_page_in_posts'], 99, 2 );
add_action( 'the_posts', [Microsite::class, '_sfmu_default_form_in_posts'], 99, 2 );
add_action( 'template_include', [Microsite::class, '_sfmu_default_template'], 99, 1 );

add_action( 'init', '\\Surefire\\Microsites\\Lib\\Sfmu\\Posttypes\\register');

/**
* Post Type Hooks
*/
add_filter( 'the_posts', '\\Surefire\\Microsites\\Lib\\Sfmu\\Posttypes\\switch_testimonials_posts', 10, 2);
add_action( 'pre_get_posts', '\\Surefire\\Microsites\\Lib\\Sfmu\\Posttypes\\remove_hidden_jobs', 10, 1);

add_filter('wp_nav_menu_args', function($args) {
    if ( is_main_site() !== FALSE )
        return $args;

    $args['blog_id'] = get_current_blog_id();
    switch_to_blog(1);

    return $args;
});

add_filter('wp_nav_menu_objects', function($sorted_menu_items, $args) {

    $exclude_from_main = [
        'events',
        'take a virtual tour',
        'our team',
    ];

    if ( ! isset($args->blog_id) ) {

        foreach ( $sorted_menu_items as $pos => $item ) {
            if ( get_current_blog_id() === 1 ) {
                if ( in_array(strtolower($item->title), $exclude_from_main) ) {
                    unset($sorted_menu_items[$pos]);
                }
            }
        }

        return $sorted_menu_items;
    } else {
        if ( absint($args->blog_id) !== absint(get_current_blog_id()) ) {
            restore_current_blog();
        }

        $siteurl = trailingslashit(get_site_url(1));
        foreach ( $sorted_menu_items as $pos => $item ) {
            if ( get_current_blog_id() === 1 && strtolower($item->title) === 'events' ) {
                unset($sorted_menu_items[$pos]);
            } elseif ( get_current_blog_id() !== 1 && strtolower($item->title) === 'locations' ) {
                unset($sorted_menu_items[$pos]);
            } elseif ( strtolower($item->title) !== 'blog' ) {
                if ( preg_match('/^\//', $item->url) ) {
                    $sorted_menu_items[$pos]->url = home_url() . $sorted_menu_items[$pos]->url;
                } else {
                    $sorted_menu_items[$pos]->url = str_replace($siteurl, home_url('/'), $sorted_menu_items[$pos]->url);
                }
            }
        }

        restore_current_blog();
    }

    return $sorted_menu_items;
}, 10, 2);

add_filter( 'option_blogdescription', function( $value, $option ) {

    if ( is_main_site() !== FALSE ) return $value;

    switch_to_blog(1);
    $value = get_option($option);
    restore_current_blog();

    return $value;

}, 110, 2 );

/*
* GTM shortcode for header
*/
function gtm_ga_cb( $atts ){
    $a = shortcode_atts( array(
        'position' => 'header',
    ), $atts );

    global $wpdb;

    $output = '';
    $blog_id = get_current_blog_id();

    $microsite = $wpdb->get_row("SELECT microsite_gtm_ga FROM `sfl_microsites` WHERE `microsite_blog_id` = {$blog_id}");

    if( $microsite && $microsite->microsite_gtm_ga ){
        if( $a['position'] === 'header' ){
            $output .= "<!-- Google Tag Manager -->";
            $output .= "<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({";
            $output .= "'gtm.start': new Date().getTime(),event:'gtm.js'});";
            $output .= "var f=d.getElementsByTagName(s)[0],";
            $output .= "j=d.createElement(s),";
            $output .= "dl=l!='dataLayer'?'&l='+l:'';j.async=true;";
            $output .= "j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);";
            $output .= "})(window,document,'script','dataLayer','". $microsite->microsite_gtm_ga ."');</script>";
            $output .= "<!-- End Google Tag Manager -->";
        }
        elseif( $a['position'] === 'footer' ){
            $output .= "<!-- Google Tag Manager (noscript) -->";
            $output .= "<noscript>";
            $output .= '<iframe src="https://www.googletagmanager.com/ns.html?id='. $microsite->microsite_gtm_ga .'"height="0" width="0" style="display:none;visibility:hidden"></iframe>';
            $output .= "</noscript>";
            $output .= "<!-- End Google Tag Manager (noscript) -->";
        }
    }

    return $output;

}
add_shortcode( 'gtm_ga', __NAMESPACE__ . '\\gtm_ga_cb' );

function content_inner_links_filter($content) {
    global $post;
    $converted = '';
    $frontpage_id = get_blog_option( get_current_blog_id(), 'page_on_front' );

    if ( strpos($content, '[gravityform') !== FALSE ) {
        preg_match_all('/\[gravityform.*?\]/', $content, $matches);
    }

    if( get_current_blog_id() === 1 )
        return $content;

    if ( (isset($post->post_type) && $post->post_type === 'page' && (int)$frontpage_id !== $post->ID) || is_post_type_archive('ff_faqs') ){
        preg_match_all('/href="(.*?)"/', $content, $matches);

        foreach ($matches[0] as $index => $var_name) {
            if( preg_match("/\[microsite/", $matches[1][$index]) == '0' &&
                preg_match("/http/", $matches[1][$index]) == '0' &&
                preg_match("/tel/", $matches[1][$index]) == '0' &&
                preg_match("/mailto/", $matches[1][$index]) == '0' ){
                $content = str_replace($var_name, 'href="[microsite homeurl]'. $matches[1][$index] .'"', $content);
            }
            else{

                if( preg_match("/\[microsite/", $matches[1][$index]) == '0' &&
                    ( preg_match("/https/", $matches[1][$index]) == '0' || preg_match("/www/", $matches[1][$index]) == '0') &&
                    preg_match("/tel/", $matches[1][$index]) == '0' &&
                    preg_match("/mailto/", $matches[1][$index]) == '0' ){

                    $url = explode('/', $matches[1][$index]);
                    $key = array_search('goldfishswimschool.com', $url);
                    $page = isset($url[3]) ? $url[3]:'';

                    //$content = str_replace($var_name, 'href="https://www.goldfishswimschool.com/' . $page .'"', $content);
                }

            }
        }

        return $content;
    } else {
        return $content;
    }

    return $content;
}
add_filter( 'the_content',  __NAMESPACE__ . '\\content_inner_links_filter' );

function form_redirect_cb(){
    global $post;

    $redirect = '';
    $current_site = get_blog_details(get_current_blog_id());

    switch_to_blog(get_current_blog_id());
        if( $post ){
            $form_id = get_post_meta( $post->ID, 'form_shortcode', true );
        }
    restore_current_blog();

    switch_to_blog(1);

      if( isset($form_id) ){
        $redirect_id = get_post_meta( $form_id, 'redirect_page', true );
      }

      if( isset($redirect_id) && $redirect_id ){

        $location = get_post($redirect_id);

        $location = isset($location) ? $location:'';
        $redirect = '<script>';
        $redirect .= 'document.addEventListener( "wpcf7mailsent", function( event ) {';
        $redirect .= "location = '". $current_site->siteurl . '/' .$location->post_name."';";
        $redirect .= "}, false );";
        $redirect .= '</script>';

      }
    restore_current_blog();

    return $redirect;
}
add_shortcode( 'form_redirect', __NAMESPACE__.'\\form_redirect_cb' );

// Remove admin bar in panel
add_filter('show_admin_bar', [Bootstrap::class, 'remove_admin_bar']);

add_action('admin_init', [Bootstrap::class, 'wp_admin_redirect']);
add_action('init', [Bootstrap::class, 'wp_admin_redirect']);

// Enabled wp-admin redirect
add_action('login_redirect', [Bootstrap::class, 'wp_admin_redirect']);
add_action('login_url', [Bootstrap::class, 'surefire_login_page']);

add_action('wp_logout', [Bootstrap::class, 'session_destroy']);
add_action('wp_login', [Bootstrap::class, 'session_destroy']);

/*function page_delete() {
    global $post;

    switch_to_blog(get_current_blog_id());
        $has_parent = get_post_meta($post->ID, '_sfmu_parent_id', true);
    restore_current_blog();

    if( $has_parent ) {
        wp_safe_redirect( wp_get_referer() );
        exit;
    }
    else {
        return true;
    }
        
}
add_action( 'delete_post', __NAMESPACE__ . '\\page_delete', 10 );
add_action( 'wp_trash_post', __NAMESPACE__ . '\\page_delete' );*/