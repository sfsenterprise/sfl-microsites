<?php


namespace Surefire\Microsites\Lib\Sfmu;

/**
 *
 */
class WP_Overrides
{
    private $media_options;

    public function __construct()
    {
        if ( is_main_site() === FALSE && ! is_admin() ) {
            add_filter('query', [__NAMESPACE__ . '\\WP_Overrides', 'modify_get_post_query'], 10, 1);
        }

        $this->media_options = array(
            '_sfmu_stellar_event_hero_image_id',
            '_sfmu_ff_news_hero_image_id',
            '_sfmu_ff_faqs_hero_image_id',
            '_sfmu_ff_job_listings_hero_image_id',
            '_sfmu_s8_staff_member_hero_image_id',
            '_sfmu_ff_promos_hero_image_id',
            '_sfmu_404_hero_image_id',
            '_sfmu_search_hero_image_id',
        );

        $this->modify_socialmedia();
        $this->modify_cta_footer();
        $this->modify_gss_pricing();
        $this->modify_hours();

        $this->media_filters();

        $this->meta_filters();
    }

    public static function modify_get_post_query($query)
    {
        global $wpdb, $wp_query;
        return $query;

        if ( ! isset($wp_query->nap) ) return $query;
        remove_filter('query', [__NAMESPACE__ . '\\WP_Overrides', 'modify_get_post_query']);
        switch_to_blog($wp_query->nap->microsite_blog_id);
        $sitetable = $wpdb->posts;
        restore_current_blog();
        add_filter('query', [__NAMESPACE__ . '\\WP_Overrides', 'modify_get_post_query'], 10, 1);

        // "SELECT * FROM $wpdb->posts WHERE ID = %d LIMIT 1"
        if ( strpos($query, "SELECT * FROM $sitetable WHERE ID") !== FALSE ) {

            remove_filter('query', [__NAMESPACE__ . '\\WP_Overrides', 'modify_get_post_query']);
            $_post = $wpdb->get_row($query);
            add_filter('query', [__NAMESPACE__ . '\\WP_Overrides', 'modify_get_post_query'], 10, 1);
            if ( $_post ) return $query;

            if ( !preg_match('/ID = (\d+)/', $query, $matches) ) return $query;
            $post_id = $matches[1];
            switch_to_blog(1);
            $table = $wpdb->posts;
            restore_current_blog();

            return $wpdb->prepare("SELECT * FROM $table WHERE ID = %d AND post_type = 'attachment' LIMIT 1", $post_id);
        }
        return $query;
    }


    private function modify_socialmedia()
    {
        add_filter('pre_option_microsite_socialmedia', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_socialmedia_return'], 10, 2);
        add_filter('option_microsite_socialmedia', [__NAMESPACE__ . '\\WP_Overrides', 'modfiy_microsite_socialmedia_return'], 10, 2);
    }

    private function modify_cta_footer()
    {
        add_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_return'], 10, 4);
        add_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_main_return'], 10, 4);
    }

    public static function modify_cta_footer_return($value, $object_id, $meta_key, $single) {
        if ( is_main_site() === FALSE ) {
            if ( $meta_key === '_call_to_action_footer' ) {
                remove_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_return']);
                switch_to_blog(1);
                $parent = get_post_meta($object_id, '_sfmu_default_page', true);
                if ( $parent === 'yes' ) {
                    $cta = get_post_meta($object_id, $meta_key, $single);
                    if ( $cta && $cta['local'] ) {
                        $value = array($cta);
                    } else {

                        $corporate = get_post_meta($object_id, '_gss_page_footer_cta_local', true);
                        $local = get_post_meta($object_id, '_gss_page_footer_cta_local', true);

                        // update_post_meta($object_id, '_call_to_action_footer', [
                        //     'corporate' => $corporate,
                        //     'local' => $local,
                        // ]);

                        $value = array(['corporate' => $corporate, 'local' => $local]);
                    }
                }
                restore_current_blog();
                add_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_return'], 10, 4);
            }
        }

        return $value;
    }

    public static function modify_cta_footer_main_return($value, $object_id, $meta_key, $single) {
        if ( is_main_site() ) {
            if ( $meta_key === '_call_to_action_footer' ) {
                remove_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_main_return']);
                $cta = get_post_meta($object_id, $meta_key, $single);
                if ( $cta && ($cta['corporate'] || $cta['local']) ) {
                    $value = array($cta);
                } else {

                    $corporate = get_post_meta($object_id, '_gss_page_footer_cta_local', true);
                    $local = get_post_meta($object_id, '_gss_page_footer_cta_local', true);

                    update_post_meta($object_id, '_call_to_action_footer', [
                        'corporate' => $corporate,
                        'local' => $local,
                    ]);

                    $value = array(['corporate' => $corporate, 'local' => $local]);
                }
                add_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_main_return'], 10, 4);
            }
        }

        return $value;
    }

    public static function modify_pre_microsite_socialmedia_return($value, $option)
    {
        if ( $option === 'microsite_socialmedia' ) {
            remove_filter("pre_option_microsite_socialmedia", [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_socialmedia_return']);
            if ( ! get_option('microsite_socialmedia', array() ) ) {
                if ( is_main_site() !== FALSE ) {
                    $new_value = array(
                        'facebook' => get_site_option('s8theme_facebook_url', ''),
                        'twitter' => get_site_option('s8theme_twitter_url', ''),
                        'yelp' => get_site_option('s8theme_yelp_url', ''),
                        'instagram' => get_site_option('s8theme_instagram_url', ''),
                        'youtube' => get_site_option('s8theme_youtube_url', ''),
                        'google-plus' => get_site_option('s8theme_google_url', ''),
                        'pinterest' => get_site_option('s8theme_pinterest_url', ''),
                        'linkedin' => get_site_option('s8theme_linkedin_url', ''),
                        'tumblr' => '',
                    );

                } else {
                    $replace_this_keys = array(
                        's8theme_facebook_url',
                        's8theme_twitter_url',
                        's8theme_yelp_url',
                        's8theme_instagram_url',
                        's8theme_youtube_url',
                        's8theme_google_url',
                        's8theme_pinterest_url',
                        's8theme_linkedin_url',
                    );
                    $old_value = get_option('theme_mods_goldfish-swim-school-franchisee', array());
                    foreach( $old_value as $key => $replace ) {
                        if ( is_array($replace) ) continue;

                        if ( ! in_array($key, $replace_this_keys) ) continue;

                        if ( $replace === '#' ) continue;

                        if ( strpos($replace, 'http') !== FALSE ) {
                            $old_value[$key] = str_replace('http://', 'https://', $replace);
                        } else {
                            if ( strpos($replace, '//') !== FALSE ) {
                                $old_value[$key] = 'https://' . (str_replace('//', '', $replace));
                            } else {
                                $old_value[$key] = ($replace) ? 'https://' . $replace : null;
                            }
                        }
                    }

                    $new_value = array(
                        'facebook' => isset($old_value['s8theme_facebook_url']) ? $old_value['s8theme_facebook_url']: '',
                        'twitter' => isset($old_value['s8theme_twitter_url']) ? $old_value['s8theme_twitter_url']:'',
                        'yelp' => isset($old_value['s8theme_yelp_url']) ? $old_value['s8theme_yelp_url']:'',
                        'instagram' => isset($old_value['s8theme_instagram_url']) ? $old_value['s8theme_instagram_url']:'',
                        'youtube' => isset($old_value['s8theme_youtube_url']) ? $old_value['s8theme_youtube_url']:'',
                        'google-plus' => isset($old_value['s8theme_google_url']) ? $old_value['s8theme_google_url']:'',
                        'pinterest' => isset($old_value['s8theme_pinterest_url']) ? $old_value['s8theme_pinterest_url']:'',
                        'linkedin' => isset($old_value['s8theme_linkedin_url']) ? $old_value['s8theme_linkedin_url']:'',
                        'tumblr' => '',
                    );

                }

                update_option('microsite_socialmedia', $new_value);

            }
            add_filter("pre_option_microsite_socialmedia", [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_socialmedia_return'], 10, 2);
        }

        return $value;
    }

    public static function modfiy_microsite_socialmedia_return($value, $option)
    {
        if ( is_main_site() !== FALSE ) return $value;

        remove_filter("option_microsite_socialmedia", [__NAMESPACE__ . '\\WP_Overrides', 'modfiy_microsite_socialmedia_return']);

        $value = get_option('microsite_socialmedia', array());

        $parentsocial = get_blog_option(1, 'microsite_socialmedia', array());

        foreach( $value as $key => $media ) {
            if ( isset($parentsocial[$key]) && (! $media || $media === '#') ) $value[$key] = $parentsocial[$key];
        }

        add_filter("option_microsite_socialmedia", [__NAMESPACE__ . '\\WP_Overrides', 'modfiy_microsite_socialmedia_return'], 10, 2);

        return $value;
    }

    private function modify_gss_pricing()
    {
        if ( ! function_exists('get_instance') ) {
            if ( is_main_site() === FALSE )  {
                $pricing = get_blog_option(get_current_blog_id(), 'gss_pricing');

                if( isset($pricing) && empty($pricing) ){
                    add_filter('pre_option_gss_pricing', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_gss_pricing_return'], 10, 2);
                }

            }
        } else {
            $ci =& get_instance();
            if ( isset($ci) && ! $ci->input->post(NULL) ) {
                add_filter('pre_option_gss_pricing', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_gss_pricing_return'], 10, 2);
            }
        }
    }

    public static function modify_pre_gss_pricing_return($value, $option)
    {

        if ( $value ) return $value;
        remove_filter('pre_option_gss_pricing', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_gss_pricing_return']);
        $old_value_others = get_option('s8_ff_pricing_other_fees', array());

        $new_value = array(
            'first_child' => array(
                'group' => '',
                'mini' => '',
                'semi_private' => '',
                'private' => '',
                'swim_force' => '',
            ),
            'second_child' => array(
                'group' => '',
                'mini' => '',
                'semi_private' => '',
                'private' => '',
                'swim_force' => '',
            ),
            'third_child' => array(
                'group' => '',
                'mini' => '',
                'semi_private' => '',
                'private' => '',
                'swim_force' => '',
            ),
            'casual_lessons' => array(
                'group' => '',
                'mini' => '',
                'semi_private' => '',
                'private' => '',
                'swim_force' => '',
            ),
            'annual_membership' => array(
                'fee' => $old_value_others['annual_membership']['cost'],
                'second_child' => $old_value_others['annual_membership']['cost_first_second'],
                'family_max' => $old_value_others['annual_membership']['cost_family_max'],
            ),
            'family_swim' => array(
                'fee' => $old_value_others['family_swim']['cost'],
                'family_max' => $old_value_others['family_swim']['cost_family_max'],
            ),
            'jump_start_clinic' => array(
                'fee' => $old_value_others['jump_start_clinic']['cost'],
                'start_day' => $old_value_others['jump_start_clinic']['start_day'],
                'end_day' => $old_value_others['jump_start_clinic']['end_day'],
            ),
            'technic_clinic' => array(
                'fee' => isset($old_value_others['technic_clinic']) ? $old_value_others['technic_clinic']['cost'] : $old_value_others['jump_start_clinic']['cost'],
                'start_day' => isset($old_value_others['technic_clinic']) ? $old_value_others['technic_clinic']['start_day'] : $old_value_others['jump_start_clinic']['start_day'],
                'end_day' => isset($old_value_others['technic_clinic']) ? $old_value_others['technic_clinic']['end_day'] : $old_value_others['jump_start_clinic']['end_day'],
            ),
            'party' => array(
                'fee' => $old_value_others['party']['cost']
            ),
        );


        $old_value = get_option('s8_ff_pricing_settings_v2', array());

        foreach ( $old_value as $lesson => $type ) {
            foreach( $type as $key => $fee ) {
                if ( $key === 0 ) {
                    $new_value[$lesson]['group'] = $fee['price'];
                } else if ( $key === 1 ) {
                    $new_value[$lesson]['mini'] = $fee['price'];
                } else if ( $key === 2 ) {
                    $new_value[$lesson]['semi_private'] = $fee['price'];
                } else if ( $key === 3 ) {
                    $new_value[$lesson]['private'] = $fee['price'];
                } else if ( $key === 4 ) {
                    $new_value[$lesson]['swim_force'] = $fee['price'];
                }
            }
        }

        update_option('gss_pricing', $new_value);

        add_filter('pre_option_gss_pricing', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_gss_pricing_return'], 10, 2);
        return $new_value;
    }

    private function modify_hours()
    {
        if ( ! function_exists('get_instance') ) {
            if ( is_main_site() === FALSE )  {
                add_filter('pre_option_microsite_hours_operations', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_hours_operations_return'], 10, 2);
            }
        } else {
            $ci =& get_instance();
            if ( isset($ci) && ! $ci->input->post(NULL) ) {
                add_filter('pre_option_microsite_hours_operations', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_hours_operations_return'], 10, 2);
            }
        }
    }

    public static function modify_pre_microsite_hours_operations_return($value, $option)
    {
        remove_filter('pre_option_microsite_hours_operations', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_hours_operations_return']);
        if ( get_option($option, '') ) {
            add_filter('pre_option_microsite_hours_operations', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_hours_operations_return'], 10, 2);
            return $value;
        }

        $new_value = array(
            'hours_operation' => array(
                'monday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'tuesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'wednesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'thursday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'friday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'saturday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'sunday' => array(
                    'active' => '',
                    'hours' => array()
                ),
            ),
            'family_swim' => array(
                'monday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'tuesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'wednesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'thursday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'friday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'saturday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'sunday' => array(
                    'active' => '',
                    'hours' => array()
                ),
            ),
            'swim_team' => array(
                'monday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'tuesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'wednesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'thursday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'friday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'saturday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'sunday' => array(
                    'active' => '',
                    'hours' => array()
                ),
            ),
            'party_hour' => array(
                'monday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'tuesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'wednesday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'thursday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'friday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'saturday' => array(
                    'active' => '',
                    'hours' => array()
                ),
                'sunday' => array(
                    'active' => '',
                    'hours' => array()
                ),
            ),
        );

        $old_value = get_option('s8_ff_hours_settings', array());

        $default_days = array(
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        );

        foreach($old_value as $type => $hours ) {
            if ( ! in_array($type, ['lesson_hours', 'office_hours', 'rec_team_hours']) ) {

                if ( $type === 'family_swim_hours') {
                    $key = 'family_swim';
                } else if ( $type === 'party_hours') {
                    $key = 'party_hour';
                } else if ( $type === 'hours_operation') {
                    $key = 'hours_operation';
                } else if ( $type === 'swim_team_hours') {
                    $key = 'swim_team';
                }

                foreach( $hours as $day => $data ) {
                    if ( $day !== 'title' ) {
                        $new_value[$key][$default_days[$day]]['active'] = (absint($data['isActive']) === 1) ? 'on' : 'off';
                        foreach( $data['timeSpans'] as $span ) {
                            $new_value[$key][$default_days[$day]]['hours'][] = array(
                                'start' => date('H:i:s', strtotime($span['timeFrom'])),
                                'end' => date('H:i:s', strtotime($span['timeTill'])),
                            );
                        }
                    }
                }
            }
        }
        update_option('microsite_hours_operations', $new_value);
        add_filter('pre_option_microsite_hours_operations', [__NAMESPACE__ . '\\WP_Overrides', 'modify_pre_microsite_hours_operations_return'], 10, 2);

        return $new_value;
    }

    private function media_filters()
    {
        array_walk($this->media_options, function($option) {
            add_filter("pre_option_{$option}", [__NAMESPACE__ . '\\WP_Overrides', 'modify_media_return'], 10, 2);
        });

        add_filter('wp_get_attachment_image_src', [__NAMESPACE__ . '\\WP_Overrides', 'modify_image_src_return'], 10, 4);
        add_filter('wp_get_attachment_image_attributes', [__NAMESPACE__ . '\\WP_Overrides', 'modify_wp_get_attaachment_image_attributes'], 10, 3);
        add_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_post_meta_return'], 10, 4);
        add_filter('update_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'remove_get_post_metadata_filter'], 10, 5);

    }
    public static function modify_media_return($value, $option)
    {
        if ( defined('SFMU') && SFMU ) {
            $ci =& get_instance();

            $override = false;
            if ( $ci->microsite ) {
    			if ( array_key_exists($ci->router->fetch_class(), $ci->include_media_override) ) {
                    if ( $ci->include_media_override[$ci->router->fetch_class()] === true ) {
    					$override = true;
    				} elseif ( !in_array($ci->router->fetch_method(), $ci->include_media_override[$ci->router->fetch_class()]) ) {
    					$override = true;
    				}
    			}

                if ( ! $override ) {
                    return $value;
                } else {
                    remove_filter("pre_option_{$option}", [__NAMESPACE__ . '\\WP_Overrides', 'modify_media_return']);
                    if ( ! $ci->input->post(NULL) ) {
                        $value = get_blog_option($ci->microsite, $option, '');
                        if ( ! $value ) {
                            switch_to_blog(1);
                            $value = get_option($option, '');
                            restore_current_blog();
                        }
                    }
                    add_filter("pre_option_{$option}", [__NAMESPACE__ . '\\WP_Overrides', 'modify_media_return'], 10, 2);
                }
            }
        } else {
            if ( is_main_site() === FALSE ) {
                remove_filter("pre_option_{$option}", [__NAMESPACE__ . '\\WP_Overrides', 'modify_media_return']);
                $value = get_option($option, '');
                if ( ! $value ) {
                    switch_to_blog(1);
                    $value = get_option($option, '');
                    restore_current_blog();
                }
                add_filter("pre_option_{$option}", [__NAMESPACE__ . '\\WP_Overrides', 'modify_media_return'], 10, 2);
            }
        }

        return $value;
    }

    public static function modify_image_src_return($image, $attachment_id, $size, $icon)
    {
        if ( defined('SFMU') && SFMU ) {
            $ci =& get_instance();

            $override = false;
            if ( isset($ci->microsite) ) {

                if ( array_key_exists($ci->router->fetch_class(), $ci->include_media_override) ) {
                    if ( $ci->include_media_override[$ci->router->fetch_class()] === true ) {
                        $override = true;
                    } elseif ( !in_array($ci->router->fetch_method(), $ci->include_media_override[$ci->router->fetch_class()]) ) {
                        $override = true;
                    }
                }
            }

            if ( ! $override ) return $image;

            remove_filter('wp_get_attachment_image_src', [__NAMESPACE__ . '\\WP_Overrides', 'modify_image_src_return']);
            switch_to_blog($ci->microsite);
            $siteimage = wp_get_attachment_image_src($attachment_id, $size, $icon);
            restore_current_blog();

            if ( $siteimage ) {
                $image = $siteimage;
            }
            add_filter('wp_get_attachment_image_src', [__NAMESPACE__ . '\\WP_Overrides', 'modify_image_src_return'], 10, 4);

        } else {
            if ( is_main_site() === FALSE ) {
                if ( ! $image ) {
                    remove_filter('wp_get_attachment_image_src', [__NAMESPACE__ . '\\WP_Overrides', 'modify_image_src_return']);
                    switch_to_blog(1);
                    $image = wp_get_attachment_image_src($attachment_id, $size, $icon);
                    restore_current_blog();
                    add_filter('wp_get_attachment_image_src', [__NAMESPACE__ . '\\WP_Overrides', 'modify_image_src_return'], 10, 4);
                }
            }
        }

        return $image;
    }

    public static function modify_wp_get_attaachment_image_attributes($attr, $attachment, $size)
    {
        if ( is_main_site() !== FALSE ) return $attr;

        global $wp_query;

        if ( ! isset($wp_query->nap) ) return $attr;

        if ( isset($attachment->ID) ) {
            $attr['src'] = wp_get_attachment_url($attachment->ID);
        }

        return $attr;
    }

    public static function modify_post_meta_return($data, $object_id, $meta_key, $single)
    {
        if ( $meta_key === '_hero_image_id') {
            if ( $data === null ) {
                remove_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_post_meta_return']);

                $hero_id = get_post_meta($object_id, $meta_key, $single);

                if ( $hero_id ) return $hero_id;

                $global_id = get_post_meta($object_id, '_sfmu_parent_id', true);
                switch_to_blog(1);
                $hero_id = get_post_meta($global_id, $meta_key, $single);
                restore_current_blog();
                add_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_post_meta_return'], 10, 4);

                // Fallback to old sql to retrieve attachment id for head
                if ( ! $hero_id ) {
                    if ( is_front_page() && is_main_site() === FALSE ) {
                        $old_settings = get_option('theme_mods_goldfish-swim-school-franchisee', array());
                        $hero_id = $old_settings['header_image'];
                    } else {
                        $hero_id = get_post_meta($object_id, '_gss_plugin_header_image', true);
                    }
                }

                return $hero_id;

            } else {
                return $data;
            }
        } elseif ( $meta_key == '_wp_attachment_image_alt' ) {
            if ( $data === null ) {
                remove_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_post_meta_return']);

                $alt = get_post_meta($object_id, $meta_key, $single);

                if ( $alt ) return $alt;

                switch_to_blog(1);
                $alt = get_post_meta($object_id, $meta_key, $single);
                restore_current_blog();
                add_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_post_meta_return'], 10, 4);

                if ( $alt ) return $alt;
            } else {
                return $data;
            }
        } else {
            return $data;
        }
    }

    public static function remove_get_post_metadata_filter($value, $object_id, $meta_key, $meta_value, $prev_value)
    {
        remove_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_post_meta_return']);
        remove_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_main_return']);
        remove_filter("get_post_metadata", [__NAMESPACE__ . '\\WP_Overrides', 'modify_cta_footer_return']);
        return $value;
    }

    private function meta_filters()
    {
        add_filter('get_term_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_term_meta_return'], 10, 4);
        add_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_custom_post_meta_return'], 10, 4);
    }

    public static function modify_term_meta_return($data, $object_id, $meta_key, $single)
    {
        if ( ! method_exists(__NAMESPACE__ . '\\WP_Overrides', 'modify_' . $meta_key) ) return $data;
        remove_filter('get_term_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_term_meta_return']);
        $data = call_user_func_array([__NAMESPACE__ . '\\WP_Overrides', 'modify_' . $meta_key], array($data, $object_id, $meta_key, $single));
        add_filter('get_term_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_term_meta_return'], 10, 4);

        return $data;
    }

    private static function modify_group_image($data, $object_id, $meta_key, $single)
    {
        switch_to_blog(1);
        $data = get_term_meta($object_id, $meta_key, $single);
        if ( ! $data ) {
            $options = get_option('taxonomy_' . $object_id, '');
            if ( isset($options['swim_lesson_' . $meta_key]) ) {
                update_term_meta($object_id, $meta_key, $options['swim_lesson_' . $meta_key]);
                $data = $options['swim_lesson_' . $meta_key];
            }
        }
        restore_current_blog();

        return $data;
    }

    private static function modify_group_youtube($data, $object_id, $meta_key, $single)
    {
        switch_to_blog(1);
        $data = get_term_meta($object_id, $meta_key, $single);
        if ( ! $data ) {
            $options = get_option('taxonomy_' . $object_id, '');
            if ( isset($options['swim_lesson_' . $meta_key . '_id']) ) {
                update_term_meta($object_id, $meta_key, $options['swim_lesson_' . $meta_key . '_id']);
                $data = $options['swim_lesson_' . $meta_key .'_id'];
            }
        }
        restore_current_blog();

        return $data;
    }

    public static function modify_custom_post_meta_return($data, $object_id, $meta_key, $single)
    {
        if ( ! method_exists(__NAMESPACE__ . '\\WP_Overrides', 'modify_' . $meta_key) ) return $data;
        remove_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_custom_post_meta_return']);
        $data = call_user_func_array([__NAMESPACE__ . '\\WP_Overrides', 'modify_' . $meta_key], array($data, $object_id, $meta_key, $single));
        add_filter('get_post_metadata', [__NAMESPACE__ . '\\WP_Overrides', 'modify_custom_post_meta_return'], 10, 4);

        return $data;
    }

    private static function modify_lesson_age($data, $object_id, $meta_key, $single)
    {
        switch_to_blog(1);
        $data = get_post_meta($object_id, $meta_key, $single);

        if ( ! $data ) {
            $data = get_post_meta($object_id, '_s8_ff_gss_swim_lesson_age', $single);
            update_post_meta($object_id, $meta_key, $data);
        }
        restore_current_blog();

        return $data;
    }

    private static function modify_lesson_details($data, $object_id, $meta_key, $single)
    {
        switch_to_blog(1);
        $data = get_post_meta($object_id, $meta_key, $single);

        if ( ! $data ) {
            $data = get_post_meta($object_id, '_s8_ff_gss_swim_lesson_details', $single);
            update_post_meta($object_id, $meta_key, $data);
        }
        restore_current_blog();

        return $data;
    }

    private static function modify_lesson_requirements($data, $object_id, $meta_key, $single)
    {
        switch_to_blog(1);
        $data = get_post_meta($object_id, $meta_key, $single);

        if ( ! $data ) {
            $data = get_post_meta($object_id, '_s8_ff_gss_swim_lesson_requirements', $single);
            update_post_meta($object_id, $meta_key, $data);
        }
        restore_current_blog();

        return $data;
    }

    public static function get_instance()
    {
        return new WP_Overrides();
    }
}
