<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes;

function register() {
    foreach( glob(SFL_MICROSITES_DIR . 'lib/sfmu/posttypes/*.php') as $posttype ) {

        $name = str_replace('.php', '', basename($posttype));
        $args = include $posttype;
        register_post_type($name, $args);
    }

    foreach( glob(SFL_MICROSITES_DIR . 'lib/sfmu/taxonomies/*.php') as $taxonomy ) {
        $name = str_replace('.php', '', basename($taxonomy));
        $args = include $taxonomy;
        register_taxonomy( $name, $args['post_type'], $args );
    }
}

function switch_testimonials_posts( $posts, $query ) {
    if ( get_current_blog_id() === 1 ) return $posts;

    if ( $query->get('post_type') !== 's8_testimonials' ) return $posts;

    switch_to_blog(1);
    $testimonials = new \WP_Query($query->query_vars);
    restore_current_blog();
    $posts = $testimonials->posts;

    return $posts;
}

function remove_hidden_jobs( $query ) {
    if ( $query->get('post_type') !== 'ff_job_listings' ) return $query;
    if ( ! $query->is_main_query() ) return $query;

    $query->set('meta_key', '_gss_post_hidden');
    $query->set('meta_value', 1);
    $query->set('meta_compare', '!=');

    return $query;
}
