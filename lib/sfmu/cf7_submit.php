<?php

namespace Surefire\Microsites\Lib\Sfmu;

/**
 *
 */
class CF7_Submit
{

    function __construct()
    {
        /**
        * Modified the CF7 submit url
        */
        add_filter('rest_url', [__NAMESPACE__ . '\\CF7_Submit', 'modify_rest_url'], 10, 2);
        add_filter( 'wpcf7_contact_form_properties', [__NAMESPACE__ . '\\CF7_Submit', 'change_properties'], 10, 2);
        add_action('wpcf7_mail_sent', [__NAMESPACE__ . '\\CF7_Submit', 'before_send']);
        add_action( 'wpcf7_submit', [__NAMESPACE__ . '\\CF7_Submit', 'action_wpcf7_submit'], 10, 2);
    }

    public static function action_wpcf7_submit( $instance, $result )
    {

        $microsite = self::get_microsite();
        $blog_id = isset($microsite->blog_id) ? $microsite->blog_id:$microsite;
        $admin_email = get_blog_option($blog_id, 'admin_email');
        $form = $instance->id();

        if( (int)$blog_id === 1 ){
            switch_to_blog( 1 );
                $notifs = get_post_meta($form, '_sfmu_mail', true);
            restore_current_blog();

            $notifs = isset($notifs['main']) ? $notifs['main']:'';
        }
        else{
            switch_to_blog( 1 );
                $notifs = get_post_meta($form, '_sfmu_mail_'.$blog_id, true);
            restore_current_blog();

            $notifs = isset($notifs['microsite']) ? $notifs['microsite']:'';
        }

        if( isset($notifs) && is_array($notifs) && ! empty($notifs) ){

            foreach ($notifs as $index => $notif) {

                if($notif['recipient']){

                    if( strpos($notif['recipient'], ',') ){
                        $recipients = explode(',', str_replace(' ', '', $notif['recipient'] ) );
                    }
                    else{
                        $recipients = ($notif['recipient'] === '{admin_email}') ? array( $admin_email ) : array( self::parse_mail_tag($notif['recipient']) );
                    }

                    if( $index === 0 && is_array($recipients) && ! empty($recipients) ){
                        unset($recipients[0]);
                    }

                    foreach ($recipients as $recipient) {

                        $to = $recipient;
                        $subject = self::parse_mail_tag($notif['subject']);
                        $fields = '';

                        if( strpos($notif['body'], 'all_fields') > 0 ){
                            unset($_POST['_wpcf7']);
                            unset($_POST['_wpcf7_version']);
                            unset($_POST['_wpcf7_locale']);
                            unset($_POST['_wpcf7_unit_tag']);
                            unset($_POST['_wpcf7_container_post']);
                            unset($_POST['_wpcf7_nonce']);
                            unset($_POST['location']);

                            $rg_form = self::get_form_meta($instance->title(), $blog_id);

                            $fields .= '<table width="99%" border="0" cellpadding="1" cellspacing="0" bgcolor="#EAEAEA"><tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">';


                            foreach ($_POST as $key => $value) {

                                if( $value && ( $value !== 'N/A' || $value !== '-- CHOOSE ONE --' ) && $key !== 'location' || ! is_array($value) ){

                                    $key = ( $key === '_loc_address' ) ? 'Location':$key;

                                    if( $rg_form ){
                                        $field_number = explode('-', $key);

                                        foreach ($rg_form as $meta) {

                                            if( strpos(end($field_number), '_') > 0 ){

                                                $parent_field = explode('_', end($field_number));

                                                    if( (int)$parent_field[0] === (int)$meta->id ){
                                                        if( is_array($meta->inputs) && !empty($meta->inputs) ){

                                                            foreach ($meta->inputs as $input) {

                                                                if( $input->id != str_replace('_', '.', end($field_number)) )
                                                                    continue;

                                                                $field = $input->label;
                                                                break;
                                                            }
                                                        }

                                                        break;
                                                    }
                                                    else{

                                                        $field = ucwords(str_replace('_', ' ', $key));

                                                        foreach ($rg_form as $meta) {
                                                            if( (int)$parent_field[0] === (int)$meta->id ){
                                                                if( is_array($meta->inputs) && !empty($meta->inputs) ){
                                                                    foreach ($meta->inputs as $input) {

                                                                        if( $input->id != str_replace('_', '.', end($field_number)) )
                                                                            continue;

                                                                        $field = $input->label;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        break;
                                                    }

                                                break;

                                            }
                                            else{
                                                if( end($field_number) == $meta->id ){
                                                    $field = str_replace(':', '', $meta->label);
                                                }

                                            }

                                        }

                                    }
                                    else{
                                        $field = $key;
                                    }

                                    if( is_array($value) && ! empty($value) ){

                                        //$fields .= ucwords(str_replace('-', ' ', str_replace('_', ' ', $key))) . ': ' . implode(', ', $value) . '<br/>';
                                        $fields .= '<tr bgcolor="#EAF2FA" style="padding: 7px;"><td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>'.ucwords(str_replace('-', ' ', str_replace('_', ' ', $field))).'</strong></font></td></tr>';
                                        $fields .= '<tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td><td><font style="font-family: sans-serif; font-size:12px;">'.implode(', ', $value).'</font></td></tr>';

                                    }
                                    else{
                                        //$fields .= ucwords(str_replace('-', ' ', str_replace('_', ' ', $key))) . ': ' . $value . '<br/>';
                                        $fields .= '<tr bgcolor="#EAF2FA" style="padding: 7px;"><td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>'.ucwords(str_replace('-', ' ', str_replace('_', ' ', $field))).'</strong></font></td></tr>';
                                        $fields .= '<tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td><td><font style="font-family: sans-serif; font-size:12px;">'.$value.'</font></td></tr>';
                                    }

                                }
                            }

                            $fields .= '</table></td></tr></table>';

                            $body = $fields;

                        }
                        else{
                            $body = self::parse_mail_tag($notif['body']);
                        }

                        if( $notif['sender'] ){
                            $headers[] = "From: " . self::parse_mail_tag($notif['sender']);
                        }

                        if( $notif['additional_headers'] ){
                            $headers[] = "Reply-To: " . self::parse_mail_tag($notif['additional_headers']);
                        }

                        $headers[] = 'Content-Type: text/html; charset=UTF-8';

                        wp_mail( $to, $subject, $body, $headers );
                    }

                }

            }
        }

            // submit to emma after email sent
            if( ucwords($instance->title()) === 'Newsletter' ){
                $emma = self::post_to_emma($instance->title(), $blog_id, $_POST);

                if( (int)$emma === -1 ){
                    dd('Missing Emma Account ID');
                }
                elseif( (int)$emma === -2 ){
                    dd('Missing Public API Key');
                }
                elseif( (int)$emma === -3 ){
                    dd('Missing Private API Key');
                }
            }
    }

    public static function change_properties($properties, $instance)
    {
        $fields = '';
        $microsite = self::get_microsite();
        $blog_id = isset($microsite->blog_id) ? $microsite->blog_id:$microsite;
        $form = $instance->id();

        if ( ! is_admin() ) {

            if ( $blog_id ) {
                switch_to_blog( 1 );
                $parent_redirect = get_post_meta( $form, 'redirect_page', true );
                restore_current_blog();
            }

            if ( isset($parent_redirect) && $parent_redirect ) {
                $messages['mail_sent_ok'] = '';
            } else {
                $messages = get_post_meta($form, '_messages', true);
            }


            if ( isset($messages) && $messages ) {
                $properties['messages']['mail_sent_ok'] = $messages['mail_sent_ok'];
            }

            if ( (int)$blog_id === 1 ) {
                switch_to_blog( 1 );
                $mail = get_post_meta($form, '_sfmu_mail', true);
                restore_current_blog();

                $notifs = isset($mail['main']) ? $mail['main']:'';
            } else {
                switch_to_blog( 1 );
                $mail = get_post_meta($form, '_sfmu_mail_'.$blog_id, true);
                $mail = unserialize($mail);
                
                restore_current_blog();

                $notifs = isset($mail['microsite']) ? $mail['microsite']:'';
            }

            if ( isset($notifs) && $notifs ) {
                $admin_email = get_microsite_email_alias($blog_id);
                switch_to_blog( $blog_id );
                if ( ! $admin_email ) {
                    $admin_email = get_option('admin_email');
                }
                restore_current_blog();

                if ( strpos($notifs[0]['recipient'], ',') ) {
                    $recipients = explode(',', str_replace(' ', '', $notifs[0]['recipient'] ) );
                } else {
                    $recipients = ($notifs[0]['recipient'] === '{admin_email}') ? array( $admin_email ) : array( self::parse_mail_tag($notifs[0]['recipient']) );
                }

                if ( strpos($notifs[0]['body'], 'all_fields') > 0 ) {
                    unset($_POST['_wpcf7_version']);
                    unset($_POST['_wpcf7_locale']);
                    unset($_POST['_wpcf7_unit_tag']);
                    unset($_POST['_wpcf7_nonce']);
                    unset($_POST['hidden_referrer']);

                    $rg_form = self::get_form_meta($instance->title(), $blog_id);

                    $fields .= '<table width="99%" border="0" cellpadding="1" cellspacing="0" bgcolor="#EAEAEA"><tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">';

                        foreach( $_POST as $key => $value ) {

                            if ( $key !== 'location' && ! is_array($value) ) {
                                $key = ( $key === '_loc_address' ) ? 'Location':$key;

                                if ( $value && $value !== 'N/A' && $value !== '-- CHOOSE ONE --' && $key !== '_wpcf7' && $key !== '_wpcf7_container_post' ) {

                                    if( $rg_form ){
                                        $field_number = explode('-', $key);

                                        foreach ($rg_form as $meta) {

                                            if( strpos(end($field_number), '_') > 0 ){

                                                $parent_field = explode('_', end($field_number));

                                                    if( (int)$parent_field[0] === (int)$meta->id ){
                                                        if( is_array($meta->inputs) && !empty($meta->inputs) ){

                                                            foreach ($meta->inputs as $input) {

                                                                if( $input->id != str_replace('_', '.', end($field_number)) )
                                                                    continue;

                                                                $field = $input->label;
                                                                break;
                                                            }
                                                        }

                                                        break;
                                                    }
                                                    else{

                                                        $field = ucwords(str_replace('_', ' ', $key));

                                                        foreach ($rg_form as $meta) {
                                                            if( (int)$parent_field[0] === (int)$meta->id ){
                                                                if( is_array($meta->inputs) && !empty($meta->inputs) ){
                                                                    foreach ($meta->inputs as $input) {

                                                                        if( $input->id != str_replace('_', '.', end($field_number)) )
                                                                            continue;

                                                                        $field = $input->label;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        break;
                                                    }

                                                break;

                                            }
                                            else{
                                                if( end($field_number) == $meta->id ){
                                                    $field = str_replace(':', '', $meta->label);
                                                }

                                            }

                                        }

                                    }
                                    else{
                                        $field = $key;
                                    }


                                    if ( is_array($value) && ! empty($value) ) {
                                        $fields .= '<tr bgcolor="#EAF2FA" style="padding: 7px;"><td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>'. ucwords(str_replace('-', ' ', str_replace('_', ' ', $field))) .'</strong></font></td></tr>';
                                        $fields .= '<tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td><td><font style="font-family: sans-serif; font-size:12px;">'. implode(', ', $value) .'</font></td></tr>';
                                    }
                                    else {
                                        if( $key === 'Location' ){
                                            $fields .= '<tr bgcolor="#EAF2FA" style="padding: 7px;"><td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>'. $key .'</strong></font></td></tr>';
                                            $fields .= '<tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td><td><font style="font-family: sans-serif; font-size:12px;">'.$value.'</font></td></tr>';
                                        }
                                        else{
                                            $fields .= '<tr bgcolor="#EAF2FA" style="padding: 7px;"><td colspan="2"><font style="font-family: sans-serif; font-size:12px;"><strong>'. ucwords(str_replace('-', ' ', str_replace('_', ' ', $field))) .'</strong></font></td></tr>';
                                            $fields .= '<tr bgcolor="#FFFFFF" style="padding: 7px;"><td width="20">&nbsp;</td><td><font style="font-family: sans-serif; font-size:12px;">'. $value .'</font></td></tr>';
                                        }
                                    }
                                }
                            }
                        }

                    $fields .= '</table></td></tr></table>';

                    $body = $fields;
                } else {
                    $body = self::parse_mail_tag($notifs[0]['body']);
                }

                $properties['mail']['recipient'] = ( $recipients[0] === '{admin_email}' ) ? $admin_email : self::parse_mail_tag( $recipients[0] );
                $properties['mail']['sender'] = self::parse_mail_tag($notifs[0]['sender']);
                $properties['mail']['subject'] = self::parse_mail_tag($notifs[0]['subject']);
                $properties['mail']['body'] = $body;
                $properties['mail']['additional_headers'] = 'Reply-To: '.self::parse_mail_tag($notifs[0]['additional_headers']);

                return $properties;

            } else {
                return $properties;
            }
        }

        return $properties;
    }

    public static function modify_rest_url($url, $path)
    {
        if ( get_current_blog_id() === 1 ) return $url;

        if (strpos($path, 'contact-form-7/v1') !== FALSE)  {
            switch_to_blog(1);
            $url = rest_url($path);
            restore_current_blog();
        }

        return $url;
    }

    public static function before_send($contact_form)
    {

        if ( is_main_site() === FALSE ) return $contact_form;

        $path = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
        $slug = explode('/', $path);
        $microsite = array();

        if ( isset($slug[1]) ) {
            global $wpdb;
            $microsite = $wpdb->get_row("SELECT * FROM wp_blogs WHERE `path` = '/{$slug[1]}/'");

            if ( ! $microsite ) {
                self::add_leads('', $contact_form);
            } else {
                self::add_leads($microsite->blog_id, $contact_form);
            }
        } else {
            self::add_leads('', $contact_form);
        }

        $contact_form->skip_mail = true;

        do_action('sfmu_before_send_mail', $microsite, $contact_form);

        return $contact_form;
    }

    public static function add_leads($blog_id = null, $contact_form)
    {
        global $wpdb;

        if ( $blog_id !== NULL ) {
            switch_to_blog($blog_id);
                $prefix = $wpdb->prefix;
                $permalink = get_the_permalink($_POST['_wpcf7_container_post']);
            restore_current_blog();
        }
        else {
            $prefix = $wpdb->prefix;
            $permalink = get_the_permalink($_POST['_wpcf7_container_post']);
        }

        define( 'DIEONDBERROR', true );

        $wpdb->show_errors();

        $lead = array(
            $_POST['_wpcf7'],
            $_POST['_wpcf7_container_post'],
            current_time('mysql'),
            self::get_ip(),
            $permalink,
            $_SERVER['HTTP_USER_AGENT'],
            get_current_user_id() ?: NULL,
            'USD'
        );

        $what = $wpdb->query( $wpdb->prepare(
            "
                INSERT INTO {$prefix}rg_lead
                ( form_id, post_id, date_created, ip, source_url, user_agent, created_by, currency )
                VALUES ( %d, %d, %s, %s, %s, %s, %d, %s )
            ",
            $lead
        ) );

        $lead_id = $wpdb->insert_id;

        $form_id = $_POST['_wpcf7'];

        unset($_POST['_wpcf7']);
        unset($_POST['_wpcf7_version']);
        unset($_POST['_wpcf7_locale']);
        unset($_POST['_wpcf7_unit_tag']);
        unset($_POST['_wpcf7_nonce']);

        $rg_form = self::get_form_meta($contact_form->title(), $blog_id);

        foreach( $_POST as $field => $value ) {
            if($field !== '_wpcf7_container_post' && $value && $value !== 'N/A' && $value !== '-- CHOOSE ONE --'){

                if( $rg_form ){

                    $field_number = explode('-', $field);
                    foreach ($rg_form as $meta) {

                        if( stripos(end($field_number), '_') ){
                            $parent_field = explode('_', end($field_number));

                            foreach ($rg_form as $meta) {
                                if( (int)$parent_field[0] === (int)$meta->id ){
                                    if( is_array($meta->inputs) && !empty($meta->inputs) ){
                                        foreach ($meta->inputs as $input) {
                                            if( $input->id == str_replace('_', '.', end($field_number)) ){
                                                //dd($meta->label);
                                                $field = $input->label;
                                            }
                                        }
                                    }
                                }
                            }

                        }
                        else{
                            if( end($field_number) == $meta->id ){
                                $field = str_replace(':', '', $meta->label);
                            }

                        }

                    }

                }

                $wpdb->query( $wpdb->prepare(
                        "
                        INSERT INTO {$prefix}sfl_lead_detail
                        ( lead_id, form_id, field_name, value )
                        VALUES ( %d, %d, %s, %s )
                        ",
                        $lead_id,
                        $form_id,
                        $field,
                        (is_array($value) && ! empty($value)) ? implode(', ', $value) : $value
                ) );

                $lead_detail_id = $wpdb->insert_id;

                if( !is_array($value) && strlen($value) >= 201 ){
                    $wpdb->query( $wpdb->prepare(
                        "
                        INSERT INTO {$prefix}rg_lead_detail_long
                        ( lead_detail_id, value )
                        VALUES ( %d, %s )
                        ",
                        $lead_detail_id,
                        $value
                    ) );
                }

            }
        }

    }

    private static function get_ip() {

        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {

            $ip = $_SERVER['HTTP_CLIENT_IP'];

        } elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {

            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    private static function get_microsite()
    {
        if( isset($_SERVER['HTTP_REFERER']) ){
            $path = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH);
            $slug = explode('/', $path);
            $microsite = array();
        }

        if ( isset($slug[1]) ) {
            global $wpdb;
            $microsite = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}blogs WHERE `path` = '/{$slug[1]}/'");

            if ( $microsite ) {
                return $microsite;
            } else {
                return 1;
            }
        }
    }

    private static function parse_mail_tag($str)
    {

        $nap_data = array();
        $microsite = self::get_microsite();
        $id = isset($microsite->blog_id) ? $microsite->blog_id: 1;

        if ( function_exists('get_microsite_nap') ) {
            $nap = get_microsite_nap($id);
        }

        $content = $str;

        preg_match_all('/{(.*?)}/', $content, $matches);

        if ( ! in_array('all_fields', $matches[1]) ) {

            foreach ($matches[0] as $index => $var_name) {

                if ( $var_name === '{location_address}' ) {
                    $converted = str_replace($var_name, $_POST['_loc_address'], $content);
                } else if ( isset($_POST[$matches[1][$index]]) ) {
                    if ( (int) $index === 0 ) {
                        $converted = str_replace($var_name, $_POST[$matches[1][$index]], $content);
                    } else {
                        if ( is_array($_POST[$matches[1][$index]]) ) {

                            $arr_var = '<ul>';
                            foreach( $_POST[$matches[1][$index]] as $value ) {
                                $arr_var .= '<li>' . $value . '</li>';
                            }
                            $arr_var .= '</ul>';

                            $converted = str_replace($var_name, $arr_var, $converted);
                        } else {
                            $converted = str_replace($var_name, $_POST[$matches[1][$index]], $converted);
                        }
                    }
                }
            }
        }

        if ( isset($converted) ) {
            $content = $converted;
        }

        preg_match_all('/\[[^\]]*\]/', $content, $tags);

        if( is_array($tags) && ! empty($tags) ){
            foreach ($tags as $tag) {
                if( is_array($tag) && ! empty($tag) ){

                    foreach ($tag as $key => $value) {
                        $fields = explode(' ', str_replace(']', '', $value));

                        if( count($fields) > 1 ){
                            $field = 'microsite_'.$fields[1];

                            if( isset($nap->$field) && $fields[1] !== 'address' ){
                                $nap_data = $nap->$field . '<br/>';
                            }
                            else{
                                $nap_data = $nap->microsite_street . ' ' . $nap->microsite_city . ', ' . $nap->microsite_state_province . ' ' . $nap->microsite_zip_postal . '<br/>';
                            }

                            if( (int)$key === 0 ){
                                $converted = str_replace($value, $nap_data, $content);
                            }
                            else{
                                $converted = str_replace($value, $nap_data, $converted);
                            }

                        }

                    }

                }
            }
        }

        if ( isset($converted) ) {
            $result = $converted;
        } else {
            $result = $str;
        }

        return $result;

    }

    private static function post_to_emma($title, $id, $post)
    {
        // Authentication Variables
        $emma_api = gss_get_blog_option($id, 'gravityformsaddon_gravityformsemma_settings');

        $account_id = $emma_api['account_id'];
        $public_api_key = $emma_api['public_api_key'];
        $private_api_key = $emma_api['private_api_key'];
        $groups = $emma_api['group_ids'];

        if( $account_id === '' )
            return -1;

        if( $public_api_key === '' )
            return -2;

        if( $private_api_key === '' )
            return -3;

        // Form variable(s)
        $email = $post['email-2'];
        $first_name = strstr($email, '@', true);
        $last_name = 'Goldfish Subscriber';

        // Member data other than email should be passed in an array called "fields"
        $member_data = array(
            "email" => $email,
            "fields" => array(
                'first_name' => $first_name,
                'last_name'  => $last_name,
            ),
            "group_ids" => array($groups),
        );

        // Set URL
        $url = "https://api.e2ma.net/".$account_id."/members/add";

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_USERPWD, $public_api_key . ":" . $private_api_key);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($member_data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($member_data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $head = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        //execute post
        if($http_code > 200) {
            /**
            * Added action hook to catch details on failed emma subscription
            * @param $http_code - HTTP POST code
            * @param $member_data - The post data
            */
            do_action('sfmu_emma_failed_subscription', $http_code, $member_data);
            $app_message = "Error sending subscription request HTTP returned {$http_code}";

            error_log($app_message);
            return new \WP_Error( 'sfmu_emma_failed_subscription', $app_message );
        } else {
            return true;
        }

    }

    public static function get_instance()
    {
        return new CF7_Submit();
    }

    public static function get_form_meta($title, $blog_id)
    {
        $table_name = 'wp_'.$blog_id.'_rg_form';

        // if old microsites
        switch_to_blog($blog_id);
            global $wpdb;
            $fields = array();

            if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name) {
                $form = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}rg_form WHERE title = '{$title}'", OBJECT );

                if( $form ){
                    $form_meta = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}rg_form_meta WHERE form_id = '{$form[0]->id}'", OBJECT );
                    $form_meta = json_decode($form_meta[0]->display_meta);

                    if ( $form_meta && isset($form_meta->fields) ) {
                        $fields = $form_meta->fields;
                    }
                    else{
                        return false;
                    }

                }
            }
        restore_current_blog();

        if( isset($form) && !empty($form) ){

            if ( ! $fields ) return false;

            return $fields;
        }
        else{

            // if new microsites get rg_form from main site
            switch_to_blog(1);
                global $wpdb;
                $fields = array();

                $form = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}rg_form WHERE title = '{$title}'", OBJECT );

                if( $form ){
                    $form_meta = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}rg_form_meta WHERE form_id = '{$form[0]->id}'", OBJECT );
                    $form_meta = json_decode($form_meta[0]->display_meta);

                    if ( $form_meta && isset($form_meta->fields) ) {
                        $fields = $form_meta->fields;
                    }
                    else{
                        return false;
                    }
                }
            restore_current_blog();

            if ( ! $fields ) return false;

            return $fields;
        }

    }
}
