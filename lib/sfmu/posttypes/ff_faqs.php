<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Faq;

/**
* FAQs Post Type
*/
$labels = array(
    'name'               => _x( 'FAQs', 'gss-faqs', '' ),
    'singular_name'      => _x( 'FAQ', 'gss-faq', '' ),
    'menu_name'          => _x( 'FAQs', 'faqs', '' ),
    'name_admin_bar'     => _x( 'FAQ', 'faq', '' ),
    'add_new'            => _x( 'Add New', 'faq', '' ),
    'add_new_item'       => __( 'Add New FAQ', '' ),
    'new_item'           => __( 'New FAQ', '' ),
    'edit_item'          => __( 'Edit FAQ', '' ),
    'view_item'          => __( 'View FAQ', '' ),
    'all_items'          => __( 'All FAQs', '' ),
    'search_items'       => __( 'Search FAQs', '' ),
    'parent_item_colon'  => __( 'Parent FAQs:', '' ),
    'not_found'          => __( 'No FAQ found.', '' ),
    'not_found_in_trash' => __( 'No FAQ found in Trash.', '' )
);

return [
    'labels'      => $labels,
    'has_archive' => true,
    'public'      => true,
    'supports'    => array( 'title', 'editor', 'author', 'page-attributes', 'excerpt', 'thumbnail', 'custom-fields' ),
    'rewrite'     => array( 'slug' => 'faq', 'with_front' => false ),
];
