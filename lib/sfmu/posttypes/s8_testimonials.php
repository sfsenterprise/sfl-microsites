<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Events;

/**
* Testimonials Post Type
*/
$labels = array(
    'name'               => _x( 'Testimonials', 'gss-testimonials', 'sfl-microsites' ),
    'singular_name'      => _x( 'Testimonial', 'gss-testimonial', 'sfl-microsites' ),
    'menu_name'          => _x( 'Testimonials', 'testimonials', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Testimonial', 'testimonials', 'sfl-microsites' ),
    'add_new'            => _x( 'Add Testimonial', 'testimonials', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New Testimonial', 'sfl-microsites' ),
    'new_item'           => __( 'New Testimonial', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Testimonial', 'sfl-microsites' ),
    'view_item'          => __( 'View Testimonial', 'sfl-microsites' ),
    'all_items'          => __( 'All Testimonials', 'sfl-microsites' ),
    'search_items'       => __( 'Search Testimonials', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Testimonial:', 'sfl-microsites' ),
    'not_found'          => __( 'No testimonial found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No testimonial found in Trash.', 'sfl-microsites' )
);

return [
    'labels'      => $labels,
    'has_archive' => true,
    'public'      => true,
    'exclude_from_search' => true,
    'show_in_admin_bar'   => false,
    'supports'    => array( 'title', 'editor', 'thumbnail' ),
    'rewrite'     => array( 'slug' => 'testimonials', 'with_front'=>false ),
];
