<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Forms;

$labels = array(
    'name'               => _x( 'Forms', 'gss-forms', 'sfl-microsites' ),
    'singular_name'      => _x( 'Form', 'gss-forms', 'sfl-microsites' ),
    'menu_name'          => _x( 'Forms', 'forms', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Form', 'forms', 'sfl-microsites' ),
    'add_new'            => _x( 'Add New', 'forms', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New', 'sfl-microsites' ),
    'new_item'           => __( 'New Form', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Form', 'sfl-microsites' ),
    'view_item'          => __( 'View Form', 'sfl-microsites' ),
    'all_items'          => __( 'Forms', 'sfl-microsites' ),
    'search_items'       => __( 'Search Forms', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Form:', 'sfl-microsites' ),
    'not_found'          => __( 'No form found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No form found in Trash.', 'sfl-microsites' )
);
return array(
    'labels'      => $labels,
    'has_archive' => false,
    'public'      => true,
    'supports'    => array( 'title', 'editor' ),
    'rewrite'     => array( 'slug' => 'forms', 'with_front'=>false ),
);
