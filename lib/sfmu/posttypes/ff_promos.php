<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Events;

/**
* Promos Post Type
*/

$labels = array(
    'name'               => _x( 'Promos', 'gss-promos', 'sfl-microsites' ),
    'singular_name'      => _x( 'Promo', 'gss-promo', 'sfl-microsites' ),
    'menu_name'          => _x( 'Promos', 'promos', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Promo', 'promos', 'sfl-microsites' ),
    'add_new'            => _x( 'Add Promo', 'promos', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New Promo', 'sfl-microsites' ),
    'new_item'           => __( 'New Promo', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Promo', 'sfl-microsites' ),
    'view_item'          => __( 'View Promo', 'sfl-microsites' ),
    'all_items'          => __( 'All Promos', 'sfl-microsites' ),
    'search_items'       => __( 'Search Promos', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Promo:', 'sfl-microsites' ),
    'not_found'          => __( 'No promo found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No promo found in Trash.', 'sfl-microsites' )
);

return [
    'labels'      => $labels,
    'has_archive' => true,
    'public'      => true,
    'supports'    => array( 'title', 'editor', 'thumbnail' ),
    'rewrite'     => array( 'slug' => 'promos', 'with_front' =>false ),
];
