<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Lessons;

$labels = array(
    'name'               => _x( 'Swim Lesson Levels', 'gss-swimlessons', 'sfl-microsites' ),
    'singular_name'      => _x( 'Swim Lesson Level', 'gss-swimlesson', 'sfl-microsites' ),
    'menu_name'          => _x( 'Swim Lesson Levels', 'swimlessons', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Swim Lesson Level', 'swimlessons', 'sfl-microsites' ),
    'add_new'            => _x( 'Add New', 'swimlessons', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New', 'sfl-microsites' ),
    'new_item'           => __( 'New Swim', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Swim Lesson Level', 'sfl-microsites' ),
    'view_item'          => __( 'View Swim Lesson Level', 'sfl-microsites' ),
    'all_items'          => __( 'Swim Lesson Levels', 'sfl-microsites' ),
    'search_items'       => __( 'Search Swim Lesson Levels', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Swim Lesson Level:', 'sfl-microsites' ),
    'not_found'          => __( 'No swim lesson level found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No swim lesson level found in Trash.', 'sfl-microsites' )
);

$categories_labels = array(
    'name'              => _x( 'Swim Lesson Groups', 'Swim Lesson Groups', 'sfl-microsites' ),
    'singular_name'     => _x( 'Swim Lesson Group', 'Swim Lesson Group', 'sfl-microsites' ),
    'search_items'      => __( 'Search Groups', 'sfl-microsites' ),
    'all_items'         => __( 'All Groups', 'sfl-microsites' ),
    'parent_item'       => __( 'Parent Groups', 'sfl-microsites' ),
    'parent_item_colon' => __( 'Parent Groups:', 'sfl-microsites' ),
    'edit_item'         => __( 'Edit Group', 'sfl-microsites' ),
    'update_item'       => __( 'Update Group', 'sfl-microsites' ),
    'add_new_item'      => __( 'Add New Group', 'sfl-microsites' ),
    'new_item_name'     => __( 'New Group Name', 'sfl-microsites' ),
    'menu_name'         => __( 'Swim Lesson Groups', 'sfl-microsites' ),
);
$categories_args = array(
    'hierarchical'      => true,
    'labels'            => $categories_labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'lesson-groups' ),
);

return array(
    'labels'                => $labels,
    'has_archive'           => false,
    'hierarchical'          => true,
    'public'                => true,
    'publicly_queryable'    => false,
    'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
    'rewrite'               => array( 'slug' => 'lessons', 'with_front'=>false ),
    'categories_args'       => $categories_args
);
