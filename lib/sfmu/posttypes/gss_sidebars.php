<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Sidebars;

$labels = array(
    'name'               => _x( 'Sidebars', 'gss-sidebars', 'sfl-microsites' ),
    'singular_name'      => _x( 'Sidebar', 'gss-sidebar', 'sfl-microsites' ),
    'menu_name'          => _x( 'Sidebars', 'sidebars', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Sidebar', 'sidebars', 'sfl-microsites' ),
    'add_new'            => _x( 'Add Sidebar', 'sidebars', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New Sidebar', 'sfl-microsites' ),
    'new_item'           => __( 'New Sidebar', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Sidebar', 'sfl-microsites' ),
    'view_item'          => __( 'View Sidebar', 'sfl-microsites' ),
    'all_items'          => __( 'All Sidebars', 'sfl-microsites' ),
    'search_items'       => __( 'Search Sidebars', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Sidebar:', 'sfl-microsites' ),
    'not_found'          => __( 'No sidebar found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No sidebar found in Trash.', 'sfl-microsites' )
);
return [
    'labels'                => $labels,
    'has_archive'           => true,
    'public'                => true,
    'publicly_queryable'    => false,
    'supports'              => array( 'title', 'editor', 'thumbnail' ),
    'rewrite'               => array( 'slug' => 'sidebars', 'with_front'=>false ),
];
