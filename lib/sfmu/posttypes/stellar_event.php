<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Events;

/**
* Events Post Type
*/
$labels = array(
    'name'               => _x( 'Events', 'gss-events', '' ),
    'singular_name'      => _x( 'Event', 'gss-event', '' ),
    'menu_name'          => _x( 'Events', 'events', '' ),
    'name_admin_bar'     => _x( 'Event', 'event', '' ),
    'add_new'            => _x( 'Add New', 'event', '' ),
    'add_new_item'       => __( 'Add New Event', '' ),
    'new_item'           => __( 'New Event', '' ),
    'edit_item'          => __( 'Edit Event', '' ),
    'view_item'          => __( 'View Event', '' ),
    'all_items'          => __( 'All Events', '' ),
    'search_items'       => __( 'Search Events', '' ),
    'parent_item_colon'  => __( 'Parent Events:', '' ),
    'not_found'          => __( 'No events found.', '' ),
    'not_found_in_trash' => __( 'No events found in Trash.', '' )
);

// TAXONOMIES - CATEGORIES
$categories_labels = array(
    'name'              => _x( 'Categories', 'GSS Events Categories', 'sfl-microsites' ),
    'singular_name'     => _x( 'Category', 'GSS Events Category', 'sfl-microsites' ),
    'search_items'      => __( 'Search Categories', 'sfl-microsites' ),
    'all_items'         => __( 'All Categories', 'sfl-microsites' ),
    'parent_item'       => __( 'Parent Categories', 'sfl-microsites' ),
    'parent_item_colon' => __( 'Parent Categories:', 'sfl-microsites' ),
    'edit_item'         => __( 'Edit Category', 'sfl-microsites' ),
    'update_item'       => __( 'Update Category', 'sfl-microsites' ),
    'add_new_item'      => __( 'Add New Category', 'sfl-microsites' ),
    'new_item_name'     => __( 'New Category Name', 'sfl-microsites' ),
    'menu_name'         => __( 'Category', 'sfl-microsites' ),
);
$categories_args = array(
    'name'              => 'stellar_event_categories',
    'hierarchical'      => true,
    'labels'            => $categories_labels,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => 'event-category' ),
);

return [
    'labels'          => $labels,
    'has_archive'     => true,
    'public'          => true,
    'supports'        => array( 'title', 'editor', 'author', 'excerpt', 'thumbnail', 'custom-fields' ),
    'rewrite'         => array( 'slug' => 'events', 'with_front' => false ),
    'categories_args' => $categories_args,
];
