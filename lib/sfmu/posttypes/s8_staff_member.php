<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Events;

/**
* Saff Post Type
*/

$labels = array(
    'name'               => _x( 'Staff', 'gss-staff', 'sfl-microsites' ),
    'singular_name'      => _x( 'Staff', 'gss-staff', 'sfl-microsites' ),
    'menu_name'          => _x( 'Staff', 'staff', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Staff', 'staff', 'sfl-microsites' ),
    'add_new'            => _x( 'Add Staff', 'staff', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New Staff', 'sfl-microsites' ),
    'new_item'           => __( 'New Staff', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Staff', 'sfl-microsites' ),
    'view_item'          => __( 'View Staff', 'sfl-microsites' ),
    'all_items'          => __( 'All Staffs', 'sfl-microsites' ),
    'search_items'       => __( 'Search Staffs', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Staff:', 'sfl-microsites' ),
    'not_found'          => __( 'No staff found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No staff found in Trash.', 'sfl-microsites' )
);

return [
    'labels'      => $labels,
    'has_archive' => true,
    'public'      => true,
    'supports'    => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
    'rewrite'     => array( 'slug' => 'staff', 'with_front' => false ),
];
