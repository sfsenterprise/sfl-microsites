<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Events;

/**
* Announcements Post Type
*/

$labels = array(
    'name'               => _x( 'Announcements', 'gss-announcements', 'sfl-microsites' ),
    'singular_name'      => _x( 'Announcement', 'gss-announcement', 'sfl-microsites' ),
    'menu_name'          => _x( 'Announcements', 'announcements', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Announcement', 'announcements', 'sfl-microsites' ),
    'add_new'            => _x( 'Add Announcement', 'announcements', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New Announcement', 'sfl-microsites' ),
    'new_item'           => __( 'New Announcement', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Announcement', 'sfl-microsites' ),
    'view_item'          => __( 'View Announcement', 'sfl-microsites' ),
    'all_items'          => __( 'All Announcements', 'sfl-microsites' ),
    'search_items'       => __( 'Search Announcements', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Announcement:', 'sfl-microsites' ),
    'not_found'          => __( 'No announcement found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No announcement found in Trash.', 'sfl-microsites' )
);

return [
    'labels'      => $labels,
    'has_archive' => true,
    'public'      => true,
    'supports'    => array( 'title', 'editor', 'thumbnail' ),
    'rewrite'     => array( 'slug' => 'announcements', 'with_front' => false ),
];
