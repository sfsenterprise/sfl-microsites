<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\Events;

/**
* Jobs Post Type
*/
$labels = array(
    'name'               => _x( 'Job Listings', 'gss-job-listings', 'sfl-microsites' ),
    'singular_name'      => _x( 'Job Listing', 'gss-job-listing', 'sfl-microsites' ),
    'menu_name'          => _x( 'Job Listing', 'job-listing', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'Job Listing', 'job-listing', 'sfl-microsites' ),
    'add_new'            => _x( 'Add Job', 'job-listing', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New Job', 'sfl-microsites' ),
    'new_item'           => __( 'New Job', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit Job', 'sfl-microsites' ),
    'view_item'          => __( 'View Job', 'sfl-microsites' ),
    'all_items'          => __( 'All Jobs', 'sfl-microsites' ),
    'search_items'       => __( 'Search Jobs', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent Job:', 'sfl-microsites' ),
    'not_found'          => __( 'No job found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No job found in Trash.', 'sfl-microsites' )
);
return [
    'labels'      => $labels,
    'has_archive' => true,
    'public'      => true,
    'supports'    => array( 'title', 'editor', 'thumbnail' ),
    'rewrite'     => array( 'slug' => 'jobs', 'with_front' => false ),
];
