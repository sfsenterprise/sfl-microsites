<?php

namespace Surefire\Microsites\Lib\Sfmu\Posttypes\News;

/**
* News Post Type
*/
$labels = array(
    'name'               => _x( 'News', 'gss-news', 'sfl-microsites' ),
    'singular_name'      => _x( 'News', 'gss-news', 'sfl-microsites' ),
    'menu_name'          => _x( 'News', 'news', 'sfl-microsites' ),
    'name_admin_bar'     => _x( 'News', 'news', 'sfl-microsites' ),
    'add_new'            => _x( 'Add New', 'news', 'sfl-microsites' ),
    'add_new_item'       => __( 'Add New News', 'sfl-microsites' ),
    'new_item'           => __( 'New News', 'sfl-microsites' ),
    'edit_item'          => __( 'Edit News', 'sfl-microsites' ),
    'view_item'          => __( 'View News', 'sfl-microsites' ),
    'all_items'          => __( 'All News', 'sfl-microsites' ),
    'search_items'       => __( 'Search News', 'sfl-microsites' ),
    'parent_item_colon'  => __( 'Parent News:', 'sfl-microsites' ),
    'not_found'          => __( 'No events found.', 'sfl-microsites' ),
    'not_found_in_trash' => __( 'No events found in Trash.', 'sfl-microsites' )
);

return [
    'labels'          => $labels,
    'has_archive'     => true,
    'public'          => true,
    'supports'        => array( 'title', 'editor', 'author', 'excerpt', 'thumbnail', 'custom-fields' ),
    'rewrite'         => array( 'slug' => 'news', 'with_front' => false )
];
