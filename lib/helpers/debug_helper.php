<?php

/**
 * Suremak ENT-DEV standard debugger function
 * sfsd - debugger
 * @param string|int|array|object
 * @return value(s)
 */
if ( !function_exists('dd') ) :
    function dd($value) {
        print_r($value);
        exit;
    }
endif;
