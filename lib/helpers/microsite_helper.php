<?php

use Roots\Sage\Assets;

function get_microsite_email($blog_id = false) {

    if ( $blog_id ) {
        switch_to_blog($blog_id);
        $email = get_option('admin_email', '');
        restore_current_blog();
        return $email;
    }

    global $wp_query, $post;

    if ( is_main_site() !== FALSE ) {
        if ( ! isset( $post->microsite_blog_id ) ) return;

        return get_blog_option($post->microsite_blog_id, 'admin_email');
    }

    if ( !isset($wp_query) && !isset($wp_query->nap) )
        return;

    extract((array) $wp_query->nap);
    return get_option('admin_email', '');
}

function get_microsite_email_alias($blog_id)
{
    if ( ! $blog_id ) return;
    global $wpdb;

    $microsite = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE microsite_blog_id = '{$blog_id}'");

    if ( ! $microsite ) return;

    $fields = maybe_unserialize($microsite->microsite_fields);

    if ( ! isset($fields['admin_email_alias'])  ) return;

    return $fields['admin_email_alias'];

}

function get_microsite_name() {
    return get_microsite_prop('name');
}

function get_microsite_street()
{
    $street = get_microsite_prop('street');
    $street .= get_microsite_prop('street2') ? ', ' . get_microsite_prop('street2') : null;

    return $street;
}

function get_microsite_street2() {
    return get_microsite_prop('street2');
}

function get_microsite_city()
{
    return get_microsite_prop('city');
}

function get_microsite_state($abbr = TRUE)
{
    $state = get_microsite_prop('state_province');
    if ( strlen($state) > 2 && $abbr = TRUE ) {
        return abbr_state($state);
    }

    return $state;
}

function get_microsite_zip()
{
    return get_microsite_prop('zip_postal');
}

function get_microsite_phone()
{
    return get_microsite_prop('phone');
}

function get_microsite_manager() {
    return get_microsite_prop('general_manager');
}

function get_microsite_address($site = false)
{
    if ( ! $site ) {
        return vsprintf('%1$s<br /> %2$s, %3$s %4$s', [
            get_microsite_street(),
            get_microsite_city(),
            get_microsite_state(),
            get_microsite_zip()
        ]);
    } else {
        $street = $site->microsite_street;
        $street .= $site->microsite_street2 ? ', ' . $site->microsite_street : null;

        return vsprintf('%1$s<br /> %2$s, %3$s %4$s', [
            $street,
            $site->microsite_city,
            $site->microsite_state_province,
            $site->microsite_zip_postal
        ]);
    }

}

function get_microsite_lat()
{
    return get_microsite_prop('lat');
}

function get_microsite_lng()
{
return get_microsite_prop('lng');
}

function get_microsite_post_id()
{
return get_microsite_prop('post_id');
}

function get_microsite_homeurl()
{
    if ( is_main_site() !== FALSE ) {
        $blog_id = get_microsite_prop('blog_id');
        return get_site_url($blog_id);
    } else {
        return site_url();
    }
}

function get_microsite_loginurl()
{
  /*optimize me DAN --mama j*/ //this is for the microsite homepage sidebar "Member Login Link"
  $option = get_option("s8_ff_location_settings");
  return $option['member_login_url'];
}

function get_microsite_tour()
{
  /*optimize me DAN --mama j*/ //this is for the iframe in the /tour page
  $tour_url = get_option("tour_iframe");
  $iframe = ($tour_url) ? '<iframe class="embed-responsive-item" frameborder="0"  src="'.$tour_url.'" style="width:100%;height:450px;"></iframe>': '<p>Virtual Tour Coming Soon!!</p>';
  return $iframe;
}

function get_microsite_direction($site = false)
{
    if ( $site === false ) {
        $daddr = urlencode(
            get_microsite_street() . ',' .
            get_microsite_city() . ',' .
            get_microsite_state() . ' ' . get_microsite_zip()
        );
    } else {
        $street = $site->microsite_street;
        $street .= $site->microsite_street2 ? ', ' . $site->microsite_street2 : null;
        $daddr = urlencode($street . ',' . $site->microsite_city . ',' . $site->microsite_state_province . ' ' . $site->microsite_zip_postal);
    }


    return 'https://www.google.com/maps?' . 'saddr=Current+Location&daddr=' . $daddr;
}

function get_microsite_json_nap($site = false, $raw = false)
{
    global $wp_query;
    if ( isset($wp_query->site) ) {
        $site = $wp_query->site;
    }

    if ( $site === false ) {
        $nap =array(
            'name' => get_microsite_name() ?: '',
            'address' => get_microsite_address() ?: '',
            'city' => get_microsite_city() ?: '',
            'state' => get_microsite_state() ?: '',
            'zip' => get_microsite_zip() ?: '',
            'stateabbr' => abbr_state(get_microsite_state()),
            'lat' => get_microsite_lat() ?: 0,
            'lng' => get_microsite_lng() ?: 0,
            'phone' => get_microsite_phone() ?: '',
            'manager' => get_microsite_manager() ?: '',
            'email' => get_microsite_email() ?: '',
            'url' => get_microsite_homeurl() ?: '',
            'directions' => get_microsite_direction()
        );
    } else {

        $nap = array(
            'name' => $site->microsite_name ?: '',
            'address' => get_microsite_address($site) ?: '',
            'city' => $site->microsite_city,
            'state' => $site->microsite_state_province,
            'zip' => $site->microsite_zip_postal,
            'stateabbr' => abbr_state($site->microsite_state_province),
            'lat' => $site->microsite_lat ?: 0,
            'lng' => $site->microsite_lng ?: 0,
            'phone' => $site->microsite_phone ?: '',
            'manager' => $site->microsite_general_manager ?: '',
            'email' => get_blog_option($site->microsite_blog_id, 'admin_email') ?: '',
            'url' => get_site_url($site->microsite_blog_id) ?: '',
            'directions' => get_microsite_direction($site)
        );
    }

    if ( $raw === true ) {
        return $nap;
    } else {
        return htmlspecialchars(json_encode($nap));
    }
}

function get_sites_json()
{
    global $wpdb;

    switch_to_blog(1);
    $sites = $wpdb->get_results("SELECT * FROM sfl_microsites AS m LEFT JOIN {$wpdb->prefix}blogs AS b ON m.microsite_blog_id = b.blog_id WHERE m.microsite_post_id > 0 AND b.archived != 1");
    restore_current_blog();
    $locations = array();

    foreach( $sites as $site ) {
        $locations[] = get_microsite_json_nap($site, true);
    }
    return htmlspecialchars(json_encode($locations));
}

function get_microsite_prop($property)
{
    $property = 'microsite_' . $property;
    global $wp_query, $post;

    if ( is_main_site() !== FALSE ) {
        if ( isset( $wp_query->site ) ) {
            return $wp_query->site->$property;
        } elseif ( ! isset( $post->microsite_blog_id ) ) {
            if ( !isset($wp_query) && !isset($wp_query->nap) )
                return;

            extract((array) $wp_query->nap);

            if( isset($$property) )
                return $$property;
        }

        return $post->$property;
    }

    if ( !isset($wp_query) && !isset($wp_query->nap) )
        return;

    extract((array) $wp_query->nap);

    if( isset($$property) )
        return $$property;
}

function have_sites() {
    global $wp_query;

    if ( isset($wp_query->sites) && $wp_query->sites_count > 0 ) {
        return true;
    }

    if (isset($wp_query->site)) unset($wp_query->site);

    return false;
}

function the_site() {
    global $wp_query;

    if ( !isset($wp_query->sites) ) return;

    if ( count($wp_query->sites) < 1 ) return;

    $key = count($wp_query->sites) - $wp_query->sites_count;

    $wp_query->sites_count = $wp_query->sites_count - 1;
    $wp_query->site = $wp_query->sites[$key];
}

function abbr_state($state)
{
    $states = array(
        'AL'=>'Alabama',
        'AK'=>'Alaska',
        'AZ'=>'Arizona',
        'AR'=>'Arkansas',
        'CA'=>'California',
        'CO'=>'Colorado',
        'CT'=>'Connecticut',
        'DE'=>'Delaware',
        'DC'=>'District of Columbia',
        'FL'=>'Florida',
        'GA'=>'Georgia',
        'HI'=>'Hawaii',
        'ID'=>'Idaho',
        'IL'=>'Illinois',
        'IN'=>'Indiana',
        'IA'=>'Iowa',
        'KS'=>'Kansas',
        'KY'=>'Kentucky',
        'LA'=>'Louisiana',
        'ME'=>'Maine',
        'MD'=>'Maryland',
        'MA'=>'Massachusetts',
        'MI'=>'Michigan',
        'MN'=>'Minnesota',
        'MS'=>'Mississippi',
        'MO'=>'Missouri',
        'MT'=>'Montana',
        'NE'=>'Nebraska',
        'NV'=>'Nevada',
        'NH'=>'New Hampshire',
        'NJ'=>'New Jersey',
        'NM'=>'New Mexico',
        'NY'=>'New York',
        'NC'=>'North Carolina',
        'ND'=>'North Dakota',
        'OH'=>'Ohio',
        'OK'=>'Oklahoma',
        'OR'=>'Oregon',
        'PA'=>'Pennsylvania',
        'RI'=>'Rhode Island',
        'SC'=>'South Carolina',
        'SD'=>'South Dakota',
        'TN'=>'Tennessee',
        'TX'=>'Texas',
        'UT'=>'Utah',
        'VT'=>'Vermont',
        'VA'=>'Virginia',
        'WA'=>'Washington',
        'WV'=>'West Virginia',
        'WI'=>'Wisconsin',
        'WY'=>'Wyoming',
        'AB'=>'Alberta',
        'BC'=>'British Columbia',
        'MB'=>'Manitoba',
        'NB'=>'New Brunswick',
        'NL'=>'Newfoundland and Labrador',
        'NT'=>'Northwest Territories',
        'NS'=>'Nova Scotia',
        'NU'=>'Nunavut',
        'ON'=>'Ontario',
        'PE'=>'Prince Edward Island',
        'QC'=>'Quebec',
        'SK'=>'Saskatchewan',
        'YT'=>'Yukon',
    );

    if ( strlen($state) < 3 ) {
        return isset($states[$state]) ? $states[$state] : false;
    }  else {
        return array_search($state, $states);
    }
}

function get_the_main_ID() {
    global $post;

    if ( ! $post )
        return;

    return get_post_meta($post->ID, '_sfmu_parent_id', true);
}

function get_attachment_url( $blog_id, $post_id, $key='_thumbnail', $single = true ){

    switch_to_blog( $blog_id );
        $postmeta = get_post_meta( $post_id, $key, $single );
    restore_current_blog();

    switch_to_blog( 1 );
        $attachment_url = wp_get_attachment_url( $postmeta );
    restore_current_blog();

    return $attachment_url;
}

function has_hero() {
    //print_r(wp_load_alloptions());
    if ( is_archive() ) {
        $archive = get_queried_object();
        if ( get_option("_sfmu_{$archive->name}_hero_image_id", '') ) return true;

    } elseif ( is_404() ) {
        if ( get_option("_sfmu_404_hero_image_id", '') ) return true;
    } elseif ( is_search() ) {
        if ( get_option("_sfmu_search_hero_image_id", '') ) return true;
    } elseif ( is_single() ) {
        global $post;

        if ( is_singular('location') ) {
            if ( $post->post_parent > 0 ) {
                if ( get_option("_sfmu_city_hero_image_id", '') ) return true;
            } else {
                if ( get_option("_sfmu_state_hero_image_id", '') ) return true;
            }
        } else if ( is_singular('stellar_event') || is_singular('s8_staff_member') || is_singular('ff_news') ) {
            $cpt = get_post_type($post->ID);
            if ( get_option("_sfmu_".$cpt."_hero_image_id", '') ) return true;
        } else {
            if ( get_post_meta($post->ID, '_hero_image_id', true) ) {
                 return true;
            } else {
                $attachment_id = get_option('site_header_image', '');
                if ( $attachment_id ) {
                    return true;
                } else {
                    if ( is_main_site() === FALSE ) {
                        switch_to_blog(1);
                        $attachment_id = get_option('site_header_image', '');
                        restore_current_blog();

                        if ($attachment_id) return true;
                    }
                }
            }
        }
    } elseif ( is_page() ) {
        global $post;
        if ( get_post_meta($post->ID, '_hero_image_id', true) ) {
             return true;
        } else {
            $attachment_id = get_option('site_header_image', '');
            if ( $attachment_id ) {
                return true;
            } else {
                if ( is_main_site() === FALSE ) {
                    switch_to_blog(1);
                    $attachment_id = get_option('site_header_image', '');
                    restore_current_blog();

                    if ($attachment_id) return true;
                }
            }
        }
    }

    return false;
}

function get_hero_banner( $location = 'null') {
    if ( is_archive() ) {
        $archive = get_queried_object();

        if ( $attachment_id = get_option("_sfmu_{$archive->name}_hero_image_id", '') )  {
            $hero = wp_get_attachment_image($attachment_id, 'full');
        }

    } elseif ( is_404() ) {
            if ( $attachment_id = get_option("_sfmu_404_hero_image_id", '') ) $hero = wp_get_attachment_image($attachment_id, 'full');
    } elseif ( is_single() ) {
        global $post;

        if ( is_singular('location') ) {
            if ( $post->post_parent > 0 ) {
                if ( $attachment_id = get_option("_sfmu_city_hero_image_id", '') ) $hero = wp_get_attachment_image($attachment_id, 'full');
            } else {
                if ( $attachment_id = get_option("_sfmu_state_hero_image_id", '') ) $hero = wp_get_attachment_image($attachment_id, 'full');
            }
        } else if ( is_singular('stellar_event') || is_singular('s8_staff_member') || is_singular('ff_news')) {
               $cpt = get_post_type($post->ID);
               if ( $attachment_id = get_option("_sfmu_".$cpt."_hero_image_id", '') ) $hero = wp_get_attachment_image($attachment_id, 'full');
        } else {
            if ( $attachment_id = get_post_meta($post->ID, '_hero_image_id', true) ) {
                 $hero = wp_get_attachment_image($attachment_id, 'full');
            } else {
                $attachment_id = get_option('site_header_image', '');
                if ( $attachment_id ) {
                    $hero = wp_get_attachment_image($attachment_id, 'full');
                } else {
                    if ( is_main_site() === FALSE ) {
                        switch_to_blog(1);
                        $attachment_id = get_option('site_header_image', '');
                        restore_current_blog();

                        if ($attachment_id) $hero = wp_get_attachment_image($attachment_id, 'full');
                    }
                }
            }
        }
    } elseif ( is_page() ) {
        global $post;

        if ( $attachment_id = get_post_meta($post->ID, '_hero_image_id', true) ) {
             $hero = wp_get_attachment_image($attachment_id, 'full');
        } else {
            $attachment_id = get_option('site_header_image', '');
            if ( $attachment_id ) {
                $hero = wp_get_attachment_image($attachment_id, 'full');
            } else {
                if ( is_main_site() === FALSE ) {
                    switch_to_blog(1);
                    $attachment_id = get_option('site_header_image', '');
                    restore_current_blog();

                    if ($attachment_id) $hero = wp_get_attachment_image($attachment_id, 'full');
                }
            }
        }
    }
    return str_replace("https://goldfishswimschool.com","https://www.goldfishswimschool.com", $hero);
}
