<?php

function locate_script($script_names, $load = false, $require_once = true ) {
	$located = '';
	foreach ( (array) $script_names as $script_name ) {
		if ( !$script_name )
			continue;
		if ( file_exists(SFL_MICROSITES_DIR . $script_name)) {
			$located = SFL_MICROSITES_DIR . $script_name;
			break;
		} elseif ( file_exists(SFL_MICROSITES_DIR . $script_name) ) {
			$located = SFL_MICROSITES_DIR . $script_name;
			break;
		}
	}

	if ( $load && $located !== '' )
		load_script( $located, $require_once );

	return $located;
}

function load_script( $_script_file, $require_once = true ) {
	global $posts, $post, $wp_did_header, $wp_query, $wp_rewrite, $wpdb, $wp_version, $wp, $id, $comment, $user_ID;

	if ( isset($wp_query->query_vars) && is_array( $wp_query->query_vars ) ) {
		extract( $wp_query->query_vars, EXTR_SKIP );
	}

	if ( isset( $s ) ) {
		$s = esc_attr( $s );
	}

	if ( $require_once ) {
		require_once( $_script_file );
	} else {
		require( $_script_file );
	}
}
