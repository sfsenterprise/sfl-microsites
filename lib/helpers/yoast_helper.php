<?php

function yoast_seo_title($post_id)
{
	$fixed_title = get_post_meta('yoast_wpseo_title', true);

	if ( !empty($fixed_title) ) {
		return $fixed_title;
	}

	if ( ! function_exists('wpseo_replace_vars') ) return;

	$post    = get_post( $post_id );
	$options = get_option( 'wpseo_titles' );
	if ( is_object( $post ) && ( isset( $options[ 'title-' . $post->post_type ] ) && $options[ 'title-' . $post->post_type ] !== '' ) ) {
		$title_template = $options[ 'title-' . $post->post_type ];
		$title_template = str_replace( ' %%page%% ', ' ', $title_template );

		return wpseo_replace_vars( $title_template, $post );
	}

	return wpseo_replace_vars( '%%title%%', $post );
}

function yoast_seo_description( $post_id, $post_type ) {
    $meta_description = get_post_meta( 'yoast_wpseo_metadesc' );
    if ( !empty($meta_description) ) {
        return $meta_description;
    }

    $test = new WPSEO_Recalculate_Posts();
    $default_from_options = $test->default_from_options( 'metadesc', $post_type );

    if ( false !== $default_from_options ) {
        return $default_from_options;
    }

    return '';
}
