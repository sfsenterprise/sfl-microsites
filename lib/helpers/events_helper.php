<?php

function get_sfmu_events($year = false, $month = false)
{
    if ( ! $year || ! $month ) {
        $limit = 3;

        $yearmonth = current_time('mysql');

        $year = date('Y', strtotime($yearmonth));
        $month = date('m', strtotime($yearmonth));

        $date = date('Y-m-d H:i:s', strtotime($year . '-' . $month));

    } else {
        $limit = -1;
        $date = date('Y-m-d H:i:s', strtotime($year . '-' . $month));
    }

    $months = array(
        date('Y-m', strtotime($date)),
        date('Y-m', strtotime("+1 month", strtotime($date))),
        date('Y-m', strtotime("+2 month", strtotime($date))),
        date('Y-m', strtotime("+3 month", strtotime($date))),
        date('Y-m', strtotime("+4 month", strtotime($date))),
        date('Y-m', strtotime("+5 month", strtotime($date))),
    );

    $today = date('Y-m-d');

    $query = new \WP_Query([
        'post_type' => 'stellar_event',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'meta_query' => array(
            array(
                'key' => '_gss_post_hidden',
                'value' => 1,
                'compare' => '!=',
            )
        ),
    ]);
    wp_reset_query();

    if ( ! $query->have_posts() ) return;

    $events = array(
        $months[0] => array(),
        $months[1] => array(),
        $months[2] => array(),
        $months[3] => array(),
        $months[4] => array(),
        $months[5] => array(),
    );
    foreach( $query->posts as $post ) {
        $repeating = get_post_meta($post->ID, '_stellar_event_repeats', true) ?: 'never';
        $start = date('Y-m-d H:i:s', strtotime(get_post_meta($post->ID, '_stellar_event_startDate', true)));
        $end = date('Y-m-d H:i:s', strtotime(get_post_meta($post->ID, '_stellar_event_endDate', true)));

        if ( $repeating !== 'never' ) {

            $repeating_ends = date('Y-m-d H:i:s', strtotime(get_post_meta($post->ID, '_stellar_event_repeating_ends_on', true)));
            $repeating_ends = ($end > $repeating_ends) ? $end : $repeating_ends;

            $repeating_every = get_post_meta($post->ID, '_stellar_event_repeat_every', true);
            $repeating_on = get_post_meta($post->ID, '_stellar_event_repeat_on', true);

            if ( $repeating_ends >= current_time('mysql') ) {
                $post->event = array(
                    'start' => $start,
                    'end' => $end,
                    'repeating' => $repeating,
                    'repeating_every' => $repeating_every,
                    'repeating_on' => $repeating_on,
                    'repeating_ends' => $repeating_ends,
                );

                switch($repeating) {
                    case "daily": {
                        // Do daily
                        $current_date = date('Y-m-d', strtotime(current_time('mysql')));
                        foreach ( $months as $month ) {
                            if ( date('Y-m-d', strtotime($month . '-01')) <= $repeating_ends ) {

                                $end_date = date('Y-m-t', strtotime($month .'-01'));
                                $days = date_diff(date_create($month .'-01'), date_create($end_date));
                                $days = $days->days ? $days->days + 1 : 0;

                                for($i=1;$i<=$days;$i++) {

                                    if ( $month.'-'.sprintf('%02d', $i) <= date("Y-m-d", strtotime($repeating_ends)) ) {

                                        if ( date("Y-m-d", strtotime($start)) <= strtotime($month.'-'.sprintf('%02d', $i)) ) {
                                            if ( date('Y-m-d', strtotime($start)) <= $month.'-'.sprintf('%02d', $i) ) {
                                                if ( $month === date('Y-m', strtotime(current_time('mysql'))) && $i >= absint(date('d', strtotime(current_time('mysql')))) ) {
                                                    $events[$month][$month.'-'.sprintf('%02d', $i)][] = $post;
                                                } else {
                                                    $events[$month][$month.'-'.sprintf('%02d', $i)][] = $post;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        break;

                    }
                    case "weekly": {
                        $repeating_on = array();
                        if ( is_array($post->event['repeating_on']) ) {
                            foreach ( $post->event['repeating_on'] as $name ) {
                                if ( $name === 'Monday' ) {
                                    $repeating_on[1] = $name;
                                } else if ( $name === 'Tuesday' ) {
                                    $repeating_on[2] = $name;
                                } else if ( $name === 'Wednesday' ) {
                                    $repeating_on[3] = $name;
                                } else if ( $name === 'Thursday' ) {
                                    $repeating_on[4] = $name;
                                } else if ( $name === 'Friday' ) {
                                    $repeating_on[5] = $name;
                                } else if ( $name === 'Saturday' ) {
                                    $repeating_on[6] = $name;
                                } else if ( $name === 'Sunday' ) {
                                    $repeating_on[0] = $name;
                                }
                            }
                            $post->event['repeating_on'] = $repeating_on;
                        }

                        foreach ( $months as $monthly ) {
                            $days_in_month = sfmu_days_in_month($monthly);
                            $diff = date_diff(date_create($monthly .'-01'), date_create($monthly .'-'.$days_in_month));

                            for( $i=1;$i<=$diff->days;$i++ ) {
                                $current = $monthly .'-'.sprintf("%02d", $i);

                                if ( array_key_exists(sfmu_num_of_the_day($current), $repeating_on) ) {
                                    if ( $today <=  date('Y-m-d', strtotime($current) ) ) {
                                        if ( $repeating_ends > $current && substr($start,0,-9) <= $current ) {
                                            $events[$monthly][$current][] = $post;
                                        }
                                    }
                                }
                            }

                        }

                        break;
                    }
                    case "monthly": {
                        $monthly_current_month = date('Y-m-d', strtotime($start));
                        $monthly_current_month_end = date('Y-m-d', strtotime($end));
                        $monthly_current_day = date('Y-m-d', strtotime($start));

                        foreach( $months as $i => $month ) {
                            $jday =  $month.'-'.date('d', strtotime($start));    
                            $jday_actual =  date("$month-d", strtotime($start)); 

                            //if ( $month === $monthly_current_month ) {
                            //line above is wrong due to it must show months BETWEEN current month vs. start/end months --mamah J
                            if ( $jday_actual >= $monthly_current_month && $jday_actual <= $monthly_current_month_end ) {
                                //$monthly_current_month = date('Y-m', strtotime(($i+1) . " month", strtotime($start)));
                                //$monthly_current_day = date('Y-m-d', strtotime(($i+1) . " month", strtotime($start)));
                                //$events[$monthly_current_month][$monthly_current_day][] = $post;
                                //So what's the point in all of this? When you will never change. The days have pass, The weather's changed. Should I be sorry? Could I be sorry? --mamah J
                                $events[$month][$jday][] = $post;
                            }

                            //$monthly_current_month = date('Y-m', strtotime(($i+1) . " month", strtotime($start)));
                            //$monthly_current_day = date('Y-m-d', strtotime(($i+1) . " month", strtotime($start)));
                            //So what's the point in all of this? When you will never change. The days have pass, The weather's changed. Should I be sorry? Could I be sorry? --mamah J
                        }
                        break;
                    }
                    case "yearly": {
                        $yearly_current_month = date('Y-m', strtotime($start));
                        $yearly_current_day = date('Y-m-d', strtotime($start));
                        $yearly_month =  date('Y-m', strtotime("+1 year", strtotime($start)));
                        $yearly_day = date('Y-m-d', strtotime("+1 year", strtotime($start)));

                        if ( in_array($yearly_current_month, $months) ) {
                            $events[$yearly_current_month][$yearly_current_day][] = $post;
                        } else if ( in_array($yearly_month, $months) ) {
                            $events[$yearly_month][$yearly_day][] = $post;
                        }
                        break;
                    }
                }
            }

        } else {
            if ( $end >= current_time('mysql') ) {

                $post->event = array(
                    'start' => $start,
                    'end' => $end,
                    'repeating' => '',
                    'repeating_every' => '',
                    'repeating_on' => '',
                    'repeating_ends' => '',
                );
                $events[date('Y-m', strtotime($start))][date('Y-m-d', strtotime($start))][] = $post;
            }
        }
    }

    ksort($events);
    foreach($events as $date => $event) {
        ksort($event);
        foreach( $event as $row => $items ) {
            if ( count( $items ) > 1 ) {
                usort($items, function( $a, $b ) {
                    $t1 = date('H:i:s', strtotime($a->event['start']));
                    $t2 = date('H:i:s', strtotime($b->event['start']));

                    return $t1 - $t2;
                });
                $events[$date][$row] = $items;
            } else {
                $events[$date] = $event;
            }
        }
    }

    if ( $limit === 3 ) {
        if ( $events ) {
            $events = array_reduce($events, 'array_merge', array());

            return $events;
        } else {
            return $events;
        }
    } else {
        return $events;
    }
}

function sfmu_days_in_month($date)
{
    return cal_days_in_month(CAL_GREGORIAN, date('m', strtotime($date)), date('Y', strtotime($date)));
}

function sfmu_num_of_the_day($date)
{
    return date('w', strtotime($date));
}
