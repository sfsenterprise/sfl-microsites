<?php

namespace Surefire\Microsites\Lib;

add_shortcode('microsite', [Shortcodes::class, 'microsite_shortcode']);
add_shortcode('franchise_location_name', 'get_microsite_name');
add_shortcode('franchise_location_full_address', 'get_microsite_address');
add_shortcode('franchise_phone_number', 'get_microsite_phone');

add_shortcode('ff_hours', function( $atts ) {
    if ( $atts['id'] === 'hours_operation' ) {
        return do_shortcode("[sidebar operation \"Hours of Operation\"]");
    } else if ( $atts['id'] === 'family_swim_hours' ) {
        return do_shortcode("[sidebar family \"Family Swim Hours\"]");
    } else if ( $atts['id'] === 'swim_team_hours' ) {
        return do_shortcode("[sidebar team \"Swim Force Hours\"]");;
    } else if ( $atts['id'] === 'party_hours' ) {
        return do_shortcode("[sidebar party \"Party Hours\"]");
    }
});

add_action('init', function() {
    if ( is_main_site() !== FALSE )
        return;

    remove_shortcode('contact-form-7');
    remove_shortcode('contact-form');

    add_shortcode('contact-form-7', [Shortcodes::class, 'contact_form_7_override']);
    add_shortcode('contact-form', [Shortcodes::class, 'contact_form_7_override'] );
});

/**
 *
 */
class Shortcodes
{

    public static function microsite_shortcode( $atts )
    {
        if ( ! is_array($atts) ) return;

        $field = $atts[0];

        return call_user_func('get_microsite_' . $field);
    }

    public static function contact_form_7_override($atts, $content = null, $code = '')
    {
        if ( is_feed() ) {
    		return '[contact-form-7]';
    	}

    	if ( 'contact-form-7' === $code ) {
    		$atts = shortcode_atts(
    			array(
    				'id' => 0,
    				'title' => '',
    				'html_id' => '',
    				'html_name' => '',
    				'html_class' => '',
    				'output' => 'form',
    			),
    			$atts, 'wpcf7'
    		);

    		$id = (int) $atts['id'];
    		$title = trim( $atts['title'] );

            switch_to_blog(1);
    		if ( ! $contact_form = wpcf7_contact_form( $id ) ) {
    			$contact_form = wpcf7_get_contact_form_by_title( $title );
    		}
            restore_current_blog();

    	} else {
    		if ( is_string( $atts ) ) {
    			$atts = explode( ' ', $atts, 2 );
    		}

    		$id = (int) array_shift( $atts );
            switch_to_blog(1);
    		$contact_form = wpcf7_get_contact_form_by_old_id( $id );
            restore_current_blog();
    	}

    	if ( ! $contact_form ) {
    		return '[contact-form-7 404 "Not Found"]';
    	}
        $atts['blog_id'] = get_current_blog_id();

    	return $contact_form->form_html( $atts );
    }
}
