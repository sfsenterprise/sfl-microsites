<?php

/**
* Franchisee Roles and Capabilities for single site
*/

$ZEE['edit_users'] = true;
$ZEE['edit_files'] = true;
$ZEE['manage_options'] = true;
$ZEE['moderate_comments'] = true;
$ZEE['manage_categories'] = true;
$ZEE['manage_links'] = true;
$ZEE['upload_files'] = true;
$ZEE['import'] = true;
$ZEE['unfiltered_html'] = true;
$ZEE['edit_posts'] = true;
$ZEE['edit_others_posts'] = true;
$ZEE['edit_published_posts'] = true;
$ZEE['publish_posts'] = true;
$ZEE['edit_pages'] = true;
$ZEE['read'] = true;
$ZEE['level_9'] = true;
$ZEE['level_8'] = true;
$ZEE['level_7'] = true;
$ZEE['level_6'] = true;
$ZEE['level_5'] = true;
$ZEE['level_4'] = true;
$ZEE['level_3'] = true;
$ZEE['level_2'] = true;
$ZEE['level_1'] = true;
$ZEE['level_0'] = true;
$ZEE['edit_others_pages'] = true;
$ZEE['edit_published_pages'] = true;
$ZEE['publish_pages'] = true;
$ZEE['delete_pages'] = true;
$ZEE['delete_others_pages'] = true;
$ZEE['delete_published_pages'] = true;
$ZEE['delete_posts'] = true;
$ZEE['delete_others_posts'] = true;
$ZEE['delete_published_posts'] = true;
$ZEE['delete_private_posts'] = true;
$ZEE['edit_private_posts'] = true;
$ZEE['read_private_posts'] = true;
$ZEE['delete_private_pages'] = true;
$ZEE['edit_private_pages'] = true;
$ZEE['read_private_pages'] = true;
$ZEE['delete_users'] = true;
$ZEE['create_users'] = true;
$ZEE['unfiltered_upload'] = true;
$ZEE['edit_dashboard'] = true;
$ZEE['list_users'] = true;
$ZEE['remove_users'] = true;
$ZEE['promote_users'] = true;
$ZEE['edit_theme_options'] = true;
$ZEE['export'] = true;
$ZEE['wpseo_bulk_edit'] = true;
$ZEE['franchisee'] = true;

return $ZEE;
