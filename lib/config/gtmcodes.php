<?php

return array(
    30	=>	'GTM-WZH5BDC',
    49	=>	'GTM-TDT4992',
    59	=>	'GTM-NGLD3LD',
    3	=>	'GTM-KQ4M8DM',
    27	=>	'GTM-W6RMZN4',
    66	=>	'GTM-TBSN7NW',
    78	=>	'GTM-W9RWHGH',
    35	=>	'GTM-MVBHWB5',
    13	=>	'GTM-WBRDCPL',
    19	=>	'GTM-NDV4MBR',
    4	=>	'GTM-NZC8NMT',
    70	=>	'GTM-TPZV4WN',
    5	=>	'GTM-5GW4HMT',
    6	=>	'GTM-WWZR5SW',
    22	=>	'GTM-PLCV7PF',
    33	=>	'GTM-KBQ96MH',
    7	=>	'GTM-MQPGB2F',
    47	=>	'GTM-M24W5P8',
    44	=>	'GTM-KJNCXTV',
    24	=>	'GTM-TL4FWXD',
    32	=>	'GTM-PGWKFVN',
    58	=>	'GTM-KM2NNGK',
    85	=>	'GTM-N7JKM5N',
    8	=>	'GTM-P2N8HTW',
    20	=>	'GTM-PXKBH5M',
    23	=>	'GTM-M36TNJ9',
    37	=>	'GTM-PMSMVDZ',
    46	=>	'GTM-5696RLB',
    9	=>	'GTM-P3MFBBH',
    40	=>	'GTM-5QKWVZP',
    61	=>	'GTM-WDD8LW6',
    43	=>	'GTM-5DKJVPC',
    51	=>	'GTM-N9XS3PB',
    11	=>	'GTM-MVX4DRR',
    77	=>	'GTM-5B8GJXC',
    62	=>	'GTM-PFVXJDC',
    74	=>	'GTM-PWPQ2VK',
    38	=>	'GTM-T9TGK2X',
    26	=>	'GTM-NT7RBFH',
    14	=>	'GTM-MR2PNJN',
    21	=>	'GTM-W7R2HRL',
    60	=>	'GTM-TRWTLVT',
    83	=>	'GTM-NNTCBJH',
    48	=>	'GTM-M3T5FZ4',
    75	=>	'GTM-542WXR9',
    64	=>	'GTM-WW86LTL',
    15	=>	'GTM-PM4WWXJ',
    39	=>	'GTM-PTVR76P',
    76	=>	'GTM-KXZRJJ9',
    42	=>	'GTM-WD2Q4GK',
    28	=>	'GTM-576DJ3S',
    16	=>	'GTM-KDVDZCH',
    67	=>	'GTM-5PLCTLK',
    12	=>	'GTM-PP8THXF',
    53	=>	'GTM-M2Z4TCJ',
    17	=>	'GTM-NHGF247',
    50	=>	'GTM-N6ZVBWZ',
    18	=>	'GTM-PQJLGV4',
    52	=>	'GTM-M78G3QK',
    73	=>	'GTM-59VPMKQ',
    41	=>	'GTM-MKF5XPB',
    34	=>	'GTM-KPQG7HS',
    29	=>	'GTM-NS2NXCJ',
    10	=>	'GTM-W5R92F7',
    36	=>	'GTM-WFTSCQT',
    45	=>	'GTM-MVMXCDV',
    25	=>	'GTM-5BJQ58D'
);
