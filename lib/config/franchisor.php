<?php

/**
* Franchisor Roles and Capabilities
*/

// Network Site
$ZOR['create_sites'] = true;
$ZOR['delete_sites'] = true;
$ZOR['manage_network'] = true;
$ZOR['manage_sites'] = true;
$ZOR['manage_network_users'] = true;
$ZOR['manage_network_plugins'] = true;
$ZOR['manage_network_themes'] = true;
$ZOR['manage_network_options'] = true; // Removed to keep the integrity of netwrork settings. can be enabled if needed.

$ZOR['switch_themes'] = true;
$ZOR['edit_themes'] = true;
$ZOR['activate_plugins'] = true;
$ZOR['edit_plugins'] = true;
$ZOR['edit_users'] = true;
$ZOR['edit_files'] = true;
$ZOR['manage_options'] = true;
$ZOR['moderate_comments'] = true;
$ZOR['manage_categories'] = true;
$ZOR['manage_links'] = true;
$ZOR['upload_files'] = true;
$ZOR['import'] = true;
$ZOR['unfiltered_html'] = true;
$ZOR['edit_posts'] = true;
$ZOR['edit_others_posts'] = true;
$ZOR['edit_published_posts'] = true;
$ZOR['publish_posts'] = true;
$ZOR['edit_pages'] = true;
$ZOR['read'] = true;
$ZOR['level_9'] = true;
$ZOR['level_8'] = true;
$ZOR['level_7'] = true;
$ZOR['level_6'] = true;
$ZOR['level_5'] = true;
$ZOR['level_4'] = true;
$ZOR['level_3'] = true;
$ZOR['level_2'] = true;
$ZOR['level_1'] = true;
$ZOR['level_0'] = true;
$ZOR['edit_others_pages'] = true;
$ZOR['edit_published_pages'] = true;
$ZOR['publish_pages'] = true;
$ZOR['delete_pages'] = true;
$ZOR['delete_others_pages'] = true;
$ZOR['delete_published_pages'] = true;
$ZOR['delete_posts'] = true;
$ZOR['delete_others_posts'] = true;
$ZOR['delete_published_posts'] = true;
$ZOR['delete_private_posts'] = true;
$ZOR['edit_private_posts'] = true;
$ZOR['read_private_posts'] = true;
$ZOR['delete_private_pages'] = true;
$ZOR['edit_private_pages'] = true;
$ZOR['read_private_pages'] = true;
$ZOR['delete_users'] = true;
$ZOR['create_users'] = true;
$ZOR['unfiltered_upload'] = true;
$ZOR['edit_dashboard'] = true;
$ZOR['update_plugins'] = true;
$ZOR['delete_plugins'] = true;
$ZOR['install_plugins'] = true;
$ZOR['update_themes'] = true;
$ZOR['install_themes'] = true;
$ZOR['update_core'] = true;
$ZOR['list_users'] = true;
$ZOR['remove_users'] = true;
$ZOR['promote_users'] = true;
$ZOR['edit_theme_options'] = true;
$ZOR['delete_themes'] = true;
$ZOR['export'] = true;
$ZOR['wpseo_bulk_edit'] = true;
$ZOR['franchisor'] = true;

return $ZOR;
