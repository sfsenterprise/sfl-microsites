<?php

/**
* Franchisee Roles and Capabilities
*/

$ZEE['read'] = true;
$ZEE['franchisee'] = true;
$ZEE['upload_files'] = true;
$ZEE['edit_dashboard'] = true;

return $ZEE;
