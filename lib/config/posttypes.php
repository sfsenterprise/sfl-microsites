<?php

return [
    'sidebars' => 2, // global
    'swim_lessons' => 2, // global
    'forms' => 2, // global
    'testimonials' => 1, // parent only
    'ff_faqs' => 2, // global
    'stellar_event' => 3, // microsite
    'ff_news' => 3, // microsite
    'ff_job_listings' => 3, // microsite
    's8_staff_member' => 3, //microsite
    'announcements' => 3, // microsite
    'ff_promos' => 3, // microsite
    'post' => 1,
];
