<?php
namespace Surefire\Microsites\Lib;

/**
 *
 */
class Bootstrap
{
    /**
    * Load the CodeIgniter framework inside WordPress
    * using the WordPress template_include filter.
    * Application is loaded under the page "surefire".
    * @param $template string
    */
    public static function app_init($template) {
        $page = get_queried_object();

        if ( ! is_admin() && isset($page->post_name) && $page->post_name === 'surefire' ) {
            $template = SFL_MICROSITES_DIR . 'microsites/index.php';
            define('SFMU', true);
        }
        return $template;
    }

    /**
    * Always load the app under "surefire" url segment.
    * Removes auto redirection from 404 or not found page.
    * @param $query object
    */
    public static function app_loader($query) {
        if ( ! preg_match('/^surefire?\//i', $query->request) ) {
            return $query;
        }

        if ( isset($query->query_vars['attachment']) ) {
            unset($query->query_vars['attachment']);
        }

        if ( isset($query->query_vars['error']) ) {
            unset($query->query_vars['error']);
        }

        $query->set_query_var('page', "");
        $query->set_query_var('pagename', "surefire");
        $query->matched_rule = "(.?.+?)(?:/([0-9]+))?/?$";
        $query->matched_query = "pagename=surefire&amp;page=";

        return $query;
    }

    public static function exclude_dashboard_action($query) {
        if ( ! $query->is_main_query() ) {
            global $wpdb;

            $panel = $wpdb->get_var("SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = '_sfmu_panel'");

            $post__no_in = $query->get('post__not_in');
            $post__no_in[] = $panel;
            $query->set('post__not_in', $post__no_in);
        }

        return $query;
    }

    public static function create_dashboard_placeholder()
    {
        global $wpdb;

        $panel = $wpdb->get_var("SELECT ID FROM {$wpdb->posts} WHERE post_name = 'surefire' AND post_password = 'surefire' LIMIT 1");
        if ( ! $panel ) {

            $panel = wp_insert_post([
                'post_type' => 'page',
                'post_title' => 'Surefire',
                'post_name' => 'surefire',
                'post_status' => 'publish',
                'post_password' => 'surefire',
                'ping_status' => 'closed'
            ]);

            update_post_meta($panel, '_sfmu_panel', '1');

        }

        if ( ! $noindex = get_post_meta($panel, '_yoast_wpseo_meta-robots-noindex', true) ) {
            update_post_meta($panel, '_yoast_wpseo_meta-robots-noindex', '1');
        }

        if ( ! $nofollow = get_post_meta($panel, '_yoast_wpseo_meta-robots-nofollow', true) ) {
            update_post_meta($panel, '_yoast_wpseo_meta-robots-nofollow', '1');
        }

        if ( ! $sfmu_panel = get_post_meta($panel, '_sfmu_panel', true) ) {
            update_post_meta($panel, '_sfmu_panel', '1');
        }

    }

    public static function remove_admin_bar()
    {
        return false;
        // return ( is_super_admin() ) ? true : false;
    }

    public static function wp_admin_redirect()
    {
        global $wpdb;
        $panel = $wpdb->get_var("SELECT post_id FROM wp_postmeta WHERE meta_key = '_sfmu_panel'");

        if ( !$panel ) {
            return;
        }

        if ( 'wp-login.php' == $GLOBALS['pagenow'] && ( is_super_admin() === FALSE && ! current_user_can('franchisor') ) ) {
            switch_to_blog(1);
            wp_redirect( get_permalink(absint($panel)) );
            restore_current_blog();
            exit;
        } elseif ( preg_match('/wp-admin/', $GLOBALS['PHP_SELF']) && ( is_super_admin() === FALSE && ! current_user_can('franchisor') ) ) {

            if ( defined('DOING_AJAX') && DOING_AJAX) return;

            switch_to_blog(1);
            wp_redirect( get_permalink(absint($panel)) );
            restore_current_blog();
            exit;
        }
    }

    public static function surefire_login_page()
    {
        global $wpdb;
        $panel = $wpdb->get_var("SELECT post_id FROM {$wpdb->prefix}postmeta WHERE meta_key = '_sfmu_panel'");

        if ( !$panel ) {
            return;
        }

        switch_to_blog(1);
        $loginpage = get_permalink(absint($panel));
        restore_current_blog();
        return $loginpage;
    }

    public static function session_destroy()
    {
        if ( isset($_SESSION) )
            session_destroy();
    }

    public static function exclude_main_site_below_franchisor($sites, $user_id)
    {
        if ( ! user_can($user_id, 'franchisor') ) {
            foreach($sites as $row => $site) {
                if ( absint($site->userblog_id) === 1 ) {
                    unset($sites[$row]);
                }
            }
        }

        return $sites;
    }

    public static function _sfmu_global_pages()
    {
        global $wp_query;
        if ( is_main_site() !== FALSE && !defined('SFMU') ) return;

        if ( ! $wp_query->is_main_query() ) return;

        global $wpdb;
        switch_to_blog(1);
        $queryglobal = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}posts AS p LEFT JOIN {$wpdb->prefix}postmeta as m ON p.ID = m.post_id WHERE m.meta_key = '_sfmu_default_page' AND m.meta_value = 'yes' ORDER by p.post_date ASC");
        restore_current_blog();
        $querylocal = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}posts AS p LEFT JOIN {$wpdb->prefix}postmeta as m ON p.ID = m.post_id WHERE m.meta_key = '_sfmu_parent_id' AND m.meta_value IS NOT NULL ORDER by p.post_date ASC");

        $globals = array();
        $locals = array();

        foreach($queryglobal as $globalpage) {
            $globals[$globalpage->ID] = $globalpage;
        }

        foreach($querylocal as $localpage) {
            $locals[$localpage->meta_value] = $localpage;
        }

        $newpages = array_diff(array_keys($globals), array_keys($locals));

        if ( count($newpages) > 0 ) {
            $newlocals = array();
            foreach($newpages as $newpage) {
                $args = array(
                    'post_title' => $globals[$newpage]->post_title,
                    'post_status' => $globals[$newpage]->post_status,
                    'post_date' => $globals[$newpage]->post_date,
                    'post_modified' => $globals[$newpage]->post_modified,
                    'comment_status' => $globals[$newpage]->comment_status,
                    'ping_status' => $globals[$newpage]->ping_status,
                    'post_password' => $globals[$newpage]->post_password,
                    'post_name' => $globals[$newpage]->post_name,
                    'to_ping' => $globals[$newpage]->to_ping,
                    'pinged' => $globals[$newpage]->pinged,
                    'menu_order' => $globals[$newpage]->menu_order,
                    'post_type' => $globals[$newpage]->post_type
                );

                if ( $globals[$newpage]->post_parent > 0 ) {
                    if ( isset($locals[$globals[$newpage]->post_parent]) ) {
                        $args['post_parent'] = $locals[$globals[$newpage]->post_parent]->ID;
                    } elseif ( isset($newlocals[$newpage]) ) {
                        $args['post_parent'] = $newlocals[$newpage]->ID;
                    }
                }

                $post_type = $globals[$newpage]->post_type;
                $post_name = $globals[$newpage]->post_name;
                $localpost = $wpdb->get_row("SELECT * FROM {$wpdb->posts} WHERE post_type = '{$post_type}' AND post_name = '{$post_name}'");

                if ( $localpost ) {
                    update_post_meta($localpost->ID, '_sfmu_parent_id', $newpage);
                } else {
                    $newlocals[$newpage] = wp_insert_post($args);
                    update_post_meta($newlocals[$newpage], '_sfmu_parent_id', $globals[$newpage]->ID);
                }
            }
        }
    }

    public static function _sfmu_update_global_page($query)
    {
        if ( is_main_site() !== FALSE ) return $query;

        global $wp, $wpdb;

        if ( $query->is_404() ) {
            switch_to_blog(1);
            $global = get_page_by_path($wp->request, OBJECT, 'page');
            restore_current_blog();
            if ( !$global ) return $query;

            $local = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}posts as p LEFT JOIN {$wpdb->prefix}postmeta as m ON p.ID = m.post_id WHERE m.meta_key = '_sfmu_parent_id' AND m.meta_value = '{$global->ID}'");

            if ( ! $local ) return $query;

        } else {
            if ( !isset($query->queried_object) ) return $query;

            if ( $query->queried_object->post_type !== 'page' ) return $query;

            $local = $query->queried_object;

            $global_id = get_post_meta($local->ID, '_sfmu_parent_id', true);
            switch_to_blog(1);
            $global = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}posts WHERE ID = {$global_id}");
            if ( $global && $is_parent = get_post_meta($global->ID, '_sfmu_default_page', true) !== 'yes') {
                $global = false;
            }
            restore_current_blog();

        }

        if ( ! $global ) {
            return $query;
            // wp_delete_post($local->ID);
            // wp_safe_redirect( $_SERVER['REQUEST_URI'], 301 ); exit;
        }

        if ( $global->post_modified !== $local->post_modified) {
            $args = array(
                'ID' => $local->ID,
                'post_title' => $global->post_title,
                'post_status' => $global->post_status,
                'post_date' => $global->post_date,
                'comment_status' => $global->comment_status,
                'ping_status' => $global->ping_status,
                'post_password' => $global->post_password,
                'post_name' => $global->post_name,
                'to_ping' => $global->to_ping,
                'pinged' => $global->pinged,
                'menu_order' => $global->menu_order,
                'post_type' => $global->post_type
            );

            if ( $global->post_parent !== get_post_meta($local->post_parent, '_sfmu_parent_id', true) ) {
                $parent = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts as p LEFT JOIN {$wpdb->prefix}postmeta as m ON p.ID = m.post_id WHERE m.meta_key = '_sfmu_parent_id' AND m.meta_value = '{$global->post_parent}'");
                $args['post_parent'] = $parent;
            } elseif ( $global->post_parent === 0 ) {
                $args['post_parent'] = 0;
            }

            wp_update_post($args);
            $wpdb->update( $wpdb->prefix ."posts", ['post_modified' => $global->post_modified, 'post_modified_gmt' => $global->post_modified], ['ID' => $local->ID] );

            wp_safe_redirect( $_SERVER['REQUEST_URI'], 301 ); exit;
        }
        return $query;
    }

    public static function _sfmu_update_global_form($query)
    {
        if ( is_main_site() !== FALSE ) return $query;

        if ( is_admin() ) return $query;

        global $wp, $wpdb;

        if ( $query->is_404() ) {
            switch_to_blog(1);
            $global = get_page_by_path($wp->request, OBJECT, 'ff_forms');
            restore_current_blog();
            if ( !$global ) return $query;

            $local = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}posts as p LEFT JOIN {$wpdb->prefix}postmeta as m ON p.ID = m.post_id WHERE m.meta_key = '_sfmu_parent_id' AND m.meta_value = '{$global->ID}'");

            if ( !$local ) return $query;
            return $query;
        } else {

            if ( $query->get('post_type') !== 'ff_forms' ) return $query;
            $name = $query->get('name');

            $local = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}posts WHERE post_name = '{$name}' AND post_type = 'ff_forms' AND post_status ='publish'");

            switch_to_blog(1);
            $global = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}posts WHERE post_name = '{$name}' AND post_type = 'ff_forms' AND post_status = 'publish'");
            if ( $global && $is_parent = get_post_meta($global->ID, '_sfmu_default_page', true) !== 'yes') {
                $global = false;
            }
            restore_current_blog();
        }

        if ( ! $global ) {
            return $query;
            // wp_delete_post($local->ID);
            // wp_safe_redirect( $_SERVER['REQUEST_URI'], 301 ); exit;
        }

        if ( $global->post_modified !== $local->post_modified) {
            $args = array(
                'ID' => $local->ID,
                'post_title' => $global->post_title,
                'post_status' => $global->post_status,
                'post_date' => $global->post_date,
                'comment_status' => $global->comment_status,
                'ping_status' => $global->ping_status,
                'post_password' => $global->post_password,
                'post_name' => $global->post_name,
                'to_ping' => $global->to_ping,
                'pinged' => $global->pinged,
                'menu_order' => $global->menu_order,
                'post_type' => $global->post_type
            );

            wp_update_post($args);
            $wpdb->update( $wpdb->prefix ."posts", ['post_modified' => $global->post_modified, 'post_modified_gmt' => $global->post_modified], ['ID' => $local->ID] );

            wp_safe_redirect( $_SERVER['REQUEST_URI'], 301 ); exit;
        }

        return $query;
    }
}
