<?php

namespace Surefire\Microsite\Lib\Filters;

/**
* Permanently hides the WP adminbar in the Surefire Dashboard
*/
add_filter( 'show_admin_bar', function() {
    global $sfl_microsite_config;

    if ( !is_page($sfl_microsite_config['dashboard_slug']) && is_user_logged_in() )
        return true;

    return false;
});

/**
* Set Surefire Dashboard title
*/
add_filter( 'pre_get_document_title', function() {
    global $sfl_microsite_config;

    if (!is_page($sfl_microsite_config['dashboard_slug']))
        return;

    return $sfl_microsite_config['dashboard_title'];
}, 100 );

/*
* Modify the permalink of microsite
*/
function sflm_remove_slugs( $post_link, $post, $leavename ) {
    global $sfl_microsite_config;

    if ( $post->post_type !== $sfl_microsite_config['post_type'] || 'publish' !== $post->post_status )
        return $post_link;

    if( isset($GLOBALS['wp_post_types'][$post->post_type], $GLOBALS['wp_post_types'][$post->post_type]->rewrite['slug']) ) {
        $slug = $GLOBALS['wp_post_types'][$post->post_type]->rewrite['slug'];
    } else {
        $slug = $post->post_type;
    }

    $post_link = str_replace( '/' . $slug . '/', '/', $post_link );
    $ancestors = get_ancestors( $post->ID, 'microsite' );

    if ( count($ancestors) > 1 ) {
        $state_province = (count($ancestors) > 2) ? get_post_field( 'post_name', $ancestors[2] ) : get_post_field( 'post_name', $ancestors[1] );
        $city = (count($ancestors) > 2) ? get_post_field( 'post_name', $ancestors[1] ) : get_post_field( 'post_name', $ancestors[0] );

        if ( $sfl_microsite_config['with_state_province'] === false && $sfl_microsite_config['with_city'] === false ) {
            $post_link = str_replace( '/' . $state_province . '/' . $city . '/', '/', $post_link );
        } else if ( $sfl_microsite_config['with_state_province'] && $sfl_microsite_config['with_city'] === false ) {
            $post_link = preg_replace( '/' . $state_province . '/' . $city . '/', '/' . $state_province . '/', $post_link );
        } else if ( $sfl_microsite_config['with_state_province'] === false && $sfl_microsite_config['with_city'] ) {
            $post_link = preg_replace( '/' . $state_province . '/' . $city . '/', '/' . $city . '/', $post_link );
        }
    }

    return $post_link;
}
add_filter( 'post_type_link', __NAMESPACE__ . '\\sflm_remove_slugs', 10, 3 );

function modiy_blog_post_link( $post_link, $post, $leavename ) {
    global $wpdb, $sfl_microsite_config;

    if ( 'post' !== get_post_type( $post ) )
        return $post_link;

    $microsite_categories = $wpdb->get_results("SELECT * FROM sfl_micrositeterms");

    if ( ! $microsite_categories )
        return $post_link;

    foreach($microsite_categories as $cat => $category) {
        if ( has_category($category->term_id, $post->ID) ) {
            $microsite_object = $wpdb->get_row("SELECT * FROM sfl_microsites WHERE ID = '{$category->microsite_id}'");
            return str_replace(home_url('/'), home_url('/') . @$microsite_object->microsite_slug . '/', $post_link);
        }
    }

    return '#';
}
add_filter('post_link', __NAMESPACE__ . '\\modiy_blog_post_link', 99, 3);

// Modify archive link
function modify_month_link( $monthlink, $year, $month ) {
    global $microsite_object;

    if ( ! $microsite_object )
        return '#';

    return str_replace(home_url('/blog'), home_url('/'. $microsite_object->microsite_slug .'/blog'), $monthlink);
}
add_filter( 'month_link', __NAMESPACE__ . '\\modify_month_link', 99, 3 );

// Modify archive link
function modify_year_link( $yearlink, $year ) {
    global $microsite_object;

    if ( ! $microsite_object )
        return '#';

    return str_replace(home_url('/blog'), home_url('/'. $microsite_object->microsite_slug .'/blog'), $yearlink);
}
add_filter( 'year_link', __NAMESPACE__ . '\\modify_year_link', 99, 3 );

function modify_archive_link( $link_html, $url, $text, $format, $before, $after ){
    global $microsite_object;

    if ( ! $microsite_object )
        return $link_html;

    if (is_day() || is_month() || is_year()) {
        if (is_day()) {
            $data = get_the_time('Y/m/d');
        } elseif (is_month()) {
            $data = get_the_time('Y/m');
        } elseif (is_year()) {
            $data = get_the_time('Y');
        }

        $link = get_permalink($microsite_object->microsite_post_id) .'blog/'.$data;

        $strpos = strpos($link_html, $link);

        if ($strpos !== false) {
            $link_html = str_replace('<li>', '<li class="active-archive">', $link_html);
        }

    }

    return $link_html;
}
add_filter('get_archives_link', __NAMESPACE__ . '\\modify_archive_link', 99, 6);


// Modify category link
function modify_cat_link( $url, $term, $taxonomy ) {
    global $microsite_object;

    if ( ! $microsite_object )
        return '#';

    return str_replace(home_url('/blog'), home_url('/'. $microsite_object->microsite_slug .'/blog'), $url);
}
add_filter( 'term_link', __NAMESPACE__ . '\\modify_cat_link', 99, 3 );

// Modify author link
function modify_author_link( $link ) {
    global $microsite_object;

    if ( ! $microsite_object )
        return '#';

    return str_replace(home_url('/blog'), home_url('/'. $microsite_object->microsite_slug .'/blog'), $link);
}
add_filter( 'author_link', __NAMESPACE__ . '\\modify_author_link', 99, 1 );

/**
* Modify the query to show blogposts on microsite blog
*/
add_filter('posts_where', function ($where) {
    global $wp, $wpdb, $microsite_object, $wp_query;

    if ( ! $microsite_object )
        return $where;

    $microsite_slug = explode('/', $wp->request);

    $cat = get_category_by_slug(microsite_cat());

    // Return the original where if there is no found category.
    if ( is_wp_error($cat) )
        return $where;

    if (
        (isset( $microsite_slug[1] ) && $microsite_slug[1] === 'blog' && count($microsite_slug) < 3) ||
        (isset( $microsite_slug[1] ) && $microsite_slug[1] === 'blog' && $microsite_slug[2] === 'page')
    ) {
        return "AND wp_posts.post_type = 'post' AND (wp_posts.post_status = 'publish' OR wp_posts.post_status = 'private') AND wp_term_relationships.term_taxonomy_id = '{$cat->term_id}'";
    } elseif ( isset( $microsite_slug[1] ) && $microsite_slug[1] === 'blog' && $microsite_slug[2] === date('Y') && $wp_query->is_main_query() ) {
        // return "AND wp_posts.post_type = 'post' AND (wp_posts.post_status = 'publish') AND wp_term_relationships.term_taxonomy_id = '{$cat->term_id}'";
    }
    return $where;
});

function modifiy_rewrite_rules( $rules ) {
    global $sfl_microsite_config;

    $microsite_rules = array();
    $current_rules = array();
    $current_rules['(.+?)/blog/page/?([0-9]{1,})/?$'] = 'index.php?'.$sfl_microsite_config['post_type'].'=$matches[1]/blog&post_type='.$sfl_microsite_config['post_type'].'&paged=$matches[2]';

    foreach ( $rules as $rule => $rewrite ) {
        if ( strpos( $rule, $sfl_microsite_config['post_type'] . '/' ) !== false ) {
            $key = str_replace( $sfl_microsite_config['slug'] . '/', '', $rule);
            $microsite_rules[$key] = $rewrite;
            unset($rules[$rule]);
        } else {
            // $current_rules[$rule] = $rewrite;
            if ( strpos($rule, 'blog/') !== false ) {
                $rewrite = str_replace('[2]', '[3]', $rewrite);
                $rewrite = str_replace('[1]', '[2]', $rewrite);
                $current_rules[str_replace('blog/', '(.+?)/blog/', $rule)] = $rewrite;
            } else {
                $current_rules[$rule] = $rewrite;
            }
        }
    }

    return array_merge( $current_rules, $microsite_rules );
}
add_filter( 'rewrite_rules_array', __NAMESPACE__ . '\\modifiy_rewrite_rules', 110, 1 );

function body_class( $classes ) {
    global $sfl_microsite_config;

    if ( ! is_page( $sfl_microsite_config['dashboard_slug'] ) )
        return $classes;

    global $wp;
    $requests = explode( '/', $wp->request );

    if ( ! isset( $requests[2]) )
        return $classes;

    unset($requests[0]);

    foreach( $requests as $request ) {
        $classes[] = 'surefire-' . $request;
    }

    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

function microsite_body_class( $classes ) {
    global $wp, $microsite_object, $post;

    if ( ! $microsite_object || ! $post )
        return $classes;

    $requests = explode( '/', $wp->request );

    $classes[] = 'microsite-' . $requests[0];
    if ( count($requests) > 1 ) {
        if ( $key = array_search($requests[1], $classes) ) {
            $classes[$key] = 'microsite-' . $requests[1];
        } else {
            if ( is_home() ) {
                $classes[] = 'microsite-blog';
            } else if ( is_category() ) {
                $classes[] = 'microsite-category';
            } else if ( is_archive() ) {
                $classes[] = 'microsite-archive';
            }
        }
    }

    if ( (int) $microsite_object->microsite_post_id === (int) $post->ID ) {
        $classes[] = 'microsite-home';
    }

    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\microsite_body_class', 99, 1);

add_filter('posts_request', function($request) {
    global $wp, $sfl_microsite_config;
    if ( isset($wp->query_vars['post_type']) && $wp->query_vars['post_type'] !== $sfl_microsite_config['post_type'] )
        return $request;

    $post_name = str_replace('/', '', $wp->request);
    $request = str_replace(" AND wp_posts.post_name = '{$post_name}'", '', $request);

    return $request;
}, 99, 1);

// Filters recent post by microsite
function filter_recent_post_widget( $widget_args ) {
    global $microsite_object;

    if ( ! $microsite_object )
        return false;

    $widget_args['category_name'] = $microsite_object->microsite_name . '_' . $microsite_object->ID;

    return $widget_args;

}
add_filter( 'widget_posts_args', __NAMESPACE__ . '\\filter_recent_post_widget', 10, 1);

function filter_recent_comments_by_microsite( $widget_args ) {
    global $microsite_object;

    if ( ! $microsite_object )
        $widget_args['status'] = 'no-comment';

    $r = new \WP_Query( array( 'category_name' => $microsite_object->microsite_name . '_' . $microsite_object->ID ) );

    if ( ! $r->have_posts() )
        $widget_args['status'] = 'no-comment';

    while ( $r->have_posts() ) {
        $r->the_post();
        $widget_args['post__in'][] = get_the_ID();
    }
    wp_reset_postdata();

    return $widget_args;
}
add_filter( 'widget_comments_args', __NAMESPACE__ . '\\filter_recent_comments_by_microsite', 10, 1);

// Archive filter by microsite
function filter_archive_where_by_microsite( $where, $r ) {
    global $microsite_object;

    if ( ! $microsite_object )
        return;

    $r = new \WP_Query( array( 'category_name' => microsite_cat() ) );

    if ( ! $r->have_posts() )
        return $where;

    $post_ids = array();
    while ( $r->have_posts() ) {
        $r->the_post();
        $post_ids[] = get_the_ID();
    }
    wp_reset_postdata();
    $post_ids = implode(',', $post_ids);

    $where = $where ." AND ID IN({$post_ids})";

    return $where;
}
add_filter( 'getarchives_where', __NAMESPACE__ . '\\filter_archive_where_by_microsite', 10, 2 );

// Exclude other microsite categories on categories
function filter_other_microsite_cats( $widget_args) {
    global $wpdb, $microsite_object;

    if ( ! $microsite_object )
        $widget_args['taxonomy'] = 'no-cats';

    $microsite_terms = $wpdb->get_results("SELECT term_id FROM sfl_micrositeterms", 'ARRAY_N');
    $widget_args['exclude'] = array_reduce($microsite_terms, 'array_merge', array());

    return $widget_args;
}
add_filter( 'widget_categories_args', __NAMESPACE__ . '\\filter_other_microsite_cats', 10, 1);

//Removes wordpress guessing redirect
add_filter('redirect_canonical', function($redirect_url) {
    if ( is_404() ) {
        return false;
    }

    return $redirect_url;
}, 10, 1);

// Removes admin bar in front end
add_filter('show_admin_bar', '__return_false');