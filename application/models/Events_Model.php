<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function save($eventarr, $blog_id, $update = false)
    {
        $post_title = $eventarr['post_title'];
        $post_content = $eventarr['post_content'];
        $hide_event = $eventarr['post_status'] ?: 'publish';

    }

    public function gss_events($arr, $id, $update=false)
    {

        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_event = array(
                'post_title'      => $arr['event_title'],
                'post_content'    => $arr['event_content'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 'stellar_event',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $event = wp_insert_post( $add_event );
            }
            else {
                $add_event['ID'] = $update;
                $event = wp_update_post( $add_event );
            }

            if( isset($_POST['tax_input']) && count($arr['tax_input']['gss_event_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_event_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $event, $cat_ids, 'stellar_event_categories' );
            }

            unset($arr['event_title']);
            unset($arr['event_content']);
            unset($arr['tax_input']);
            unset($arr['gss_events']);

            if ( ! array_key_exists('_gss_post_hidden', $arr) ){
                $arr['_gss_post_hidden'] = '';
            } else {
                $arr['_gss_post_hidden'] = 1;
            }

            $startTime = sprintf('%1s:%2s:00 %3s', $arr['event_startTime']['hour'], $arr['event_startTime']['minute'], $arr['event_startTime']['noon'] );
            $startTime = date_format(date_create($startTime),"G:i:s+00:00");

            $endtTime = sprintf('%1s:%2s:00 %3s', $arr['event_endTime']['hour'], $arr['event_endTime']['minute'], $arr['event_endTime']['noon'] );
            $endtTime = date_format(date_create($endtTime),"G:i:s+00:00");

            foreach ($arr as $key => $value) {

                if( $key !== 'event_startTime' || $key !== 'event_endTime' ){
                    if( $key === 'event_startDate' ) {
                        $date = date_format(date_create($value),"Y-m-d");
                        $startDate = sprintf('%1sT%2s', $date, $startTime);

                        update_post_meta( $event, '_stellar_event_startDate', $startDate );
                    }
                    elseif( $key === 'event_endDate' ){
                        $date = date_format(date_create($value),"Y-m-d");
                        $endDate = sprintf('%1sT%2s', $date, $endtTime);

                        update_post_meta( $event, '_stellar_event_endDate', $endDate );
                    }
                    elseif( $key === '_stellar_repeating_events' ){
                        $end_on = date_format(date_create($value['ends_on']),"Y-m-d");
                        $endOn = sprintf('%1sT%2s', $end_on, '00:00:00+00:00');

                        update_post_meta( $event, '_stellar_event_repeats', $value['repeats'] );
                        update_post_meta( $event, '_stellar_event_repeat_every', $value['repeat_every'] );
                        update_post_meta( $event, '_stellar_event_repeating_ends_on', $endOn );
                        update_post_meta( $event, '_stellar_event_repeat_on', $arr['_stellar_event_repeat_on']);
                    }
                    else {
                        update_post_meta( $event, $key, $value );
                    }
                }

            }

        restore_current_blog();

        if( $event ) {
            sf_clear_cloud_cache();
            return $event;
        }
        else {
            return false;
        }

    }

    public function search($key, $offset=1, $microsite)
    {
        global $wpdb;

        switch_to_blog($microsite);
            $this->db->select('ID, post_title, post_date');
            $this->db->like('post_title', $key);
            $this->db->like('post_content', $key);
            $this->db->where('post_type', 'stellar_event');
            $result = $this->db->get("{$wpdb->prefix}posts");

            if( $result->num_rows() > 0 ){
                return $result->result();
            }
            else{
                return false;
            }
        restore_current_blog();
    }

    public function save_category($name, $arr, $blog_id, $update = false)
    {

        $user = wp_get_current_user();

        switch_to_blog( $blog_id );

            if( ! $update ) {
                $event_cat = wp_insert_term(
                        $arr['category_name'],
                        'stellar_event_categories',
                        array(
                            'description'   => $arr['description'],
                            'slug'          => ($arr['slug']) ?: sanitize_title($arr['category_name']),
                            'parent'        => $arr['parent']
                        )
                );
            }
            else {
                $event_cat = wp_update_term(
                        $update,
                        'stellar_event_categories',
                        array(
                            'name'          => $arr['category_name'],
                            'slug'          => ($arr['slug']) ?: sanitize_title($arr['category_name']),
                            'parent'        => $arr['parent'],
                            'description'   => $arr['description'],
                        )
                );
            }

            unset($arr['category_name']);
            unset($arr['description']);
            unset($arr['slug']);
            unset($arr['parent']);
            unset($arr['gss_event_category']);

            foreach ($arr as $key => $value) {
                update_term_meta($event_cat['term_id'], $key, $value);
            }

        restore_current_blog();

        if( $event_cat ) {
            return $event_cat;
        }
        else {
            return false;
        }
    }

}
