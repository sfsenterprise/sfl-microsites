<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_Local_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function _recheck_globals($microsite_id = false)
    {
        global $wpdb;
        $microsite = $microsite_id ?: $this->microsite;

        if ( $microsite === 1 ) return;

        switch_to_blog(1);
        $globalpages = $this->db->select('*')
            ->from("{$wpdb->prefix}posts AS p")
            ->join("{$wpdb->prefix}postmeta as m", 'm.post_id = p.ID')
            ->where(['m.meta_key' => '_sfmu_default_page', 'm.meta_value' => 'yes'])
            ->get();
        restore_current_blog();

        foreach( $globalpages->result() as $page ) {
            $globals[$page->ID] = $page;
        }

        switch_to_blog($microsite);
        $localpages = $this->db->select('*')
            ->from("{$wpdb->prefix}posts AS p")
            ->join("{$wpdb->prefix}postmeta as m", 'm.post_id = p.ID')
            ->where(['m.meta_key' => '_sfmu_parent_id'])
            ->get();
        restore_current_blog();

        foreach( $localpages->result() as $page ) {
            $locals[$page->meta_value] = $page;
        }

        if( isset($locals) ){
            foreach( $locals as $global_id => $local ) {
                if ( $local->post_name !== 'home' ) {
                    if ( isset($globals[$global_id]->post_modified) && $local->post_modified !== $globals[$global_id]->post_modified ) {
                        switch_to_blog($microsite);
                        $args = array(
                            'ID' => $local->ID,
                            'post_title' => $globals[$global_id]->post_title,
                            'post_status' => $globals[$global_id]->post_status,
                            'post_date' => $globals[$global_id]->post_date,
                            'post_modified' => $globals[$global_id]->post_modified,
                            'comment_status' => $globals[$global_id]->comment_status,
                            'ping_status' => $globals[$global_id]->ping_status,
                            'post_password' => $globals[$global_id]->post_password,
                            'post_name' => $globals[$global_id]->post_name,
                            'to_ping' => $globals[$global_id]->to_ping,
                            'pinged' => $globals[$global_id]->pinged,
                            'menu_order' => $globals[$global_id]->menu_order,
                            'post_type' => $globals[$global_id]->post_type
                        );

                        if ( $globals[$global_id]->post_parent > 0 ) {
                            if ( isset($locals[$globals[$global_id]->post_parent]) ) {
                                $args['post_parent'] = $locals[$globals[$global_id]->post_parent]->ID;
                            } else {
                                $args['post_parent'] = 0;
                            }
                        }

                        wp_update_post($args);
                        $wpdb->update( $wpdb->prefix ."posts", ['post_modified' => $globals[$global_id]->post_modified, 'post_modified_gmt' => $globals[$global_id]->post_modified], ['ID' => $local->ID] );
                        restore_current_blog();
                    }
                }
            }
        }
    }
}
