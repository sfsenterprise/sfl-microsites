<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function gss_blogs($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_blog = array(
                'post_title'      => $arr['blog_title'],
                'post_content'    => stripslashes_deep($arr['blog_content']),
                'post_status'     => $arr['post_status'],
                'post_type'       => 'post',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $blog = wp_insert_post( $add_blog );
            }
            else {
                $add_blog['ID'] = $update;
                $blog = wp_update_post( $add_blog );
            }

            if( isset($arr['tax_input']['gss_blog_categories']) && count($arr['tax_input']['gss_blog_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_blog_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $blog, $cat_ids, 'category' );
            }
            else{
                $term_taxonomy_ids = wp_set_object_terms( $blog, 1, 'category' );
            }

            if( isset($arr['post_tags']) ){
                wp_set_post_tags( $blog, implode(',', $arr['post_tags']), true );
            }

            unset($arr['blog_title']);
            unset($arr['blog_content']);
            unset($arr['tax_input']);
            unset($arr['gss_blog']);
            unset($arr['post_tags']);

            foreach ($arr as $key => $value) {
                if( $key === 'yoast_wpseo_meta-robots-adv' ){
                    update_post_meta( $blog, $key, maybe_serialize($value) );
                } else {
                    update_post_meta( $blog, $key, $value );
                }
            }

        restore_current_blog();

        if( $blog ) {
            sf_clear_cloud_cache();
            return $blog;
        }
        else {
            return false;
        }
    }

    public function search($search, $offset=1, $limit=null)
    {
        global $wpdb;

        switch_to_blog(1);
            $this->db->select('ID, post_title, post_date, post_author');
            $this->db->like('post_title', $search);
            $this->db->like('post_content', $search);
            $this->db->where('post_type', 'post');

            if( $limit )
                $this->db->limit($limit, $offset);
            
            $result = $this->db->get("{$wpdb->prefix}posts");

            if( $result->num_rows() > 0 ){
                return $result->result();
            }
            else{
                return false;
            }
        restore_current_blog();
    }

    public function gss_blog_category($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );

            if( ! $update ) {
                $blog_cat = wp_insert_term(
                        $arr['category_name'],
                        'category',
                        array(
                            'description'   => $arr['category_description'],
                            'slug'          => ($arr['category_slug']) ?: sanitize_title($arr['category_name']),
                            'parent'        => $arr['category_parent']
                        )
                );
            }
            else {
                $blog_cat = wp_update_term(
                        $update,
                        'category',
                        array(
                            'name'          => $arr['category_name'],
                            'slug'          => ($arr['category_slug']) ?: sanitize_title($arr['category_name']),
                            'parent'        => $arr['category_parent'],
                            'description'   => $arr['category_description'],
                        )
                );
            }

            unset($arr['category_name']);
            unset($arr['category_description']);
            unset($arr['category_slug']);
            unset($arr['category_parent']);
            unset($arr['gss_blog_category']);

            foreach ($arr as $key => $value) {
                update_term_meta($blog_cat['term_id'], $key, $value);
            }

        restore_current_blog();

        if( $blog_cat ) {
            return $blog_cat;
        }
        else {
            return false;
        }
    }

}
