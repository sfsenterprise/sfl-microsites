<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setup_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function modify_super_admin($user_id)
    {
        global $wpdb;

        $this->db->where('ID', 1)->update("{$wpdb->users}", ['user_login' => 'surefire-admin', 'user_email' => 'sis@surefiresocial.com', 'user_nicename' => 'Surefire Local', 'user_url' => 'https://surefirelocal.com', 'display_name' => 'Surefire Local']);
        update_user_meta(1, 'description', 'This is the "surefire-admin" admin user that our staff uses to gain access to your admin area to provide support and troubleshooting.');
        update_user_meta(1, 'first_name', 'Surefire');
        update_user_meta(1, 'last_name', 'Local');
        wp_set_password( $this->input->post('password', NULL), 1 );
        grant_super_admin(1);

        wp_set_current_user( 1, 'surefire-admin');
        wp_set_auth_cookie( 1, true );
    }

    public function which_setup_page()
    {
        $recheck = $this->input->get('recheck');

        if ( null !== $recheck ) {

            call_user_func(array($this, 'recheck_' . $recheck));

            echo 'Recheck Complete!';
            exit;
        } else if ( null !== $import = $this->input->get('import') ) {
            if ( method_exists($this, 'lead_' . $import) ) {
                call_user_func(array($this, 'lead_' . $import));
            }
            exit;
        }

        $skip = $this->input->get('skip');

        if ( null !== $skip ) {
            update_option('_sfmu_setup', $skip);

            if ( $skip > 0 ) {
                redirect(base_url('/setup/step_' . $skip . '/'));
            } else {
                redirect(base_url('/setup/done/'));
            }
            exit;
        }

        $reset = $this->input->get('reset');
        if ( null !== $reset ) {

            $this->reset_setup();

            update_option('_sfmu_setup', '');
            redirect(base_url('/setup/'));
            exit;
        }

        $sfmu_setup = absint(get_option('_sfmu_setup', 1));
        if ( $sfmu_setup !== 0 ) {
            if ( $sfmu_setup === 1 ) {
                if ( $this->router->fetch_method() !== 'index' ) {
                    redirect(base_url('/setup/'));
                    exit;
                }
            } else {
                if ( $this->router->fetch_method() !== 'step_' . $sfmu_setup ) {
                    redirect(base_url('/setup/step_' . $sfmu_setup .'/'));
                    exit;
                }
            }
        }
    }

    public function microsites()
    {
        $swim_lesson = get_page_by_path('programs/swim-lesson-levels');
        update_post_meta($swim_lesson->ID, '_wp_page_template', 'templates/template-programs_swim-lessons.php');

        $this->load->dbforge();
        $sites = get_sites();

        $nap = $this->nap($sites[0], '/', $sites[0]->domain);
        if ( $nap ) $this->save_microsite($nap);

        array_shift($sites);

        foreach( $sites as $site ) {
            $prefix = "wp_{$site->blog_id}_";

            $path_pos = strpos($site->domain, '.');
            if (  $path_pos !== FALSE && absint($site->blog_id) !== 1 ) {
                $domain = 'www.' . substr($site->domain, ($path_pos + 1));
                $mainsite = untrailingslashit(str_replace(['http://', 'https://'], '', home_url()));

                if ( untrailingslashit($domain) === $mainsite ) {
                    $path = '/' . substr($site->domain, 0, $path_pos) . '/';
                    $new_slugs = include SFL_MICROSITES_DIR . 'lib/config/new_slugs.php';
                    $new_slug = str_replace('/', '', $path);
                    if ( array_key_exists($new_slug, $new_slugs) ) {
                        $path = '/' . $new_slugs[$new_slug] . '/';
                    }

                    $protocol = is_ssl() ? 'https://' : 'http://';
                    $siteurl = $protocol . $domain . $path;

                    $nap = $this->nap($site, $path, $site->domain);

                    if ( $nap ) $this->save_microsite($nap);

                    update_blog_details($site->blog_id, ['domain' => $domain, 'path' => $path]);
                    update_blog_option($site->blog_id, 'siteurl', $siteurl);
                    update_blog_option($site->blog_id, 'home', $siteurl);

                    switch_to_blog($site->blog_id);

                    switch_theme('surepress');
                    $home = get_page_by_path('home');
                    $swim_lesson = get_page_by_path('programs/swim-lesson-levels');
                    update_option('page_on_front', $home->ID);
                    update_option('show_on_front', 'page');
                    update_post_meta($home->ID, '_wp_page_template', 'templates/template-microsite-home.php');
                    update_post_meta($swim_lesson->ID, '_wp_page_template', 'templates/template-programs_swim-lessons.php');
                    update_option('permalink_structure', '/%postname%/');
                    flush_rewrite_rules();

                    restore_current_blog();
                }
            }

            $this->create_lead_table($prefix);
        }

        $this->dbforge->drop_table('wp', TRUE);
        $this->dbforge->drop_table('wp_gf_locations_active', TRUE);
        $this->dbforge->drop_table('wp_gf_locations', TRUE);

        flush_rewrite_rules();
    }

    private function nap($blog, $path, $vanity)
    {

        $select = [
            'site_id AS microsite_blog_id',
            'name AS microsite_name',
            'street_address AS microsite_street',
            'state AS microsite_state_province',
            'city AS microsite_city',
            'zip AS microsite_zip_postal',
            'phone AS microsite_phone',
            'owner_name AS microsite_general_manager',
            'lat AS microsite_lat',
            'lng AS microsite_lng'
        ];

        $query = $this->db->select($select)->where(['site_id' => $blog->blog_id])->get('wp_gf_locations_active');

        if ( ! $nap = $query->row() ) {
            $nap = new \StdClass;
            $nap->microsite_blog_id = $blog->blog_id;
            $nap->microsite_updated = $blog->registered;
            $nap->microsite_date = $blog->last_updated;

            if ( absint($blog->blog_id) !== 1 ) {
                $napdemo = get_blog_details($blog->blog_id);
                $nap->microsite_name = $napdemo->blogname;
                $nap->microsite_slug = str_replace('/', '', $path);
                $nap->microsite_vanity_url = $vanity;

                if ( strtolower($napdemo->blogname) === 'demo' ) {
                    $country = 'usa';
                    $street = '390 Park Street';
                    $street2 = 'Suite #250';
                    $city = 'Birmingham';
                    $state_province = abbr_state('MI');
                    $zip_postal = '48009';
                    $phone = '(800) 856-5120';
                    $lat = 42.5487955;
                    $lng = -83.2130485;
                } else {
                    $country = 'canada';
                    $street = '1130 Eighth Line Road';
                    $street2 = 'Unit 17';
                    $city = 'Oakville';
                    $state_province = abbr_state('ON');
                    $zip_postal = 'L6H 2R4';
                    $phone = '(289) 815-3806';
                    $lat = 43.470020;
                    $lng = -79.681084;
                }

                $nap->microsite_street = apply_filters('sfmu_demo_street', $street, $country);
                $nap->microsite_street2 = apply_filters('sfmu_demo_street2', $street2, $country);
                $nap->microsite_city = apply_filters('sfmu_demo_city', $city, $country);
                $nap->microsite_state_province = apply_filters('sfmu_demo_state_province', $state_province, $country);
                $nap->microsite_zip_postal = apply_filters('sfmu_demo_zip_postal', $zip_postal, $country);
                $nap->microsite_phone = apply_filters('sfmu_demo_zip_postal', $phone, $country);
                $nap->microsite_lat = apply_filters('sfmu_demo_lat', $lat, $country);
                $nap->microsite_lng = apply_filters('sfmu_demo_lng', $lng, $country);

                update_blog_details($blog->blog_id, ['public' => 0, 'archived' => 1]);

            } else {
                $nap->microsite_name = 'Goldfish Swim School';
                $nap->microsite_slug = $path;
                $nap->microsite_vanity_url = '';

                $street = '390 Park Street';
                $street2 = 'Suite #250';
                $city = 'Birmingham';
                $state_province = abbr_state('MI');
                $zip_postal = '48009';
                $phone = '(800) 856-5120';
                $lat = 42.5487955;
                $lng = -83.2130485;

                $nap->microsite_street = apply_filters('sfmu_main_street', $street);
                $nap->microsite_street2 = apply_filters('sfmu_main_street2', $street2);
                $nap->microsite_city = apply_filters('sfmu_main_city', $city);
                $nap->microsite_state_province = apply_filters('sfmu_main_state_province', $state_province);
                $nap->microsite_zip_postal = apply_filters('sfmu_main_zip_postal', $zip_postal);
                $nap->microsite_phone = apply_filters('sfmu_main_zip_postal', $phone);
                $nap->microsite_lat = apply_filters('sfmu_main_lat', $lat);
                $nap->microsite_lng = apply_filters('sfmu_main_lng', $lng);
            }

        } else {

            if ( ! $nap->microsite_street &&  ! $nap->microsite_city && ! $nap->microsite_state_province ) {
                $nap->microsite_slug = str_replace('/', '', $path);
                $nap->microsite_vanity_url = $vanity;

                if ( $nap->microsite_slug === 'demo' ) {
                    $country = 'usa';
                    $street = '390 Park Street';
                    $street2 = 'Suite #250';
                    $city = 'Birmingham';
                    $state_province = abbr_state('MI');
                    $zip_postal = '48009';
                    $phone = '(800) 856-5120';
                    $lat = 42.5487955;
                    $lng = -83.2130485;
                } elseif ( $nap->microsite_slug === 'democa') {
                    $country = 'canada';
                    $street = '1130 Eighth Line Road';
                    $street2 = 'Unit 17';
                    $city = 'Oakville';
                    $state_province = abbr_state('ON');
                    $zip_postal = 'L6H 2R4';
                    $phone = '(289) 815-3806';
                    $lat = 43.470020;
                    $lng = -79.681084;
                }

                $nap->microsite_street = apply_filters('sfmu_demo_street', $street, $country);
                $nap->microsite_street2 = apply_filters('sfmu_demo_street2', $street2, $country);
                $nap->microsite_city = apply_filters('sfmu_demo_city', $city, $country);
                $nap->microsite_state_province = apply_filters('sfmu_demo_state_province', $state_province, $country);
                $nap->microsite_zip_postal = apply_filters('sfmu_demo_zip_postal', $zip_postal, $country);
                $nap->microsite_phone = apply_filters('sfmu_demo_zip_postal', $phone, $country);
                $nap->microsite_lat = apply_filters('sfmu_demo_lat', $lat, $country);
                $nap->microsite_lng = apply_filters('sfmu_demo_lng', $lng, $country);

                update_blog_details($blog->blog_id, ['public' => 0, 'archived' => 1]);
            } else {
                $nap->microsite_updated = $blog->registered;
                $nap->microsite_date = $blog->last_updated;
                $nap->microsite_state_province = abbr_state($nap->microsite_state_province);
                $nap->microsite_slug = str_replace('/', '', $path);
                $nap->microsite_vanity_url = $vanity;
            }
        }

        $site_gtma = include SFL_MICROSITES_DIR . 'lib/config/gtmcodes.php';

        if ( array_key_exists(absint($blog->blog_id), $site_gtma) ) {
            $nap->microsite_gtm_ga = $site_gtma[absint($blog->blog_id)];
        }

        if ( absint($blog->blog_id) !== 1 ) $nap->microsite_post_id = $this->location($nap);

        return $nap;
    }

    private function location($nap)
    {
        global $wpdb;
        $state_slug = sanitize_title($nap->microsite_state_province);
        $city_slug = sanitize_title($nap->microsite_city);
        $location_slug = sanitize_title($nap->microsite_slug);

        $state = $this->db->get_where($wpdb->posts, ['post_name' => $state_slug, 'post_parent' => 0, 'post_type' => 'location'])->row();
        if ( ! $state  ) {
            $state_content = $this->get_location_content('state', $state_slug);
            $state_id = wp_insert_post(['post_type' => 'location', 'post_status' => 'publish', 'post_content' => $state_content, 'post_name' => $state_slug, 'post_title' => $nap->microsite_state_province]);
            $state = get_post($state_id);
        }

        $city = $this->db->get_where($wpdb->posts, ['post_name' => $city_slug, 'post_parent' => $state->ID, 'post_type' => 'location'])->row();
        if ( ! $city ) {
            $city_content = $this->get_location_content('city', $state_slug, $city_slug);
            $city_id = wp_insert_post(['post_type' => 'location', 'post_status' => 'publish', 'post_content' => $city_content, 'post_parent' => $state->ID, 'post_name' => $city_slug, 'post_title' => $nap->microsite_city]);
            $city = get_post($city_id);
        }

        $location = $this->db->get_where($wpdb->posts, ['post_name' => $nap->microsite_slug, 'post_parent' => $city->ID, 'post_type' => 'location'])->row();
        if ( ! $location ) {
            $location_id = wp_insert_post(['post_type' => 'location', 'post_status' => 'publish', 'post_parent' => $city->ID, 'post_name' => $nap->microsite_slug, 'post_title' => $nap->microsite_name]);
            $location = get_post($location_id);
        }

        return $location->ID;
    }

    private function get_location_content($state_or_city, $state_slug, $city_slug = null)
    {
        $content = '';
        if ( $state_or_city === 'state' ) {
            $file = SFL_MICROSITES_DIR . "lib/locations/{$state_slug}/state_province.php";
        } else {
            $file = SFL_MICROSITES_DIR . "lib/locations/{$state_slug}/{$city_slug}.php";
        }

        if ( file_exists( $file ) ) {
            ob_start();
            include_once $file;
            $content .= ob_get_clean();
        }

        return $content;
    }

    private function save_microsite($nap)
    {
        $nap = (array) $nap;

        $has_microsite = $this->db->get_where('sfl_microsites', ['microsite_blog_id' => $nap['microsite_blog_id']]);
        if ( $has_microsite->num_rows() ) return;

        $CA = array(
            'AB' => 'Alberta',
            'BC' => 'British Columbia',
            'MB' => 'Manitoba',
            'NB' => 'New Brunswick',
            'NL' => 'Newfoundland and Labrador',
            'NT' => 'Northwest Territories',
            'NS' => 'Nova Scotia',
            'NU' => 'Nunavut',
            'ON' => 'Ontario',
            'PE' => 'Prince Edward Island',
            'QC' => 'Quebec',
            'SK' => 'Saskatchewan',
            'YT' => 'Yukon',
        );

        $template = ['template' => 'usa'];
        if ( in_array($nap['microsite_state_province'], $CA) ) $template = ['template' => 'canada'];

        $nap['microsite_fields'] = maybe_serialize($template);
        $this->db->insert('sfl_microsites', $nap);
    }

    public function convert_events()
    {
        $sites = get_sites();

        foreach ( $sites as $site ) {
            $this->convert_site_events($site->blog_id);
        }

    }

    private function convert_site_events($blog_id) {
        $table = absint($blog_id) !== 1 ? "wp_{$blog_id}_posts" : 'wp_posts';
        $events = $this->db->select(['ID', 'meta_key'])
            ->where($table, ['post_type' => 'stellar_event'])->result();

        dd($events);
    }

    private function create_lead_table($prefix)
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 20,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ),
            'lead_id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 20,
                'null'              => FALSE,
            ),
            'form_id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 20,
                'null'              => FALSE,
            ),
            'field_name' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 100,
                'null'              => FALSE,
            ),
            'value' => array(
                'type'              => 'LONGTEXT',
                'null'              => FALSE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('lead_id');
        $this->dbforge->add_key('form_id');
        $this->dbforge->create_table("{$prefix}sfl_lead_detail", TRUE);
    }

    private function recheck_locations() {

        $locations = get_posts(['post_type' => 'location', 'posts_per_page' => -1, 'post_status' => 'publish']);

        set_time_limit(0);
        foreach( $locations as $location ) {
            $level = get_post_ancestors($location);

            if ( count($level) < 1 ) {
                $file = SFL_MICROSITES_DIR . 'lib/locations/' . $location->post_name . '/state_province.php';
                $update = 0;
                if ( file_exists( $file ) ) {
                    $content = include $file;
                    $update = wp_update_post(['ID' => $location->ID, 'post_content' => $content]);
                }

                echo $update . ' - ' . $location->post_title . ' | State or Province<br/>';
            } elseif ( count($level) === 1 ) {
                $parent = get_post($level[0]);
                $file = SFL_MICROSITES_DIR . 'lib/locations/' . $parent->post_name . '/' . $location->post_name . '.php';
                $update = 0;
                if ( file_exists( $file ) ) {
                    $content = include $file;
                    $update = wp_update_post(['ID' => $location->ID, 'post_content' => $content]);
                }

                echo $update . ' - ' . $location->post_title . ' | City<br/>';
            }
        }
    }

    private function recheck_microsite_strip_and_notification_form()
    {
        $sites = get_sites();
        $form_to_cpt = [
            'pre-job-application' => 'pre_employment_application_form',
            'registration-form' => 'register_form',
            'single-class-cancellation-form' => 'cancel_form',
            'refer-a-friend' => 'refer_a_friend_form',
            'pre-registration-form' => 'pre_registration_form',
            'location-contact-form' => 'general_contact_form',
            'job-application-form' => 'pre_employment_application_form',
        ];

        set_time_limit(0);
        foreach( $sites as $site ) {
            if ( $site->blog_id === 1 ) continue;

            switch_to_blog($site->blog_id);
            $forms = get_posts(['post_type' => 'ff_forms', 'posts_per_page' => -1, 'post_status' => 'any']);

            foreach( $forms as $form ) {

                if ( strpos($form->post_content, '[gravityform') !== FALSE ) {
                    $content = preg_replace('/(^\[gravityform.*?\])/', '', $form->post_content);
                    $args = ['ID' => $form->ID, 'post_content' => $content];
                    wp_update_post($args);
                }

                if ( array_key_exists($form->post_name, $form_to_cpt) ) {

                    $file = SFL_MICROSITES_DIR . "lib/forms/{$form_to_cpt[$form->post_name]}/microsite_{$form_to_cpt[$form->post_name]}.json";
                    if ( file_exists($file) ) {
                        $meta_input = json_decode(file_get_contents($file), true);

                        foreach($meta_input as $meta_key => $meta_value) {
                            update_post_meta($form->ID, $meta_key, $meta_value);
                        }
                    }
                }
            }
            restore_current_blog();
        }
    }

    public function recheck_staffs()
    {
        global $wpdb;
        $sites = get_sites();
        foreach( $sites as $site ) {

            switch_to_blog($site->blog_id);
            $posts = $wpdb->get_results("SELECT * FROM {$wpdb->posts} AS p LEFT JOIN $wpdb->postmeta AS m ON p.ID = m.post_id WHERE meta_key = 'menu_order' AND post_type = 's8_staff_member'");

            if ( $posts )  {
                foreach ( $posts as $post ) {
                    wp_update_post(['ID' => $post->ID, 'menu_order' => $post->meta_value]);
                }
            }
            restore_current_blog();
        }
    }

    public function recheck_cpt_forms_status()
    {
        $segment = $this->input->get('segment');
        $sites = get_sites( array('site__not_in'=>[1, 2]) );

        $hide_forms = array(
                'Family Swim Waiver',
                'Inclement Weather Policy',
                'Party Waiver',
                'Policies Document'
            );

        switch ($segment) {
            case '1':
                $sites = array_slice($sites, 0, 7);
                break;
            case '2':
                $sites = array_slice($sites, 7, 7);
                break;
            case '3':
                $sites = array_slice($sites, 14, 7);
                break;
            case '4':
                $sites = array_slice($sites, 21, 7);
                break;
            case '5':
                $sites = array_slice($sites, 28, 7);
                break;
            case '6':
                $sites = array_slice($sites, 35, 7);
                break;
            case '7':
                $sites = array_slice($sites, 42, 7);
                break;
            case '8':
                $sites = array_slice($sites, 49, 7);
                break;
            case '9':
                $sites = array_slice($sites, 56, 7);
                break;
            case '10':
                $sites = array_slice($sites, 63, 7);
                break;
            case '11':
                $sites = array_slice($sites, 70, 7);
                break;
            default:
                break;
        }

        foreach ($sites as $site) {

            switch_to_blog($site->blog_id);

                foreach ($hide_forms as $form) {

                    $page = get_page_by_title($form, OBJECT, 'ff_forms');

                    if( $page ){
                        $page->post_status = 'private';

                        $update = wp_update_post($page);

                        if( !$update ){
                            echo 'Blog ID: ' . $site->blog_id . '<br/>';
                            echo 'Form: ' . $form;
                            dd('Something went wrong, please reload.');
                            break;
                        }
                    }

                }

            restore_current_blog();

        }

        dd("Successfully update segment {$segment}");
    }

    public function recheck_microsite_form_settings()
    {
        $this->microsite_form_settings();
    }

    public function convert_testimonials()
    {
        $posts = get_posts(['post_type' => 's8_testimonials', 'post_status' => 'any', 'posts_per_page' => -1]);

        $testimonials = array(
            [
                'post_type' => 's8_testimonials',
                'post_title' => 'Katie Parker',
                'post_content' => 'The staff at Goldfish are exceptional, the water is comfortable and the class sizes are small. Within a few lessons, the whole staff addressed Hailey by name, making her feel so special. We will be forever grateful for receiving such high-quality lessons from Goldfish. We recommend Goldfish to all our friends.',
            ],
            [
                'post_type' => 's8_testimonials',
                'post_title' => 'Matthew Allen',
                'post_content' => "We wanted to let you know about the fantastic experience we have had this suumer with Coach Mike at your Birningham location. Our two children, Avery(6) and Connor (5) were non-swimmers when we came to Goldfish in June of this year. We wanted the kids in Goldfish's Jump Start Clinics and began the journey of every day swim classes with Coach Mike.",
            ],
            [
                'post_type' => 's8_testimonials',
                'post_title' => 'Lisa Ferris',
                'post_content' => "My 4-year-old daughter has been attending Goldfish Swim School for the past few years. As a result the wonderful environment at Goldfish - excellent and cheerful teachers, warm water and air, and positive, child-oriented atmosphere - she quickly learned to love the water and feel entirely at home in the pool.",
            ],
            [
                'post_type' => 's8_testimonials',
                'post_title' => 'Troy and Jacqueline Bergman',
                'post_content' => "We have two children at Goldfish and they absolutely love it! The instructors have shown true, genuine compassion with our children and are dedicated to their growth with each lesson.",
            ],
            [
                'post_type' => 's8_testimonials',
                'post_title' => 'Susan Pepper',
                'post_content' => "Your staff members have consistently demonstrated kindness, thoughtfulness and an excellence in swimming instruction. I would recommend your school to anyone interested in not only teaching their children how to swim but to also be safe in the water!",
            ],
        );

        foreach ( $posts as $key => $post ) {
            $testimonials[$key]['ID'] = $post->ID;
            wp_update_post($testimonials[$key]);
        }
    }

    public function microsite_form_settings()
    {
        $cpt_to_forms = array(
            'Job Application',
            'Location Contact Form',
            'Pre-Registration Form',
            'Refer-a-Friend Form',
            'Registration Form',
            'Single Class Cancellation Form',
        );

        $metadatas = array();
        foreach( glob(SFL_MICROSITES_DIR . 'lib/forms/**/*.json') as $json_file ) {

            $metadata = json_decode(file_get_contents($json_file), true);

            if ( !is_array($metadata) ) dd('You have an error in your json file: ' . basename($json_file));

            $metadatas[str_replace('.json', '', basename($json_file))] = $metadata;
        }

        $wpcf7_contact_forms = get_posts([
            'post_type' => 'wpcf7_contact_form',
            'posts_per_page' => -1,
        ]);

        $reforms = array(
            'cancel_form' => 'Cancel Form',
            'contest_form' => 'Contest Form',
            'creative_request_form' => 'Creative Request Form',
            'general_contact_form' => 'General Contact Form',
            'holiday_form' => 'Holiday Form',
            'holiday_gift_card_form' => 'Holiday Gift Card Form',
            'newsletter' => 'Newsletter',
            'pre_employment_application_form' => 'Pre-Employment Application',
            'pre_registration_form' => 'Pre-Registration Form - Full (with additional kids)',
            'refer_a_friend_form' => 'Refer-a-Friend Form',
            'register_form' => 'Register Form',
        );

        $recheck = $this->input->get('recheck');

        if ( null !== $recheck ) {
            $renotifications = array();
            foreach( $metadatas as $form => $meta) {
                foreach ( $meta as $meta_key => $meta_data ) {
                    if ( $meta_key !== '_sfmu_mail' ) continue;

                    $renotifications[$form]['_sfmu_mail']['microsite'] = $meta_data['microsite'];
                }
            }
        }

        foreach( $wpcf7_contact_forms as $wpcf7_contact_form ) {
            $folder_name = array_search($wpcf7_contact_form->post_title, $reforms);

            if ( $folder_name ) {

                $recheck = $this->input->get('recheck');

                if ( null === $recheck ) {
                    $file = SFL_MICROSITES_DIR . "lib/forms/{$folder_name}/{$folder_name}.php";
                    if ( file_exists($file) ) {
                        ob_start();
                        include $file;
                        $metadatas[$folder_name]['_form'] = ob_get_clean();
                        unset($file);
                    }

                    foreach( $metadatas[$folder_name] as $meta_key => $meta_value ) {
                        update_post_meta($wpcf7_contact_form->ID, $meta_key, $meta_value);
                    }
                } else {
                    $sites = get_sites();
                    array_shift($sites);

                    foreach( $sites as $site ) {
                        foreach( $renotifications[$folder_name] as $meta_key => $meta_value ) {

                            $messa_body = SFL_MICROSITES_DIR . "lib/forms/{$folder_name}/{$folder_name}_body.php";
                            if ( file_exists($messa_body) ) {
                                $meta_value['microsite'][0]['body'] = file_get_contents($messa_body);
                            }

                            update_post_meta($wpcf7_contact_form->ID, $meta_key . '_' . $site->blog_id, $meta_value);
                        }
                    }
                }
            }
        }
    }

    public function microsite_global_pages()
    {
        $contact = get_page_by_title('Contact');

        $contact_form = get_page_by_title('General Contact Form', OBJECT, 'wpcf7_contact_form');
        $shortcode = '';

        if ( $contact_form ) {
            $shortcode = '[contact-form-7 id="'.$contact_form->ID.'" title="'.$contact_form->post_title.'"]';
        }

        if ( $contact ) {

            if ( strpos($contact->post_content, '[gravityform') !== FALSE ) {
                $content = preg_replace('/(\[gravityform id="2".*?\])/', $shortcode, $contact->post_content);

                $args = ['ID' => $contact->ID, 'post_content' => $content];
                wp_update_post($args);
            }
        }

        $parents = get_posts(['post_type' => 'page', 'post_status' => 'publish', 'meta_key' => '_sfmu_default_page', 'meta_value' => 'yes', 'posts_per_page' => -1]);
        $sites = get_sites();
        array_shift($sites);

        foreach($sites as $site) {
            switch_to_blog($site->blog_id);

            $contact = get_page_by_title('Contact');

            if ( $contact ) {

                if ( strpos($contact->post_content, '[gravityform') !== FALSE ) {
                    $content = preg_replace('/(\[gravityform .*?\])/', $shortcode, $contact->post_content);
                    $args = ['ID' => $contact->ID, 'post_content' => $content];
                    wp_update_post($args);
                }
            }

            foreach($parents as $parent) {
                $child = get_page_by_title($parent->post_title);

                if ( ! $child ) {
                    $insert = wp_insert_post(['post_type' => 'page', 'post_title' => $parent->post_title, 'post_status' => 'publish']);
                    $child = get_post($insert);
                }

                update_post_meta($child->ID, '_sfmu_parent_id', $parent->ID);
            }

            restore_current_blog();
        }
    }

    public function recheck_alter_lat_lng_db()
    {
        $this->load->dbforge();
        $this->dbforge->modify_column(
            'sfl_microsites',
            array(
                'microsite_lat' => array(
                    'name' => 'microsite_lat',
                    'type' => 'decimal',
                    'constraint' => '10,6',
                    'null' => FALSE,
                ),
                'microsite_lng' => array(
                    'name' => 'microsite_lng',
                    'type' => 'decimal',
                    'constraint' => '10,6',
                    'null' => FALSE,
                )
            )
        );
    }

    public function recheck_users_per_microsites()
    {
        $allusers = array();
        $sites = get_sites();
        foreach( $sites as $site ) {
            $users = get_users(['blog_id' => $site->blog_id]);

            if ( is_array($users) ) {
                foreach( $users as $user ) {
                    $allusers[] = array(
                        'microsite_blog_id' => $site->blog_id,
                        'microsite_slug' => $site->path === '/' ? 'main' : $site->path,
                        'user_id' => $user->ID,
                        'user_login' => $user->data->user_login,
                        'user_pass' => $user->data->user_pass,
                        'user_nicename' => $user->data->user_nicename,
                        'user_email' => $user->data->user_email,
                        'display_name' => $user->data->display_name,
                        'user_registered' => $user->data->user_registered,
                    );
                }
            }
        }
        $this->load->helper('export_helper');
        $headings = array(
            'microsite_blog_id',
            'microsite_slug',
            'user_id',
            'user_login',
            'user_pass',
            'user_nicename',
            'user_email',
            'display_name',
            'user_registered',
        );

        sf_exports_data($allusers, $headings, 'users_per_microsites');
    }

    private function reset_setup()
    {
        $this->load->dbforge();

        $this->dbforge->drop_table('wp', TRUE);
        $this->dbforge->drop_table('wp_gf_locations_active', TRUE);
        $this->dbforge->drop_table('wp_gf_locations', TRUE);
        $this->dbforge->drop_table('wp_blogs', TRUE);
        $this->db->empty_table('sfl_microsites');
        $this->db->truncate('sfl_microsites');

        $file = SFL_MICROSITES_DIR . 'lib/config/gss_resetup.sql';
        if (!file_exists($file))
            die("no sql dump found.");

        $this->load->helper('import_helper');

        $query = @fread(@fopen($file, 'r'), @filesize($file)) or die('problem ');
        $query = remove_remarks($query);
        $query = split_sql_file($query, ';');

        foreach($query as $sql) {
            $this->db->query($sql) or die('error in query');
        }
    }

    private function lead_prereg()
    {
        $this->load->helper('import');
        $imports = array();
        $form_id = 3878; // pre-reg form ID
        $duplicates = array();

        foreach( glob(SFL_MICROSITES_DIR . 'application/views/setup/prereg/*.csv') as $filename ) {

            $key = strstr(basename($filename), '_', true);
            $sites = get_sites(['site__in' => $key]);
            $site = $this->input->get('site');

            if ( ( null !== $site ) && absint($site) !== absint($key) ) continue;
            if ( count($sites) < 1 ) continue;

            $rg_leads = sf_parseCSV($filename, true);
            if ( ! $rg_leads ) continue;

            foreach( $rg_leads as $row => $rg_lead ) {
                $imports[$key][$row] = array(
                    'entry_id' => $rg_lead["Entry Id"],
                    'blog_id' => $key,
                    'form_id' => $form_id,
                    'post_id' => $rg_lead["Post Id"] ?: null,
                    'date_created' => $rg_lead["Entry Date"],
                    'ip' => $rg_lead["User IP"],
                    'source_url' => $rg_lead["Source Url"],
                    'user_agent' => $rg_lead["User Agent"],
                    'currency' => 'USD',
                    'status' => 'active',
                    'details' => array(),
                );
                unset($rg_lead["Entry Date"]);
                unset($rg_lead["User IP"]);
                unset($rg_lead["Source Url"]);
                unset($rg_lead["User Agent"]);
                unset($rg_lead["Created By (User Id)"]);
                unset($rg_lead["Entry Id"]);
                unset($rg_lead["Transaction Id"]);
                unset($rg_lead["Payment Amount"]);
                unset($rg_lead["Payment Date"]);
                unset($rg_lead["Payment Status"]);
                unset($rg_lead["Post Id"]);

                $imports[$key][$row]['details'] = $rg_lead;
            }
        }

        if ( !is_array($imports) ) return;

        foreach( $imports as $blog_id => $newleads ) {
            if ( absint($blog_id) === 1 ) {
                $prefix = '';
            } else {
                $prefix = $blog_id .'_';
            }

            foreach( $newleads as $newlead ) {
                $details = $newlead['details'];
                $entry_id = $newlead['entry_id'];

                unset($newlead['details']);

                $duplicate = $this->db->get_where('wp_' . $prefix . 'rg_lead', ['source_url' => $newlead['source_url'], 'ip' => $newlead['ip'], 'date_created' => $newlead['date_created']])->result();

                if ( $duplicate ) {
                    $duplicates[] = $newlead;
                } else {
                    unset($newlead['entry_id']);
                    unset($newlead['blog_id']);
                    $this->db->insert('wp_' . $prefix . 'rg_lead', $newlead);
                    $lead_id = $this->db->insert_id();

                    foreach( $details as $field => $detail ) {
                        if ( ! $detail ) continue;
                        $data = array(
                            'lead_id' => $lead_id,
                            'form_id' => $newlead['form_id'],
                            'field_name' => $field,
                            'value' => $detail,
                        );
                        $this->db->insert('wp_' . $prefix . 'sfl_lead_detail', $data);
                    }
                }
            }
        }

        if ( $duplicates ) {
            $this->load->helper('export');

            sf_exports_data($duplicates, [
                'entry_id',
                'blog_id',
                'form_id',
                'post_id',
                'date_created',
                'ip',
                'source_url',
                'user_agent',
                'currency',
                'status',
            ], 'pre-registration-form-duplicates-' .date('Y-m-d', strtotime(current_time('mysql'))));
        }

    }

    private function lead_otherforms()
    {
        $this->load->helper('import');
        $site = $this->input->get('site');

        // site_id 58
        // last_id_lead 1338
        // last_id_details 1614
        $duplicates = array();

        $forms = array(
            'cancel-form' => array(),
            'general-contact-form' => array(),
            'pre-employment-application' => array(),
            'register-form' => array(),
            'newsletter' => array(),
            'refer-a-friend-form' => array(),
        );

        $form_ids = array(
            'cancel-form' => 3876,
            'general-contact-form' => 3874,
            'pre-employment-application' => 3879,
            'register-form' => 3877,
            'newsletter' => 3873,
            'refer-a-friend-form' => 3875,
        );

        foreach( glob(SFL_MICROSITES_DIR . 'application/views/setup/otherforms/*.csv') as $filename ) {

            $blog_id = strstr(basename($filename), '_', true);
            if ( null !== $site && absint($site) !== absint($blog_id) ) continue;

            $sites = get_sites(['site__in' => $blog_id]);
            if ( count($sites) < 1 ) continue;

            foreach( $forms as $form => $leads ) {
                if ( strpos($filename, $form) !== FALSE ) {

                    $rg_leads = sf_getcsv_otherforms($filename);
                    if ( ! $rg_leads ) continue;

                    foreach( $rg_leads as $key => $rg_lead ) {

                        $forms[$form][$blog_id][$key] = array(
                            'entry_id' => $rg_lead["Entry Id"],
                            'blog_id' => $blog_id,
                            'form_id' => $form_ids[$form],
                            'post_id' => $rg_lead["Post Id"] ?: null,
                            'date_created' => $rg_lead["Entry Date"],
                            'ip' => $rg_lead["User IP"],
                            'source_url' => $rg_lead["Source Url"],
                            'user_agent' => $rg_lead["User Agent"],
                            'currency' => 'USD',
                            'status' => 'active',
                            'details' => array(),
                        );

                        unset($rg_lead["Entry Date"]);
                        unset($rg_lead["User IP"]);
                        unset($rg_lead["Source Url"]);
                        unset($rg_lead["User Agent"]);
                        unset($rg_lead["Created By (User Id)"]);
                        unset($rg_lead["Entry Id"]);
                        unset($rg_lead["Transaction Id"]);
                        unset($rg_lead["Payment Amount"]);
                        unset($rg_lead["Payment Date"]);
                        unset($rg_lead["Payment Status"]);
                        unset($rg_lead["Post Id"]);

                        $forms[$form][$blog_id][$key]['details'] = $rg_lead;
                    }

                }
            }
        }

        foreach( $forms as $form => $formfills ) {
            foreach ( $formfills as $blog_id => $leads ) {
                if ( absint($blog_id) === 1 ) {
                    $prefix = '';
                } else {
                    $prefix = $blog_id .'_';
                }

                foreach( $leads as $lead ) {
                    $details = $lead['details'];
                    $entry_id = $lead['entry_id'];

                    unset($lead['details']);
                    $duplicate = $this->db->get_where('wp_' . $prefix . 'rg_lead', ['source_url' => $lead['source_url'], 'ip' => $lead['ip'], 'date_created' => $lead['date_created']])->result();

                    if ( $duplicate ) {
                        $duplicates[] = $lead;
                    } else {
                        unset($lead['entry_id']);
                        unset($lead['blog_id']);

                        $this->db->insert('wp_' . $prefix . 'rg_lead', $lead);
                        $lead_id = $this->db->insert_id();

                        foreach( $details as $field => $detail ) {
                            if ( ! $detail ) continue;
                            $data = array(
                                'lead_id' => $lead_id,
                                'form_id' => $lead['form_id'],
                                'field_name' => $field,
                                'value' => $detail,
                            );
                            $this->db->insert('wp_' . $prefix . 'sfl_lead_detail', $data);
                        }
                    }
                }
            }
        }

        if ( $duplicates ) {
            $this->load->helper('export');
            sf_exports_data($duplicates, [
                'entry_id',
                'blog_id',
                'form_id',
                'post_id',
                'date_created',
                'ip',
                'source_url',
                'user_agent',
                'currency',
                'status',
            ], 'all-other-forms-duplicates-' .date('Y-m-d', strtotime(current_time('mysql'))));
        }
    }
}