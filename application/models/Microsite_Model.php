<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Microsite_Model extends CI_Model {
    public $new_site_id;

    public function __construct()
    {
        parent::__construct();
    }

    public function save_microsite($microsite_blog_id = false)
    {
        $microsite_arr = $this->input->post(NULL, FALSE);

        $microsite = array(
            "microsite_date"            => current_time('mysql'),
            "microsite_name"            => sanitize_text_field($microsite_arr['microsite_name']),
            "microsite_slug"            => sanitize_title($microsite_arr['microsite_slug']),
            "microsite_vanity_url"      => sanitize_text_field($microsite_arr['microsite_vanity_url']),
            "microsite_street"          => sanitize_text_field($microsite_arr['microsite_street']),
            "microsite_street2"         => sanitize_text_field($microsite_arr['microsite_street2']),
            "microsite_city"            => sanitize_text_field($microsite_arr['microsite_city']),
            "microsite_state_province"  => sanitize_text_field($microsite_arr['microsite_state_province']),
            "microsite_zip_postal"      => sanitize_text_field($microsite_arr['microsite_zip_postal']),
            "microsite_phone"           => sanitize_text_field($microsite_arr['microsite_phone']),
            "microsite_general_manager" => sanitize_text_field($microsite_arr['microsite_general_manager']),
            "microsite_gtm_ga"          => sanitize_text_field($microsite_arr['microsite_gtm_ga']),
            "microsite_lat"             => sanitize_text_field($microsite_arr['microsite_lat']),
            "microsite_lng"             => sanitize_text_field($microsite_arr['microsite_lng']),
        );

        if ( strtolower($microsite_arr['template']) === 'usa' ) {
            $clone = '/demo/';
        } else {
            $clone = '/democa/';
        }

        $template = get_blog_id_from_url(str_replace(array('http://', 'https://'), array('','',), home_url()), $clone);

        // Used for future custom fields.
        $microsite_extra_fields = array(
            'template'              => $microsite_arr['template'],
            'admin_email_alias'     => $microsite_arr['admin_email_alias'],
        );

        $microsite['microsite_fields'] = maybe_serialize($microsite_extra_fields);

        if ( ! absint($microsite_blog_id) ) {


            $this->load->dbforge();
            $exclude_tables = "'wp_{$template}_gf_addon_feed','wp_{$template}_rg_form_view','wp_{$template}_rg_incomplete_submissions','wp_{$template}_rg_lead_detail','wp_{$template}_rg_lead_detail_long','wp_{$template}_rg_lead_meta','wp_{$template}_rg_lead_notes'";
            $dbname = DB_NAME;

            $query = $this->db->query("SELECT t.TABLE_NAME AS name FROM INFORMATION_SCHEMA.TABLES AS t WHERE t.TABLE_SCHEMA = '{$dbname}' AND t.TABLE_NAME LIKE 'wp_{$template}_%' AND t.TABLE_NAME NOT IN({$exclude_tables})");
            $tables = $query->result_array();
            $demo_tables = array();

            // !IMPORTANT: Make sure we only take the demo tables
            foreach( $tables as $table ) {
                if ( strpos($table['name'], "wp_{$template}_") === FALSE ) continue;

                $demo_tables[] = $table;
            }

            $microsite['microsite_updated'] = $microsite['microsite_date'];
            $email = sanitize_email($microsite_arr['microsite_admin_email']);

            $user_id = wpmu_create_user( $microsite['microsite_slug'], 'admin', $email );
            $user_id = $user_id ?: get_user_by('email', $email)->ID;

            $domain = preg_replace('/^(https:\/\/|http:\/\/)/', '', network_site_url());

            $microsite['microsite_blog_id'] = wpmu_create_blog($domain, "/{$microsite['microsite_slug']}", $microsite['microsite_name'], $user_id, array('public' => 1));
            $this->new_site_id = $microsite['microsite_blog_id'];

            if ( isset($microsite['microsite_blog_id']) ) {
                $no_rows = array(
                    "wp_{$microsite['microsite_blog_id']}_sfl_lead_detail",
                    "wp_{$microsite['microsite_blog_id']}_rg_lead",
                );

                foreach( $demo_tables as $row => $demo_table ) {
                    // !IMPORTANT: Make sure again we only mess around the db if demo table is true!
                    if ( strpos($demo_table['name'], "wp_{$template}_") === FALSE ) continue;

                    $target = str_replace("wp_{$template}_", "wp_{$microsite['microsite_blog_id']}_", $demo_table['name']);
                    if ( ! $this->db->table_exists($target) ) {
                        //$this->db->query("CREATE TABLE IF NOT EXISTS {$target} SELECT * FROM {$demo_table['name']}");
                        if( stripos($target, 'rg_lead') !== FALSE || stripos($target, 'sfl_lead_detail') !== FALSE ){
                            $this->db->query("CREATE TABLE IF NOT EXISTS {$target} (id int NOT NULL AUTO_INCREMENT, PRIMARY KEY (id)) SELECT * FROM {$demo_table['name']}");
                        }
                        else{
                            $this->db->query("CREATE TABLE IF NOT EXISTS {$target} SELECT * FROM {$demo_table['name']}");
                        }
                    }

                    $this->db->truncate($target);
                    if ( ! in_array($target, $no_rows) ) {
                        $this->db->query("INSERT {$target} SELECT * FROM {$demo_table['name']}");
                    }

                }

                $blogdesc = get_option('blogdesc', '');
                $user = get_userdata($user_id);
                switch_to_blog(absint($microsite['microsite_blog_id']));

                if ( !is_super_admin($user_id) && !in_array('franchisor', $user->roles ) && !in_array('franchisee', $user->roles ) ) {
                    add_user_to_blog(absint($microsite['microsite_blog_id']), $user_id, 'franchisee');
                }

                // Set Default Options
                switch_theme('surepress');
                $networkurl = network_site_url();
                update_blog_option(absint($microsite['microsite_blog_id']), 'siteurl', $networkurl . $microsite['microsite_slug']);
                update_blog_option(absint($microsite['microsite_blog_id']), 'home', $networkurl . $microsite['microsite_slug']);
                update_blog_option(absint($microsite['microsite_blog_id']), 'blogname', $microsite['microsite_name']);
                update_blog_option(absint($microsite['microsite_blog_id']), 'admin_email', $user->user_email);
                update_blog_option(absint($microsite['microsite_blog_id']), 'blogdescription', $blogdesc);
                update_blog_option(absint($microsite['microsite_blog_id']), 'show_on_front', 'page');
                update_blog_option(absint($microsite['microsite_blog_id']), 'permalink_structure', '/%postname%/');

                /*
                * get user roles of $clone microsite
                * update new microsite user roles option
                * delete $clone user role option
                */
                $clone_roles = get_blog_option(absint($microsite['microsite_blog_id']), 'wp_'.$template.'_user_roles');
                update_blog_option(absint($microsite['microsite_blog_id']), 'wp_'.$microsite['microsite_blog_id'].'_user_roles', $clone_roles);
                delete_blog_option(absint($microsite['microsite_blog_id']), 'wp_'.$template.'_user_roles');

                flush_rewrite_rules();
                restore_current_blog();

                //add location page
                $microsite['microsite_post_id'] = $this->microsite_location($microsite);

                $add_microsite = $this->db->insert('sfl_microsites', $microsite);

                //create form db tables
                // $form_tables = $this->create_tables($microsite['microsite_blog_id']);

                // save new microsite's form notifications
                $this->save_notifications($this->new_site_id, $microsite_arr['template']);

                // clone $template's upload folder
                $this->dir_clone($microsite['microsite_blog_id'], $template);

                sf_clear_cloud_cache();
                return $microsite;
            }

        } else {
            unset($microsite['microsite_date']);
            // $microsite['microsite_blog_id'] = $id;

            if ( absint($microsite_blog_id) !== 1 ) {
                update_blog_option( absint($microsite_blog_id), 'siteurl', network_site_url() . $microsite['microsite_slug'] );

                // update blog home
                update_blog_option( absint($microsite_blog_id), 'home', network_site_url() . $microsite['microsite_slug'] );

                // update blog blogname
                update_blog_option( absint($microsite_blog_id), 'blogname', $microsite['microsite_name'] );

                // update blog path in wp_blogs table
                update_blog_details( absint($microsite_blog_id), array('path' => $microsite['microsite_slug']) );

            } else {
                unset($microsite['microsite_name']);
                unset($microsite['microsite_slug']);
            }

            // update blog admin email
            update_blog_option( absint($microsite_blog_id), 'admin_email', sanitize_email($this->input->post('microsite_admin_email', false)) );

            //update location page
            $microsite['microsite_post_id'] = $this->microsite_location($microsite, $microsite_blog_id);

            // update blog siteurl
            $microsite['microsite_updated'] = current_time('mysql');

            // check if microsite exist in sfl_microsite table
            $blog_exist = $this->db->get_where('sfl_microsites', ['microsite_blog_id' => $microsite_blog_id]);

            if( $blog_exist->num_rows() > 0 ){
                $this->db->where('microsite_blog_id', $microsite_blog_id);
                $update = $this->db->update('sfl_microsites', $microsite);
            }
            else{
                $microsite['microsite_blog_id'] = $microsite_blog_id;
                $update = $this->db->insert('sfl_microsites', $microsite);
            }

            sf_clear_cloud_cache();
            return $update;
        }
    }

    private function microsite_location($microsite, $blog_id = false)
    {
        if ( absint($blog_id) === 1 ) return;

        global $wpdb;

        extract($microsite);

            $state = sanitize_title($microsite_state_province);
            $state = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts WHERE post_name = '{$state}' AND post_type = 'location' AND post_parent = 0");
            if ( ! $state ) {
                $state = wp_insert_post([
                    'post_title'    => $microsite_state_province,
                    'post_name'     => $state,
                    'post_type'     => 'location',
                    'post_status'   => 'publish',
                ]);
            }

            $city = sanitize_title($microsite_city);
            $city = $wpdb->get_var("SELECT ID FROM {$wpdb->prefix}posts WHERE post_name = '{$city}' AND post_type = 'location' AND post_parent = $state");

            if ( ! $city ) {
                $city = wp_insert_post([
                    'post_title'    => $microsite_city,
                    'post_name'     => $city,
                    'post_parent'   => $state,
                    'post_type'     => 'location',
                    'post_status'   => 'publish',
                ]);
            }

            if ( ! $blog_id ) {
                $location = wp_insert_post([
                    'post_title'    => $microsite_name,
                    'post_name'     => $microsite_slug,
                    'post_parent'   => $city,
                    'post_type'     => 'location',
                    'post_status'   => 'publish',
                ]);
            } else {
                $location = $this->db->get_where('sfl_microsites', ['microsite_blog_id' => $blog_id]);

                if( $location->num_rows() > 0 ){
                    $location = wp_update_post([
                        'ID'            => isset($location->row()->microsite_post_id) ? $location->row()->microsite_post_id:'',
                        'post_title'    => $microsite_name,
                        'post_name'     => $microsite_slug,
                        'post_parent'   => $city,
                        'post_type'     => 'location',
                        'post_status'   => 'publish',
                    ]);
                }
                else{
                    $location = wp_insert_post([
                        'post_title'    => $microsite_name,
                        'post_name'     => $microsite_slug,
                        'post_parent'   => $city,
                        'post_type'     => 'location',
                        'post_status'   => 'publish',
                    ]);
                }

            }

        return $location;
    }

    public function save_notifications($blog_id, $template)
    {
        global $wpdb;

        if( $template === 'usa' ){
            $sfmu_mail = '_sfmu_mail_2';
        }
        else{
            $sfmu_mail = '_sfmu_mail_68';
        }

        // get posts with post_type ff_forms of main site
        $main_forms = gss_get_posts(1, ['post_type'=>'ff_forms', 'posts_per_page'=>-1]);

        // get default notifications
        $defaults = $wpdb->get_results( "SELECT post_id, meta_value FROM `wp_postmeta` WHERE `meta_key` = '{$sfmu_mail}'", ARRAY_A );

        // get posts with post_type ff_forms of current microsite
        $microsite_forms = gss_get_posts($blog_id, ['post_type'=>'ff_forms', 'posts_per_page'=>-1]);

        //storage
        $stored = array();

        foreach ($main_forms as $main_form) {
            
            switch_to_blog($blog_id);
                if( post_exists($main_form->post_title) ){
                    $post = get_page_by_title( $main_form->post_title, OBJECT, 'ff_forms' );
                }
            restore_current_blog();

            if( $post && !in_array($post->post_title, $stored) ){
                switch_to_blog(1);

                    $cf7_form = get_post_meta( $main_form->ID, 'form_shortcode', true );
                    $value = get_post_meta( $cf7_form, $sfmu_mail, true );
                    
                    update_post_meta($cf7_form, '_sfmu_mail_'.$blog_id, $value);
                restore_current_blog();
            }

            $stored[] = $post->post_title;

        }

        // draft these forms
        foreach ($microsite_forms as $microsite_form) {
            $draft = array(
                    'Holiday Gift Form',
                    'Inclement Weather Policy',
                    'Party Waiver',
                    'Policies Document',
                );

            if(in_array($microsite_form->post_title, $draft)){
                switch_to_blog($blog_id);
                    wp_update_post( ['ID'=>$microsite_form->ID, 'post_status'=>'private'] );
                restore_current_blog();
            }
        }

    }

    public function get_roles($user_id)
    {
        global $wpdb;
        $roles = array();

        $this->db->where("user_id", absint($user_id));
        $this->db->like('meta_key', 'capabilities', 'before');
        $this->db->like('meta_value', 'franchisor');
        $franchisor = $this->db->get("{$wpdb->prefix}usermeta");

        if ( $franchisor->result() ) {
            $roles[] = 'franchisor';
        }

        $this->db->where("user_id", absint($user_id));
        $this->db->like('meta_key', 'capabilities', 'before');
        $this->db->like('meta_value', 'franchisee');
        $franchisee = $this->db->get("{$wpdb->prefix}usermeta");

        if ( $franchisee->result() ) {
            $roles[] = 'franchisee';
        }

        return $roles;

    }

    /*
    * Adds/Updates Microsite's Social Media
    * @param mixed - array $_POST, int blog_id
    */
    public function add_socialmedia($arr, $id)
    {
        unset($arr['microsite_socials']); // unset submit button
        $update = update_blog_option($id, 'microsite_socialmedia', $arr);
        sf_clear_cloud_cache();
        return $update;
    }

    /*
    * Adds/Updates Microsite's Pricing Table
    * @param mixed - array $_POST, int blog_id
    */
    public function update_pricing($arr, $id)
    {
        unset($arr['gss_pricing']); // unset submit button
        $gss_pricing = update_blog_option($id, 'gss_pricing', $arr );
        sf_clear_cloud_cache();
        return;
    }

    /*
    * Add/Remove Users
    * @param array|int - $_POST, blog_id
    */
    public function add_users($arr, $blog_id)
    {
        unset($arr['gss_users']);

        foreach ($arr['assigned_user'] as $user) {
            add_user_to_blog($blog_id, $user, 'editor');
        }

        redirect( base_url('microsites/users') );
        return;
    }

    /*
    * Announcement Settings
    * @param array|int - $_POST, blog_id
    */
    public function announcement_settings($arr, $blog_id)
    {
        update_blog_option($blog_id, 'ff_announcements_settings', $arr['ff_announcements_settings']);
        sf_clear_cloud_cache();
        return;
    }

    /*
    * Adds/Updates Microsite's News
    * @param array|int|mixed - $_POST, blog_id
    */
    public function gss_news($arr, $id, $update=false)
    {

        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_news = array(
                'post_title'      => $arr['news_title'],
                'post_content'    => $arr['news_content'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 'ff_news',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $news = wp_insert_post( $add_news );
            }
            else {
                $add_news['ID'] = $update;
                $news = wp_update_post( $add_news );
            }

            if( isset($arr['tax_input']['gss_news_categories']) && count($arr['tax_input']['gss_news_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_news_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $news, $cat_ids, 'gss_news_categories' );
            }

            unset($arr['news_title']);
            unset($arr['news_content']);
            unset($arr['tax_input']);
            unset($arr['gss_news']);

            if( ! array_key_exists('gss_seo_meta_nofollow', $arr) ){
                $arr['gss_seo_meta_nofollow'] = 'off';
            }
            if( ! array_key_exists('gss_seo_meta_noindex', $arr) ){
                $arr['gss_seo_meta_noindex'] = 'off';
            }

            foreach ($arr as $key => $value) {
                update_post_meta( $news, $key, $value );
            }

        restore_current_blog();

        if( $news ) {
            sf_clear_cloud_cache();
            return $news;
        }
        else {
            return false;
        }
    }

    /*
    * Adds/Updates Microsite's Job Listings
    * @param array|int|mixed - $_POST, blog_id
    */
    public function job_listings($arr, $id, $update=false)
    {

        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_job_listings = array(
                'post_title'      => $arr['job_title'],
                'post_content'    => $arr['job_description'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 'ff_job_listings',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $job_listings = wp_insert_post( $add_job_listings );
            }
            else {
                $add_job_listings['ID'] = $update;
                $job_listings = wp_update_post( $add_job_listings );
            }

            // uncomment this area if we have categories in this post type
            /*if( isset($arr['tax_input']['gss_job_listings_categories']) && count($arr['tax_input']['gss_job_listings_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_job_listings_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $job_listings, $cat_ids, 'gss_job_listings_categories' );
            }*/

            unset($arr['job_title']);
            unset($arr['job_description']);
            //unset($arr['tax_input']);
            unset($arr['gss_job_listings']);

            if ( ! array_key_exists('_gss_post_hidden', $arr) ){
                $arr['_gss_post_hidden'] = '';
            } else {
                $arr['_gss_post_hidden'] = 1;
            }

            foreach ($arr as $key => $value) {
                update_post_meta( $job_listings, $key, $value );
            }

        restore_current_blog();

        if( $job_listings ) {
            sf_clear_cloud_cache();
            return $job_listings;
        }
        else {
            return false;
        }
    }

    /*
    * Adds/Updates Microsite's Staff
    * @param array|int|mixed - $_POST, blog_id
    */
    public function add_staff($arr, $id, $update=false)
    {

        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_staff = array(
                'post_title'      => $arr['staff_name'],
                'post_content'    => $arr['staff_description'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 's8_staff_member',
                'post_author'     => $user->ID,
                'menu_order'      => $arr['menu_order'] ?: 0,
            );

            if( ! $update ) {
                $staff = wp_insert_post( $add_staff );
            }
            else {
                $add_staff['ID'] = $update;
                $staff = wp_update_post( $add_staff );
            }

            // uncomment this area if we have categories in this post type
            /*if( isset($arr['tax_input']['gss_staff_categories']) && count($arr['tax_input']['gss_staff_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_staff_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $staff, $cat_ids, 'gss_staff_categories' );
            }*/

            unset($arr['staff_name']);
            unset($arr['staff_description']);
            unset($arr['menu_order']);
            //unset($arr['tax_input']);
            unset($arr['gss_staff']);

            foreach ($arr as $key => $value) {
                update_post_meta( $staff, $key, $value );
            }

        restore_current_blog();

        if( $staff ) {
            sf_clear_cloud_cache();
            return $staff;
        }
        else {
            return false;
        }
    }

    /*
    * Adds/Updates Microsite's Announcements
    * @param array|int|mixed - $_POST, blog_id
    */
    public function gss_announcements($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_announcement = array(
                'post_title'      => $arr['announcement_title'],
                'post_content'    => $arr['announcement_description'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 'ff_announcements',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $announcement = wp_insert_post( $add_announcement );
            }
            else {
                $add_announcement['ID'] = $update;
                $announcement = wp_update_post( $add_announcement );
            }

            // uncomment this area if we have categories in this post type
            /*if( isset($arr['tax_input']['gss_announcement_categories']) && count($arr['tax_input']['gss_announcement_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_announcement_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $announcement, $cat_ids, 'gss_announcement_categories' );
            }*/

            unset($arr['announcement_title']);
            unset($arr['announcement_description']);
            //unset($arr['tax_input']);
            unset($arr['gss_announcement']);

            foreach ($arr as $key => $value) {
                update_post_meta( $announcement, $key, $value );
            }

        restore_current_blog();

        if( $announcement ) {
            sf_clear_cloud_cache();
            return $announcement;
        }
        else {
            return false;
        }
    }

    /*
    * Adds/Updates Microsite's Promos
    * @param array|int|mixed - $_POST, blog_id
    */
    public function gss_promos($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_promos = array(
                'post_title'      => $arr['promo_title'],
                'post_content'    => $arr['promo_content'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 'ff_promos',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $promo = wp_insert_post( $add_promos );
            }
            else {
                $add_promos['ID'] = $update;
                $promo = wp_update_post( $add_promos );
            }

            // uncomment this area if we have categories in this post type
            /*if( isset($arr['tax_input']['gss_promo_categories']) && count($arr['tax_input']['gss_promo_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_promo_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $promo, $cat_ids, 'gss_promo_categories' );
            }*/

            unset($arr['promo_title']);
            unset($arr['promo_content']);
            //unset($arr['tax_input']);
            unset($arr['gss_promo']);

            foreach ($arr as $key => $value) {
                if( $key === '_ff_promo_startDate' || $key === '_ff_promo_endDate' ) {
                    $date = date_format(date_create($value),"Y-m-d");
                    update_post_meta( $promo, $key, $date );
                }
                else {
                    update_post_meta( $promo, $key, $value );
                }
            }

        restore_current_blog();

        if( $promo ) {
            sf_clear_cloud_cache();
            return $promo;
        }
        else {
            return false;
        }
    }

    /*
    * Adds/Updates Microsite's Testimonials
    * @param array|int|mixed - $_POST, blog_id
    */
    public function gss_testimonials($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_testimonial = array(
                'post_title'      => $arr['testimonial_title'],
                'post_content'    => $arr['testimonial_description'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 's8_testimonials',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $testimonial = wp_insert_post( $add_testimonial );
            }
            else {
                $add_testimonial['ID'] = $update;
                $testimonial = wp_update_post( $add_testimonial );
            }

            // uncomment this area if we have categories in this post type
            /*if( isset($arr['tax_input']['gss_testimonial_categories']) && count($arr['tax_input']['gss_testimonial_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_testimonial_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $testimonial, $cat_ids, 'gss_testimonial_categories' );
            }*/

            unset($arr['testimonial_title']);
            unset($arr['testimonial_description']);
            //unset($arr['tax_input']);
            unset($arr['gss_testimonials']);

            foreach ($arr as $key => $value) {
                update_post_meta( $testimonial, $key, $value );
            }

        restore_current_blog();

        if( $testimonial ) {
            sf_clear_cloud_cache();
            return $testimonial;
        }
        else {
            return false;
        }
    }

    /*
    * Adds/Updates Microsite's FAQs
    * @param array|int|mixed - $_POST, blog_id
    */
    public function gss_faqs($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_faq = array(
                'post_title'      => $arr['faq_question'],
                'post_content'    => $arr['faq_answer'],
                'post_status'     => $arr['post_status'],
                'post_type'       => 'ff_faqs',
                'post_author'     => $user->ID,
                'menu_order'      => $arr['menu_order'] ?: 0,
            );

            if( ! $update ) {
                $faq = wp_insert_post( $add_faq );
            }
            else {
                $add_faq['ID'] = $update;
                $faq = wp_update_post( $add_faq );
            }

            // uncomment this area if we have categories in this post type
            /*if( isset($arr['tax_input']['gss_faq_categories']) && count($arr['tax_input']['gss_faq_categories']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_faq_categories'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $faq, $cat_ids, 'gss_faq_categories' );
            }*/

            unset($arr['faq_question']);
            unset($arr['faq_answer']);
            unset($arr['menu_order']);
            //unset($arr['tax_input']);
            unset($arr['gss_faqs']);

            foreach ($arr as $key => $value) {
                update_post_meta( $faq, $key, $value );
            }

        restore_current_blog();

        if( $faq ) {
            sf_clear_cloud_cache();
            return $faq;
        }
        else {
            return false;
        }
    }

    public function search( $key, $offset = 1 )
    {
        global $wpdb;

        switch_to_blog(1);
            $this->db->select('ID, post_title, post_date');
            $this->db->like('post_title', $key);
            $this->db->like('post_content', $key);
            $this->db->where('post_type', 'ff_faqs');
            $result = $this->db->get("{$wpdb->prefix}posts");

            if( $result->num_rows() > 0 ){
                return $result->result();
            }
            else{
                return false;
            }
        restore_current_blog();
    }

    /*
    * Retrieve Microsite NAP data
    * @param int - blog_id
    */
    public function get_microsite($microsite_blog_id)
    {
        $microsite = $this->db->get_where('sfl_microsites', array('microsite_blog_id' => $microsite_blog_id) )->row();

        if ( ! $microsite ) {
            return;
        }

        $microsite->microsite_admin_email = get_blog_option($microsite_blog_id, 'admin_email');
        $microsite->details = get_blog_details($microsite_blog_id);

        return $microsite;
    }

    public function delete_microsite($microsite_blog_id)
    {
        if ( ! current_user_can( 'delete_sites' ) ) {
            return;
        }

        $microsite = $this->get_microsite($microsite_blog_id);

        if ( $microsite ) {
            global $wpdb;
            require_once ABSPATH . 'wp-admin/includes/ms.php';

            wpmu_delete_blog( $microsite_blog_id, true );
            $this->db->delete('sfl_microsites', array('microsite_blog_id' => $microsite_blog_id));
            if ( $extratables = $this->db->query("SELECT TABLE_NAME AS name FROM INFORMATION_SCHEMA.TABLES  WHERE TABLE_NAME LIKE '{$wpdb->prefix}{$microsite_blog_id}%'") ) {
                $this->load->dbforge();
                foreach ( $extratables->result() as $extra ) {
                    $this->dbforge->drop_table($extra->name, TRUE);
                }
            }

            if ( absint($this->microsite) === absint($microsite_blog_id) )
            {
                delete_cookie('selected_microsite');
            }

            wp_delete_post( $microsite->microsite_post_id, true );

            // delete upload folder to save space
            $upload_dir = wp_upload_dir();
            $dir = $upload_dir['basedir'] . '/sites/' . $microsite_blog_id;
            if (is_dir($dir)) { 
                $objects = scandir($dir); 
                foreach ($objects as $object) { 
                    if ($object != "." && $object != "..") { 
                        if (is_dir($dir."/".$object))
                            rmdir($dir."/".$object);
                        else
                            unlink($dir."/".$object); 
                    } 
                }

                rmdir($dir); 
            } 
        }
    }

    public function has_mainsite_nap()
    {
        $mainsite = $this->db->get_where('sfl_microsites', array('microsite_blog_id' => 1))->row();

        if ( ! $mainsite ) {
            return false;
        } else {
            return true;
        }
    }

    public function create_default_nap()
    {
        $site = get_blog_details(1);
        $mainsite = array(
            'microsite_blog_id' => 1,
            'microsite_date' => $site->registered,
            'microsite_updated' => $site->last_updated,
            'microsite_name' => $site->blogname,
            'microsite_slug' => $site->path,
            'microsite_street' => '390 Park Street',
            'microsite_street2' => 'Suite #250',
            'microsite_city' => 'Birmingham',
            'microsite_state_province' => 'Michigan',
            'microsite_zip_postal' => '48009',
            'microsite_phone' => '(800) 856-5120',
            'microsite_lat' => '42.5487955',
            'microsite_lng' => '-83.2130485'
        );

        $this->db->insert('sfl_microsites', $mainsite);
    }

    public function validate_microsite_arr($microsite_blog_id = false)
    {
        $this->form_validation->set_rules(
            'microsite_name',
            'Site Title',
            'required',
            array('required' => 'You must provide a %s.')
        );

        $this->form_validation->set_rules(
            'microsite_slug',
            'Site Slug',
            array(
                'required',
                array('validate_blog_signup', array($this, 'validate_blog_signup'))
            ),
            array(
                'required' => 'You must provide a %s.'
            )
        );

        $this->form_validation->set_rules('microsite_street', 'Street', 'required',
            array('required' => 'You must provide a %s.'));
        $this->form_validation->set_rules('microsite_city', 'City', 'required',
            array('required' => 'You must provide a %s.'));
        $this->form_validation->set_rules('microsite_state_province', 'State / Province', 'required',
            array('required' => 'You must provide a %s.'));
        $this->form_validation->set_rules('microsite_zip_postal', 'Zip / Postal', 'required',
            array('required' => 'You must provide a %s.'));
        $this->form_validation->set_rules('microsite_admin_email', 'Admin Email', 'required|valid_email',
            array(
                'required' => 'You must provide a %s.',
                'valid_email' => 'Invalid email.'
            )
            );

        if ($this->form_validation->run() !== false) {
            return true;
        }

        return false;
    }

    public function validate_blog_signup()
    {
        if ( ! isset($this->uri->segments[4]) ) {
            $microsite_blog_id = null;
        } else {
            $microsite_blog_id = $this->uri->segments[4];
        }

        if ( $microsite_blog_id === 1 ) {
            return true;
        }

        //validate if microsite slug duplicates
        $slug_check = $this->slug_check($this->input->post('microsite_slug', false));

        if( ! $slug_check ){
            return true;
        }
        else{
            if( $slug_check[0]->microsite_blog_id === $this->uri->segment(4) ){
                return true;
            }
            else{
                $this->form_validation->set_message('validate_blog_signup', 'Given slug already exist.');
                return false;
            }
        }

        if ( isset($blog) && $microsite_blog_id ) {
            $match = get_blog_details($blog['blogname']);

            if ( $match && absint($match->blog_id) === absint($microsite_blog_id) ) {
                return true;
            }
        }

        if ( isset($blog) && ($blog['errors']->get_error_message() !== "" && $blog['errors']->get_error_message() !== 'Sorry, that site is reserved!') ) {

            $this->form_validation->set_message('validate_blog_signup', $blog['errors']->get_error_message());
            return false;
        } else {
            return true;
        }

    }

    public function slug_check($slug)
    {
        $check = $this->db->get_where('sfl_microsites', array('microsite_slug' => $slug));

        if( $check->num_rows() > 0 ){
            return $check->result();
        }
        else{
            return false;
        }
    }

    public function save_user($user_id = false)
    {

        if ( $user_id ) {
            $user = wpmu_create_user(
                sanitize_title($this->input->post('user[username]'), true),
                sanitize_text_field($this->input->post('user[password]', true)),
                $this->input->post('user[email]', true)
            );
        }

        if ( ! empty($this->input->post('user[role]')) ) {
            wp_update_user(['ID' => $user_id, 'role' => $this->input->post('user[role]', true)]);
        } else {
            remove_user_from_blog($user_id, 1);
        }

    }

    private function create_tables($microsite)
    {
        global $wpdb;

        switch_to_blog($microsite);
            $prefix = $wpdb->prefix;
        restore_current_blog();

        $this->load->dbforge();

        $this->dbforge->add_field(array(
            'id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 10,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ),
            'form_id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 10,
                'null'              => FALSE,
            ),
            'post_id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 10,
                'null'              => FALSE,
            ),
            'date_created' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'is_starred' => array(
                'type'              => 'TINYINT',
                'constraint'        => 1,
                'null'              => FALSE,
            ),
            'is_read' => array(
                'type'              => 'TINYINT',
                'constraint'        => 1,
                'null'              => FALSE,
            ),
            'ip' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 39,
                'null'              => FALSE,
            ),
            'source_url' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 200,
                'null'              => FALSE,
            ),
            'user_agent' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 250,
                'null'              => FALSE,
            ),
            'currency' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 5,
                'null'              => FALSE,
            ),
            'payment_status' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 15,
                'null'              => FALSE,
            ),
            'payment_date' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'payment_amount' => array(
                'type'              => 'DECIMAL',
                'constraint'        => 19,2,
                'null'              => FALSE,
            ),
            'payment_method' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 30,
                'null'              => FALSE,
            ),
            'transaction_id' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 50,
                'null'              => FALSE,
            ),
            'is_fulfilled' => array(
                'type'              => 'TINYINT',
                'constraint'        => 1,
                'null'              => FALSE,
            ),
            'created_by' => array(
                'type'              => 'BIGINT',
                'constraint'        => 20,
                'null'              => FALSE,
            ),
            'transaction_type' => array(
                'type'              => 'TINYINT',
                'constraint'        => 1,
                'null'              => FALSE,
            ),
            'status' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 20,
                'null'              => FALSE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table("{$prefix}rg_lead");

        $this->dbforge->add_field(array(
            'id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 10,
                'unsigned'          => TRUE,
                'auto_increment'    => TRUE
            ),
            'lead_id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 10,
                'null'              => FALSE,
            ),
            'form_id' => array(
                'type'              => 'BIGINT',
                'constraint'        => 10,
                'null'              => FALSE,
            ),
            'field_name' => array(
                'type'              => 'VARCHAR',
                'constraint'        => 100,
                'null'              => FALSE,
            ),
            'value' => array(
                'type'              => 'LONGTEXT',
                'null'              => FALSE,
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('lead_id');
        $this->dbforge->add_key('form_id');
        $this->dbforge->create_table("{$prefix}sfl_lead_detail");

    }

    public function dir_clone($new, $template)
    {
        $src = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/sites/{$template}";
        $dest  = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/uploads/sites/{$new}";

        // create site's upload folder
        if( !is_dir($dest) )
            mkdir($dest);

        foreach (scandir($src) as $file) {

            if (!is_readable($src . '/' . $file)) continue;

            if (is_dir($src .'/' . $file) && ($file != '.') && ($file != '..') ) {
                if( !is_dir($dest . '/' . $file) )
                    mkdir($dest . '/' . $file);

                if( is_array(scandir($src . '/' .$file)) && !empty(scandir($src . '/' .$file)) ){
                    foreach (scandir($src . '/' .$file) as $subfolder) {
                        if( !is_dir($dest . '/' . $file . '/' .$subfolder) )
                            @mkdir($dest . '/' . $file . '/' .$subfolder);

                        if( count( @scandir($src . '/' . $file . '/' .$subfolder)) > 2 ){
                            foreach ( scandir($src . '/' . $file . '/' . $subfolder) as $subfolder_files ) {
                                if( !is_dir($src . '/' . $file . '/' . $subfolder . '/' . $subfolder_files) && $subfolder_files != '.' && $subfolder_files != '..' )
                                    copy($src . '/' . $file . '/' . $subfolder . '/' . $subfolder_files, $dest . '/' . $file . '/' . $subfolder . '/' . $subfolder_files);
                            }
                        }
                    }
                }

            }
        }

        return;
    }
}
