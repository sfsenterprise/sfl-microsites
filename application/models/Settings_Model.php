<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /*
    * Adds/Updates Microsite's Sidebar
    * @param array|int|mixed - $_POST, blog_id
    */
    public function add_sidebar($arr, $id, $update=false)
    {

        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_sidebar = array(
                'post_title'      => $arr['sidebar_name'],
                'post_content'    => $arr['sidebar_content'],
                'post_status'     => 'publish',
                'post_type'       => 'gss_sidebars',
                'post_author'     => $user->ID,
            );

            if( ! $update ) {
                $sidebar = wp_insert_post( $add_sidebar );
            }
            else {
                $add_sidebar['ID'] = $update;
                $sidebar = wp_update_post( $add_sidebar );
            }

            unset($arr['sidebar_name']);
            unset($arr['sidebar_content']);
            unset($arr['gss_sidebar']);

            if( ! array_key_exists('sidebar_hidetitle', $arr) ){
                $arr['sidebar_hidetitle'] = '';
            }

            if( ! array_key_exists('sidebar_hidewidget', $arr) ){
                $arr['sidebar_hidewidget'] = '';
            }

            if( ! array_key_exists('hide_inner_page_settings', $arr) ){
                $arr['hide_inner_page_settings'] = '';
            }

            foreach ($arr as $key => $value) {
                update_post_meta( $sidebar, $key, $value );
            }

        restore_current_blog();

        if( $sidebar ) {
            sf_clear_cloud_cache();
            return $sidebar;
        }
        else {
            return false;
        }
    }

}
