<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function save_user($user_id = false)
    {
        if ( ! $user_id ) {
            switch_to_blog($this->microsite);
            $user_id = wp_create_user(
                sanitize_title($this->input->post('user[username]', true)),
                sanitize_text_field($this->input->post('user[password]', true)),
                $this->input->post('user[email]', true)
            );

            wp_update_user(['ID' => $user_id, 'role' => $this->input->post('user[role]', true)]);
            restore_current_blog();
        } else {
            $userdata = array();
            $userdata['ID'] = $user_id;
            $userdata['user_email'] = $this->input->post('user[email]', true);
            $userdata['role'] = $this->input->post('user[role]', true);
            if ( $this->input->post('user[password]') ) {
                $userdata['user_pass'] = $this->input->post('user[password]', true);
            }
            switch_to_blog($this->microsite);
            wp_update_user($userdata);
            restore_current_blog();
        }

        return $user_id;
    }

    public function valid_email($user_email)
    {
        if ( $this->data['edit_user']->user_email === $user_email ) {
            return true;
        }

        global $wpdb;
        $taken = $this->db->get_where("{$wpdb->prefix}users", array('user_email' => $user_email))->row();

        if ( $taken ) {
            $this->form_validation->set_message('is_unique_email', 'The {field} is already taken.');
            return false;
        }

        return true;
    }
}
