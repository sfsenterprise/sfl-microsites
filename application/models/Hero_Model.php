<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hero_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function save($blog_id = false)
    {

        unset($_POST['update']);

        foreach($_POST as $option => $value) {
            update_blog_option($blog_id, $option, $value);
        }
        sf_clear_cloud_cache();
        return;

    }
}
