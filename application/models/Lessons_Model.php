<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lessons_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function gss_lessons($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

        switch_to_blog( $id );
            $add_lessons = array(
                'post_title'    => $arr['lesson_name'],
                'post_status'   => $arr['post_status'],
                'post_type'     => 'gss_swim_lessons',
                'post_author'   => $user->ID,
                'menu_order'    => $arr['menu_order'],
            );

            if( ! $update ) {
                $job_lessons = wp_insert_post( $add_lessons );
            }
            else {
                $add_lessons['ID'] = $update;
                $job_lessons = wp_update_post( $add_lessons );
            }

            if( isset($arr['tax_input']['gss_swim_lesson_groups']) && count($arr['tax_input']['gss_swim_lesson_groups']) > 0 ){
                $cat_ids = array_map( 'intval', $arr['tax_input']['gss_swim_lesson_groups'] );
                $cat_ids = array_unique( $cat_ids );
                $term_taxonomy_ids = wp_set_object_terms( $job_lessons, $cat_ids, 'gss_swim_lesson_groups' );
            }

            unset($arr['lesson_name']);
            unset($arr['tax_input']);
            unset($arr['gss_swim_lessons']);
            unset($arr['menu_order']);

            if( ! array_key_exists('gss_push_to_sites', $arr) ){
                $arr['gss_push_to_sites'] = '';
            }
            else {
                $this->microsite_push($job_lessons);
            }

            foreach ($arr as $key => $value) {
                update_post_meta( $job_lessons, $key, $value );
            }

        restore_current_blog();

        if( $job_lessons ) {
            sf_clear_cloud_cache();
            return $job_lessons;
        }
        else {
            return false;
        }
    }

    public function microsite_push($post_id)
    {
        $sites = array();
        $post_cats = array();
        $microsites = get_sites(['site__not_in'=>array(1)]);
        $current_user = wp_get_current_user();

        switch_to_blog( 1 );
            $post = get_post($post_id);
            $categories = get_the_terms( $post->ID, 'gss_swim_lesson_groups' );
        restore_current_blog();

        foreach ($microsites as $microsite) {

            switch_to_blog( $microsite->blog_id );

                // checks if term exist, if not create tax
                foreach ($categories as $category) {
                    $term = term_exists($category->name, 'gss_swim_lesson_groups');

                    // term dont exist then create one
                    if( ! $term ){
                        $term = wp_insert_term(
                                    $category->name, // the term
                                    'gss_swim_lesson_groups', // the taxonomy
                                    array(
                                        'description'   => $category->description,
                                        'slug'          => sanitize_title($category->name),
                                        'parent'        => $category->parent,
                                    )
                                );

                        $post_cats[] = $term['term_id'];
                    }
                    else {
                        // term exist get id
                        $post_cats[] = $term['term_id'];
                    }

                }

                // checks if post exists by slug
                $lesson = get_page_by_path($post->post_name, OBJECT, 'gss_swim_lessons');

                if( ! $lesson ) {

                    $data = array(
                            'post_title'    => $post->post_title,
                            'post_name'     => $post->post_name,
                            'post_status'   => $post->post_status,
                            'post_author'   => $current_user->ID,
                            'post_type'     => 'gss_swim_lessons',
                        );

                    $result = wp_insert_post($data);

                    $sites[] = $result;
                }
                else {

                    $data = array(
                            'ID'            => $lesson->ID,
                            'post_title'    => $post->post_title,
                            'post_name'     => $post->post_name,
                        );

                    $result = wp_update_post($data);

                    $sites[] = $result;
                }

                // assigning terms to the post
                wp_set_object_terms( $result, $post_cats, 'gss_swim_lesson_groups' );

            restore_current_blog();

        }

        if( count($sites) === count($microsites) ){
            return true;
        }
        else {
            return false;
        }

    }

    public function gss_lesson_group($arr, $id, $update=false)
    {
        global $wpdb;
        $user = wp_get_current_user();

        switch_to_blog( $id );

            if( ! $update ) {
                $lesson_group = wp_insert_term(
                        $arr['group_name'],
                        'gss_swim_lesson_groups',
                        array(
                            'description'   => $arr['group_description'],
                            'slug'          => ($arr['group_slug']) ?: sanitize_title($arr['group_name']),
                            'parent'        => $arr['group_parent']
                        )
                );

                //get last row object id
                $this->db->select("object_id");
                $this->db->order_by('object_id', 'DESC');
                $this->db->limit(1);
                $object = $this->db->get("{$wpdb->prefix}term_relationships");
                $object = $object->result();

                $data = array(
                        'object_id'         => (int)$object[0]->object_id + 1,
                        'term_taxonomy_id'  => $lesson_group['term_taxonomy_id'],
                        'term_order'        => $arr['sort_lesson']
                    );
                $this->db->insert("{$wpdb->prefix}term_relationships", $data);
            }
            else {
                $lesson_group = wp_update_term(
                        $update,
                        'gss_swim_lesson_groups',
                        array(
                            'name'          => $arr['group_name'],
                            'slug'          => ($arr['group_slug']) ?: sanitize_title($arr['group_name']),
                            'parent'        => $arr['group_parent'],
                            'description'   => $arr['group_description'],
                        )
                );

                if( isset($arr['sort_lesson']) ){
                    $this->db->set('term_order', $arr['sort_lesson'], FALSE);
                    $this->db->where('term_taxonomy_id', $update);
                    $this->db->update("{$wpdb->prefix}term_relationships");
                }

            }

            unset($arr['group_name']);
            unset($arr['group_description']);
            unset($arr['group_slug']);
            unset($arr['group_parent']);

            foreach ($arr as $key => $value) {
                update_term_meta($lesson_group['term_id'], $key, $value);
            }

        restore_current_blog();

        if( $lesson_group ) {
            return $lesson_group;
        }
        else {
            return false;
        }
    }

    public function get_term_order($term_id)
    {
        if( $term_id ){
            $this->db->distinct();
            $this->db->select('term_order');
            $this->db->where('term_taxonomy_id', $term_id);
            $term_order = $this->db->get('wp_term_relationships');

            if( $term_order->num_rows() > 0 ){
                return $term_order->result();
            }
        }

        return;
    }

}
