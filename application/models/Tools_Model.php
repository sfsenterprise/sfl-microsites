<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_redirections($args)
    {
        global $wpdb;

        if( is_array($args) )
            extract($args);

        switch_to_blog( $microsite );

            if( $this->db->table_exists("{$wpdb->prefix}redirection_items") ){
                
                if(isset($limit) && $limit)
                    $this->db->limit($limit, $offset);

                if(isset($orderby) && $orderby){
                    $this->db->order_by($orderby, $order);
                }
                else{
                    $this->db->order_by('last_access', 'DESC');
                }

                if(isset($search))
                    $this->db->like('url', $search);
                
                $redirects = $this->db->get("{$wpdb->prefix}redirection_items");
            }
            
        restore_current_blog();

        if( isset($redirects) && $redirects->num_rows() > 0 ) {
            return $redirects->result();
        }
        else {
            return false;
        }
    }

    public function add_redirection($microsite, $post)
    {   
        global $wpdb;

        $tables = $this->check_tables($microsite);

        if( $tables || is_array($tables) ){

            switch_to_blog($microsite);
                $this->db->select("position");
                $this->db->order_by('position', 'DESC');
                $this->db->limit(1);
                $query = $this->db->get("{$wpdb->prefix}redirection_items");

                $position = $query->result();

            restore_current_blog();

            $data = array(
                'url'           => isset($post['url']) ? $post['url'] : '',
                'position'      => isset($post['position']) ? $post['position'] :  ($position) ? (int)$position[0]->position + 1 : 0,
                'group_id'      => isset($post['group']) ? $post['group'] : '',
                'action_type'   => isset($post['type']) ? $post['type'] : 'url',
                'action_code'   => isset($post['code']) ? $post['code'] : '301',
                'action_data'   => isset($post['action_data']) ? $post['action_data'] : '',
                'match_type'    => isset($post['match_type']) ? $post['match_type'] : 'url'
            );

            switch_to_blog($microsite);
                $redirect = $this->db->insert("{$wpdb->prefix}redirection_items", $data);
            restore_current_blog();

            return $redirect;
        }
        else{
            return false;
        }
    }

    public function action($microsite, $arr, $action)
    {
        global $wpdb;

        $microsite = $microsite?:1;

        switch_to_blog($microsite);

            if( $action === 'delete' ){
                foreach( $arr as $item ){
                    $this->db->where('id', $item);
                    $this->db->delete("{$wpdb->prefix}redirection_items");
                }
            }
            elseif( $action === 'enable' || $action === 'disable' ){
                foreach( $arr as $item ){
                    $this->db->set('status', $action .'d');
                    $this->db->where('id', $item);
                    $this->db->update("{$wpdb->prefix}redirection_items");
                }
            }
            else{
                foreach( $arr as $item ){
                    $this->db->set('last_count', 0);
                    $this->db->set('last_access', '');
                    $this->db->where('id', $item);
                    $this->db->update("{$wpdb->prefix}redirection_items");
                }
            }

        restore_current_blog();

        return;
    }

    private function check_tables($microsite)
    {
        global $wpdb;

        switch_to_blog( $microsite );
            $tables = array(
                "{$wpdb->prefix}redirection_items"      => $this->db->table_exists("{$wpdb->prefix}redirection_items"),
                "{$wpdb->prefix}redirection_groups"     => $this->db->table_exists("{$wpdb->prefix}redirection_groups"),
                "{$wpdb->prefix}redirection_logs"       => $this->db->table_exists("{$wpdb->prefix}redirection_logs"),
                "{$wpdb->prefix}redirection_404"        => $this->db->table_exists("{$wpdb->prefix}redirection_404")
            );
        restore_current_blog();
        
        if( in_array(false, $tables) || in_array('', $tables) ){
            return $this->table_create($microsite);
        }
        else{
            return true;
        }
    }

    private function table_create($microsite)
    {
        global $wpdb;

        $this->load->dbforge();

        $tables = array();
        $charset_collate = $this->get_charset();

        switch_to_blog($microsite);
            $prefix = $wpdb->prefix;
        restore_current_blog();

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'auto_increment' => TRUE
            ),
            'url' => array(
                'type' => 'MEDIUMTEXT',
                'null' => FALSE,
            ),
            'regex' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'default' => '0'
            ),
            'position' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'default' => '0'
            ),
            'last_count' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'null' => FALSE,
                'default' => '0'
            ),
            'last_access' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'group_id' => array(
                'type' => 'INT',
                'null' => FALSE,
                'default' => '0'
            ),
            'status' => array(
                'type' => 'ENUM("enabled","disabled")',
                'null' => FALSE,
                'default' => 'enabled'
            ),
            'action_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => FALSE,
            ),
            'action_code' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'action_code' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'action_data' => array(
                'type' => 'MEDIUMTEXT',
                'null' => FALSE,
            ),
            'match_type' => array(
                'type' => 'VARCHAR',
                'constraint' => 20,
                'null' => FALSE,
            ),
            'title' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE,
                'default' => ''
            ),
        ));

        $this->dbforge->add_key('id', TRUE);
        //$this->dbforge->add_key('url');
        $this->dbforge->add_key('status');
        $this->dbforge->add_key('regex');
        $tables['redirection_items'] = $this->dbforge->create_table("{$prefix}redirection_items", TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => 50,
                'null' => TRUE,
            ),
            'tracking' => array(
                'type' => 'INT',
                'constraint' => 11,
                'null' => TRUE,
                'default' => '1'
            ),
            'module_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
                'default' => '0'
            ),
            'status' => array(
                'type' => 'ENUM("enabled", "disabled")',
                'null' => TRUE,
                'default' => 'enabled'
            ),
            'position' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'default' => '0'
            ),
        ));
        
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('module_id');
        $this->dbforge->add_key('status');
        $tables['redirection_groups'] = $this->dbforge->create_table("{$prefix}redirection_groups", TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'auto_increment' => TRUE
            ),
            'created' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'url' => array(
                'type' => 'MEDIUMTEXT',
                'null' => TRUE,
            ),
            'sent_to' => array(
                'type' => 'MEDIUMTEXT',
            ),
            'agent' => array(
                'type' => 'MEDIUMTEXT',
                'null' => FALSE,
            ),
            'referrer' => array(
                'type' => 'MEDIUMTEXT',
            ),
            'redirection_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => TRUE,
            ),
            'ip' => array(
                'type' => 'VARCHAR',
                'constraint' => 17,
                'null' => FALSE,
                'default' => ''
            ),
            'module_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'group_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'default' => '0'
            ),
        ));
    
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('created');
        $this->dbforge->add_key('redirection_id');
        $this->dbforge->add_key('ip');
        $this->dbforge->add_key('group_id');
        $this->dbforge->add_key('module_id');
        $tables['redirection_logs'] = $this->dbforge->create_table("{$prefix}redirection_logs", TRUE);

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'null' => FALSE,
                'auto_increment' => TRUE
            ),
            'created' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'url' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => FALSE,
                'default' => ''
            ),
            'agent' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => ''
            ),
            'referrer' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => ''
            ),
            'ip' => array(
                'type' => 'INT',
                'constraint' => 10,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_key('created');
        $this->dbforge->add_key('url');
        $this->dbforge->add_key('ip');
        $this->dbforge->add_key('referrer');
        $tables['redirection_404'] = $this->dbforge->create_table("{$prefix}redirection_404", TRUE);

        return $tables;

    }

    private function get_charset() {
        global $wpdb;

        $charset_collate = '';
        if ( ! empty( $wpdb->charset ) ) {
            $charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
        }

        if ( ! empty( $wpdb->collate ) ) {
            $charset_collate .= " COLLATE=$wpdb->collate";
        }

        return $charset_collate;
    }

}