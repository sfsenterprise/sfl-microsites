<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Json_Export_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->db_export = $this->another = $this->load->database('export',TRUE);
    }

    public function in_between_leads()
    {
        $start_date = '2018-01-15 18:55:00';
        $leads = array();

        // Start ID of leads per microsite
        $leads_per_microsite = array(
            0  => 87573,
            3  => 3664,
            4  => 2342,
            7  => 4487,
            8  => 2488,
            9  => 6598,
            10 => 3819,
            11 => 2260,
            12 => 2538,
            13 => 2544,
            16 => 6682,
            18 => 2363,
            21 => 5176,
            22 => 4076,
            25 => 9226,
            29 => 4544,
            30 => 4507,
            32 => 3092,
            34 => 38855,
            35 => 4319,
            36 => 3331,
            38 => 3123,
            42 => 2154,
            47 => 890,
            49 => 771,
            50 => 1732,
            51 => 1212,
            52 => 1174,
            62 => 1304,
            64 => 601,
            66 => 653,
            67 => 282,
            70 => 1001,
            74 => 643,
            78 => 636,
            85 => 304,
            88 => 207,
            95 => 41,
        );

        $sites = get_sites();
        $this->db_export->order_by('id', 'asc');

        foreach($sites as $site) {
            $microsite_id = absint($site->blog_id) === 1 ? 0 : absint($site->blog_id);

            $prefix = 'wp_' . ($microsite_id !== 0 ? $microsite_id . '_' : '');
            $lead_table = $prefix . 'rg_lead';
            $lead_detail_table = $prefix . 'sfl_lead_detail';
            $lead_query = $this->db_export->get_where($lead_table, ['date_created >=' => $start_date]);
            $leads[$microsite_id] = $lead_query->result_array();
            foreach( $leads[$microsite_id] as $row => $lead) {
                $lead_detail_query = $this->db_export->get_where($lead_detail_table, ['lead_id' => $lead['id']]);
                $leads[$microsite_id][$row]['lead_detail'] = $lead_detail_query->result_array();
            }
        }

        // foreach($leads_per_microsite as $microsite_id => $lead_id) {
        //     $prefix = 'wp_' . ($microsite_id !== 0 ? $microsite_id . '_' : '');
        //     $lead_table = $prefix . 'rg_lead';
        //     $lead_detail_table = $prefix . 'sfl_lead_detail';
        //     // $lead_query = $this->db_export->get_where($lead_table, ['id >=' => $lead_id]);
        //     $lead_query = $this->db_export->get_where($lead_table, ['date_created >=' => $start_date]);
        //     $leads[$microsite_id] = $lead_query->result_array();
        //     foreach( $leads[$microsite_id] as $row => $lead) {
        //         $lead_detail_query = $this->db_export->get_where($lead_detail_table, ['lead_id' => $lead['id']]);
        //         $leads[$microsite_id][$row]['lead_detail'] = $lead_detail_query->result_array();
        //     }
        // }

        if ( ! $leads ) echo 'No leads found.';

        return $leads;
    }

    public function download_in_between_leads($leads)
    {
        header('Content-disposition: attachment; filename=in_between_leads.json');
        header('Content-type: application/json');
        echo json_encode($leads);
        exit;
    }

    public function import_in_between_leads($leads_per_microsite)
    {
        ob_start();
        $total = 0;
        foreach( $leads_per_microsite as $microsite_id => $leads ) {
            $total = $total + count($leads);
        }
        echo $total . '<br />';
        ob_flush();
        foreach( $leads_per_microsite as $microsite_id => $leads ) {
            $prefix = 'wp_' . ($microsite_id !== 0 ? $microsite_id . '_' : '');
            $lead_table = $prefix . 'rg_lead';
            $lead_detail_table = $prefix . 'sfl_lead_detail';
            $blog = get_blog_details($microsite_id);

            if ( $microsite_id === 0 ) {
                echo 'Mainsite <br />';
            } else {
                echo $blog->blogname . ' | ID: ' . $microsite_id . '<br/>';
            }
            echo 'LEAD ID | DATE CREATED | IP<br/>';
            ob_flush();

            foreach($leads as $lead) {
                $verify = $this->db->get_where($lead_table, ['date_created' => $lead['date_created'], 'ip' => $lead['ip']]);
                if ( $verify->num_rows() === 0 ) {
                    $details = $lead['lead_detail'];
                    unset($lead['lead_detail']);
                    unset($lead['id']);

                    // Insert the leads into the database
                    $this->db->insert($lead_table, $lead);
                    $lead_id = $this->db->insert_id();
                    echo sprintf('%1$s | %2$s | %3$s<br/>', $lead_id, $lead['date_created'], $lead['ip']);
                    ob_flush();

                    foreach($details as $detail) {
                        unset($detail['id']);
                        $detail['lead_id'] = $lead_id;

                        // Insert lead details
                        $this->db->insert($lead_detail_table, $detail);
                    }

                    unset($details);
                }
            }

            echo '<hr />';
            ob_flush();
        }
        ob_end_flush();
    }
}
