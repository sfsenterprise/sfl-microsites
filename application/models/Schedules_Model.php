<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedules_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /*
    * Adds/Updates Microsite's Hours Operations
    * @param mixed - array $_POST, int blog_id
    */
    public function save_schedules($id, $data = array())
    {
        if ( ! $data ) {
            $data = $this->input->post(NULL, TRUE);
            unset($data['gss_hours']);
        }

        $schedules = array();
        $days = array(
            'mon' => 'monday',
            'tue' => 'tuesday',
            'wed' => 'wednesday',
            'thu' => 'thursday',
            'fri' => 'friday',
            'sat' => 'saturday',
            'sun' => 'sunday'
        );
        foreach ( $days as $day ) {
            // dd($data['hours_operation'][$day]);

            $schedules['hours_operation'][$day] = array(
                'active' => isset($data['hours_operation'][$day]['active']) ? 'on' : 'off',
                'hours' => isset($data['hours_operation'][$day]['hours']) ? $data['hours_operation'][$day]['hours'] : array(),
            );
            $schedules['family_swim'][$day] = array(
                'active' => isset($data['family_swim'][$day]['active']) ? 'on' : 'off',
                'hours' => isset($data['family_swim'][$day]['hours']) ? $data['family_swim'][$day]['hours'] : array(),
            );
            $schedules['swim_team'][$day] = array(
                'active' => isset($data['swim_team'][$day]['active']) ? 'on' : 'off',
                'hours' => isset($data['swim_team'][$day]['hours']) ? $data['swim_team'][$day]['hours'] : array(),
            );
            $schedules['party_hour'][$day] = array(
                'active' => isset($data['party_hour'][$day]['active']) ? 'on' : 'off',
                'hours' => isset($data['party_hour'][$day]['hours']) ? $data['party_hour'][$day]['hours'] : array(),
            );
        }

        if ( isset($data['_schedule_notif_email']) ){
            update_blog_option($id, '_schedule_notif_email', $data['_schedule_notif_email']);
            unset($data['_schedule_notif_email']);
        }

        update_blog_option($id, 'microsite_hours_operations', $schedules);
        sf_clear_cloud_cache();
    }

    public function get_schedules()
    {
        $days = array(
            'mon' => 'monday',
            'tue' => 'tuesday',
            'wed' => 'wednesday',
            'thu' => 'thursday',
            'fri' => 'friday',
            'sat' => 'saturday',
            'sun' => 'sunday'
        );

        foreach( $days as $day ) {
            $defaults['hours_operation'][$day] = array(
                'active' => 'off',
                'hours' => array()
            );
            $defaults['family_swim'][$day] = array(
                'active' => 'off',
                'hours' => array()
            );
            $defaults['swim_team'][$day] = array(
                'active' => 'off',
                'hours' => array()
            );
            $defaults['party_hour'][$day] = array(
                'active' => 'off',
                'hours' => array()
            );
        }

        $schedules = get_blog_option($this->microsite, 'microsite_hours_operations');
        if ( ! isset( $schedules ) ) {
            return $defaults;
        } else {
            return $schedules;
        }
    }
}
