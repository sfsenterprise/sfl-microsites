<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function add($arr)
    {

        $user = wp_get_current_user();

        if(isset($arr))
            extract($arr);

        $arr['url'] = base_url(sprintf('%1s/%2s', $module, ($action !== 'index')? $action:''));
        $arr['ip'] = $this->input->ip_address();

        switch ($module) {
            case 'microsites':
                $insert_logs = $this->microsite_logs($arr);
                break;
            case 'pricing':
                $insert_logs = $this->pricing_logs($arr);
                break;
            case 'schedules':
                $insert_logs = $this->schedule_logs($arr);
                $insert_logs = (isset($insert_logs['new_values'])) ? $insert_logs:array();
                break;
            case 'socialmedia':
                $insert_logs = $this->socialmedia_logs($arr);
                break;
            case 'sidebars':
                $insert_logs = $this->sidebar_logs($arr);
                break;
            case 'settings':
                $insert_logs = $this->normalize_logs($arr);
                break;
            default:
                $insert_logs = $arr;
                break;
        }

        if( ! empty($insert_logs) ){
            unset($insert_logs['franchise']);
            unset($insert_logs['module']);
            unset($insert_logs['action']);

            $log = $this->db->insert('sfl_logs', ['microsite'=>($franchise) ? $franchise:1, 'module'=>$module, 'user'=>$user->ID, 'action'=>$action, 'variables'=>maybe_serialize($insert_logs), 'date'=>current_time('mysql')]); 
        }
        else {
            return;
        }

    }

    private function microsite_logs($arr)
    {
        if(isset($arr))
            extract($arr);

        $posts = $this->input->post(NULL);

        if( ! empty($old_values) ){
            /* 
            *  removes unnecessary fields 
            *  from old value before saving to logs
            */
            unset($old_values->details);
            unset($old_values->ID);
            unset($old_values->microsite_blog_id);
            unset($old_values->microsite_fields);
            unset($old_values->microsite_date);
            unset($old_values->microsite_updated);

            $changes = array_diff_assoc($posts, (array)$old_values);
            unset($changes['microsite_edit']);

            $arr['new_values'] = $changes;

            // if have changes, get old values
            if( $changes ){
                foreach ($changes as $key => $value) {
                    $oldies[$key] = @$old_values->$key;
                }

                $arr['old_values'] = $oldies;
            }
        }
        else {
            $arr['franchise'] = $this->input->post('microsite_name');
            $arr['new_values'] = $this->input->post(null);
        }

        return $arr;
    }

    private function pricing_logs($arr)
    {

        $filtered_old_values = array();
        $new_values = $this->input->post(NULL);
        unset($new_values['gss_pricing']);

        foreach ($new_values as $new_key => $new_value) {
            if (isset($arr['old_values']) && ! empty($arr['old_values']) ){
                if ( array_key_exists($new_key, $arr['old_values']) ) {
                    $arr['new_values'][$new_key] = (is_array($new_values[$new_key]) && is_array($arr['old_values'][$new_key]) ) ? array_diff_assoc($new_values[$new_key], $arr['old_values'][$new_key]):'';
                }
            }
            else {
                foreach ($new_values as $sub_key => $sub_value) {
                    $arr['new_values'][$sub_key] = (is_array($sub_value)) ? array_filter($sub_value):'';
                }
            }
        }

        $arr['new_values'] = array_filter( $arr['new_values'] );

        if (isset($arr['old_values']) && ! empty($arr['old_values']) ){
            foreach ($arr['old_values'] as $old_key => $old_value) {
                if ( array_key_exists($old_key, array_filter($arr['new_values'])) ) {
                    foreach ( $arr['new_values'][$old_key] as $sub_key => $sub_val ) {
                        if ( array_key_exists($sub_key, $old_value) ) {
                            $filtered_old_values[$old_key][$sub_key] = $old_value[$sub_key];
                        }
                    }
                }
            }

            $arr['old_values'] = ($filtered_old_values) ?: '';
        }
        else {
            // if no option saved in dbase, assign empty to old_values sub values
            foreach ($arr['new_values'] as $new_key => $new_value) {
                if( is_array($new_value) ){
                    foreach ($new_value as $sub_new_key => $sub_new_val) {
                        $arr['old_values'][$new_key][$sub_new_key] = '';
                    }
                }
            }
        }

        return $arr;
    }

    private function schedule_logs($arr)
    {
        $posts = $this->input->post(NULL);
        unset($posts['gss_hours']);

        if (isset($arr['old_values']) && ! empty($arr['old_values']) ){
            // filtering the old values to be saved in logs
            foreach ($arr['old_values'] as $sub_key => $sub_val) {
                
                foreach ($sub_val as $day_key => $day_val) {

                    // unset day with active = off and hours = empty
                    if( $arr['old_values'][$sub_key][$day_key]['active'] === 'off' && empty($arr['old_values'][$sub_key][$day_key]['hours']) ){
                        unset($arr['old_values'][$sub_key][$day_key]);
                    }

                }
            }

            $arr['old_values'] = array_filter($arr['old_values']);
        }

        if( ! empty($posts) )
            $arr['new_values'] = array('Updated'=>'Multiple Fields');

        return $arr;
    }

    private function socialmedia_logs($arr)
    {
        $posts = $this->input->post(NULL);
        unset($posts['microsite_socials']);

        if( ! empty(array_filter($posts)) ){
            $arr['old_values'] = array_filter($arr['old_values']);

            $arr['new_values'] = array_filter(array_diff($posts, $arr['old_values']));

            $arr['old_values'] = array_diff($arr['old_values'], $posts);

        }

        if( empty($arr['new_values']) ){
            $arr['old_values'] = array_filter(array_diff(array_filter($arr['old_values']), $posts));

            if( ! empty($arr['old_values']) ){
                foreach ($arr['old_values'] as $key => $value) {
                    $arr['new_values'][$key] = '';
                }
            }
        }

        if( empty($arr['old_values']) ){

            foreach ($arr['new_values'] as $key => $value) {
                $arr['old_values'][$key] = '';
            }

        }

        return $arr;
    }

    private function sidebar_logs($arr)
    {
        $posts = $this->input->post(NULL);
        unset($posts['gss_sidebars_update']);

        if( is_array($arr['new_values']) && ! empty($arr['new_values']) ){
            foreach ($arr['new_values'] as $key => $value) {
                if( array_key_exists($key, $arr['old_values']) ){
                    $arr['old_values'][$key] = $arr['old_values'][$key];
                }
            }
        }
        elseif( ! isset($_POST['gss_sidebar_homepage']) || ! isset($_POST['gss_sidebar_frontend']) ){
            return $arr;
        }
        else {
            $arr['new_values'] = (isset($posts['gss_sidebar_homepage'])) ? $posts['gss_sidebar_homepage']:$posts['gss_sidebar_frontend'];
        }

        return $arr;
 
    }

    private function normalize_logs($arr)
    {

        if( $arr['action'] === 'footer_logos' ) {
            if( is_array($arr['new_values']['footer_logos']) && !empty($arr['new_values']['footer_logos']) ){
                foreach ($arr['new_values']['footer_logos'] as $footer_key => $footer_val) {
                    if( array_key_exists($footer_key, $arr['old_values']['footer_logos']) ){
                        $arr['new_values']['footer_logos'][$footer_key] = array_diff_assoc($footer_val, $arr['old_values']['footer_logos'][$footer_key]);
                    }
                }
            }

            if( is_array($arr['old_values']['footer_logos']) && !empty($arr['old_values']['footer_logos']) ){
                foreach ($arr['old_values']['footer_logos'] as $footer_key => $footer_val) {
                    if( array_key_exists($footer_key, $arr['new_values']['footer_logos']) ){
                        $arr['old_values']['footer_logos'][$footer_key] = array_diff_assoc($footer_val, $arr['new_values']['footer_logos'][$footer_key]);
                    }
                }
            }

            $arr['new_values']['footer_logos'] = array_filter($arr['new_values']['footer_logos']);
        }
        else {
            $arr['new_values'] = array_diff_assoc($arr['new_values'], $arr['old_values']);
            $arr['old_values'] = array_intersect_key($arr['old_values'], $arr['new_values']);
        }

        return $arr;
    }

    public function all_logs($limit=null, $offset=0)
    {

        $filterby = isset($_GET['filterby']) ? $_GET['filterby']:'';
        $filter = isset($_GET['filter']) ? $_GET['filter']:'';

        if( $filterby === 'microsite' && $filter ){
            $mcirosite = $this->get_microsite($filter);

            if( $mcirosite ){
                $this->db->where($filterby, $mcirosite[0]->microsite_blog_id);
            }
            elseif( stripos($filter, 'gold') || stripos($filter, 'swim') ){
                $this->db->where($filterby, 1);
            }            
        }
        elseif( $filterby === 'module' && $filter ){
            $this->db->like($filterby, $filter);
        }
        elseif( $filterby === 'date' && $filter ){
            $date = date_format(date_create($filter), "Y-m-d");
 
            $this->db->like($filterby, $date);
        }           
        
        if($limit)
            $this->db->limit($limit, $offset);
         
        $this->db->order_by("ID", "desc"); 
        $logs = $this->db->get('sfl_logs');

        return $logs->result();
    }

    private function get_microsite($mcirosite)
    {
        $this->db->like('microsite_name', $mcirosite);
        $result = $this->db->get('sfl_microsites');

        if( $result->num_rows() > 0 ){
            return $result->result();
        }
        else{
            return false;
        }
        
    }

}