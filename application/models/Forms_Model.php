<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function gss_forms($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

            $add_form = array(
                'post_title'        => (isset($arr['name'])) ? $arr['name']:'',
                'post_status'       => (isset($arr['post_status'])) ? $arr['post_status']:'publish',
                'post_content'      => (isset($arr['content'])) ? $arr['content']:'',
                'post_type'         => 'ff_forms',
                'post_author'       => $user->ID,
                'post_name'         => $arr['slug'] ?: sanitize_title($arr['slug'])
            );

            switch_to_blog( $id );

                if( ! $update ) {
                    $form = wp_insert_post( $add_form );
                }
                else {
                    $add_form['ID'] = $update;
                    $form = wp_update_post( $add_form );
                }

            restore_current_blog();

                if( $form ){
                    unset($arr['name']);
                    unset($arr['slug']);
                    unset($arr['content']);
                    unset($arr['gss_forms']);
                    unset($arr['wpcf7-mailchimp']);

                    foreach ($arr as $key => $value) {
                        if( $key === '_sfmu_mail' && isset($arr['form_shortcode']) ){

                            if( (int)$this->microsite === 1 ){
                                switch_to_blog( 1 );
                                    $main = get_post_meta( $arr['form_shortcode'], '_sfmu_mail', true );

                                    if( is_array($main) && ! empty($main) ){
                                        $notifs = array_merge($main, $arr['_sfmu_mail']);
                                        update_post_meta( $arr['form_shortcode'], '_sfmu_mail',  $notifs);
                                    }
                                    else{
                                        update_post_meta( $arr['form_shortcode'], '_sfmu_mail',  $arr['_sfmu_mail']);
                                    }
                                restore_current_blog();
                            }
                            else{
                                switch_to_blog( 1 );
                                    update_post_meta( $arr['form_shortcode'], '_sfmu_mail_'.$this->microsite,  $arr['_sfmu_mail']);
                                restore_current_blog();
                            }

                        }
                        else{
                            switch_to_blog( $id );
                                update_post_meta( $form, $key, $value );
                            restore_current_blog();
                        }

                    }
                    sf_clear_cloud_cache();
                    return $form;
                }
                else {
                    return false;
                }
    }

    public function change_status($blog_id, $forms=array(), $deactivate=array())
    {

        $user = wp_get_current_user();

        switch_to_blog( $blog_id );

            if( !empty($forms) ){
                foreach ($forms['publish'] as $form) {

                    $post = get_post( $form );

                    $post->post_status = 'publish';
                    $update = wp_update_post( $post );
                }
            }

            if( !empty($deactivate) ){
                foreach ($deactivate as $form) {
                    $post = get_post( $form );

                    $post->post_status = 'draft';
                    $update = wp_update_post( $post );
                }
            }

        restore_current_blog();

        if( $user->ID ){
            $updated_by = update_blog_option( $blog_id, '_forms_updated_by', $user->ID );
        }

        sf_clear_cloud_cache();
        return $update;
    }

    public function add_form($arr, $id, $update=false)
    {
        $user = wp_get_current_user();

            $add_form = array(
                'post_title'        => (isset($arr['name'])) ? $arr['name']:'',
                'post_status'       => 'publish',
                'post_type'         => 'wpcf7_contact_form',
                'post_author'       => $user->ID,
                'post_name'         => sanitize_title($arr['name'])
            );

            if( ! $update ) {
                $form = wp_insert_post($add_form);
            }
            else {
                $form = wp_update_post(
                        array(
                            'ID'            => $update,
                            'post_title'    => $arr['name']
                        )
                    );
            }

            if( $form ) {
                update_post_meta( $form, '_form', $arr['wpcf7_form'] );
                update_post_meta( $form, '_messages', $arr['wpcf7-messages'] );
                update_post_meta( $form, 'redirect_page', $arr['redirect_page'] );

                $main = get_post_meta( $update, '_sfmu_mail', true );
                if( $main ){
                    unset($main['main']);
                }

                if( is_array($main) && ! empty($main) ){
                    $notifs = array_merge($arr['_sfmu_mail'], $main);
                    update_post_meta( $form, '_sfmu_mail', $notifs);
                }
                else{
                    update_post_meta( $form, '_sfmu_mail',  $arr['_sfmu_mail']);
                }

                sf_clear_cloud_cache();
                return $form;
            }
            else {
                return false;
            }
    }

    public function form_settings($microsite, $arr)
    {
        unset($arr['form_settings']);

        foreach( $arr as $key => $value ){
            update_blog_option($microsite, $key, $value);
        }
        sf_clear_cloud_cache();
    }

    public function microsite_updates($blog_id, $arr)
    {
        $current_forms = gss_get_posts( $blog_id, ['post_type'=>'ff_forms', 'post_status'=>array('publish', 'draft')]);
        $user = wp_get_current_user();

        switch_to_blog( $blog_id );

            foreach ($current_forms as $form) {

                if( ! in_array($form->ID, $arr) ){
                    $form->post_status = 'draft';
                    $update = wp_update_post( $form );
                } else {
                    $form->post_status = 'publish';
                    $update = wp_update_post( $form );
                }

            }

        restore_current_blog();

        if( $user->ID ){
            $updated_by = update_blog_option( $blog_id, '_forms_updated_by', $user->ID );
        }

        return;
    }

    public function form_leads($form_id, $microsite, $count_only, $limit=10, $offset=1)
    {
        global $wpdb;
        $this->load->helper('leads_helper');

        switch_to_blog(1);
            $cf7_form = get_post($form_id);
        restore_current_blog();

        switch_to_blog($microsite);
            //$rg_form = $this->db->get_where("{$wpdb->prefix}rg_form", array('title' => $cf7_form->post_title));
            $rg_form = $this->get_rg_form($form_id, $microsite);

            if( $cf7_form->post_title === 'Pre-Registration Form - Full (with additional kids)' ){
                $rg_title = 'Pre-Registration Form - Full';

                if( $this->db->table_exists("{$wpdb->prefix}rg_form") ){
                    $this->db->select('id');
                    $this->db->where('title =', $rg_title);
                    $old_rgform = $this->db->get("{$wpdb->prefix}rg_form");

                    if( $old_rgform->num_rows() > 0 ){
                        $old_rgform_id = $old_rgform->result();
                    }
                }
            }

            if( isset($_GET['search']) && $_GET['search'] ){
                $search_leads = array();
                $rg_search_leads = array();
                $searchby = explode('-', $_GET['searchby']);

                $this->db->distinct();
                $this->db->select('sfl.lead_id, sfl.form_id');
                $this->db->from("{$wpdb->prefix}sfl_lead_detail as sfl");
                $this->db->where('value = ', $_GET['search']);
                $this->db->where('form_id = ', $form_id);
                $this->db->like('field_name', $searchby[0]);
                $this->db->order_by('lead_id', 'DESC');
                $search = $this->db->get();

                // search lead details from rg_lead_details
                if( count($searchby) > 1 && isset($searchby[1]) ){
                    $field_number = (float)str_replace('_', '.', $searchby[1]);
                    
                    $rg_search = array();
                    $this->db->distinct();
                    $this->db->select('rg.lead_id, rg.form_id');
                    $this->db->from("{$wpdb->prefix}rg_lead_detail as rg");
                    $this->db->where('value = ', $_GET['search']);
                    $this->db->where('form_id = ', $rg_form[0]->id);
                    $rg_search = $this->db->get();

                    if( $cf7_form->post_title === 'Pre-Registration Form - Full (with additional kids)' && isset($old_rgform_id) ){
                        $rg_old_search = array();
                        $this->db->distinct();
                        $this->db->select('rg.lead_id, rg.form_id');
                        $this->db->from("{$wpdb->prefix}rg_lead_detail as rg");
                        $this->db->where('value = ', $_GET['search']);
                        $this->db->where('form_id = ', $old_rgform_id[0]->id);
                        $rg_old_search = $this->db->get();
                    }
                        
                    if( isset($rg_search) && isset($rg_old_search) && ($rg_search->num_rows() > 0 || $rg_old_search->num_rows() > 0) ){
                        $rg_form_leads = array_merge($rg_search->result(), $rg_old_search->result());
                    }

                    if( isset($rg_form_leads) && !empty($rg_form_leads) ){
                        foreach ($rg_form_leads as $value) {
                            $object = new stdClass();
                            $object->id = $value->lead_id;
                            $object->form_id = $value->form_id;
                            $object->date_created = get_lead_date_created($value->lead_id, $microsite);

                            $rg_search_leads[] = $object;
                        }

                    }
                }

                if( !$count_only ){
                    foreach ($search->result() as $value) {
                        $object = new stdClass();
                        $object->id = $value->lead_id;
                        $object->form_id = $value->form_id;
                        $object->date_created = get_lead_date_created($value->lead_id, $microsite);

                        $search_leads[] = $object;
                    }

                    if( isset($rg_search_leads) ){
                        $output = array_merge($search_leads, $rg_search_leads);
                    }
                    else{
                        $output = $search_leads;
                    }

                }
                else{
                    $output = count($search_leads);
                }
            }
            else{

                if( $cf7_form->post_title === 'Pre-Registration Form - Full (with additional kids)' ){
                    $rg_title = 'Pre-Registration Form - Full';

                    if( $this->db->table_exists("{$wpdb->prefix}rg_form") ){
                        $this->db->select('id');
                        $this->db->where('title =', $rg_title);
                        $old_rgform = $this->db->get("{$wpdb->prefix}rg_form");

                        if( $old_rgform->num_rows() > 0 ){
                            $old_rgform_id = $old_rgform->result();
                        }
                    }
                }
                
                if( $count_only ){
                    $this->db->where('form_id', ($rg_form) ? $rg_form[0]->id : $cf7_form->ID);
                    $this->db->or_where('form_id = ', $cf7_form->ID);

                    if( isset($old_rgform_id) )
                        $this->db->or_where('form_id = ', $old_rgform_id[0]->id);

                    $this->db->from("{$wpdb->prefix}rg_lead");
                    $output = $this->db->count_all_results();
                }
                else{
                    $this->db->select('rg.id, rg.form_id, rg.post_id, rg.date_created');
                    $this->db->from("{$wpdb->prefix}rg_lead as rg");
                    $this->db->where('form_id = ', ($rg_form) ? $rg_form[0]->id : $cf7_form->ID);
                    $this->db->or_where('form_id = ', $cf7_form->ID);

                    if( isset($old_rgform_id) )
                        $this->db->or_where('form_id = ', $old_rgform_id[0]->id);

                    $this->db->order_by('date_created', 'DESC');
                    $this->db->limit($limit, $offset);
                    $rg_leads = $this->db->get();

                    if( $rg_leads || $rg_leads->num_rows() ){
                        $output = $rg_leads->result();
                    }
                    else{
                        $output = false;
                    }
                }
            }

        restore_current_blog();

        return $output;
    }

    public function export_leads($cf7_id, $rg_id=null)
    {
        global $wpdb;

        switch_to_blog(1);
            $cf7_form = get_post($cf7_id);
        restore_current_blog();

        switch_to_blog($this->microsite);

            //$rg_form = $this->db->get_where("{$wpdb->prefix}rg_form", array('title' => $_POST['form_name']));
            //$rg_form = $rg_form->result();

            if( $cf7_form->post_title === 'Pre-Registration Form - Full (with additional kids)' ){
                $rg_title = 'Pre-Registration Form - Full';
                if( $this->db->table_exists("{$wpdb->prefix}rg_form") ){
                    $this->db->select('id');
                    $this->db->where('title =', $rg_title);
                    $old_rgform = $this->db->get("{$wpdb->prefix}rg_form");

                    if( $old_rgform->num_rows() > 0 ){
                        $old_rgform_id = $old_rgform->result();
                    }
                }
            }

            if($this->input->post('export_start') && $this->input->post('export_end')){

                $export_start =  date_format(date_create($this->input->post('export_start')), "Y-m-d");
                $export_end = date_format(date_create($this->input->post('export_end')), "Y-m-d");
                $rg_oldleads_rows = array();
                $rg_leads_rows = array();
                $cf7_leads_rows = array();

                if( isset($old_rgform_id) && is_array($old_rgform_id) && !empty($old_rgform_id) ){
                    $this->db->select('id, date_created');
                    $this->db->where('date_created >=', $export_start);
                    $this->db->where('date_created <=', $export_end);
                    $this->db->where('form_id = ', $old_rgform_id[0]->id);
                    $this->db->order_by('date_created', 'DESC');
                    $rg_oldleads = $this->db->get("{$wpdb->prefix}rg_lead");

                    if( $rg_oldleads->num_rows() > 0 ){
                        $rg_oldleads_rows = $rg_oldleads->result_array();
                    }
                }

                if( $rg_id ){
                    $this->db->select('id, date_created');
                    $this->db->where('date_created >=', $export_start);
                    $this->db->where('date_created <=', $export_end);
                    $this->db->where('form_id = ', $rg_id);
                    $this->db->order_by('date_created', 'DESC');
                    $rg_leads = $this->db->get("{$wpdb->prefix}rg_lead");

                    if( $rg_leads->num_rows() > 0 ){
                        $rg_leads_rows = $rg_leads->result_array();
                    }
                }
                
                if( $cf7_id ){
                    $this->db->select('id, date_created');
                    $this->db->where('date_created >=', $export_start);
                    $this->db->where('date_created <=', $export_end);
                    $this->db->where('form_id = ', $cf7_id);
                    $this->db->order_by('date_created', 'DESC');
                    $cf7_leads = $this->db->get("{$wpdb->prefix}rg_lead");

                    if( $cf7_leads->num_rows() > 0 ){
                        $cf7_leads_rows = $cf7_leads->result_array();
                    }
                }

                $leads = array(['all_leads'=>array_merge($cf7_leads_rows, $rg_leads_rows, $rg_oldleads_rows), 'form_name'=>$_POST['form_name']]);

                if( !empty($leads) ){
                    return $leads;
                }
                else{
                    return false;
                }

            }
            else{
                $this->db->select('id, date_created');
                $this->db->where('form_id = ', ($rg_id) ? $rg_id : $cf7_id);
                $this->db->or_where('form_id = ', $cf7_id);

                if( isset($old_rgform_id) )
                    $this->db->or_where('form_id = ', $old_rgform_id[0]->id);

                $this->db->order_by('date_created', 'DESC');
                $rg_leads = $this->db->get("{$wpdb->prefix}rg_lead");

                if( $rg_leads || $rg_leads->num_rows() ){
                    $leads = array(['all_leads'=>$rg_leads->result_array(), 'form_name'=>$_POST['form_name']]);

                    return $leads;
                }
                else{
                    return false;
                }
            }
            
        restore_current_blog();
    }

    public function lead_details($lead, $microsite, $result_array=false)
    {
        global $wpdb;

        switch_to_blog($microsite);
            if( $this->db->table_exists("{$wpdb->prefix}rg_lead_detail") ){

                $this->db->select('l.*, f.title');
                $this->db->from("{$wpdb->prefix}rg_lead_detail as l");
                $this->db->join("{$wpdb->prefix}rg_form as f", "l.form_id = f.id");
                $this->db->where('lead_id', $lead);
                $rg_details = $this->db->get();
            }

            if( $this->db->table_exists("{$wpdb->prefix}sfl_lead_detail") ){
                $sfl_details = $this->db->get_where("{$wpdb->prefix}sfl_lead_detail", array('lead_id' => $lead));
            }
        restore_current_blog();

        if( isset($rg_details) && $rg_details->num_rows() > 0 ){
            return ($result_array) ? $rg_details->result_array() : $rg_details->result();
        }
        else{
            return $sfl_details->result_array();
        }

    }

    public function get_forms($blog_id, $limit=null, $offset=1)
    {
        global $wpdb;

        if( $blog_id ){

            $leads_count = array();

            switch_to_blog(1);
                $cf7_forms = $this->db->order_by('post_date', 'DESC')->get_where("{$wpdb->prefix}posts", array('post_type' => 'wpcf7_contact_form'));
            restore_current_blog();

            if( $cf7_forms->num_rows() > 0 ){
                $forms = $cf7_forms->result();
            }

            if( $forms ){

                foreach ($forms as $form) {

                    switch_to_blog($blog_id);

                        if ($this->db->table_exists("{$wpdb->prefix}rg_form") ){
                            $this->db->select('id');
                            $this->db->from("{$wpdb->prefix}rg_form");
                            $this->db->where('title = ', $form->post_title);
                            $rg_form = $this->db->get()->result();
                        }

                        if ($this->db->table_exists("{$wpdb->prefix}rg_lead") ){

                            if( $form->post_title === 'Pre-Registration Form - Full (with additional kids)' ){
                                // get form id 3 - old pre-reg form
                                $rg_title = 'Pre-Registration Form - Full';

                                if( $this->db->table_exists("{$wpdb->prefix}rg_form") ){
                                    $this->db->select('id');
                                    $this->db->where('title =', $rg_title);
                                    $old_rgform = $this->db->get("{$wpdb->prefix}rg_form");

                                    if( $old_rgform->num_rows() > 0 ){
                                        $old_rgform_id = $old_rgform->result();
                                    }
                                }
                            }

                            // get count using old gravity form id
                            $this->db->where('form_id', isset($rg_form[0]->id) ? $rg_form[0]->id : '');

                            if( $form->post_title === 'Pre-Registration Form - Full (with additional kids)' && isset($old_rgform_id) )
                                $this->db->or_where('form_id', $old_rgform_id[0]->id);

                            $this->db->from("{$wpdb->prefix}rg_lead");
                            $rg_leads = $this->db->count_all_results();

                            // get count using given cf7 form id
                            $this->db->where('form_id', $form->ID);
                            $this->db->from("{$wpdb->prefix}rg_lead");
                            $sfl_leads = $this->db->count_all_results();

                        }

                        if( isset($rg_leads) || isset($sfl_leads ) ){
                            $leads_count[$form->ID] = $rg_leads + $sfl_leads;
                        }

                    restore_current_blog();

                }

                if( $limit ){
                    $leads_count = array_chunk($leads_count, $limit, true);

                    if( !empty($leads_count) )
                        return $leads_count[$offset - 1];
                }
                else{
                    return $leads_count;
                }



            }
            else{
                return false;
            }
        }

    }

    public function get_rg_form($form_id, $microsite)
    {
        global $wpdb;

        switch_to_blog(1);
            $cf7_form = get_post($form_id);
        restore_current_blog();

        switch_to_blog($microsite);
            if( $this->db->table_exists("{$wpdb->prefix}rg_form") ){
                $rg_form = $this->db->get_where("{$wpdb->prefix}rg_form", array('title' => $cf7_form->post_title));
            }
        restore_current_blog();

        if( isset($rg_form) && $rg_form->num_rows() > 0 ){
            return $rg_form->result();
        }
        else{
            switch_to_blog(1);
                if( $this->db->table_exists("{$wpdb->prefix}rg_form") ){
                    $rg_form = $this->db->get_where("{$wpdb->prefix}rg_form", array('title' => $cf7_form->post_title));
                }
            restore_current_blog();      

            if( isset($rg_form) && $rg_form->num_rows() > 0 ){
                return $rg_form->result();
            }
            else{
                return false;
            }
            
        }
    }

}
