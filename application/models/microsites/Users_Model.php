<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_roles($user_id)
    {
        global $wpdb;
        $roles = array();

        $this->db->where("user_id", absint($user_id));
        $this->db->like('meta_key', 'capabilities', 'before');
        $this->db->like('meta_value', 'franchisor');
        $franchisor = $this->db->get("{$wpdb->prefix}usermeta");

        if ( $franchisor->result() ) {
            $roles[] = 'franchisor';
        }

        $this->db->where("user_id", absint($user_id));
        $this->db->like('meta_key', 'capabilities', 'before');
        $this->db->like('meta_value', 'franchisee');
        $franchisee = $this->db->get("{$wpdb->prefix}usermeta");

        if ( $franchisee->result() ) {
            $roles[] = 'franchisee';
        }

        return $roles;

    }

    public function save_user($user_id = false) {
        global $wpdb;

        if ( $this->input->post('user[role]') === 'su' && !is_super_admin() ) {
            $this->data['error_message'] = 'You cannot add/edit/delete a super admin.';
            return;
        }

        if ( ! $user_id ) {
            $user = wpmu_create_user(
                sanitize_title($this->input->post('user[username]', true)),
                sanitize_text_field($this->input->post('user[password]', true)),
                $this->input->post('user[email]', true)
            );

            // Add role to the microsites user
            if ( $this->input->post('user[role]') !== 'none' ) {
                wp_update_user(['ID' => $user, 'role' => $this->input->post('user[role]', true)]);
            } else {
                wp_update_user(['ID' => $user, 'role' => 'subscriber']);
            }

            $this->data['message'] = 'Microsite user added.';

        } else {
            $args = array('ID' => $user_id);

            if ( $this->input->post('user[email]') ) {
                $args['user_email'] = $this->input->post('user[email]');
            }

            if ( $role = $this->input->post('user[role]') ) {
                $blogs = get_blogs_of_user($user_id);

                if ( $role === 'none') {
                    if ( count($blogs) === 1 && in_array($blogs, array($blogs, 'userblog_id' => 1)) ) {
                        wp_update_user(['ID' => $user_id, 'role' => 'subscriber']);
                    } else {
                        foreach( $blogs as $blog ) {
                            if ( absint($blog->userblog_id) === 1 ) {
                                $this->data['edit_user']->set_role('subscriber');
                            } else {
                                switch_to_blog(absint($blog->userblog_id));
                                $siteuser = get_userdata($user_id);
                                $siteuser->remove_role('administrator');
                                $siteuser->remove_cap('franchisee');
                                $siteuser->remove_cap('franchisor');
                                $siteuser->set_role('editor');
                            }
                        }
                        restore_current_blog();
                    }

                    if ( is_super_admin(absint($user_id)) ) {
                        revoke_super_admin($user_id);
                    }

                } elseif ( $role === 'su' ) {
                    if ( count($blogs) > 0) {
                        foreach( $blogs as $blog ) {
                            if ( absint($blog->userblog_id) === 1 ) {
                                $this->data['edit_user']->set_role('administrator');
                            } else {
                                switch_to_blog(absint($blog->userblog_id));
                                $siteuser = get_userdata($user_id);
                                $siteuser->set_role('administrator');
                            }
                        }
                        restore_current_blog();
                    }
                    grant_super_admin(absint($user_id));

                } else {
                    if ( count($blogs) > 0) {
                        foreach( $blogs as $blog ) {
                            switch_to_blog($blog->userblog_id);
                            $siteuser = get_userdata($user_id);
                            if ( $role === 'franchisor' ) {
                                $siteuser->set_role('franchisor');
                                $siteuser->remove_cap('franchisee');
                                if ( $blog->userblog_id === 1) {
                                    update_user_meta($user_id, "wp_user_level", 9);
                                } else {
                                    update_user_meta($user_id, "wp_{$blog->userblog_id}_user_level", 9);
                                }
                            } else {
                                $siteuser->set_role('franchisee');
                                $siteuser->remove_cap('franchisor');
                                if ( $blog->userblog_id === 1) {
                                    update_user_meta($user_id, "wp_user_level", 8);
                                } else {
                                    update_user_meta($user_id, "wp_{$blog->userblog_id}_user_level", 8);
                                }
                            }
                        }
                        restore_current_blog();

                    }

                    if ( is_super_admin(absint($user_id)) ) {
                        revoke_super_admin($user_id);
                    }
                }
            }

            if ( $this->input->post('user[password]') ) {
                wp_set_password($this->input->post('user[password]'), absint($user_id) );
            }

            $user = wp_update_user( $args );
            $this->data['message'] = 'Microsite user updated.';
        }

        if ( $this->input->post('sendmail') ) {
            // add email send script
        }

        return $user;

    }

    public function valid_email($user_email)
    {
        if ( $this->data['edit_user']->user_email === $user_email ) {
            return true;
        }

        global $wpdb;
        $taken = $this->db->get_where("{$wpdb->prefix}users", array('user_email' => $user_email))->row();

        if ( $taken ) {
            $this->form_validation->set_message('is_unique_email', 'The {field} is already taken.');
            return false;
        }

        return true;
    }

}
