<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages_Model extends CI_Model {

    public function get_posts_count($post = 'post', $default = false)
    {
        $count = new \StdClass;

        $args = array(
            'post_type' => $post,
            'posts_per_page' => -1,
        );

        $args['post_status'] = 'publish';

        if ( $default ) {
            $args['meta_key'] = '_sfmu_default_page';
            $args['meta_value'] = 'yes';
        }

        $publish = new \WP_Query($args);

        $args['post_status'] = 'draft';
        $draft = new \WP_Query($args);

        $args['post_status'] = 'pending';
        $pending = new \WP_Query($args);

        $args['post_status'] = 'trash';
        $trash = new \WP_Query($args);

        $args['post_status'] = array('publish', 'draft', 'pending');
        $default = new \WP_Query(array(
            'post_type' => $post,
            'posts_per_page' => -1,
            'meta_key' => '_sfmu_default_page',
            'meta_value' => 'yes',
        ));

        $count->publish = $publish->post_count ?: 0;
        $count->draft = $draft->post_count ?: 0;
        $count->pending = $pending->post_count ?: 0;
        $count->trash = $trash->post_count ?: 0;
        $count->default = $default->post_count ?: 0;
        $count->all = $count->publish + $count->draft + $count->pending;
        wp_reset_query();

        return $count;
    }

    public function search($search, $offset=1)
    {
        global $wpdb;

        switch_to_blog(1);
            $this->db->select('*');
            $this->db->like('post_title', $search);
            $this->db->like('post_content', $search);
            $this->db->where('post_type', 'page');
            $result = $this->db->get("{$wpdb->prefix}posts");

            if( $result->num_rows() > 0 ){
                return $result->result();
            }
            else{
                return false;
            }
        restore_current_blog();
    }

    public function get_pages($posts_per_page, $paged, $default_pages = false)
    {
        $pages = get_pages(['post_status' => 'publish,draft', 'sort_column' => 'menu_order post_title']);
        wp_reset_query();

        $pages = $this->arrange_pages($pages, $posts_per_page, $paged, $default_pages);

        $display_pages = array_chunk($pages['pages'], $posts_per_page);

        if ( !isset($display_pages[$paged - 1]) ) {
            return;
        }

        return array('pages' => $display_pages[$paged - 1], 'rows' => $pages['rows']);
    }

    private function arrange_pages($pages, $posts_per_page, $paged, $default_pages)
    {
        $display_pages = array();
        $row = 0;

        if ( $default_pages ) {
            foreach( $pages as $key => $page ) {
                if ( get_post_meta($page->ID, '_sfmu_default_page', true) !== 'yes' ) {
                    unset($pages[$key]);
                }
            }
            $pages = array_values($pages);
        }

        while($pages) {

            if ( ( count($display_pages) % absint($posts_per_page) ) === 0 ) {
                if ( absint($pages[$row]->post_parent) > 0 ) {
                    $display_pages[] = get_post($pages[$row]->post_parent);
                }
            }

            $display_pages[] = $pages[$row];
            unset($pages[$row]);
            ksort($pages);
            $row++;
        }

        return array('pages' => $display_pages, 'rows' => count($display_pages) );
    }


    public function save_page($postarr, $update = false)
    {
        if ( !isset($postarr['page_update']) ) {
            return;
        }

        unset($postarr['page_update']);

        $user = wp_get_current_user();

        switch_to_blog( 1 );

            if ( $update ) {

                $post_id = wp_update_post([
                    'ID'            => $update,
                    'post_title'    => $postarr['page_title'],
                    'post_content'  => $postarr['content'],
                    'post_author'   => $user->ID,
                    'post_type'     => 'page',
                    'post_status'   => (isset($postarr['status']))? $postarr['status']:'publish',
                    'post_parent'   => $postarr['parent_id'],
                ]);

            } else {

                $post_id = wp_insert_post([
                    'post_title'    => $postarr['page_title'],
                    'post_content'  => $postarr['content'],
                    'post_author'   => $user->ID,
                    'post_type'     => 'page',
                    'post_status'   => ($postarr['status'])?:'publish',
                    'post_parent'   => $postarr['parent_id'],
                ]);

                // push this page to all microsite post meta
                
            }

            $postarr['_sfmu_default_page'] = (isset($postarr['_sfmu_default_page'])) ? 'yes':'no';

            unset($postarr['page_title']);
            unset($postarr['content']);
            unset($postarr['post_type']);

            foreach($postarr as $key => $postmeta) {
                if ( $key === '_yoast_wpseo_meta-robots-adv' ) {
                    $adv = implode(',', $postmeta);
 
                    update_post_meta($post_id, $key, $adv);
                } 
                elseif( $key === '_sfmu_default_page' ){
                    update_post_meta($post_id, '_sfmu_default_page', $postmeta);
                }
                else {
                    update_post_meta($post_id, $key, $postmeta);
                }
            }

            if ( !array_key_exists('yoast_wpseo_meta-robots-noindex', $postarr) ) {
                update_post_meta($post_id, 'yoast_wpseo_meta-robots-noindex', '');
            }

            if ( !array_key_exists('yoast_wpseo_meta-robots-nofollow', $postarr) ) {
                update_post_meta($post_id, 'yoast_wpseo_meta-robots-nofollow', '');
            }

        restore_current_blog();

        if( $post_id ) {
            return $post_id;
        }
        else {
            return false;
        }
    }
}
