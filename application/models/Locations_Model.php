<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function get_locations($address)
    {
        $search = $this->input->get('search');

        if( isset($search) && $search && $by = $this->input->get('searchby') ){
            $this->db->distinct();
            $this->db->select('microsite_city, microsite_state_province, microsite_zip_postal');
            $this->db->group_by('microsite_city');
            $this->db->from('sfl_microsites');
            $this->db->like("microsite_{$by}", $search);

            if($this->uri->segment(3) == 'states')
                $this->db->limit(1);

            $location = $this->db->get();

            if( $location->num_rows() > 0 ){
                $locations = $location->result();
            }
            else{
                $locations = array();
            }
        }
        else{
            $locations = array();
            $all = get_pages(['hierarchical'=>1, 'post_type'=>'location']);

            if( $all ){
                foreach($all as $location){

                    if( $address === 'cities' ){
                        $parent_parent = get_post($location->post_parent);

                        if( $location->post_parent && (int)$parent_parent->post_parent === 0 ){
                            $locations[] = $location;
                        }
                    }
                    else{
                        if( ! $location->post_parent ){
                            $locations[] = $location;
                        }
                    }
                }
            }
        }

        return $locations;
    }

    public function update($arr, $microsite=1, $post)
    {
        switch_to_blog( $microsite );
            $update = wp_update_post(array(
                    'ID'            => $post,
                    'post_content'  => $arr['content'],
                    'post_parent'   => $arr['post_parent']
                ));
        restore_current_blog();

        if( $update ){
            sf_clear_cloud_cache();
            return $update;
        }
        else{
            return false;
        }
    }

}
