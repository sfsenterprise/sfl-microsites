    <div class="row">
        <div class="col">
            <div class="card">

                <div class="card-block">
                    <h4 class="card-title">Redirections</h4>
                    <?php echo form_open( 'tools/redirections' ); ?>
                        <div class="form-group row">
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-3">
                                        <select class="form-control" name="action" id="bulk-action-selector-top">
                                            <option value="">Bulk Actions</option>
                                            <option value="delete">Delete</option>
                                            <option value="enable">Enable</option>
                                            <option value="disable">Disable</option>
                                            <option value="reset">Reset hits</option>
                                        </select>
                                    </div>

                                    <div class="col-2"><input type="submit" class="btn btn-secondary redirect-action" value="Apply"/></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-8"><input class="form-control" type="text" name="search" value="<?php echo $this->input->get('search') ?: ''; ?>" placeholder="Seach URL here"/></div>
                                    <div class="col-2"><input class="btn btn-secondary" type="submit" value="Search" /></div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="form-group row">
                            <div class="col-12">
                            <?php if( $redirects ) : ?>
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>
                                                <div class="form-check">
                                                    <label class="form-check-label">
                                                        <input id="select-all" class="form-check-input" type="checkbox" name="item[]" value="">
                                                        <span class="fa fa-check"></span>
                                                    </label>
                                                </div>
                                            </th>
                                            <th>Type</th>
                                            <th>URL</th>
                                            <th><a href="<?php echo sprintf(base_url('tools/redirections/?orderby=position&order=%1s'), $order); ?>">Pos</a></th>
                                            <th><a href="<?php echo sprintf(base_url('tools/redirections/?orderby=last_count&order=%1s'), $order); ?>">Hits</a></th>
                                            <th><a href="<?php echo sprintf(base_url('tools/redirections/?orderby=last_access&order=%1s'), $order); ?>">Last Access</a></th>
                                        </tr>
                                    </thead>

                                    <tbody id="redirects-list">
                                        <?php foreach($redirects as $redirect) : ?>
                                            <tr>
                                                <?php $last_access = nice_date($redirect->last_access, 'F d, Y'); ?>
                                                <td>
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" name="item[]" value="<?php echo $redirect->id; ?>">
                                                            <span class="fa fa-check"></span>
                                                        </label>
                                                    </div>
                                                </td>
                                                <td><?php echo $redirect->action_code; ?></td>
                                                <td>
                                                    <a target="_blank" href="<?php echo home_url($redirect->url); ?>" <?php echo ($redirect->status === 'disabled') ? 'rel="noopener noreferrer"':''; ?>>
                                                        <?php echo ($redirect->status === 'disabled') ? '<strike>':''; ?>
                                                            <?php echo $redirect->url; ?>
                                                        <?php echo ($redirect->status === 'disabled') ? '</strike>':''; ?>
                                                    </a><br/>
                                                    <span style="color: rgba(153, 153, 153, 0.5);" class="target"><?php echo $redirect->action_data; ?></span>
                                                </td>
                                                <td><?php echo $redirect->position; ?></td>
                                                <td><?php echo $redirect->last_count; ?></td>
                                                <td><?php echo ($last_access !== 'Invalid Date') ? $last_access:'-'; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            <?php endif; ?>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>

                <?php $this->load->view('partials/pagination', $this->data); ?>
            </div>
        </div>
    </div>
    
    <?php echo form_open( 'tools/redirections' ); ?>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Add New Redirections</h4>
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Source URL</label>
                            <div class="col-10">
                                <input type="text" name="url" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Target URL</label>
                            <div class="col-10">
                                <input type="text" name="action_data" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Group</label>
                            <div class="col-10">
                                <select name="group" class="form-control">
                                    <optgroup label="WordPress">
                                        <option value="1">Redirections</option>
                                        <option value="2">Modified Posts</option>
                                    </optgroup>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2"></label>
                            <div class="col-10">
                                <input type="submit" name="add_redirect" class="btn btn-primary" value="Add Redirect" />
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>


<script>
    jQuery(document).ready(function($){

        $('#select-all').on('click', function(){
            if( $(this).is(':checked') ){
                $('tbody#redirects-list tr input[type="checkbox"]').prop('checked', true);
            }
            else{
                $('tbody#redirects-list tr input[type="checkbox"]').prop('checked', false);
            }
        });
    });
</script>