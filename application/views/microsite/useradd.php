<div class="row microsite-users">
    <div class="col-md-6 offset-md-3">
        <?php $this->load->view('partials/notification'); ?>
        <?php if ( $this->router->fetch_method() === 'useradd') : ?>
            <form method="post" action="<?php echo base_url("/microsites/useradd/"); ?>" autocomplete="off">
        <?php else : ?>
            <form method="post" action="<?php echo base_url("/microsites/useredit/{$edit_user->ID}/"); ?>" autocomplete="off">
        <?php endif; ?>
            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row <?php echo form_error('user[username]') ? 'has-danger' : ''; ?>">
                            <label for="username" class="col-4 col-form-label">Username</label>
                            <div class="col-8">
                                <input id="username" class="form-control form-control-danger" <?php echo ( $this->router->fetch_method() === 'useredit' ) ? 'readonly' : ''; ?> type="text" name="user[username]" value="<?php echo set_value('user[username]', isset($edit_user) ? $edit_user->user_login : null); ?>" />
                                <?php echo form_error('user[username]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row <?php echo form_error('user[email]') ? 'has-danger' : ''; ?>">
                            <label for="email" class="col-4 col-form-label">Email</label>
                            <div class="col-8">
                                <input id="email" class="form-control form-control-danger" type="text" name="user[email]" value="<?php echo set_value('user[email]', isset($edit_user) ? $edit_user->user_email : null); ?>" />
                                <?php echo form_error('user[email]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="role" class="col-4 col-form-label">Role</label>
                            <div class="col-8">
                                <select id="role" name="user[role]" class="form-control form-control-warning">
                                    <option value="none" <?php echo set_select('user[role]', 'none', $select['none']); ?>>None</option>
                                    <option value="franchisor" <?php echo set_select('user[role]', 'franchisor', $select['franchisor']); ?>>Franchisor</option>
                                    <option value="franchisee" <?php echo set_select('user[role]', 'franchisee', $select['franchisee']); ?>>Franchisee</option>
                                    <?php if ( is_super_admin() ) : ?>
                                        <option value="su" <?php echo set_select('user[role]', 'su', $select['su']); ?>>Super Administrator</option>
                                    <?php endif; ?>
                                </select>
                                <small class="from-text">Adding role will automatically add the user to the main site. You can remove it later by adding the user to a different microsite and it will inherit the role.</small>
                            </div>
                        </div>
                        <div class="form-group row <?php echo form_error('user[password]') ? 'has-danger' : ''; ?>">
                            <label for="password" class="col-4 col-form-label">Password</label>
                            <div class="col-8">
                                <input id="password" class="form-control form-control-danger" type="password" name="user[password]" value="<?php echo set_value('user[password]'); ?>" />
                                <?php echo form_error('user[password]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row <?php echo form_error('user[cpassword]') ? 'has-danger' : ''; ?>">
                            <label for="cpassword" class="col-4 col-form-label">Confirm Password</label>
                            <div class="col-8">
                                <input id="cpassword" class="form-control form-control-danger" type="password" name="user[cpassword]" value="<?php echo set_value('user[cpassword]'); ?>" />
                                <?php echo form_error('user[cpassword]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="sendmail" value="1" <?php echo set_checkbox('sendmail', 1); ?>>
                                  <span class="fa fa-check"></span>
                                   Send an email of account details to the new user.
                                </label>
                            </div>
                        </div>
                        <div class="col text-right">
                            <input class="btn btn-primary" type="submit" value="<?php echo $this->router->fetch_method() === 'useradd' ? 'Add User' : 'Update User' ; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
