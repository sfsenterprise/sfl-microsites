<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-block">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>ID</th>
				      <th>Site</th>
				      <th>City</th>
				      <th>State</th>
				      <th>Zip / Postal</th>
					  <th>Users</th>
				      <th>URL</th>
				      <th>Registered</th>
				    </tr>
				  </thead>
				  <tbody>
					  <?php foreach( $microsites as $site ) : $nap = get_microsite_nap($site->blog_id); ?>
						  <tr>
						    <td><?php echo isset($site->blog_id) ? $site->blog_id : 'NULL'; ?></td>
						    <td>
								<a href="<?php echo base_url("/microsites/edit/{$site->blog_id}/"); ?>" title="Edit <?php echo $site->blogname; ?>"><?php echo $site->blogname; ?></a>
								<br /><small><?php echo isset($nap->microsite_street) ? $nap->microsite_street : 'NULL'; ?></small>
							</td>
						    <td><?php echo isset($nap->microsite_city) ? $nap->microsite_city : 'NULL'; ?></td>
						    <td><?php echo isset($nap->microsite_state_province) ? $nap->microsite_state_province : 'NULL'; ?></td>
							<td><?php echo isset($nap->microsite_zip_postal) ? $nap->microsite_zip_postal : 'NULL'; ?></td>
						    <td>
								<?php echo count(get_users(['blog_id' => $site->blog_id, 'fields' => 'ID'])); ?>
							</td>
						    <td><a href="<?php echo get_blog_option($site->blog_id, 'siteurl'); ?>" target="_blank"><?php echo get_blog_option($site->blog_id, 'siteurl'); ?></a></td>
							<td><?php echo nice_date($site->registered, 'M. j, Y'); ?></td>
						  </tr>
					  <?php endforeach; ?>
				  </tbody>
				</table>
			</div>
			<?php $this->load->view('partials/pagination', $this->data); ?>
		</div>
	</div>
</div>
