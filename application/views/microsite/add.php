<div class="row">
  <div class="col">
    <?php $this->load->view('partials/notification'); ?>
    <div class="card">
      <div class="card-block">
        <?php if ( isset($edit_microsite) ) : ?>
          <?php if( isset($edit_microsite->microsite_blog_id) ) : ?>
            <?= form_open( base_url("microsites/edit/{$edit_microsite->microsite_blog_id}") ); ?>
          <?php else : ?>
            <?= form_open( base_url("microsites/edit/{$edit_microsite}") ); ?>
          <?php endif; ?>
        <?php else : ?>
          <?= form_open( base_url('microsites/add/') ); ?>
        <?php endif; ?>

        <fieldset>
            <div class="form-group row <?php echo form_error('microsite_name') ? 'has-danger' : '' ?>">
            <label for="microsite-name" class="col-2 col-form-label">Name *</label>
            <div class="col-10">
                <input <?php echo current_user_can('franchisor') ? '':'readonly'; ?> class="form-control form-control-danger" type="text" <?php echo (isset($edit_microsite->microsite_blog_id) && absint($edit_microsite->microsite_blog_id) === 1) ? 'readonly' : null; ?> value="<?php echo set_value('microsite_name', isset($edit_microsite->microsite_name) ? $edit_microsite->microsite_name : null); ?>" name="microsite_name" id="microsite-name">
              <?php echo form_error('microsite_name', '<div class="form-control-feedback">', '</div>'); ?>
              <small class="form-text">Site title</small>
            </div>
            </div>
            <div class="form-group row <?php echo form_error('microsite_slug') ? 'has-danger' : '' ?>">
              <label for="microsite-slug" class="col-2 col-form-label">Slug *</label>
              <div class="col-10">
                <input <?php echo current_user_can('franchisor') ? '':'readonly'; ?> class="form-control form-control-danger" <?php echo (isset($edit_microsite->microsite_blog_id) &&  absint($edit_microsite->microsite_blog_id) === 1) ? 'readonly' : null; ?> type="text" value="<?php echo set_value('microsite_slug', (isset($edit_microsite->microsite_slug) ? $edit_microsite->microsite_slug : null) ); ?>" name="microsite_slug" id="microsite-slug">
                <?php echo form_error('microsite_slug', '<div class="form-control-feedback">', '</div>'); ?>
                <small class="form-text"><?php echo home_url('/'); ?>(Slug)</small>
              </div>
            </div>
            <div class="form-group row">
              <label for="microsite-vanity-url" class="col-2 col-form-label">Vanity URL</label>
              <div class="col-10">
                <input <?php echo current_user_can('franchisor') ? '':'readonly'; ?> class="form-control" type="text" value="<?php echo set_value('microsite_vanity_url', (isset($edit_microsite->microsite_vanity_url) ? $edit_microsite->microsite_vanity_url : null) ); ?>" name="microsite_vanity_url" id="microsite-vanity-url">
              </div>
            </div>
            <div class="form-group row">
              <label for="microsite-template" class="col-2 col-form-label">Template</label>
              <div class="col-10">
                <?php if( ! current_user_can('franchisor') ) : ?>
                  <input readonly class="form-control" type="text" value="<?php echo isset($microsite_fields['template']) ? strtoupper($microsite_fields['template']):''; ?>" name="template" id="microsite-template">
                <?php else : ?>
                  <select name="template" class="form-control">
                      <option value="usa" <?php echo (isset($microsite_fields['template']) && $microsite_fields['template'] === 'usa') ? 'selected':''; ?> >USA</option>
                      <option value="canada" <?php echo (isset($microsite_fields['template']) && $microsite_fields['template'] === 'canada') ? 'selected':''; ?>>Canada</option>
                  </select>
                <?php endif; ?>
              </div>
            </div>
            <div class="form-group row <?php echo form_error('microsite_street') ? 'has-danger' : '' ?>">
            <label for="microsite-street" class="col-2 col-form-label">Street *</label>
            <div class="col-10">
            <input class="form-control form-control-danger" type="text" value="<?php echo set_value('microsite_street', isset($edit_microsite->microsite_street) ? $edit_microsite->microsite_street : null); ?>" name="microsite_street" id="microsite-street">
            <?php echo form_error('microsite_street', '<div class="form-control-feedback">', '</div>'); ?>
            </div>
            </div>
            <div class="form-group row">
            <label for="microsite-street2" class="col-2 col-form-label">Street 2</label>
            <div class="col-10">
            <input class="form-control" type="text" value="<?php echo set_value('microsite_street2', isset($edit_microsite->microsite_street2) ? $edit_microsite->microsite_street2 : null); ?>" name="microsite_street2" id="microsite-street2">
            </div>
            </div>
            <div class="form-group row <?php echo form_error('microsite_city') ? 'has-danger' : '' ?>">
            <label for="microsite-city" class="col-2 col-form-label">City *</label>
            <div class="col-10">
            <input class="form-control form-control-danger" type="text" value="<?php echo set_value('microsite_city', isset($edit_microsite->microsite_city) ? $edit_microsite->microsite_city : null); ?>" name="microsite_city" id="microsite-city">
            <?php echo form_error('microsite_city', '<div class="form-control-feedback">', '</div>'); ?>
            </div>
            </div>
            <div class="form-group row <?php echo form_error('microsite_state') ? 'has-danger' : '' ?>">
            <label for="microsite-state" class="col-2 col-form-label">State *</label>
            <div class="col-10">
            <input class="form-control form-control-danger" type="text" value="<?php echo set_value('microsite_state_province', isset($edit_microsite->microsite_state_province) ? $edit_microsite->microsite_state_province : null); ?>" name="microsite_state_province" id="microsite-state">
            <?php echo form_error('microsite_state', '<div class="form-control-feedback">', '</div>'); ?>
            </div>
            </div>
            <div class="form-group row <?php echo form_error('microsite_zip_postal') ? 'has-danger' : '' ?>">
            <label for="microsite-zip-postal" class="col-2 col-form-label">Zip / Postal *</label>
            <div class="col-10">
            <input class="form-control form-control-danger" type="text" value="<?php echo set_value('microsite_zip_postal', isset($edit_microsite->microsite_zip_postal) ? $edit_microsite->microsite_zip_postal : null); ?>" name="microsite_zip_postal" id="microsite-zip-postal">
            <?php echo form_error('microsite_zip_postal', '<div class="form-control-feedback">', '</div>'); ?>
            </div>
            </div>
            <div class="form-group row <?php echo form_error('microsite_admin_email') ? 'has-danger' : '' ?>">
              <label for="microsite-admin-email" class="col-2 col-form-label" id="search-user" onkeyup="searchUser()">Admin Email *</label>
              <div class="col-4">
                <noscript>
                  <input class="form-control form-control-danger" type="email" value="<?php echo set_value('microsite_admin_email', isset($edit_microsite->microsite_admin_email) ? $edit_microsite->microsite_admin_email : null); ?>" name="microsite_admin_email" id="microsite-admin-email">
                </noscript>
                <select class="microsite-users-select" name="microsite_admin_email">
                  <option value="" disabled selected></option>
                  <?php foreach( $users as $networkuser ) : ?>
                    <option value="<?php echo $networkuser->user_email; ?>" <?php echo isset($edit_microsite->microsite_admin_email) && $edit_microsite->microsite_admin_email === $networkuser->user_email ? 'selected' : null; ?>><?php echo $networkuser->user_login; ?> (<?php echo $networkuser->user_email; ?>)</option>
                  <?php endforeach; ?>
                </select>
                <?php echo form_error('microsite_admin_email', '<div class="form-control-feedback">', '</div>'); ?>
              <small class="form-text">Select an email or add new email.</small>
              <small class="form-text">New email will be registered as new users.</small>
              </div>
            </div>
            <div class="form-group row">
              <label for="microsite-phone" class="col-2 col-form-label">Admin Email Alias</label>
              <div class="col-4">
                <input class="form-control" type="text" value="<?php echo isset($microsite_fields['admin_email_alias']) ? $microsite_fields['admin_email_alias']:''; ?>" name="admin_email_alias" >
                <small class="form-text">Form Delivery Alias</small>
              </div>
            </div>
            <div class="form-group row">
            <label for="microsite-phone" class="col-2 col-form-label">Phone *</label>
            <div class="col-4">
            <input class="form-control" type="tel" value="<?php echo isset($edit_microsite->ID) ? $edit_microsite->microsite_phone : ''; ?>" name="microsite_phone" id="microsite-phone">
            </div>
            </div>
            <div class="form-group row">
            <label for="microsite-general-manager" class="col-2 col-form-label">General Manager</label>
            <div class="col-4">
            <input class="form-control" type="text" value="<?php echo isset($edit_microsite->ID) ? $edit_microsite->microsite_general_manager : ''; ?>" name="microsite_general_manager" id="microsite-general-manager">
            </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group row">
            <label for="microsite-latitude" class="col-2 col-form-label">Latitude</label>
            <div class="col-4">
            <input class="form-control" type="number" step="0.0000001" value="<?php echo isset($edit_microsite->ID) ? $edit_microsite->microsite_lat : ''; ?>" name="microsite_lat" id="microsite-latitude">
            </div>
            <label for="microsite-longitude" class="col-2 col-form-label">Longitude</label>
            <div class="col-4">
            <input class="form-control" type="number" step="0.0000001" value="<?php echo isset($edit_microsite->ID) ? $edit_microsite->microsite_lng : ''; ?>" name="microsite_lng" id="microsite-longitude">
            </div>
            </div>
          </fieldset>
          <fieldset>
            <div class="form-group row">
            <label for="microsite-gmtgta" class="col-2 col-form-label">GTM / GTA UID</label>
            <div class="col-4">
            <input <?php echo current_user_can('franchisor') ? '':'readonly'; ?> type="text" name="microsite_gtm_ga" id="microsite-gmtgta" class="form-control" value="<?php echo isset($edit_microsite->ID) ? $edit_microsite->microsite_gtm_ga : ''; ?>" />
            </div>
            </div>
          </fieldset>
          <!--fieldset>
            <div class="form-group row">
              <label for="microsite-schema" class="col-2 col-form-label">Schema</label>
              <div class="col-10">
              <textarea class="form-control" id="microsite-schema" name="microsite_schema" rows="5"><?php //echo isset($edit_microsite->microsite_schema) ? $edit_microsite->microsite_schema : ''; ?></textarea>
              </div>
            </div>
          </fieldset-->
          <fieldset>
            <div class="form-group row">
              <div class="col-lg-7 offset-lg-2">
                <a class="btn btn-default" href="<?php echo base_url("/microsites/"); ?>">Cancel</a>
                <button type="submit" class="btn btn-primary" name="microsite_edit"><?php echo ($this->router->fetch_method() === 'edit') ? 'Update' : 'Add' ?> Microsite</button>
              </div>
            <?php if ( isset($edit_microsite ) ) : ?>
              <div class="col-lg-3">
                <div class="row">
                  <div class="col-lg-9 text-right">
                    <?php $archived = get_blog_details($edit_microsite->microsite_blog_id); ?>

                    <?php if($archived->archived) : ?>
                      <button type="button" class="btn btn-success" data-toggle="modal" data-target="#unarchive-dialog">Unarchive</button>
                    <?php else : ?>
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#archive-dialog">Archive</button>
                    <?php endif; ?>
                  </div>
                  <div class="col-lg-3 text-right">
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-dialog">Delete</button>
                  </div>
                </div>

              </div>
            <?php endif; ?>
            </div>
          </fieldset>
        <?= form_close(); ?>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="archive-dialog" tabindex="-1" role="dialog" aria-labelledby="archiveModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">Archive Microsite</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to Archive <span class="text-danger"><?php echo isset($edit_microsite) ? $edit_microsite->microsite_name : null; ?></span>?</p>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          <a class="btn btn-primary" href="<?php echo isset($microsite) ? base_url("/microsites/archive/{$edit_microsite->microsite_blog_id}") : null; ?>">Yes</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="unarchive-dialog" tabindex="-1" role="dialog" aria-labelledby="unarchiveModal" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">Unarchive Microsite</div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to Unarchive <span class="text-danger"><?php echo isset($edit_microsite) ? $edit_microsite->microsite_name : null; ?></span>?</p>
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          <a class="btn btn-primary" href="<?php echo isset($microsite) ? base_url("/microsites/unarchive/{$edit_microsite->microsite_blog_id}") : null; ?>">Yes</a>
      </div>
    </div>
  </div>
</div>
