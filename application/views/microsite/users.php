<div class="row">
	<div class="col col-12">
		<form method="get" action="<?php echo base_url("/microsites/users/"); ?>">
			<fieldset>
				<div class="form-group row">
					<div class="col-6"></div>

					<div class="col-6">
						<div class="row">
							<div class="col-8">
								<input type="text" placeholder="Search user by username or email" class="form-control" name="search" />
							</div>
							<div class="col-4">
								<input type="submit" class="btn btn-default" value="Search Users" />
							</div>
						</div>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<div class="col col-12">
		<div class="card">
			<div class="card-block">
				<div class="row">
					<div class="col-6 table-filters">
						<?php if ( isset($current_microsite) ) : ?>
							<ul>
								<li>
									<a class="<?php echo ! $this->uri->segment(4) ? 'active' : null ?>" href="<?php echo base_url("/microsites/users/"); ?>">All</a>
								</li>
								<li><a class="<?php echo $this->uri->segment(4) === 'unassigned' ? 'active' : null ?>" href="<?php echo base_url("/microsites/users/unassigned/"); ?>">Unassigned</a></li>
								<li><a class="<?php echo $this->uri->segment(4) === 'assigned' ? 'active' : null ?>" href="<?php echo base_url("/microsites/users/assigned/"); ?>">Assigned</a></li>
							</ul>
						<?php endif; ?>
					</div>
					<div class="col-6 table-search">

					</div>
				</div>
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Username</th>
				      <th>Name</th>
				      <th>Email</th>
				      <th>Role</th>
				      <th>Sites</th>
				    </tr>
				  </thead>
				  <tbody>
					  <?php if ( $users ) : ?>
						  <?php foreach( $users as $networkuser ) : ?>
	                          <tr>
	                              <td>
	                                  <a href="<?php echo base_url("/microsites/useredit/{$networkuser->ID}/"); ?>"><?php echo $networkuser->user_login; ?></a>
									  <div class="table-actions">
	                                      <ul>
	                                          	<?php if ( ! empty($microsite) ) : ?>
	                                              	<?php if ( is_user_member_of_blog($networkuser->ID, $microsite) ) : ?>
	                                                  	<li>
	                                                      	<a class="btn btn-default btn-sm" href="<?php echo base_url("/microsites/unassignuser/{$microsite}/{$networkuser->ID}/"); ?>" title="Remove user from microsite <?php echo $current_microsite->blogname; ?>">Unassign</a>
	                                                  	</li>
	                                              	<?php else : ?>
	                                                  	<li>
	                                                      	<a class="btn btn-primary btn-sm" href="<?php echo base_url("/microsites/assignuser/{$microsite}/{$networkuser->ID}/"); ?>" title="Add user from microsite <?php echo $current_microsite->blogname; ?>">Assign</a>
	                                                  	</li>
	                                              	<?php endif; ?>
	                                          	<?php endif; ?>
	                                          	<li>
	                                              	<a class="btn btn-success btn-sm" href="<?php echo base_url("/microsites/useredit/{$networkuser->ID}/"); ?>" title="Edit user">Edit</a>
	                                          	</li>
	                                          	<?php if ( current_user_can('administrator') || is_super_admin() ) : ?>
		                                          	<li>
		                                              	<a class="btn btn-danger btn-sm" href="<?php echo base_url("/microsites/userdelete/{$networkuser->ID}/"); ?>" title="Delete user">Delete</a>
		                                          	</li>
		                                        <?php endif; ?>
	                                      </ul>
	                                  </div>
	                              </td>
	                              <td><?php echo $networkuser->user_name; ?></td>
	                              <td><?php echo $networkuser->user_email; ?></td>
	                              <td>
	                                  <?php if ( is_super_admin($networkuser->ID) ) : ?>
	                                      Super Administrator
	                                  <?php elseif ( in_array('franchisor', $networkuser->roles) ): ?>
										  <?php _e('Franchisor', 'sfl-microsites'); ?>
	                                  <?php elseif ( in_array('franchisee', $networkuser->roles) ) : ?>
										  <?php _e('Franchisee', 'sfl-microsites'); ?>
	                                  <?php elseif ( in_array('subscriber', $networkuser->roles) ) : ?>
										  <?php _e('', 'sfl-microsites'); ?>
									  <?php else : ?>
	                                      <?php echo ucfirst(array_pop($networkuser->roles)); ?>
	                                  <?php endif; ?>
	                              </td>
	                              <td><?php echo count(get_blogs_of_user($networkuser->ID)); ?></td>
	                          </tr>
	                      <?php endforeach; ?>
					  <?php else : ?>
						  <tr>
							  <td colspan="5">Nothing found.</td>
						  </tr>
					  <?php endif; ?>
				  </tbody>
				</table>
			</div>
			<?php $this->load->view('partials/pagination', $this->data); ?>
		</div>
	</div>
</div>
