<?php if( $this->router->fetch_class() === 'promos' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Title</a></th>
                                    <th class="text-center">Offer Starts</th>
                                    <th class="text-center">Offer Ends</th>
                                    <th class="text-center"><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>

                            <?php if($all_promos) : ?>
                                <tbody>
                                    <?php foreach( $all_promos as $promo ) : ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('promos/edit/'.$promo->ID); ?>"><?php echo $promo->post_title; ?></a></td>
                                            <td class="text-center"><?php echo (gss_get_post_meta( $microsite, $promo->ID, '_ff_promo_startDate' )) ? nice_date(gss_get_post_meta( $microsite, $promo->ID, '_ff_promo_startDate' ), 'M d, Y'):'-'; ?></td>
                                            <td class="text-center"><?php echo (gss_get_post_meta( $microsite, $promo->ID, '_ff_promo_endDate' )) ? nice_date(gss_get_post_meta( $microsite, $promo->ID, '_ff_promo_endDate' ), 'M d, Y'):'-'; ?></td>
                                            <td class="text-center"><?php echo nice_date($promo->post_date, 'M d, Y') .' - '. date('h:i:s a', strtotime($promo->post_date)); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing offers.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>

                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>
<?php endif; ?>



<?php if( $this->router->fetch_class() === 'promos' && ( $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'add') ) : ?>
    <?= form_open( sprintf( '/promos/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Offer Title</label>
                                    <div class="col-10"><input type="text" name="promo_title" class="form-control" value="<?php echo isset($gss_promo->post_title) ? $gss_promo->post_title : ''; ?>" placeholder="Offer Title"/></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Offer Content</label>
                                    <div class="col-10">
                                        <?php switch_to_blog($microsite); ?>
                                        <?php wp_editor( isset($gss_promo->ID) ? $gss_promo->post_content:'', 'promo_content', array('media_buttons' => true, 'editor_class'=>'form-control') ); ?>
                                        <?php restore_current_blog(); ?>
                                    </div>
                                </div>
                            </fieldset>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Offer Duration</h4>
                        <fieldset>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Offer Start</label>
                                <div class="col-10"><input id="promo_startDate" class="datepicker form-control" name="_ff_promo_startDate" value="<?php echo isset($gss_promo->ID) ? date_format(date_create(gss_get_post_meta( $microsite, $gss_promo->ID, '_ff_promo_startDate' )),"F j, Y") : ''; ?>" /> Enter the starting date for this offer</div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Offer End</label>
                                <div class="col-10"><input id="promo_endDate" class="datepicker form-control" name="_ff_promo_endDate" value="<?php echo isset($gss_promo->ID) ? date_format(date_create(gss_get_post_meta( $microsite, $gss_promo->ID, '_ff_promo_endDate' )),"F j, Y"): ''; ?>" /> Enter the ending date for this offer</div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                         <div class="form-group row">
                            <div class="col-7 text-left">
                                <?php if( isset($gss_promo->ID) ) : ?>
                                    <div class="form-group">
                                        <strong>Date Created:</strong> <br/><?php echo nice_date($gss_promo->post_date, 'M d, Y h:i:s a') ; ?>
                                    </div>

                                    <div class="form-group">
                                        <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_promo->post_modified, 'M d, Y h:i:s a') ; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-5 text-right">
                                <?php echo form_submit( 'gss_promo' , ($this->uri->segment(4)) ? 'Update Offer' : 'Add Offer', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>

                        <?php if( isset($gss_promo->ID) ) : ?>
                                <div class="row">
                                    <div class="col-6 text-left"></div>

                                    <div class="col-6 text-right">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 text-left" class="thickbox">Status</label>
                            <div class="col-12">
                                <select name="post_status" class="form-control">
                                    <option value="publish" <?php echo (isset($gss_promo->ID) && isset($gss_promo->post_status) && $gss_promo->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                    <option value="draft" <?php echo (isset($gss_promo->ID) && isset($gss_promo->post_status) && $gss_promo->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                    <option value="private" <?php echo (isset($gss_promo->ID) && isset($gss_promo->post_status) && $gss_promo->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?php echo form_close(); ?>
<?php endif; ?>
