
    <?php if( $this->router->fetch_class() === 'testimonials' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>
        
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Title</a></th>
                                    <th>Affiliated Company</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>
                            
                            <?php if($all_testimonials) : ?>
                                <tbody>
                                    <?php foreach( $all_testimonials as $testimonial ) : ?>
                                        <tr>
                                            <td><a target="_blank" href="<?php echo gss_get_blog_details($microsite, 'siteurl') . '/testimonials'; ?>">View</a></td>
                                            <td><a href="<?php echo base_url('testimonials/edit/'.$testimonial->ID); ?>"><?php echo $testimonial->post_title ?: 'Testimonial ' . $testimonial->ID; ?></a></td>
                                            <td><?php echo ( gss_get_post_meta( 1, $testimonial->ID, 'affiliated_company' ) ) ?:''; ?></td>
                                            <td><?php echo nice_date($testimonial->post_date, 'F d, Y') .' - '. date('h:i:s a', strtotime($testimonial->post_date)); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing testimonials.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>
                         
                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>

    <?php endif; ?>

    <?php if( $this->router->fetch_class() === 'testimonials' && ( $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'add') ) : ?>
        
        <?= form_open( sprintf( '/testimonials/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
            <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Testimonial Title</label>
                                    <div class="col-10"><?php echo form_input(['name'=>'testimonial_title', 'class'=>'form-control', 'type'=>'text', 'value'=>isset($gss_testimonials->post_title) ? $gss_testimonials->post_title : '']); ?></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Description</label>
                                    <div class="col-10">
                                        <?php wp_editor( isset($gss_testimonials->ID) ? $gss_testimonials->post_content:'', 'testimonial_description', array('media_buttons' => true, 'editor_class'=>'form-control') ); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Testimonial Settings</h4>
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Affiliated Company</label>
                                    <div class="col-10"><?php echo form_input(['name'=>'affiliated_company', 'type'=>'text', 'class'=>'form-control', 'value'=>isset($gss_testimonials->ID) ? (gss_get_post_meta( 1, $gss_testimonials->ID, 'affiliated_company' )) ?: '' : '' ]); ?></div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-7 text-left">
                                    <?php if( isset($gss_testimonials->ID) ) : ?>
                                        <div class="form-group">
                                            <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $gss_testimonials->ID ); ?>
                                        </div>

                                        <div class="form-group">
                                            <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_testimonials->post_modified, 'M d, Y h:i:s a') ; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                
                                <div class="col-5 text-right">
                                    <?php echo form_submit('gss_testimonials', ($this->uri->segment(4)) ? 'Update' : 'Add', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>

                            <?php if( isset($gss_testimonials->ID) ) : ?>
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <a class="h2 text-primary" target="_blank" href="<?php echo gss_get_blog_details($microsite, 'siteurl') . '/testimonials'; ?>">View Page</a>
                                    </div>
                                    
                                    <div class="col-6 text-right">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>
                                        
                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-12 text-left" class="thickbox">Status</label>
                                <div class="col-12">
                                    <select name="post_status" class="form-control">
                                        <option value="publish" <?php echo (isset($gss_testimonials->ID) && isset($gss_testimonials->post_status) && $gss_testimonials->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                        <option value="draft" <?php echo (isset($gss_testimonials->ID) && isset($gss_testimonials->post_status) && $gss_testimonials->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                        <option value="private" <?php echo (isset($gss_testimonials->ID) && isset($gss_testimonials->post_status) && $gss_testimonials->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php echo form_close(); ?>

    <?php endif; ?>