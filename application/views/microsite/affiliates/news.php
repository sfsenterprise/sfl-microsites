<?php if( $this->router->fetch_class() === 'news' && $this->router->fetch_method() === 'index' ) : ?>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Date</th>
                                </tr>

                                <?php if( $all_news ) : ?>
                                    <tbody>
                                        <?php foreach( $all_news as $news ) : ?>
                                            <?php $cats = gss_wp_get_post_categories($microsite, $news->ID, 'gss_news_categories'); ?>
                                            <tr>
                                                <td>
                                                    <?php if(isset($news->post_status) && $news->post_status !== 'publish') : ?>
                                                        <a href="<?php echo sprintf(base_url('news/edit/%1$s'), $news->ID); ?>"><?php echo $news->post_title; ?></a> - <strong><?php echo ucwords($news->post_status); ?></strong>
                                                    <?php else :?>
                                                        <a href="<?php echo sprintf(base_url('news/edit/%1$s'), $news->ID); ?>"><?php echo $news->post_title; ?></a>
                                                    <?php endif; ?>
                                                </td>
                                                <td><?php echo nice_date($news->post_date, 'F d, Y') .' - '. date('h:i:s a', strtotime($news->post_date)); ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    <?php else : ?>
                                        <tbody>
                                            <tr>
                                                <td><h2>No existing news.</h2></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    <?php endif; ?>
                            </thead>
                         </table>

                    </div>
                    <div class="card-footer">
                        <?php $this->load->view('partials/pagination', $this->data); ?>
                    </div>
                </div>
            </div>
        </div>

<?php endif; ?>

<?php if( $this->router->fetch_class() === 'news' && ($this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit') ) : ?>

    <?php echo form_open( sprintf( '/news/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
        <div class="row">
                <div class="col col-9">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-block">
                                    <fieldset>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Title</label>
                                            <div class="col-10"><?php echo form_input(['name'=>'news_title', 'type'=>'text', 'class'=>'form-control', 'value'=> isset($page->post_title) ? $page->post_title : '' ]); ?></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Content</label>
                                            <div class="col-10">
                                                <?php switch_to_blog($microsite); ?>
                                                <?php wp_editor( isset($page->ID) ? $page->post_content:'', 'news_content', array('media_buttons' => true, 'editor_class'=>'form-control') ); ?>
                                                <?php restore_current_blog(); ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('partials/cards/yoast'); ?>
                </div>

                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-7 text-left">
                                    <?php if( isset($page->ID) && $page->ID ) : ?>
                                        <div class="form-group">
                                            <strong>Date Created:</strong> <br/><?php echo nice_date($page->post_date, 'M d, Y h:i:s a') ; ?>
                                        </div>

                                        <div class="form-group">
                                            <strong>Last Modified:</strong> <br/><?php echo nice_date($page->post_modified, 'M d, Y h:i:s a') ; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-5 text-right">
                                    <?php echo form_submit( 'gss_news' , ($this->uri->segment(4)) ? 'Update News' : 'Add News', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>

                            <?php if( isset($page->ID) && $page->ID ) : ?>
                                <div class="row">
                                    <div class="col-6 text-left"></div>

                                    <div class="col-6 text-right">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-12 text-left" class="thickbox">Status</label>
                                <div class="col-12">
                                    <select name="post_status" class="form-control">
                                        <option value="publish" <?php echo (isset($page->ID) && isset($page->post_status) && $page->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                        <option value="draft" <?php echo (isset($page->ID) && isset($page->post_status) && $page->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                        <option value="private" <?php echo (isset($page->ID) && isset($page->post_status) && $page->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php switch_to_blog($this->microsite); ?>
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-12"><strong>Featured Image</strong></div>
                                <div class="col-12">
                                    <a href="" id="set-post-thumbnail">Set featured image</a>
                                    <img id="post-featured-src" style="display: <?php echo (get_the_post_thumbnail_url($page->ID)) ? 'block':'none'; ?>" src="<?php echo get_the_post_thumbnail_url($page->ID); ?>" alt="" />
                                    <input type="hidden" id="_thumbnail_id" name="_thumbnail_id" value="<?php echo get_post_meta($page->ID, '_thumbnail_id', true); ?>" />
                                    <div class="remove-img" style="display: <?php echo (get_the_post_thumbnail_url($page->ID)) ? 'block':'none'; ?>;">
                                        <p>Click the image to edit or update</p>
                                        <a href="#" id="remove-post-thumbnail">Remove featured image</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php restore_current_blog(); ?>

                </div>
            </div>
        <?php echo form_close(); ?>
<?php endif; ?>
