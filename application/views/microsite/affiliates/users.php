  
    <?php if( ! isset($microsite) ) : ?>
            
        <h2>No selected microsite. Please 'Select a microsite' from top nav bar.</h2>

    <?php else : ?>
        <h2>Assign Users <a href="<?php echo get_blog_option($microsite, 'siteurl '); ?>"><?php echo get_blog_option($microsite, 'blogname '); ?></a></h2>
        <?php echo form_input(['name' => 'serach_user', 'type' => 'text', 'id' => 'microsite-search-user', 'onkeyup' => 'searchUser_asign()', 'autocomplete' => 'off' ]); ?>
           <?php $all_users = gss_get_users(); ?>

            <ul id="microsite-wp-users">
                <?php foreach ($all_users as $user) : ?>
                    <li data-userid="<?php echo $user->data->ID ?>"><a href=""><?php echo $user->data->user_email; ?></a></li>
                <?php endforeach; ?>
            </ul>

        <?php echo form_open( 'microsites/user_edit/' ); ?>
            
            <?php isset($users) ? $users = gss_get_users(['blog_id'=>$microsite]) : array(); ?>

                <div id="added-users">
                    <?php if( isset($users) ) : ?>
                        <?php foreach( $users as $user ) :?>
                            <div class="microsite-user"><input type="hidden" name="assigned_user[]" value="<?php echo $user->ID; ?>"><?php echo $user->user_email; ?><span id="remove-user-<?php echo $user->ID; ?>" onclick="removeUser(<?php echo $user->ID; ?>)" class="user user-<?php echo $user->ID; ?>">x</span></div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            
            <?php echo form_submit( 'gss_users' , 'Add Users'); ?>

        <?php echo form_close(); ?> 

        <style>
            .microsite-user {
                background: #e89700;
                color: #fff;
                display: inline-block;
                padding: 5px 32px 5px 12px;
                border-radius: 2px;
                margin-right: 10px;
                position: relative;
            }
            div#added-users {
                margin: 10px 0;
            }
            .microsite-user .user {
                position: absolute;
                background: #ff0000;
                top: 0;
                bottom: 0;
                right: 0;
                border-radius: 0px 2px 2px 0px;
                line-height: 25px;
                padding: 0px 8px;
            }

            #wp-users li, ul#microsite-wp-users li{
                display: none;
                cursor: pointer;
                padding: 5px 10px;
            }

            #wp-users li:hover, ul#microsite-wp-users li:hover{
                background: #eee;
            }

            ul#wp-users, ul#microsite-wp-users {
                padding: 0;
                margin: 0;
                position: absolute;
                background: #fff;
                left: 0;
                right: 0;
                z-index: 10000;
                display: none;
                list-style: none;
                border: 1px solid #5b9dd9;
                -webkit-box-shadow: 0 1px 2px rgba(30,140,190,.8);
                box-shadow: 0 1px 2px rgba(30,140,190,.8);
                background-color: #fff;
            }

            ul#wp-users a, ul#microsite-wp-users a{
                text-decoration: none;
                color: #000;
            }
        </style>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script>
            jQuery(document).ready(function($){
                $('#microsite-wp-users li').click(function(){
                    var user_name = $(this).children('a').html();
                    var user_id = $(this).data('userid');

                    $('#added-users').append('<div class="microsite-user"><input type="hidden" name="assigned_user[]" value="'+user_id+'"/>'+user_name+'<span id="remove-user-'+user_id+'" onclick="removeUser('+user_id+')" class="user user-'+user_id+'">x</span></div>');
                    $('#microsite-search-user').val('');
                    $('#microsite-wp-users li, #microsite-wp-users').css('display', 'none');
                    return false;
                });
            });

            function searchUser_asign() {
                var input, filter, ul, li, a, i;
                input = document.getElementById('microsite-search-user');
                filter = input.value.toUpperCase();
                ul = document.getElementById("microsite-wp-users");
                li = ul.getElementsByTagName('li');

                for (i = 0; i < li.length; i++) {
                    
                    if( input.value === '' ){
                        li[i].style.display = "none";
                        ul.style.display = "none";
                    } else {
                        a = li[i].getElementsByTagName("a")[0];
                        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            li[i].style.display = "block";
                            ul.style.display = "block";
                        } else {
                            li[i].style.display = "none";
                        }
                    }
                }
            }
        </script>

    <?php endif; ?>