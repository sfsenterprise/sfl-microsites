<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-block">
                <?= form_open( sprintf('socialmedia/edit') ); ?>

                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Facebook URL</label>
                            <div class="col-10"><?php echo form_input([ 'name'=> 'facebook', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['facebook'])) ? $socialmedia['facebook']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Twitter URL</label>
                            <div class="col-10"><?php echo form_input([ 'name'=> 'twitter', 'type'=>'text', 'value'=> (!empty($socialmedia)) ?  (isset($socialmedia['twitter'])) ? $socialmedia['twitter']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Yelp</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'yelp', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['yelp'])) ? $socialmedia['yelp']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Instagram URL</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'instagram', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['instagram'])) ? $socialmedia['instagram']: '' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Youtube URL</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'youtube', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['youtube'])) ? $socialmedia['youtube']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Google+</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'google-plus', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['google-plus'])) ? $socialmedia['google-plus']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Pinterest URL</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'pinterest', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['pinterest'])) ? $socialmedia['pinterest']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">LinkedIn URL</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'linkedin', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['linkedin'])) ? $socialmedia['linkedin']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        <?php /*japol
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Tumblr URL</label>
                            <div class="col-10"><?php echo form_input(['name'=> 'tumblr', 'type'=>'text', 'value'=> (!empty($socialmedia)) ? (isset($socialmedia['tumblr'])) ? $socialmedia['tumblr']:'' : '', 'class'=>'form-control' ]); ?></div>
                        </div>
                        */?>
                        <div class="form-group row">
                            <label class="col-2 col-form-label"></label>
                            <div class="col-10"><?php echo form_submit( 'microsite_socials' , 'Update Social Media', ['class'=>'btn btn-primary']); ?></div>
                        </div>
                    </fieldset>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
