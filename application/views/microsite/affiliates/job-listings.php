
    <?php if( $this->router->fetch_class() === 'jobs' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Title</a></th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>

                            <?php if($all_jobs) : ?>
                                <tbody>
                                    <?php foreach( $all_jobs as $job ) : ?>
                                        <tr>
                                            <td><a target="_blank" href="<?php echo get_blog_permalink( $microsite, $job->ID ); ?>">View</a></td>
                                            <td><a href="<?php echo base_url('jobs/edit/'.$job->ID); ?>"><?php echo $job->post_title; ?><?php echo ($job->post_status != 'publish') ? '- <strong>'.$job->post_status.'</strong>' : null; ?></a></td>
                                            <td><?php echo nice_date($job->post_date, 'F d, Y') .' - '. date('h:i:s a', strtotime($job->post_date)); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            <?php else : ?>
                                <tbody>
                                    <tr>
                                        <td><h2>No existing jobs.</h2></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>

    <?php endif; ?>

<?php if( $this->router->fetch_class() === 'jobs' && ( $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' ) ) : ?>

    <?php echo form_open( sprintf( '/jobs/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Job Title</label>
                                    <div class="col-10">
                                        <?php echo form_input([ 'name'=>'job_title', 'type'=>'text', 'class'=>'form-control', 'value'=> isset($gss_job->post_title) ? $gss_job->post_title : '' ]); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Job Description</label>
                                    <div class="col-10">
                                        <?php switch_to_blog($microsite); ?>
                                        <?php wp_editor( isset($gss_job->ID) ? $gss_job->post_content:'', 'job_description', array('media_buttons' => true, 'editor_class'=>'form-control') ); ?>
                                        <?php restore_current_blog(); ?>
                                    </div>
                                </div>
                            </fieldset>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <h4 class="card-title">Job Settings</h4>
                        <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Form Page</label>
                                    <div class="col-10">
                                        <?php if($forms) : ?>
                                            <select name="_s8_ff_jl_position_form" id="job-form-page" class="form-control">
                                                <option value="">--Select--</option>
                                                <?php foreach($forms as $form) : ?>
                                                    <option value="<?php echo $form->ID; ?>" <?php echo ( isset($gss_job->ID) && $job_form == $form->ID ) ? 'selected' : ''; ?>><?php echo $form->post_title; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="job_hide_show">Show/Hide This Job</label>
                                    <div class="col-10">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" id="job_hide_show" name="_gss_post_hidden" <?php echo isset($gss_job->ID) ? gss_get_post_meta( $microsite, $gss_job->ID, '_gss_post_hidden' ) === '1' ? 'checked' : '' : ''; ?>/>
                                                <span class="fa fa-check"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-7 text-left">
                                <?php if( isset($gss_job->ID) ) : ?>
                                    <div class="form-group">
                                        <strong>Date Created:</strong> <br/><?php echo nice_date($gss_job->post_date, 'M d, Y h:i:s a') ; ?>
                                    </div>

                                    <div class="form-group">
                                        <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_job->post_modified, 'M d, Y h:i:s a') ; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-5 text-right">
                                <?php echo form_submit( 'gss_job_listings' , ($this->uri->segment(4)) ? 'Update Job' : 'Add Job', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>

                        <?php if( isset($gss_job->ID) ) : ?>
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a class="h2 text-primary" target="_blank" href="<?php echo get_blog_permalink( $microsite, $gss_job->ID ); ?>">View Page</a>
                                </div>

                                <div class="col-6 text-right">
                                    <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                    <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header pb-0">
                                                    <div class="modal-title">Are you sure to delete?</div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 text-left" class="thickbox">Status</label>
                            <div class="col-12">
                                <select name="post_status" class="form-control">
                                    <option value="publish" <?php echo (isset($gss_job->ID) && isset($gss_job->post_status) && $gss_job->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                    <option value="draft" <?php echo (isset($gss_job->ID) && isset($gss_job->post_status) && $gss_job->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                    <option value="private" <?php echo (isset($gss_job->ID) && isset($gss_job->post_status) && $gss_job->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>
<?php endif; ?>
