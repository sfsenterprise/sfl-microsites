<?php if( $this->router->fetch_class() === 'lessons' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Lesson</a></th>
                                    <th>Group</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>
                            
                            <?php if($all_lessons) : ?>
                                <tbody>
                                    <?php foreach( $all_lessons as $lesson ) : ?>
                                        <tr>
                                            <td><a target="_blank" href="<?php echo gss_get_blog_details($microsite, 'siteurl') . '/programs/swim-lesson-levels/'; ?>">View</a></td>
                                            <td>
                                                <a href="<?php echo base_url('lessons/edit/'.$lesson->ID); ?>">
                                                    <?php echo $lesson->post_title; ?>
                                                </a>
                                            </td>
                                            <td>
                                                <?php $groups = gss_wp_get_post_categories(1, $lesson->ID, 'gss_swim_lesson_groups'); ?>
                                                
                                                <?php if( ! isset($groups->errors) ) : ?>
                                                    <?php $comma = count($groups) - 1; ?>
                                                    <?php foreach( $groups as $group ) : ?>
                                                        <?php echo $group->name; ?><?php echo ($comma > 0) ? ', ':''; ?>
                                                        <?php $comma--; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td><?php echo nice_date($lesson->post_date, 'F d, Y') .' - '. date('h:i:s a', strtotime($lesson->post_date)); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing lesson.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>
                         
                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php if( $this->router->fetch_class() === 'lessons' && ( $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'add') ) : ?>
    
    <?php echo form_open( sprintf( '/lessons/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                        <fieldset>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Lesson Title</label>
                                <div class="col-10"><input type="text" name="lesson_name" class="form-control" value="<?php echo isset($gss_lesson->post_title) ? $gss_lesson->post_title : ''; ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Age</label>
                                <div class="col-10"><input type="text" name="lesson_age" class="form-control" value="<?php echo isset($gss_lesson->ID) ? gss_get_post_meta( $microsite, $gss_lesson->ID, "lesson_age" ) : ''; ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Details</label>
                                <div class="col-10">
                                    <?php wp_editor( isset($gss_lesson->ID) ? gss_get_post_meta( $microsite, $gss_lesson->ID, "lesson_details" ) : '', 'lesson_details', array('textarea_name'=>'lesson_details', 'editor_class'=>'form-control') ); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Requirements</label>
                                <div class="col-10">
                                    <?php wp_editor( isset($gss_lesson->ID) ? gss_get_post_meta( $microsite, $gss_lesson->ID, "lesson_requirements" ) : '', 'lesson_requirements', array('textarea_name'=>'lesson_requirements', 'editor_class'=>'form-control') ); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-7 text-left">
                                <?php if( isset($gss_lesson->ID) ) : ?>
                                    <div class="form-group">
                                        <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $gss_lesson->ID ); ?>
                                    </div>

                                    <div class="form-group">
                                        <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_lesson->post_modified, 'M d, Y h:i:s a') ; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <div class="col-5 text-right">
                                <?php echo form_submit('gss_swim_lessons', ($this->uri->segment(4)) ? 'Update' : 'Add', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>

                        <?php if( isset($gss_lesson->ID) ) : ?>
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a class="h2 text-primary" target="_blank" href="<?php echo gss_get_blog_details($microsite, 'siteurl') . '/programs/swim-lesson-levels/'; ?>">View Page</a>
                                </div>
                                
                                <div class="col-6 text-right">
                                    <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>
                                    
                                    <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header pb-0">
                                                    <div class="modal-title">Are you sure to delete?</div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Swim Lesson Groups:</label>
                            <div class="col-12 text-left">
                                <?php $cats = array(); ?>
                                <ul class="category-list">
                                    <?php foreach( $categories as $category ) : ?>
                                        <?php foreach( $post_cats as $post_cat ) : ?>
                                            <?php if( $category->term_id === $post_cat->term_id ) : ?>
                                                <?php $cats[$post_cat->term_id] = $post_cat->name; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <li>
                                            <div class="form-check">
                                                <label for="cat-<?php echo $category->term_id; ?>" class="form-check-label">
                                                    <input class="form-check-input" id="cat-<?php echo $category->term_id; ?>" value="<?php echo $category->term_id; ?>" type="checkbox" name="tax_input[gss_swim_lesson_groups][]" <?php echo (array_key_exists($category->term_id, $cats)) ? 'checked':''; ?> />
                                                    <span class="fa fa-check"></span>
                                                    <?php echo $category->name; ?>
                                                </label>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 text-left" class="thickbox">Status</label>
                            <div class="col-12">
                                <select name="post_status" class="form-control">
                                    <option value="publish" <?php echo (isset($gss_lesson->ID) && isset($gss_lesson->post_status) && $gss_lesson->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                    <option value="draft" <?php echo (isset($gss_lesson->ID) && isset($gss_lesson->post_status) && $gss_lesson->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                    <option value="private" <?php echo (isset($gss_lesson->ID) && isset($gss_lesson->post_status) && $gss_lesson->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Order</label>
                            <div class="col-12 text-left"><input type="text" name="menu_order" value="<?php echo isset($gss_lesson->ID) ? $gss_lesson->menu_order : ''; ?>" class="form-control" /></div>
                        </div>
                    </div>
                </div>
                <?php if( (int)$microsite === 1) : ?>
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-12 col-form-label text-left" for="microsite-push">Push this swim lesson level to all sites</label>
                                <div class="col-12 text-left">
                                    <label class="form-check-label">
                                        <input type="checkbox" name="gss_push_to_sites" id="microsite-push" class="form-check-input" value="1" <?php echo (isset($gss_lesson->ID) && gss_get_post_meta( $microsite, $gss_lesson->ID, "gss_push_to_sites" )) ? 'checked' : '' ?>/>
                                        <span class="fa fa-check"></span><label for="microsite-push">Push</label>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php echo form_close(); ?>

<?php endif; ?>