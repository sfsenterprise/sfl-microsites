    <?php if( $this->router->fetch_class() === 'lessons' && $this->router->fetch_method() === 'group' ) : ?>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Group Name</th>
                                    <th>Slug</th>
                                    <th>Description</th>
                                    <th>Count</th>
                                </tr>
                            </thead>

                            <?php if($all_groups) : ?>
                                <tbody>
                                    <?php foreach( $all_groups as $group ) : ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('lessons/edit_group/'.$group->term_id); ?>"><?php echo $group->name; ?></a></td>
                                            <td><?php echo $group->slug; ?></td>
                                            <td><?php echo $group->description; ?></td>
                                            <td><?php echo $group->count; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing lesson group.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>
                         
                    </div>
                    <div class="card-footer">
                        <?php //$this->load->view('partials/pagination', $this->data); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php if( $this->router->fetch_class() === 'lessons' && ($this->router->fetch_method() === 'add_group' || $this->router->fetch_method() === 'edit_group') ) : ?>
    
    <?= form_open( sprintf( '/lessons/%1$s', ($this->uri->segment(4)) ? 'edit_group/'.$gss_lesson_group->term_id : 'add_group' ) ); ?>
        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                        <fieldset>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Group Name</label>
                                <div class="col-10"><input type="text" name="group_name" class="form-control" value="<?php echo isset($gss_lesson_group->term_id) ? $gss_lesson_group->name:'' ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Slug</label>
                                <div class="col-10"><input type="text" name="group_slug" class="form-control" value="<?php echo isset($gss_lesson_group->term_id) ? $gss_lesson_group->slug:'' ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Parents</label>
                                <div class="col-10">
                                    <select class="form-control" name="group_parent" id="">
                                        <option value="0">None</option>
                                        <?php foreach( $categories as $category ) : ?>
                                            <option value="<?php echo $category->term_id; ?>" <?php echo isset($gss_lesson_group->term_id) ? ($category->term_id===$gss_lesson_group->parent) ? 'selected' : '' :'' ?> ><?php echo $category->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Description</label>
                                <div class="col-10">
                                    <?php wp_editor( isset($gss_lesson_group->term_id) ? $gss_lesson_group->description:'', 'group_description', array('textarea_name'=>'group_description', 'editor_class'=>'form-control') ); ?>
                                </div>
                            </div>
                            <!--div class="form-group row">
                                <label class="col-2 col-form-label">Icon Class</label>
                                <div class="col-10"><?php //echo form_input(['name'=>'group_icon', 'type'=>'text', 'class'=>'form-control', 'value'=>isset($gss_lesson_group->term_id) ? gss_get_term_meta($microsite, $gss_lesson_group->term_id, 'group_icon'):'' ]); ?></div>
                            </div-->
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Image</label>
                                <div class="col-10">
                                    <?php $group_img = isset($gss_lesson_group->term_id) ? gss_get_term_meta($microsite, $gss_lesson_group->term_id, 'group_image') : ''; ?>
                                    <div class="btn btn-secondary" id="add-group">Select Image</div>
                                    <div class="btn btn-secondary" id="remove-group">Remove Image</div>
                                    <img id="group-featured-img" src="<?php echo wp_get_attachment_url($group_img); ?>" alt="" style="display: block;max-width: 250px;"/>
                                    <?php echo form_input(['name'=>'group_image', 'type'=>'hidden', 'id'=>'group-image', 'value'=>isset($gss_lesson_group->term_id)? $group_img : '']); ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">YouTube ID</label>
                                <div class="col-10"><?php echo form_input(['name'=>'group_youtube', 'type'=>'text', 'class'=>'form-control', 'value'=>isset($gss_lesson_group->term_id) ? gss_get_term_meta($microsite, $gss_lesson_group->term_id, 'group_youtube'):'']); ?></div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-12 text-right">
                                <?php echo form_submit('gss_lesson_group', ($this->uri->segment(4)) ? 'Update Group' : 'Add Group', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Order</label>
                            <div class="col-12 text-left"><input type="text" name="sort_lesson" value="<?php echo isset($gss_lesson_group->term_id) && isset($term_order[0]->term_order) ? $term_order[0]->term_order:''; ?>" class="form-control" /></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>

<?php endif; ?>