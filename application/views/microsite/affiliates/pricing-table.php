                <?php echo form_open( 'pricing/edit' ); ?>

                    <div class="row pricing-table">
                        <div class="col">
                            <div class="card">
                                <div class="card-block">
                                    <table class="table table-striped">
                                        <tbody>
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Group (4:1)</th>
                                                    <th>Mini 3/JR. (3:1)</th>
                                                    <th>Semi-Priv (2:1)</th>
                                                    <th>Private (1:1)</th>
                                                    <th>Swim Team</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <strong>First Child </strong> <em><span class="frequency">(Per Month)</span></em>
                                                    </td>

                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="first_child[group]" value="<?php echo  (isset($gss_pricing['first_child']['group'])) ? $gss_pricing['first_child']['group']:''; ?>" placeholder="0.00">
                                                            <br>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="first_child[mini]" value="<?php echo (isset($gss_pricing['first_child']['mini'])) ? $gss_pricing['first_child']['mini']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="first_child[semi_private]" value="<?php echo (isset($gss_pricing['first_child']['semi_private'])) ? $gss_pricing['first_child']['semi_private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="first_child[private]" value="<?php echo (isset($gss_pricing['first_child']['private'])) ? $gss_pricing['first_child']['private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="first_child[swim_force]" value="<?php echo (isset($gss_pricing['first_child']['swim_force'])) ? $gss_pricing['first_child']['swim_force']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <strong>Second Child</strong> <em><span class="frequency">(Per Month)</span></em>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="second_child[group]" value="<?php echo (isset($gss_pricing['second_child']['group'])) ? $gss_pricing['second_child']['group']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="second_child[mini]" value="<?php echo (isset($gss_pricing['second_child']['mini'])) ? $gss_pricing['second_child']['mini']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="second_child[semi_private]" value="<?php echo (isset($gss_pricing['second_child']['semi_private'])) ? $gss_pricing['second_child']['semi_private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="second_child[private]" value="<?php echo (isset($gss_pricing['second_child']['private'])) ? $gss_pricing['second_child']['private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="second_child[swim_force]" value="<?php echo (isset($gss_pricing['second_child']['swim_force'])) ? $gss_pricing['second_child']['swim_force']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <strong>Third Child</strong> <em><span class="frequency">(Per Month)</span></em>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="third_child[group]" value="<?php echo (isset($gss_pricing['third_child']['group'])) ? $gss_pricing['third_child']['group']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="third_child[mini]" value="<?php echo (isset($gss_pricing['third_child']['mini'])) ? $gss_pricing['third_child']['mini']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="third_child[semi_private]" value="<?php echo (isset($gss_pricing['third_child']['semi_private'])) ? $gss_pricing['third_child']['semi_private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="third_child[private]" value="<?php echo (isset($gss_pricing['third_child']['private'])) ? $gss_pricing['third_child']['private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="third_child[swim_force]" value="<?php echo (isset($gss_pricing['third_child']['swim_force'])) ? $gss_pricing['third_child']['swim_force']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <strong>Casual "Drop In" Lessons</strong> <em><span class="frequency">(Per Lesson)</span></em>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="casual_lessons[group]" value="<?php echo (isset($gss_pricing['casual_lessons']['group'])) ? $gss_pricing['casual_lessons']['group']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="casual_lessons[mini]" value="<?php echo (isset($gss_pricing['casual_lessons']['mini'])) ? $gss_pricing['casual_lessons']['mini']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="casual_lessons[semi_private]" value="<?php echo (isset($gss_pricing['casual_lessons']['semi_private'])) ? $gss_pricing['casual_lessons']['semi_private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="casual_lessons[private]" value="<?php echo (isset($gss_pricing['casual_lessons']['private'])) ? $gss_pricing['casual_lessons']['private']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="price-wrapper">
                                                            <input class="form-control" type="text" name="casual_lessons[swim_force]" value="<?php echo (isset($gss_pricing['casual_lessons']['swim_force'])) ? $gss_pricing['casual_lessons']['swim_force']:''; ?>" placeholder="0.00">
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row pricing-table">
                        <div class="col">
                            <div class="card">
                                <div class="card-block">
                                    <h2>Other Fees</h2>

                                    <table class="table table-striped">
                                        <tbody>
                                            <tr>
                                                <td><strong>Annual Membership Fee</strong></td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <span class=""></span>
                                                        <input class="form-control" type="text" name="annual_membership[fee]" value="<?php echo (isset($gss_pricing['annual_membership']['fee'])) ? $gss_pricing['annual_membership']['fee']:''; ?>" placeholder="0.00">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input class="form-control" type="text" name="annual_membership[second_child]" value="<?php echo (isset($gss_pricing['annual_membership']['second_child'])) ? $gss_pricing['annual_membership']['second_child']:''; ?>" placeholder="0.00"><span class="sub-label">Second Child</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input class="form-control" type="text" name="annual_membership[family_max]" value="<?php echo (isset($gss_pricing['annual_membership']['family_max'])) ? $gss_pricing['annual_membership']['family_max']:''; ?>" placeholder="0.00"><span class="sub-label">Family Max</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Family Swim</strong></td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input class="form-control" type="text" name="family_swim[fee]" value="<?php echo (isset($gss_pricing['family_swim']['fee'])) ? $gss_pricing['family_swim']['fee']:''; ?>" placeholder="0.00">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input class="form-control" type="text" name="family_swim[family_max]" value="<?php echo (isset($gss_pricing['family_swim']['family_max'])) ? $gss_pricing['family_swim']['family_max']:''; ?>" placeholder="0.00"><span class="sub-label">Family Max</span>
                                                    </div>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Jump Start Clinic</strong></td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input class="form-control" type="text" name="jump_start_clinic[fee]" value="<?php echo (isset($gss_pricing['jump_start_clinic']['fee'])) ? $gss_pricing['jump_start_clinic']['fee']:''; ?>" placeholder="0.00">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper no-before">
                                                        <input type="text" class="form-control" name="jump_start_clinic[start_day]" value="<?php echo (isset($gss_pricing['jump_start_clinic']['start_day'])) ? $gss_pricing['jump_start_clinic']['start_day']:''; ?>" placeholder="M"><span class="sub-label">Weekly Start Day</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper no-before">
                                                        <input type="text" class="form-control" name="jump_start_clinic[end_day]" value="<?php echo (isset($gss_pricing['jump_start_clinic']['end_day'])) ? $gss_pricing['jump_start_clinic']['end_day']:''; ?>" placeholder="F"><span class="sub-label">Weekly End Day</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Technique Clinic</strong></td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input type="text" class="form-control" name="technic_clinic[fee]" value="<?php echo (isset($gss_pricing['technic_clinic']['fee'])) ? $gss_pricing['technic_clinic']['fee']:''; ?>" placeholder="0.00">
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper no-before">
                                                        <input type="text" class="form-control" name="technic_clinic[start_day]" value="<?php echo (isset($gss_pricing['technic_clinic']['start_day'])) ? $gss_pricing['technic_clinic']['start_day']:''; ?>" placeholder="M"><span class="sub-label">Weekly Start Day</span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="price-wrapper no-before">
                                                        <input type="text" class="form-control" name="technic_clinic[end_day]" value="<?php echo (isset($gss_pricing['technic_clinic']['end_day'])) ? $gss_pricing['technic_clinic']['end_day']:''; ?>" placeholder="F"><span class="sub-label">Weekly End Day</span>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><strong>Party</strong></td>
                                                <td>
                                                    <div class="price-wrapper">
                                                        <input type="text" class="form-control" name="party[fee]" value="<?php echo (isset($gss_pricing['party']['fee'])) ? $gss_pricing['party']['fee']:''; ?>" placeholder="0.00">
                                                    </div>
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>

                                            <?php if( (int)$microsite !== 1 ) : ?>
                                                <tr>
                                                    <td></td>
                                                    <td><?php echo form_submit( 'gss_pricing' , 'Update Pricing Table', ['class'=>'btn btn-primary']); ?></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if( (int)$microsite === 1 ) : ?>
                        <div class="row">
                            <div class="col">
                                <div class="card">
                                    <div class="card-block">
                                        <table class="table table-striped">
                                            <tbody>
                                                <tr>
                                                    <td>Email Franchise Pricing Change Notifications To:</td>
                                                    <td><input type="text" class="form-control" name="email" value="<?php echo (isset( $gss_pricing['email'])) ?  $gss_pricing['email'] : ''; ?>" size="100" placeholder="Email Address here"/></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><?php echo form_submit( 'gss_pricing' , 'Update Pricing Table', ['class'=>'btn btn-primary']); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php echo form_close(); ?>
