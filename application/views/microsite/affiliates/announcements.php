    <?php if( $this->router->fetch_class() === 'announcements' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>
        
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Title</a></th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>

                            <?php if($all_announcements) : ?>
                                <tbody>
                                    <?php foreach( $all_announcements as $announcement ) : ?>
                                        <tr>
                                            <td><a target="_blank" href="<?php echo get_blog_permalink( $microsite, $announcement->ID ); ?>">View</a></td>
                                            <td><a href="<?php echo base_url('announcements/edit/'.$announcement->ID); ?>"><?php echo $announcement->post_title; ?></a></td>
                                            <td><?php echo nice_date($announcement->post_date, 'F d, Y') .' - '. date('h:i:s a', strtotime($announcement->post_date)); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing announcement.</h2></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>
                         
                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <?php if( $this->router->fetch_class() === 'announcements' && $this->router->fetch_method() === 'settings' ) : ?>
        <?= form_open( '/announcements/settings/' ); ?>

        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                        <fieldset>
                            <?php
                                $displays = get_blog_option($microsite, 'ff_announcements_settings');
                            ?>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Announcement to display</label>
                                <div class="col-10">
                                    <select name="ff_announcements_settings[announcement]" class="form-control" style="visibility:visible;opacity:1;display:block;">
                                        <option value="">-- No Announcement --</option>
                                        <?php foreach( $all_announcements as $announcement ) : ?>
                                            <option value="<?php echo $announcement->ID; ?>" <?php echo ( isset($displays['announcement']) && (int)$displays['announcement'] === $announcement->ID ) ? 'selected':'' ?> ><?php echo $announcement->post_title; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label" for="display_announcement">Display Announcement</label>
                                <div class="col-10">
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="form-check-input" type="checkbox" id="display_announcement" name="ff_announcements_settings[display]" value="checked" <?php echo (isset($displays['display']) && $displays['display'] === 'checked') ? 'checked':'' ?> />
                                            <span class="fa fa-check"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>                        
                    </div>
                </div>
            </div>

            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-12 text-left">
                                <?php echo form_submit( 'announcement_settings', 'Save Settings', ['class'=>'btn btn-primary'] ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php echo form_close(); ?>     
    <?php endif; ?>


<?php if( $this->router->fetch_class() === 'announcements' && ( $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'add') ) : ?>
    
    <?= form_open( sprintf( '/announcements/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Announcement Title</label>
                                    <div class="col-10"><input type="text" name="announcement_title" class="form-control" value="<?php echo isset($gss_announcement->post_title) ? $gss_announcement->post_title : ''; ?>" placeholder="Announcement Title"/></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Announcement Description</label>
                                    <div class="col-10">
                                        <?php switch_to_blog($microsite); ?>
                                        <?php wp_editor( isset($gss_announcement->ID) ? $gss_announcement->post_content:'', 'announcement_description', array('media_buttons' => true, 'editor_class'=>'form-control') ); ?>
                                        <?php restore_current_blog(); ?>
                                    </div>
                                </div>
                            </fieldset>
                    </div>
                </div>
            </div>

            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-7 text-left">
                                <?php if( isset($gss_announcement->ID) ) : ?>
                                    <div class="form-group">
                                        <strong>Date Created:</strong> <br/><?php echo nice_date($gss_announcement->post_date, 'M d, Y h:i:s a') ; ?>
                                    </div>

                                    <div class="form-group">
                                        <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_announcement->post_modified, 'M d, Y h:i:s a') ; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-5 text-right">
                                <?php echo form_submit( 'gss_announcement', ($this->uri->segment(4)) ? 'Update' : 'Add', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>

                        <?php if( isset($gss_announcement->ID) ) : ?>
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <a class="h2 text-primary" target="_blank" href="<?php echo get_blog_permalink( $microsite, $gss_announcement->ID ); ?>">View Page</a>
                                    </div>
                                    
                                    <div class="col-6 text-right">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>
                                        
                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 text-left" class="thickbox">Status</label>
                            <div class="col-12">
                                <select name="post_status" class="form-control">
                                    <option value="publish" <?php echo (isset($gss_announcement->ID) && isset($gss_announcement->post_status) && $gss_announcement->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                    <option value="draft" <?php echo (isset($gss_announcement->ID) && isset($gss_announcement->post_status) && $gss_announcement->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                    <option value="private" <?php echo (isset($gss_announcement->ID) && isset($gss_announcement->post_status) && $gss_announcement->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>

<?php endif; ?>