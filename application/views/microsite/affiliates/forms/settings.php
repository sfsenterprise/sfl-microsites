<?php echo form_open('form/settings'); ?>
    <div class="row">
        <div class="col-10">
            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Emma Account ID</label>
                            <div class="col-10"><input type="text" name="gravityformsaddon_gravityformsemma_settings[account_id]" value="<?php echo isset($emma_accnt_id) ? $emma_accnt_id:''; ?>" class="form-control"/></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Public API Key</label>
                            <div class="col-10"><input type="text" name="gravityformsaddon_gravityformsemma_settings[public_api_key]" value="<?php echo isset($public_api_key) ? $public_api_key:''; ?>" class="form-control"/></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Private API Key</label>
                            <div class="col-10"><input type="text" name="gravityformsaddon_gravityformsemma_settings[private_api_key]" value="<?php echo isset($private_api_key) ? $private_api_key:''; ?>" class="form-control"/></div>
                        </div>
                        <?php if ( isset($emma_groups) && $emma_groups !='' ): ?>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Group</label>
                                <div class="col-10">
                                    <select name="gravityformsaddon_gravityformsemma_settings[group_ids]" class="form-control">
                                        <?php foreach($emma_groups as $group):?>
                                            <option <?php if(isset($group_ids) && $group_ids == $group->member_group_id) echo 'selected '; ?> value="<?php echo $group->member_group_id;?>"><?php echo $group->group_name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </div>
                        <?php endif;?>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-12 text-center">
                            <input type="submit" name="form_settings" value="Save Settings" class="btn btn-primary"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>
