    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-block">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Form Name</th>
                                <th>Shortcode</th>
                                <th>Date Created</th>
                            </tr>
                            </thead>                   
                        <tbody>
                            <?php if($form_fields) : ?>
                                <?php foreach( $form_fields as $form ) : ?>
                                    <?php $form_id = gss_get_post_meta( $microsite, $form->ID, 'cf7_form_id' ); ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('/formpages/editform/'.$form->ID); ?>"><?php echo $form->post_title; ?></a></td>
                                            <td><input class="form-control" type="text" onfocus="this.select();" readonly="readonly" value='[contact-form-7 id="<?php echo $form->ID; ?>" title="<?php echo $form->post_title; ?>"]' class="large-text code"></td>
                                            <td><?php echo nice_date($form->post_date, 'M d, Y') .' - '. date('h:i:s a', strtotime($form->post_date)); ?></td>
                                        </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td><h2>No form created</h2></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>

                <?php $this->load->view('partials/pagination', $this->data); ?>
            </div>
        </div>
    </div>
