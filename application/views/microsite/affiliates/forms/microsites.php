<?php echo form_open('form/micrositestatus'); ?>
    <div class="row">
        <div class="col col-9">
            <div class="card">
                <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Form</th>
                                    <th class="text-center">Active</th>
                                </tr>
                                </thead>                   
                            <tbody>
                                <?php if($microsite_forms) : ?>
                                    <?php foreach( $microsite_forms as $form ) : ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('form/edit/'.$form->ID); ?>"><?php echo $form->post_title; ?></a></td>
                                            <td>
                                                <div class="form-check text-center">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="checkbox" value="<?php echo $form->ID; ?>" name="forms_status[]" <?php checked( $form->post_status, 'publish' ); ?>/>
                                                        <span class="fa fa-check"></span>
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <tr>
                                        <td><h2>No form added to microsites</h2></td>
                                        <td></td>
                                    </tr>
                            <?php endif; ?>
                            </tbody>
                        </table>
                </div>

                <?php //$this->load->view('partials/pagination', $this->data); ?>
            </div>
        </div>
        <div class="col col-3">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-12 text-left"><button name="update_status" class="btn btn-primary">Save Changes</button></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>