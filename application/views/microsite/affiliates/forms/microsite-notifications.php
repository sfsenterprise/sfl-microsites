                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Form Settings</label>
                            <div class="col-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#notiftab" role="tab" data-toggle="tab">Notifications</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#confirmationtab" role="tab" data-toggle="tab">Confirmation</a>
                                    </li>
                                </ul>
                                            
                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane fade in active show" id="notiftab">
                                        <?php if(isset($_mail['microsite']) && isset($_mail['microsite'][0])) : ?>
                                            <?php foreach( $_mail['microsite'] as $key => $form_setting ) : ?>
                                                <fieldset>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Send To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][{$key}][recipient]", 'value'=>isset($form_setting['recipient']) ? $form_setting['recipient']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">From</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][{$key}][sender]", 'value'=>isset($form_setting['sender']) ? $form_setting['sender']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Subject</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][{$key}][subject]", 'value'=>isset($form_setting['subject']) ? $form_setting['subject']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Reply To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][{$key}][additional_headers]", 'value'=>isset($form_setting['additional_headers']) ? $form_setting['additional_headers']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Message Body</label>
                                                        <div class="col-10">
                                                            <?php echo form_textarea(['name'=>"_sfmu_mail[microsite][{$key}][body]", 'value'=>isset($form_setting['body']) ? $form_setting['body']:'', 'cols'=>'10', 'rows'=>'15', 'class'=>'form-control', 'type'=>'text' ]); ?>
                                                            <span class="dashicons dashicons-trash remove text-danger float-right" style=""></span>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                                <fieldset>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Send To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][0][recipient]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">From</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][0][sender]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Subject</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][0][subject]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Reply To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[microsite][0][additional_headers]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Message Body</label>
                                                        <div class="col-10"><?php echo form_textarea(['name'=>"_sfmu_mail[microsite][0][body]", 'value'=>'', 'cols'=>'10', 'rows'=>'15', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                </fieldset>
                                            <?php endif; ?>
                                            <div class="btn btn-secondary" id="add-notif">Add Notification <span class="dashicons dashicons-plus-alt"></span></div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="confirmationtab">
                                        <fieldset>
                                            <div class="form-group row">
                                                <label class="col-2 col-form-label">Message</label>
                                                <div class="col-10">
                                                    <textarea name="_messages[mail_sent_ok]" class="form-control" cols="10" rows="4"><?php echo isset($_messages['mail_sent_ok']) ? $_messages['mail_sent_ok']:''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-2 col-form-label">Page Redirect</label>
                                                <div class="col-10">

                                                    <?php 
                                                        if( ! $redirect_page ){
                                                            switch_to_blog(1);
                                                                $post = get_post($parent_redirect);
                                                            restore_current_blog();

                                                            switch_to_blog($microsite);
                                                                $parent = get_page_by_title($post->post_title);
                                                            restore_current_blog();

                                                            if( $parent && isset($parent->ID) ){
                                                                $redirect_page = $parent->ID;
                                                            }
                                                            
                                                        }

                                                        switch_to_blog($microsite);
                                                            wp_dropdown_pages( ['selected'=>isset($redirect_page) ? $redirect_page:'', 'name'=>'redirect_page', 'depth'=>1, 'class'=>'form-control', 'show_option_none'=>'Select Page'] ); 
                                                        restore_current_blog();
                                                    ?> 
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>        
                            </div>
                        </div>
                    </fieldset>