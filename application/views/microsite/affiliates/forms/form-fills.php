<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-block">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Form Name</th>
                            <th>Action</th>
                            <th>Count</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if($micro_forms) : ?>
                            <?php foreach($micro_forms as $micro_id => $micro_count) : ?>
                                <?php $form = get_post($micro_id); ?>
                                <tr>
                                    <td><a href="<?php echo base_url('form/leads/'.$micro_id); ?>"><?php echo $form->post_title; ?></a></td>
                                    <td><a href="<?php echo base_url('form/leads/'.$micro_id); ?>">View</a></td>
                                    <td><a href="<?php echo base_url('form/leads/'.$micro_id); ?>"><?php echo $micro_count; ?></a></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>

            <?php $this->load->view('partials/pagination', $this->data); ?>
        </div>
    </div>
</div>