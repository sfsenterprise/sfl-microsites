<?php if( ($this->router->fetch_class() === 'formpages' || $this->router->fetch_class() === 'form') && ( $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'editpage' || $this->router->fetch_method() === 'addpage' ) ) : ?>

    <?php if( $this->router->fetch_class() === 'form' ) : ?>
        <?= form_open( sprintf( '/form/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
    <?php else : ?>
        <?= form_open( sprintf( '/formpages/%1$s', ($this->router->fetch_method() === 'addpage') ? 'addpage' : 'editpage/'.$this->uri->segment(4) ) ); ?>
    <?php endif; ?>
        <div class="row">
            <div class="col col-9">
                <div class="card">
                    <div class="card-block">
                        <fieldset>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Form Name</label>
                                <div class="col-10">
                                    <input type="text" name="name" class="form-control" value="<?php echo isset($gss_forms->post_title) ? $gss_forms->post_title:''; ?>" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Slug</label>
                                <div class="col-10">
                                    <input type="text" name="slug" class="form-control" value="<?php echo isset($gss_forms->post_name) ? $gss_forms->post_name:''; ?>" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Page Content</label>
                                <div class="col-10">
                                    <?php wp_editor( isset($gss_forms) ? $gss_forms->post_content : '', 'wpcf7_form', ['textarea_name'=>'content'] ); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <?php if($this->router->fetch_class() === 'form' && ($this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit')) : ?>
                <div class="card">
                    <div class="card-block">
                        <?php $this->load->view('microsite/affiliates/forms/microsite-notifications'); ?>
                    </div>
                </div>
                <?php endif; ?>

            </div>
            <div class="col col-3">
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-7 text-left">
                                <?php if( isset($gss_forms->ID) ) : ?>
                                    <div class="form-group">
                                        <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $gss_forms->ID ); ?>
                                    </div>

                                    <div class="form-group">
                                        <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_forms->post_modified, 'M d, Y h:i:s a') ; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="col-5 text-right">
                                <?php echo form_submit('gss_forms', ($this->uri->segment(4)) ? 'Update Form' : 'Add Form', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>

                        <?php if( isset($gss_forms->ID) ) : ?>
                            <div class="row">
                                <div class="col-6 text-left">
                                    <a class="h2 text-primary" target="_blank" href="<?php echo get_blog_permalink( ($this->router->fetch_class() === 'formpages') ? 1 : $microsite, $gss_forms->ID ); ?>">View Page</a>
                                </div>

                                <div class="col-6 text-right">
                                    <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                    <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header pb-0">
                                                    <div class="modal-title">Are you sure to delete?</div>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <a class="btn btn-primary" href="<?php echo base_url('/formpages/delete/' . $gss_forms->ID . '/'); ?>">Yes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Add Form</label>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <select class="form-control" name="form_shortcode">
                                    <option value="">Choose a form</option>
                                    <?php foreach($form_fields as $form_field) : ?>
                                        <option <?php echo (isset($parent_form) && (int)$parent_form === (int)$form_field->ID) ? 'selected':''; ?> <?php echo ( (!isset($parent_form) || !$parent_form) && (int)$form_shortcode === (int)$form_field->ID ) ? 'selected':''; ?> value="<?php echo $form_field->ID; ?>"><?php echo $form_field->post_title; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-block">
                         <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Form Position</label>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input type="radio" name="form_position" id="above-content" value="1" <?php checked( $form_position, 1 ); ?> /> <label for="above-content">Above Content</label>
                            </div>
                            <div class="col-12">
                                <input type="radio" name="form_position" id="below-content" value="2" <?php checked( $form_position, 2 ); ?> <?php echo ($form_position === 0) ? 'checked':'' ?> /> <label for="below-content">Below Content</label>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if($this->router->fetch_class() === 'formpages') : ?>
                <div class="card">
                    <div class="card-block">
                         <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Global Form</label>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <div class="form-check">
                                    <label class="form-check-label text-left">
                                        <input type="checkbox" name="_sfmu_default_page" class="form-check-input" value="yes" <?php checked( $default, 'yes' ); ?>/>
                                        <span class="fa fa-check"></span>
                                        Set as global
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <div class="card">
                    <div class="card-block">
                         <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Header Image Override</label>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <a href="" id="set-post-thumbnail" style="display: block;">Set header image</a>
                                <input type="hidden" name="_gss_plugin_header_image-id" value="3643" id="_gss_plugin_header_image-id">
                                <div class="selected-image" style="display: none;">
                                    <a href="#"><img src="" alt="" id="post-featured-src"></a>
                                    <div><em><span>Click the image to edit or update</span></em></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <label class="col-12 text-left">Status</label>
                            <div class="col-12">
                                <select name="post_status" class="form-control">
                                    <option value="publish" <?php echo (isset($gss_forms) && $gss_forms->post_status==='publish') ? 'selected':''; ?> >Publish</option>
                                    <option value="draft" <?php echo (isset($gss_forms) && $gss_forms->post_status==='draft') ? 'selected':''; ?> >Draft</option>
                                    <option value="private" <?php echo (isset($gss_forms) && $gss_forms->post_status==='private') ? 'selected':''; ?> >Private</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <?php echo form_close(); ?>
<?php endif; ?>

<?php if( $this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'editform' || $this->router->fetch_method() === 'addform' ) ) : ?>
    <?= form_open( sprintf( '/formpages/%1$s', ($this->router->fetch_method() === 'addform') ? 'addform' : 'editform/'.$this->uri->segment(4) ) ); ?>

    <div class="row">
        <div class="col col-9">
            <div class="card form-settings">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Form Name</label>
                            <div class="col-10">
                                <input type="text" name="name" class="form-control" value="<?php echo (isset($gss_form->post_title)) ? $gss_form->post_title:''; ?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Form Settings</label>
                            <div class="col-10">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#formtab" role="tab" data-toggle="tab">Form</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#notiftab" role="tab" data-toggle="tab">Notifications</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#confirmationtab" role="tab" data-toggle="tab">Confirmation</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active show" id="formtab">
                                        <div class="form-tags">
                                            <span id="tag-generator-list">
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-text" class="thickbox button" data-id="text" title="Form-tag Generator: text">text</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-email" class="thickbox button" data-id="email" title="Form-tag Generator: email">email</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-url" class="thickbox button" data-id="url" title="Form-tag Generator: URL">URL</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-tel" class="thickbox button" data-id="tel" title="Form-tag Generator: tel">tel</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-number" class="thickbox button" data-id="number" title="Form-tag Generator: number">number</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-date" class="thickbox button" data-id="date" title="Form-tag Generator: date">date</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-textarea" class="thickbox button" data-id="textarea" title="Form-tag Generator: text area">text area</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-menu" class="thickbox button" data-id="select" title="Form-tag Generator: drop-down menu">drop-down menu</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-checkbox" class="thickbox button" data-id="checkbox" title="Form-tag Generator: checkboxes">checkboxes</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-radio" class="thickbox button" data-id="radio" title="Form-tag Generator: radio buttons">radio buttons</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-recaptcha" class="thickbox button" data-id="recaptcha" title="Form-tag Generator: reCAPTCHA">reCAPTCHA</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-file" class="thickbox button" data-id="file" title="Form-tag Generator: file">file</a>
                                                <a href="#TB_inline?width=750&amp;height=500&amp;inlineId=tag-generator-panel-submit" class="thickbox button" data-id="submit" title="Form-tag Generator: submit">submit</a>
                                            </span>
                                        </div>

                                        <?php wp_editor( isset($_form) ? $_form : '', 'wpcf7_form', ['wpautop'=>false, 'media_buttons'=>false, 'tinymce'=>false, 'quicktags'=>false] ); ?>

                                        <?php if( isset($form_id) ) : ?>
                                            <div class="shortcode wp-ui-highlight">
                                                <br/><div><strong>Copy this shortcode and paste it into your post, page, content:</strong></div>
                                                <div class="card card-inverse card-primary mb-3 text-center">
                                                    <div class="card-block">
                                                        <input type="text" id="wpcf7-shortcode" class="form-control" onfocus="this.select();" readonly="readonly" class="large-text code" value='[contact-form-7 id="<?php echo isset($form_id) ? $form_id : '' ?>" title="<?php echo isset($form_id) ? $gss_forms->post_title : ''; ?>"]'>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div><!-- #formtab -->
                                    
                                    <div role="tabpanel" class="tab-pane fade <?php echo ($this->router->fetch_method() === 'micrositeform') ? 'in active show':''; ?>" id="notiftab">
                                        <?php if( isset($_mail['main']) && isset($_mail['main'][0]) ) : ?>
                                            <?php foreach( $_mail['main'] as $key => $form_setting ) : ?>
                                                <fieldset>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Send To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][{$key}][recipient]", 'value'=>isset($form_setting['recipient']) ? $form_setting['recipient']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">From</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][{$key}][sender]", 'value'=>isset($form_setting['sender']) ? $form_setting['sender']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Subject</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][{$key}][subject]", 'value'=>isset($form_setting['subject']) ? $form_setting['subject']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Reply To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][{$key}][additional_headers]", 'value'=>isset($form_setting['additional_headers']) ? $form_setting['additional_headers']:'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Message Body</label>
                                                        <div class="col-10">
                                                            <?php echo form_textarea(['name'=>"_sfmu_mail[main][{$key}][body]", 'value'=>isset($form_setting['body']) ? $form_setting['body']:'', 'cols'=>'10', 'rows'=>'15', 'class'=>'form-control', 'type'=>'text' ]); ?>
                                                            <span class="dashicons dashicons-trash remove text-danger float-right" style=""></span>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                                <fieldset>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Send To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][0][recipient]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">From</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][0][sender]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Subject</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][0][subject]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Reply To</label>
                                                        <div class="col-10"><?php echo form_input(['name'=>"_sfmu_mail[main][0][additional_headers]", 'value'=>'', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-2 col-form-label">Message Body</label>
                                                        <div class="col-10"><?php echo form_textarea(['name'=>"_sfmu_mail[main][0][body]", 'value'=>'', 'cols'=>'10', 'rows'=>'15', 'class'=>'form-control', 'type'=>'text' ]); ?></div>
                                                    </div>
                                                </fieldset>
                                            <?php endif; ?>
                                            <div class="btn btn-secondary" id="add-notif">Add Notification <span class="dashicons dashicons-plus-alt"></span></div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="confirmationtab">
                                        <fieldset>
                                            <div class="form-group row">
                                                <label class="col-2 col-form-label">Message</label>
                                                <div class="col-10">
                                                    <textarea name="wpcf7-messages[mail_sent_ok]" class="form-control" cols="10" rows="4"><?php echo isset($_messages['mail_sent_ok']) ? $_messages['mail_sent_ok']:''; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-2 col-form-label">Page Redirect</label>
                                                <div class="col-10">
                                                    <?php wp_dropdown_pages( ['selected'=>isset($redirect_page) ? $redirect_page:'', 'name'=>'redirect_page', 'depth'=>1, 'class'=>'form-control', 'show_option_none'=>'Select Page'] ); ?>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

        <div class="col col-3">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-6 text-left">
                            <?php echo form_submit('form_fields', ($this->uri->segment(4)) ? 'Update Form' : 'Add Form', ['class'=>'btn btn-primary']); ?>
                        </div>

                        <div class="col-6 text-right">
                            <?php if( isset($gss_form->ID) ) : ?>
                                <div class="row">
                                    <div class="col-12">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php if($this->uri->segment(4)) : ?>
                <div class="card">
                    <div class="card-block">
                        <div class="form-group row">
                            <div class="col-12 text-left">
                                <input type="text" id="wpcf7-shortcode" onfocus="this.select();" readonly="readonly" class="form-control" value='[contact-form-7 id="<?php echo (isset($gss_form->ID)) ? $gss_form->ID:''; ?>" title="<?php echo (isset($gss_form->post_title)) ? $gss_form->post_title:''; ?>"]'>
                                <em>Copy this shortcode and paste it into your post, page, or text widget content:</em>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <?php echo form_close(); ?>

    <?php $this->load->view('microsite/affiliates/forms/form-tags-generator'); ?>

<?php endif; ?>
