<?php $this->load->helper('leads_helper'); ?>
<?php 
    /*$ipaddress = '';
    
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    
    if( filter_var($ipaddress, FILTER_VALIDATE_IP) ){
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ipaddress)); 
        $timezone = geoip_time_zone_by_country_and_region($ipdat->geoplugin_countryCode);
    }*/
?>

<?php if( $this->router->fetch_class() === 'form' && $this->router->fetch_method() === 'leads' ) : ?>
    
    <?php $cols = array(); ?>
    
    <?php 
        switch ($form_name) {
            case 'General Contact Form':
                $col_list = array( 
                    '1.3' => array(
                            "first" => array(
                                    'contact-first-name', 'First', 'Name (First)', 'First'
                                )
                        ),
                    '1.6' => array(
                            "last" => array(
                                    'contact-last-name', 'Last', 'Name (Last)', 'Last'
                                )
                        ),
                    '2' => array(
                            "email" => array(
                                    'contact-email', 'Email'
                                )
                        ),
                    '3' => array(
                            "phone" => array(
                                    'contact-phone', 'Phone'
                                )
                        ),
                );

                break;

            case 'Pre-Registration Form - Full (with additional kids)':
                $col_list = array( 
                    '1' => array(
                            "child's first name" => array(
                                'child-first-name', 'child-first-name-1', 'Child\'s First Name'
                            )
                        ), 
                    '24' => array(
                            "child's last name" => array('child-last-name', 'child-last-name-24', 'Child\'s Last Name', '2')
                        ), 
                    '3' => array(
                            "child's date of birth" => array('child-dob', 'child-dob-3', "Child's Date of Birth")
                        ), 
                    '4' => array(
                            "gender" => array('child-gender', 'child-gender-4', 'Gender')
                        )
                );

                $old_rg_form_cols = array('1', '2', '3', '4');

                break;

            case 'Cancel Form':
                $col_list = array( 
                    '2' => array(
                            "student's first name" => array(
                                    'student-first-name', 'Student\'s First Name'
                                )
                        ),
                    '3' => array(
                            "student's last name" => array(
                                    'student-last-name', "Student's Last Name"
                                )
                        ),
                    '4' => array(
                            "lesson day and time" => array(
                                    'lesson-day-time', "Lesson Day and Time"
                                )
                        ),
                    '5' => array(
                            "date you will be cancelling for" => array(
                                    'date-of-cancellation', "Date you will be cancelling for"
                                )
                        ),
                );
                break;

            case 'Pre-Employment Application':
                $col_list = array( 
                    '15.3' => array(
                            "first" => array(
                                    'applicant-first-name', 'Name (First)', 'First'
                                )
                        ),
                    '15.6' => array(
                            "last" => array(
                                    'applicant-last-name', 'Name (Last)', 'Last'
                                )
                        ),
                    '14' => array(
                            "phone" => array(
                                    'applicant-phone', 'Phone'
                                )
                        ),
                    '13' => array(
                            "email" => array(
                                    'applicant-email', 'Email'
                                )
                        ),
                );

                break;

            case 'Refer-a-Friend Form':
                $col_list = array( 
                    '2.3' => array(
                            "first" => array(
                                    'Your Name (First)', 'your-first-name', 'First'
                                )
                        ),
                    '2.6' => array(
                            "last" => array(
                                    'Your Name (Last)', 'your-last-name', 'Last'
                                )
                        ),
                    '3' => array(
                            "your email" => array(
                                    'Your Email', 'your-email'
                                )
                        ),
                    '5.3' => array(
                            "referral email" => array(
                                    'Referral Email', 'referral-email'
                                )
                        ),
                );

                break;

            case 'Register Form':
                $col_list = array( 
                    '1' => array(
                            "child's first name" => array("Child's First Name")
                        ), 
                    '29' => array(
                            "child birthdate" => array("Child Birthdate")
                        ),
                    '10' => array(
                            "would you like your child to swim independently, without a parent?" => array("")
                        ),
                    '33' => array(
                            "is your baby able to hold their head up independently?" => array("")
                        ),
                );

                $old_rg_form_cols = array();

                break;

            case 'Holiday Form':
                $col_list = array( 
                    '1.3' => array(
                            "First" => array(
                                'First', 'first-name'
                            )
                        ), 
                    '1.6' => array(
                            "Last" => array(
                                'Last', 'last-name'
                            )
                        ),
                    "2" => array(
                            "Phone" => array(
                                'Phone', 'phone'
                            )
                        ),
                    "3" => array(
                            "Email" => array(
                                'Email', 'email'
                            )
                        ),
                );

                $old_rg_form_cols = array();
                break;

            case 'Newsletter':
                $col_list = array( 
                    '2' => array(
                            "email address" => array(
                                'Email Address', 'email address'
                            )
                        ), 
                );

                $old_rg_form_cols = array();
                break;

            default:
                $col_list = array();
                $old_rg_form_cols = array();
                break;
        }
    ?>

    <?php $formname = str_replace(' ', '_', strtolower($form_name)); ?>
    <?php $headers_file = SFL_MICROSITES_DIR . "application/views/microsite/affiliates/forms/leads_header/{$formname}.php"; ?>
                                    
    <?php if( file_exists($headers_file) ) : ?>
        <?php include_once $headers_file; ?>
    <?php else : ?>
        <?php $headers = array(); ?>
        <?php if($rg_form) : ?>
            <?php $form = get_form_meta($rg_form[0]->id, $microsite); ?>
            <?php $form_meta = json_decode($form[0]->display_meta); ?>

            <?php if( is_array($form_meta->fields) && empty($form_meta->fields) ) : ?>
                <?php $form = get_form_meta($rg_form[0]->id, 1); ?>
                <?php $form_meta = json_decode($form[0]->display_meta); ?>
            <?php endif; ?>

            <?php $head_counter = 0; ?>
            <?php $first_last = array(); ?>
            <?php foreach ($form_meta->fields as $field) : ?>
                        
                <?php if( $head_counter <= 3 ) : ?>

                    <?php if( $field->type !== 'section' && (isset($field->displayOnly) && ! $field->displayOnly) || is_array($field->inputs) ) : ?>

                        <?php if( ! empty($field->inputs) ) : ?>

                            <?php foreach($field->inputs as $input) : ?>

                                <?php if( ! isset($input->isHidden) ) : ?>

                                    <?php $segment = explode(' ', strtolower(str_replace(':', '', $input->label))); ?>

                                    <?php if( !in_array('location', $segment) && !in_array(strtolower($input->label), $headers) ) : ?>
                                        <?php $headers[$input->id] = str_replace(':', '', strtolower($input->label)); $head_counter++; ?>

                                        <?php if( stripos($input->label, 'first') !== false ) : ?>
                                            <?php $first_last['first'] = $input->id; ?>
                                        <?php endif; ?>

                                        <?php if( stripos($input->label, 'last') !== false ) : ?>
                                            <?php $first_last['last'] = $input->id; ?>
                                        <?php endif; ?>

                                    <?php endif; ?>

                                <?php endif; ?>

                            <?php endforeach; ?>

                        <?php else : ?>

                            <?php $segment = explode(' ', strtolower(str_replace(':', '', $field->label))); ?>

                            <?php if( !in_array('location', $segment) && !in_array(strtolower($field->label), $headers) ) : ?>
                                <?php $headers[$field->id] = str_replace(':', '', strtolower($field->label)); $head_counter++; ?>

                                <?php if( stripos($field->label, 'first') !== false ) : ?>
                                    <?php $first_last['first'] = $field->id; ?>
                                <?php endif; ?>

                                <?php if( stripos($field->label, 'last') !== false ) : ?>
                                    <?php $first_last['last'] = $field->id; ?>
                                <?php endif; ?>

                            <?php endif; ?>

                        <?php endif; ?>

                    <?php elseif( $field->type !== 'section' ) : ?>
                        
                        <?php if( is_array($field->inputs) && ! empty($field->inputs) ) : ?>
                            <?php foreach($field->inputs as $input) : ?>

                                <?php if( ! isset($input->isHidden) ) : ?>

                                    <?php $segment = explode(' ', strtolower(str_replace(':', '', $input->label))); ?>

                                    <?php if( !in_array('location', $segment) && !in_array(strtolower($input->label), $headers) ) : ?>
                                        <?php $headers[$input->id] = str_replace(':', '', strtolower($input->label)); $head_counter++; ?>

                                        <?php if( stripos($input->label, 'first') !== false ) : ?>
                                            <?php $first_last['first'] = $input->id; ?>
                                        <?php endif; ?>

                                        <?php if( stripos($input->label, 'last') !== false ) : ?>
                                            <?php $first_last['last'] = $input->id; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                <?php endif; ?>

                            <?php endforeach; ?>
                        <?php else : ?>
                            <?php $segment = explode(' ', strtolower(str_replace(':', '', $field->label))); ?>

                            <?php if( !in_array('location', $segment) && !in_array(strtolower($field->label), $headers) ) : ?>
                                <?php $headers[$field->id] = str_replace(':', '', strtolower($field->label)); $head_counter++; ?>

                                <?php if( stripos($field->label, 'first') !== false ) : ?>
                                    <?php $first_last['first'] = $field->id; ?>
                                <?php endif; ?>

                                <?php if( stripos($field->label, 'last') !== false ) : ?>
                                    <?php $first_last['last'] = $field->id; ?>
                                <?php endif; ?>

                            <?php endif; ?>
                        <?php endif; ?>

                    <?php endif; ?>
                                                
                <?php endif; ?>

            <?php endforeach; ?>


        <?php endif; ?>
    <?php endif; ?>

            <?php if( isset($leads) && $leads ) : ?>
                <div class="col-12">
                    <div class="row">
                        <div class="col-6">
                            <?php echo form_open( base_url(sprintf('/form/export/%1s', $this->uri->segment(4))) ); ?> 
                            <input type="hidden" name="form_name" value="<?php echo $form_name; ?>"/>
                            <div class="row">
                                <div class="col-2 text-center"><button class="btn btn-primary">Export Leads</button></div>
                                <div class="col-8">
                                    <div class="row">
                                        <div class="col-6"><input id="export-range-start" class="form-control datepicker" name="export_start" placeholder="Start Date"></div>
                                        <div class="col-6"><input id="export-range-end" class="form-control datepicker" name="export_end" placeholder="End Date"></div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                        <div class="col-6">
                            <?php echo form_open( sprintf(base_url('/form/leads/%1s'), $this->uri->segment(4) ), ['method'=>'get'] ); ?> 
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-3">
                                    <select name="searchby" class="form-control" id="lead-search">
                                        <optgroup label="Search by">
                                            <option value="last<?php echo isset($first_last['last']) ? '-' . str_replace('.', '_', $first_last['last']):''; ?>">Last Name</option>
                                            <option value="first<?php echo isset($first_last['first']) ? '-' . str_replace('.', '_', $first_last['first']):''; ?>">First Name</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-4 pl-0 pr-0"><input type="text" name="search" class="form-control" placeholder="Search here"></div>
                                <div class="col-2"><input type="submit" value="Search" class="btn btn-secondary"></div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>

    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-block">

                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Action</th>

                                <?php foreach ($headers as $key => $header) : ?>
                                    <th width="18%"><?php echo str_replace('Field', '', ucwords($header)); ?></th>
                                <?php endforeach; ?>

                                <th>Date Created</th>

                            </tr>
                        </thead>

                        <tbody>
                            <?php if($leads) : ?>

                                <?php foreach($leads as $lead) : ?>
                                    <tr>
                                        <td><a href="<?php echo base_url('form/leaddetails/'.$lead->id); ?>">View</a></td>

                                            <?php $details = get_lead_details($lead->id, $microsite); ?>
                                            <?php $fields = array(); ?>

                                            <?php if( $details ) : ?>
                                                <?php foreach($details as $detail) : ?>
                                                    <?php if( isset($details[0]->field_name) ) : ?>
                                                        <?php 
                                                            $bom = pack('H*','EFBBBF');
                                                            $fieldname = preg_replace("/^$bom/", '', $detail->field_name);
                                                        ?>

                                                        <?php $fields[] =  strtolower(get_fieldname_col($col_list, $fieldname)) ?: trim(strtolower($detail->field_name)); ?>
                                                    <?php else : ?>
                                                        <?php $fields[] =  $detail->field_number; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>

                                            <?php endif; ?>

                                            <?php if($rg_form) : ?>
                                                <?php $already_added = array(); ?>
                                                <?php $counter = 0; ?>

                                                <?php foreach( $headers as $key => $header ) : ?>

                                                    <?php if( isset($details[0]->field_name) && !in_array($header, $fields) ) : ?>

                                                        <td></td>
                                                        <?php $counter++; ?>

                                                    <?php elseif( isset($details[0]->field_number) && !in_array($key, $fields) ) : ?>

                                                        <!-- fallback for old rg form -->
                                                        <?php foreach( $details as $detail ) : ?>
                                                            <?php if( !in_array($detail->field_number, $old_rg_form_cols) ) : ?>
                                                                <?php if( $counter < count($headers) ) : ?>
                                                                    <td></td>
                                                                    <?php $counter++; ?>
                                                                <?php else : ?>
                                                                    <?php continue; ?>
                                                                <?php endif; ?>

                                                            <?php else : ?>
                                                                <?php if( $counter <= count($headers) ) : ?>
                                                                    <td><?php echo $detail->value; ?></td>
                                                                    <?php $counter++; ?>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                       
                                                    <?php else : ?>

                                                        <?php foreach( $details as $detail ) : ?>

                                                            <?php if($counter <= count($headers) && !in_array($header, $already_added)) : ?>

                                                                <?php if( isset($detail->field_name) ) : ?>

                                                                    <?php 
                                                                        $bom = pack('H*','EFBBBF');
                                                                        $bom_removed = preg_replace("/^$bom/", '', $detail->field_name);
                                                                    ?>

                                                                    <?php $fieldname = get_fieldname_col($col_list, $bom_removed); ?>
                                                                    <?php 
                                                                        $m_d_y = explode('-', $detail->value);
                                                                    ?>
                                                                    
                                                                    <?php $test = $warning = $error = ''; ?>
                                                                    <?php if( gettype($detail->value) === 'string' && (stripos($fieldname, 'date') !== FALSE || stripos($fieldname, 'dob') !== FALSE ) ) : ?>
                                                                        <?php if( (strlen($detail->value) < 10 && stripos($detail->value, '/') === FALSE ) || stripos($detail->value, 'Today') !== FALSE || stripos($detail->value, '@') !== FALSE ) : ?>
                                                                            <?php $value = $detail->value; ?>
                                                                        <?php elseif( is_array($m_d_y) && !empty($m_d_y) && isset($m_d_y[2]) && strlen($m_d_y[2]) <= 4 ) : ?>
                                                                            <?php $value = $m_d_y[2] . '-' . $m_d_y[0] . '-' .$m_d_y[1]; ?>
                                                                        <?php else : ?>
                                                                            <?php $value =  strlen($detail->value) > 2 ? date_format(date_create($detail->value),"Y-m-d") : ''; ?>
                                                                        <?php endif; ?>

                                                                    <?php else : ?>
                                                                        <?php $value = $detail->value; ?>
                                                                    <?php endif; ?>

                                                                    <?php if( $header == $fieldname ) : ?>
                                                                        <td><?php echo $value; ?></td>
                                                                        <?php $counter++; ?>
                                                                        <?php $already_added[] = $header; ?>
                                                                    <?php elseif( $header === strtolower($fieldname) ) : ?>
                                                                        <td><?php echo $value; ?></td>
                                                                        <?php $already_added[] = $header; ?>
                                                                        <?php $counter++; ?>
                                                                    <?php else : ?>
                                                                        <?php continue; ?>
                                                                    <?php endif; ?>

                                                                <?php elseif( isset($detail->field_number) ) : ?>

                                                                    <?php if( $key != $detail->field_number ) : ?>
                                                                        <?php continue; ?>
                                                                    <?php else : ?>
                                                                        <td><?php echo $detail->value; ?></td>
                                                                        <?php $counter++; ?>
                                                                    <?php endif; ?>

                                                                <?php endif; ?>

                                                            <?php endif; ?>

                                                        <?php endforeach; ?>

                                                    <?php endif;  ?>
                                       
                                                <?php endforeach; ?>

                                            <?php else : ?>
                                                <?php $counter = 0; ?>

                                                <?php foreach( $headers as $key => $header ) : ?>

                                                    <?php if( isset($details[0]->field_name) && !in_array($key, $fields) ) : ?>
                                                        
                                                        <?php if( $key == '_loc_address' ) : ?>

                                                            <?php foreach( $details as $detail ) : ?>
                                                                <?php if( strtolower($detail->field_name) === 'location' ) : ?>
                                                                    <td><?php echo get_microsite_by_email($detail->value); ?></td>
                                                                    <?php $counter++; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                            
                                                        <?php else : ?>
                                                            <td></td>
                                                            <?php $counter++; ?>
                                                        <?php endif; ?>

                                                    <?php elseif( !in_array($key, $fields) ) : ?>
                                                        <?php if( $key == '_loc_address' ) : ?>

                                                            <?php foreach( $details as $detail ) : ?>
                                                                <?php if( strtolower($detail->field_name) === 'location' ) : ?>
                                                                    <td><?php echo get_microsite_by_email($detail->value); ?></td>
                                                                    <?php $counter++; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                            
                                                        <?php else : ?>
                                                            <td></td>
                                                            <?php $counter++; ?>
                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <?php if( $counter <= 3 ) : ?>
                                                            <?php foreach( $details as $detail ) : ?>
                                                                <?php if( strtolower($key) != strtolower($detail->field_name) ) : ?>
                                                                    <?php continue; ?>
                                                                <?php else : ?>
                                                                    <td><?php echo stripslashes_deep($detail->value); ?></td>
                                                                    <?php $counter++; ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        
                                        <?php 
                                            /*$timestamp = $lead->date_created;
                                            $tm = date_default_timezone_get();

                                            $date = new DateTime();
                                            $date->setTimezone(new DateTimeZone( $timezone ));   
                                            $date->setTimestamp( strtotime($timestamp) );                           
                                            $local = $date->format('M d, Y - h:i:sa');*/ 
                                        ?>
                                        <td>
                                            <?php echo isset($lead->date_created) ? nice_date($lead->date_created, 'M d, Y') .' - '. date('h:i:s a', strtotime($lead->date_created)) : ''; ?>
                                            <?php //echo $local; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td><h2>No leads for this form</h2></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            <?php endif; ?>
                            
                        </tbody>
                    </table>
                </div>

                <?php $this->load->view('partials/pagination', $this->data); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if( $this->router->fetch_class() === 'form' && $this->router->fetch_method() === 'leaddetails' ) : ?>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-block">
                        <fieldset>
                            <?php foreach( $details as $detail ) : ?>
                                <?php $detail = (object)$detail; ?>
                                
                                <?php if( @$detail->field_name !== 'location' && @$detail->field_name !== 'hidden_referrer' && @$detail->field_name !== 'hidden_url' ) : ?>
                                    <div class="form-group row">

                                        <?php 
                                            if( isset($detail->field_number) ){
                                                $field = $detail->field_number;
                                                $form = get_form_meta($detail->form_id, $microsite);
                                                $form_meta = json_decode($form[0]->display_meta);
                                            }else{
                                                $field = $detail->field_name;
                                            }
                                        ?>

                                        <label class="col-2 col-form-label">                                        
                                            <?php if( isset($detail->field_number) ) : ?>
                                                <?php foreach($form_meta->fields as $name) : ?>

                                                    <?php if( strpos($detail->field_number, '.') ) : ?>
                                                        <?php if( (int)floor($detail->field_number) === (int)$name->id ) : ?>

                                                            <?php foreach($name->inputs as $inputs) : ?>
                                                                <?php if($inputs->id == $detail->field_number) : ?>
                                                                    <?php echo ucwords($inputs->label); ?>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>

                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <?php if((int)$name->id === (int)$detail->field_number) : ?>
                                                            <?php echo ucwords($name->label); ?>
                                                        <?php endif; ?>
                                                    <?php endif; ?>

                                                <?php endforeach; ?>
                                            <?php else : ?>
                                                <?php if($detail->field_name !== 'location') : ?>
                                                    <?php if( $detail->field_name === '_loc_address' ) : ?>
                                                        <th>Location</th>
                                                    <?php else : ?>
                                                        <?php echo ucwords(str_replace('_', ' ', str_replace('-', ' ', $detail->field_name))) .':'; ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </label>
                                        
                                        <div class="col-10">
                                            <?php if ( strpos($detail->value, '@') === false && strpos($detail->value, 'http') === false && strlen($detail->value) > 40 ) : ?>
                                                <?php $long_detail = get_long_detail($detail->id, $microsite); ?>
                                                <textarea readonly class="form-control" cols="30" rows="6"><?php echo isset($long_detail[0]->value) ? $long_detail[0]->value:$detail->value; ?></textarea>
                                            <?php else : ?>
                                                <?php if( isset($detail->field_name) ) : ?>
                                                    <?php if( stripos($detail->field_name, 'date') !== FALSE || stripos($detail->field_name, 'dob') !== FALSE ) : ?>
                                                        
                                                        
                                                         <?php if( (strlen($detail->value) < 10 && stripos($detail->value, '/') === FALSE ) || stripos($detail->value, 'Today') !== FALSE || stripos($detail->value, '@') !== FALSE ) : ?>
                                                        
                                                            <input readonly type="text" class="form-control" value="<?php echo $detail->value; ?>"/>

                                                        <?php else : ?>

                                                            <?php $m_d_y = explode('-', $detail->value); ?>

                                                            <?php if( is_array($m_d_y) && !empty($m_d_y) && isset($m_d_y[2]) && strlen($m_d_y[2]) <= 4 ) : ?>
                                                                <input readonly type="text" class="form-control" value="<?php echo $detail->value; ?>"/>
                                                            <?php else : ?>
                                                                <input readonly type="text" class="form-control" value="<?php echo @date_format(date_create($detail->value),"Y-m-d"); ?>"/>
                                                            <?php endif; ?>

                                                        <?php endif; ?>

                                                    <?php else : ?>
                                                        <input readonly type="text" class="form-control" value="<?php echo $detail->value . stripos($detail->field_name, 'date'); ?>"/>
                                                    <?php endif; ?>
                                                <?php else : ?>
                                                    <input readonly type="text" class="form-control" value="<?php echo $detail->value; ?>"/>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>

                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </fieldset>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<script>
    jQuery(document).ready(function($){

        var search = '<?php echo isset($_GET["search"]) ? $_GET["search"]:""; ?>';
        var searchby = '<?php echo isset($_GET["searchby"]) ? $_GET["searchby"] : ""; ?>';

        $("#export-range-start, #export-range-end").datepicker({ 
            changeMonth: true,
            changeYear: true, 
            yearRange: '1900:2030'
        }).val('');

        $('input[name="search"]').val(search);
        $('select#lead-search option[value="'+searchby+'"]').attr('selected', true);
    });
</script>