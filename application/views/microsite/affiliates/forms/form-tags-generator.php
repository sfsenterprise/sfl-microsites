<?php add_thickbox(); ?>
<div id="tag-generator-panel-text" class="hidden">
	<form action="" class="tag-generator-panel" id="form-text" data-id="text">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a single-line plain text input field. For more details, see <a href="https://contactform7.com/text-fields/">Text Fields</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row">Field type</th>
							<td>
								<fieldset>
									<legend class="screen-reader-text">Field type</legend>
									<label><input type="checkbox" name="required"> Required field</label>
								</fieldset>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-text-name">Name</label></th>
							<td>
								<input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-text-name" />
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-text-id">Id attribute</label></th>
							<td>
								<input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-text-id">
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-text-class">Class attribute</label></th>
							<td>
								<input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-text-class">
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-text-values">Default value</label></th>
							<td>
								<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
								<input type="text" name="values" class="oneline" id="tag-generator-panel-text-values">
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10">
				<input type="text" name="text" class="tag code form-control" id="tag-text" data-tag="text" readonly="readonly" onfocus="this.select()">
			</div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-text -->


<div id="tag-generator-panel-email" class="hidden">
	<form action="" class="tag-generator-panel" id="form-email" data-id="email">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a single-line email address input field. For more details, see <a href="https://contactform7.com/text-fields/">Text Fields</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row">Field type</th>
							<td>
								<fieldset>
									<legend class="screen-reader-text">Field type</legend>
									<label><input type="checkbox" name="required"> Required field</label>
								</fieldset>
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-email-name">Name</label></th>
							<td>
								<input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-email-name">
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-email-id">Id attribute</label></th>
							<td>
								<input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-email-id">
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-email-class">Class attribute</label></th>
							<td>
								<input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-email-class">
							</td>
						</tr>
						<tr>
							<th scope="row"><label for="tag-generator-panel-email-values">Default value</label></th>
							<td>
								<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
								<input type="text" name="values" class="oneline" id="tag-generator-panel-email-values">
							</td>
						</tr>
					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="email" class="tag code form-control" id="tag-email" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-email -->


<div id="tag-generator-panel-url" class="hidden">
	<form action="" class="tag-generator-panel" id="form-url" data-id="url">
	<div class="control-box">
	<fieldset>
	<legend>Generate a form-tag for a single-line URL input field. For more details, see <a href="https://contactform7.com/text-fields/">Text Fields</a>.</legend>

	<table class="form-table">
	<tbody>
		<tr>
		<th scope="row">Field type</th>
		<td>
			<fieldset>
			<legend class="screen-reader-text">Field type</legend>
			<label><input type="checkbox" name="required"> Required field</label>
			</fieldset>
		</td>
		</tr>

		<tr>
		<th scope="row"><label for="tag-generator-panel-url-name">Name</label></th>
		<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-url-name"></td>
		</tr>
		<tr>
		<th scope="row"><label for="tag-generator-panel-url-id">Id attribute</label></th>
		<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-url-id"></td>
		</tr>
		<tr>
		<th scope="row"><label for="tag-generator-panel-url-class">Class attribute</label></th>
		<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-url-class"></td>
		</tr>
		<tr>
		<th scope="row"><label for="tag-generator-panel-url-values">Default value</label></th>
			<td>
				<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
				<input type="text" name="values" class="oneline" id="tag-generator-panel-url-values">
			</td>
		</tr>

	</tbody>
	</table>
	</fieldset>
	</div>

	<div class="insert-box row">
		<div class="col-10"><input type="text" name="url" class="tag code form-control" id="tag-url" readonly="readonly" onfocus="this.select()"></div>
		<div class="submitbox col-2">
			<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
		</div>
	</div>
	</form>
</div><!-- #tag-generator-panel-url -->


<div id="tag-generator-panel-tel" class="hidden">
	<form action="" class="tag-generator-panel" id="form-tel" data-id="tel">
	<div class="control-box">
		<fieldset>
			<legend>Generate a form-tag for a single-line telephone number input field. For more details, see <a href="https://contactform7.com/text-fields/">Text Fields</a>.</legend>

		<table class="form-table">
		<tbody>
			<tr>
			<th scope="row">Field type</th>
			<td>
				<fieldset>
				<legend class="screen-reader-text">Field type</legend>
				<label><input type="checkbox" name="required"> Required field</label>
				</fieldset>
			</td>
			</tr>

			<tr>
			<th scope="row"><label for="tag-generator-panel-tel-name">Name</label></th>
			<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-tel-name"></td>
			</tr>
			<tr>
			<th scope="row"><label for="tag-generator-panel-tel-id">Id attribute</label></th>
			<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-tel-id"></td>
			</tr>

			<tr>
			<th scope="row"><label for="tag-generator-panel-tel-class">Class attribute</label></th>
			<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-tel-class"></td>
			</tr>

			<tr>
			<th scope="row"><label for="tag-generator-panel-tel-values">Default value</label></th>
				<td>
					<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
					<input type="text" name="values" class="oneline" id="tag-generator-panel-tel-values">
				</td>
			</tr>

		</tbody>
		</table>
		</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="tel" class="tag code form-control" id="tag-tel" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-tel -->


<div id="tag-generator-panel-number" class="hidden">
	<form action="" class="tag-generator-panel" id="form-number" data-id="number">
		<div class="control-box">
		<fieldset>
			<legend>Generate a form-tag for a field for numeric value input. For more details, see <a href="https://contactform7.com/number-fields/">Number Fields</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row">Field type</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Field type</legend>
							<!--select name="tagtype">
								<option value="number" selected="selected">Spinbox</option>
								<option value="range">Slider</option>
							</select>
							<br-->
							<label><input type="checkbox" name="required"> Required field</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-number-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-number-name"></td>
						</tr>
						<!--tr>
						<th scope="row">Range</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Range</legend>
							<label>
							Min		<input type="number" name="min" class="numeric option">
							</label>
							–
							<label>
							Max		<input type="number" name="max" class="numeric option">
							</label>
							</fieldset>
						</td>
						</tr-->

						<tr>
						<th scope="row"><label for="tag-generator-panel-number-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-number-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-number-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-number-class"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-number-values">Default value</label></th>
							<td>
								<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
								<input type="text" name="values" class="oneline" id="tag-generator-panel-number-values">
							</td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="number" class="tag code form-control" id="tag-number" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-number -->


<div id="tag-generator-panel-date" class="hidden">
	<form action="" class="tag-generator-panel" id="form-date" data-id="date">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a date input field. For more details, see <a href="https://contactform7.com/date-field/">Date Field</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row">Field type</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Field type</legend>
							<label><input type="checkbox" name="required"> Required field</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-date-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-date-name"></td>
						</tr>

						<!--tr>
						<th scope="row">Range</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Range</legend>
							<label>
							Min		<input type="date" name="min" class="date option">
							</label>
							–
							<label>
							Max		<input type="date" name="max" class="date option">
							</label>
							</fieldset>
						</td>
						</tr-->

						<tr>
						<th scope="row"><label for="tag-generator-panel-date-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-date-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-date-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-date-class"></td>
						</tr>
						<tr>
						<th scope="row"><label for="tag-generator-panel-date-values">Default value</label></th>
							<td>
								<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
								<input type="text" name="values" class="oneline" id="tag-generator-panel-date-values">
							</td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="date" id="tag-date" class="tag code form-control" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-date -->


<div id="tag-generator-panel-textarea" class="hidden">
	<form action="" class="tag-generator-panel" id="form-textarea" data-id="textarea">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a multi-line text input field. For more details, see <a href="https://contactform7.com/text-fields/">Text Fields</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row">Field type</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Field type</legend>
							<label><input type="checkbox" name="required"> Required field</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-textarea-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-textarea-name"></td>
						</tr>
						<tr>
						<th scope="row"><label for="tag-generator-panel-textarea-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-textarea-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-textarea-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-textarea-class"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-textarea-values">Default value</label></th>
							<td>
								<label><input type="checkbox" name="placeholder" class="option"> Use this text as the placeholder of the field</label><br>
								<input type="text" name="values" class="oneline" id="tag-generator-panel-textarea-values">
							</td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="textarea" class="tag code form-control" id="tag-textarea" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-textarea -->


<div id="tag-generator-panel-menu" class="hidden">
	<form action="" class="tag-generator-panel" id="form-select" data-id="select">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a drop-down menu. For more details, see <a href="https://contactform7.com/checkboxes-radio-buttons-and-menus/">Checkboxes, Radio Buttons and Menus</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row">Field type</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Field type</legend>
							<label><input type="checkbox" name="required"> Required field</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-menu-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-menu-name"></td>
						</tr>

						<tr>
						<th scope="row">Options</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Options</legend>
							<textarea name="values" class="values" id="tag-generator-panel-menu-values"></textarea>
							<label for="tag-generator-panel-menu-values"><span class="description">One option per line.</span></label><br>
							<label><input type="checkbox" name="multiple" class="option"> Allow multiple selections</label><br>
							<label><input type="checkbox" name="include_blank" class="option"> Insert a blank item as the first option</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-menu-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-menu-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-menu-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-menu-class"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="select" class="tag code form-control" id="tag-select" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-menu -->


<div id="tag-generator-panel-checkbox" class="hidden">
	<form action="" class="tag-generator-panel" id="form-checkbox" data-id="checkbox">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a group of checkboxes. For more details, see <a href="https://contactform7.com/checkboxes-radio-buttons-and-menus/">Checkboxes, Radio Buttons and Menus</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row">Field type</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Field type</legend>
							<label><input type="checkbox" name="required"> Required field</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-checkbox-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-checkbox-name"></td>
						</tr>

						<tr>
						<th scope="row">Options</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Options</legend>
							<textarea name="values" class="values" id="tag-generator-panel-checkbox-values"></textarea>
							<label for="tag-generator-panel-checkbox-values"><span class="description">One option per line.</span></label><br>
							<label><input type="checkbox" name="label_first" class="option"> Put a label first, a checkbox last</label><br>
							<label><input type="checkbox" name="use_label_element" class="option"> Wrap each item with label element</label>
							<br><label><input type="checkbox" name="exclusive" class="option"> Make checkboxes exclusive</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-checkbox-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-checkbox-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-checkbox-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-checkbox-class"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="checkbox" class="tag code form-control" id="tag-checkbox" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-checkbox -->



<div id="tag-generator-panel-radio" class="hidden">
	<form action="" class="tag-generator-panel" id="form-radio" data-id="radio">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a group of radio buttons. For more details, see <a href="https://contactform7.com/checkboxes-radio-buttons-and-menus/">Checkboxes, Radio Buttons and Menus</a>.</legend>

				<table class="form-table">
					<tbody>

						<tr>
						<th scope="row"><label for="tag-generator-panel-radio-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-radio-name"></td>
						</tr>

						<tr>
						<th scope="row">Options</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Options</legend>
							<textarea name="values" class="values" id="tag-generator-panel-radio-values"></textarea>
							<label for="tag-generator-panel-radio-values"><span class="description">One option per line.</span></label><br>
							<label><input type="checkbox" name="label_first" class="option"> Put a label first, a checkbox last</label><br>
							<label><input type="checkbox" name="use_label_element" class="option"> Wrap each item with label element</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-radio-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-radio-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-radio-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-radio-class"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="radio" class="tag code form-control" id="tag-radio" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-radio -->


<div id="tag-generator-panel-acceptance" class="hidden">
	<form action="" class="tag-generator-panel" id="form-acceptance" data-id="acceptance">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for an acceptance checkbox. For more details, see <a href="https://contactform7.com/acceptance-checkbox/">Acceptance Checkbox</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row"><label for="tag-generator-panel-acceptance-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-acceptance-name"></td>
						</tr>

						<tr>
						<th scope="row">Options</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Options</legend>
							<label><input type="checkbox" name="default:on" class="option"> Make this checkbox checked by default</label><br>
							<label><input type="checkbox" name="invert" class="option"> Make this work inversely</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-acceptance-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-acceptance-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-acceptance-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-acceptance-class"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="acceptance" class="tag code form-control" id="tag-acceptance" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-acceptance -->



<div id="tag-generator-panel-quiz" class="hidden">
	<form action="" class="tag-generator-panel" id="form-quiz" data-id="quiz">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a question-answer pair. For more details, see <a href="https://contactform7.com/quiz/">Quiz</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row"><label for="tag-generator-panel-quiz-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-quiz-name"></td>
						</tr>

						<tr>
						<th scope="row">Questions and answers</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Questions and answers</legend>
							<textarea name="values" class="values" id="tag-generator-panel-quiz-values"></textarea><br>
							<label for="tag-generator-panel-quiz-values"><span class="description">One pipe-separated question-answer pair (e.g. The capital of Brazil?|Rio) per line.</span></label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-quiz-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-quiz-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-quiz-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-quiz-class"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="quiz" class="tag code form-control" id="tag-quiz" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-quiz -->



<div id="tag-generator-panel-recaptcha" class="hidden">
	<form action="" class="tag-generator-panel" data-id="recaptcha">
		<div class="control-box">
			<fieldset><legend>To use reCAPTCHA, first you need to install an API key pair. For more details, see <a href="https://contactform7.com/recaptcha/">reCAPTCHA</a>.</legend></fieldset>
		</div>
	</form>
</div><!-- #tag-generator-panel-recaptcha -->


<div id="tag-generator-panel-file" class="hidden">
	<form action="" class="tag-generator-panel" id="form-file" data-id="file">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a file uploading field. For more details, see <a href="https://contactform7.com/file-uploading-and-attachment/">File Uploading and Attachment</a>.</legend>

				<table class="form-table">
					<tbody>
						<tr>
						<th scope="row">Field type</th>
						<td>
							<fieldset>
							<legend class="screen-reader-text">Field type</legend>
							<label><input type="checkbox" name="required"> Required field</label>
							</fieldset>
						</td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-file-name">Name</label></th>
						<td><input type="text" name="name" class="tg-name oneline" id="tag-generator-panel-file-name"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-file-limit">File size limit (bytes)</label></th>
						<td><input type="text" name="limit" class="filesize oneline option" id="tag-generator-panel-file-limit"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-file-filetypes">Acceptable file types</label></th>
						<td><input type="text" name="filetypes" class="filetype oneline option" id="tag-generator-panel-file-filetypes"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-file-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-file-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-file-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-file-class"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="file" class="tag code form-control" id="tag-file" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div><!-- #tag-generator-panel-file -->



<div id="tag-generator-panel-submit" class="hidden">
	<form action="" class="tag-generator-panel" id="form-submit" data-id="submit">
		<div class="control-box">
			<fieldset>
				<legend>Generate a form-tag for a submit button. For more details, see <a href="https://contactform7.com/submit-button/">Submit Button</a>.</legend>

				<table class="form-table">
					<tbody>

						<tr>
						<th scope="row"><label for="tag-generator-panel-submit-id">Id attribute</label></th>
						<td><input type="text" name="id" class="idvalue oneline option" id="tag-generator-panel-submit-id"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-submit-class">Class attribute</label></th>
						<td><input type="text" name="class" class="classvalue oneline option" id="tag-generator-panel-submit-class"></td>
						</tr>

						<tr>
						<th scope="row"><label for="tag-generator-panel-submit-values">Label</label></th>
						<td><input type="text" name="values" class="oneline" id="tag-generator-panel-submit-values"></td>
						</tr>

					</tbody>
				</table>
			</fieldset>
		</div>

		<div class="insert-box row">
			<div class="col-10"><input type="text" name="submit" class="tag code form-control" id="tag-submit" readonly="readonly" onfocus="this.select()"></div>
			<div class="submitbox col-2">
				<input type="button" class="btn btn-primary insert-tag" value="Insert Tag">
			</div>
		</div>
	</form>
</div>

