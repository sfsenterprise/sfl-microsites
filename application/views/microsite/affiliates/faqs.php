    <?php if( $this->router->fetch_class() === 'faqs' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>

        <div class="row">
            <div class="col-12">
                <?php echo form_open( base_url('faqs/'), ['method'=>'get']); ?>
                    <fieldset>
                        <div class="form-group row">
                            <div class="col-6"></div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-6"><input type="text" name="search" class="form-control" value="<?php echo isset($_GET['search']) ? $_GET['search']:''; ?>" placeholder="Search here" /></div>
                                    <div class="col-3"><input type="submit" name="" class="btn btn-secondary" value="Search"/></div>
                                    <div class="col-3"></div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                <?php echo form_close(); ?>
            </div>

            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Title</a></th>
                                    <th class="text-center">Order</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>

                            <?php if($all_faqs) : ?>
                                <tbody>
                                    <?php foreach( $all_faqs as $faqs ) : ?>
                                        <tr>
                                            <td><a target="_blank" href="<?php echo get_the_permalink($faqs->ID); ?>">View</a></td>
                                            <td width="50%"><a href="<?php echo base_url('faqs/edit/'.$faqs->ID); ?>"><?php echo $faqs->post_title; ?></a></td>
                                            <td class="text-center"><?php echo gss_get_post_meta( 1, $faqs->ID, "sort_faq" ); ?></td>
                                            <td><?php echo nice_date($faqs->post_date, 'F d, Y') .' - '. date('h:i:s a', strtotime($faqs->post_date)); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing FAQs.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>

                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>

    <?php endif; ?>


    <?php if( $this->router->fetch_class() === 'faqs' && ( $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'add') ) : ?>

        <?php echo form_open( sprintf( '/faqs/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
            <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Question</label>
                                    <div class="col-10"><input type="text" name="faq_question" class="form-control" value="<?php echo isset($gss_faq->post_title) ? $gss_faq->post_title : ''; ?>" /></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Answer</label>
                                    <div class="col-10">
                                        <textarea name="faq_answer" class="form-control" cols="30" rows="10"><?php echo isset($gss_faq->post_content) ? $gss_faq->post_content: ''; ?></textarea>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-7 text-left">
                                    <?php if( isset($gss_faq->ID) ) : ?>
                                        <div class="form-group">
                                            <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $gss_faq->ID ); ?>
                                        </div>

                                        <div class="form-group">
                                            <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_faq->post_modified, 'M d, Y h:i:s a') ; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-5 text-right">
                                    <?php echo form_submit('gss_faqs', ($this->uri->segment(4)) ? 'Update' : 'Add', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>

                            <?php if( isset($gss_faq->ID) ) : ?>
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <a class="h2 text-primary" target="_blank" href="<?php echo get_the_permalink($gss_faq->ID); ?>">View Page</a>
                                    </div>

                                    <div class="col-6 text-right">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-12 text-left" class="thickbox">Status</label>
                                <div class="col-12">
                                    <select name="post_status" class="form-control">
                                        <option value="publish" <?php echo (isset($gss_faq->ID) && isset($gss_faq->post_status) && $gss_faq->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                        <option value="draft" <?php echo (isset($gss_faq->ID) && isset($gss_faq->post_status) && $gss_faq->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                        <option value="private" <?php echo (isset($gss_faq->ID) && isset($gss_faq->post_status) && $gss_faq->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-12 col-form-label text-left">Order</label>
                                <div class="col-12 text-left"><input type="text" name="menu_order" value="<?php echo isset($gss_faq->menu_order) ? $gss_faq->menu_order : 0;?>" class="form-control" /></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php echo form_close(); ?>

    <?php endif; ?>
