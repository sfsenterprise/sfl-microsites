<?php if( $this->router->fetch_class() === 'staff' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>URL</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/name/'.$order ); ?>">Name</a></th>
                                    <th class="text-center">Sort Order</th>
                                    <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                </tr>
                            </thead>

                            <?php if($all_staff) : ?>
                                <tbody>
                                    <?php foreach( $all_staff as $staff ) : ?>
                                        <tr>
                                            <td><a target="_blank" href="<?php echo gss_get_blog_details($microsite, 'siteurl') . '/staff'; ?>">View</a></td>
                                            <td>
                                                <a href="<?php echo base_url('staff/edit/'.$staff->ID); ?>"><?php echo $staff->post_title; ?></a>
                                                <?php echo (isset($staff->ID) && isset($staff->post_status) && $staff->post_status !== 'publish') ? ' - <strong>' . ucwords($staff->post_status) . '</strong>':''; ?>
                                            </td>
                                            <td class="text-center"><?php echo isset($staff->ID) ? $staff->menu_order :''; ?></td>
                                            <td><?php echo nice_date($staff->post_date, 'M d, Y') .' - '. date('h:i:s a', strtotime($staff->post_date)); ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing staff.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>

                    </div>
                    <?php $this->load->view('partials/pagination', $this->data); ?>
                </div>
            </div>
        </div>
<?php endif; ?>

    <?php if( $this->router->fetch_class() === 'staff' && ( $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' ) ) : ?>
        <?php echo form_open( sprintf( '/staff/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
            <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">
                                <fieldset>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Name</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'staff_name', 'type'=>'text', 'value'=> isset($gss_staff->ID) ? $gss_staff->post_title : '', 'class'=>'form-control']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Description</label>
                                        <div class="col-10">
                                            <?php switch_to_blog($microsite); ?>
                                            <?php wp_editor( isset($gss_staff->ID) ? $gss_staff->post_content:'', 'staff_description', array('media_buttons' => false, 'editor_class'=>'form-control') ); ?>
                                            <?php restore_current_blog(); ?>
                                        </div>
                                    </div>
                                </fieldset>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <h4 class="card-title">Staff Settings</h4>
                            <fieldset>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Photo</label>
                                        <div class="col-5">
                                            <div class="btn btn-secondary btn-file upload-image" id="upload-staff-img">Upload Image &nbsp; <small><em>(275x185 pixels)</em></small></div>
                                            <div class="staff-photo" style="display: <?php echo ( isset($gss_staff->ID) && $_thumbnail ) ? 'inline-block':'none'; ?> ;">
                                                <img src="<?php echo isset($gss_staff->ID) ? $_thumbnail[0] : ''; ?>" alt="featured image" class="staff-image"/>
                                                <span class="dashicons dashicons-trash remove"></span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="_thumbnail_id" id="_thumbnail_id" value="<?php echo isset($gss_staff->ID) ? gss_get_post_meta( $microsite, $gss_staff->ID, '_thumbnail_id' ) : '' ?>" />
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Position</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'_s8_staff_job_title', 'type'=>'text', 'value'=>isset($gss_staff->ID) ? gss_get_post_meta( $microsite, $gss_staff->ID, '_s8_staff_job_title' ) : '', 'class'=>'form-control']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Email</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'_s8_staff_email', 'type'=>'text', 'value'=>isset($gss_staff->ID) ? gss_get_post_meta( $microsite, $gss_staff->ID, '_s8_staff_email' ) : '', 'class'=>'form-control']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Phone</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'_s8_staff_phone', 'type'=>'text', 'value'=>isset($gss_staff->ID) ? gss_get_post_meta( $microsite, $gss_staff->ID, '_s8_staff_phone' ) : '', 'class'=>'form-control']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Staff Sort Order</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'menu_order', 'type'=>'text', 'value'=>isset($gss_staff->ID) ? $gss_staff->menu_order : '', 'class'=>'form-control']); ?></div>
                                    </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-7 text-left">
                                    <?php if( isset($gss_staff->ID) ) : ?>
                                        <div class="form-group">
                                            <strong>Date Created:</strong> <br/><?php echo nice_date($gss_staff->post_date, 'M d, Y h:i:s a') ; ?>
                                        </div>

                                        <div class="form-group">
                                            <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_staff->post_modified, 'M d, Y h:i:s a') ; ?>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <div class="col-5 text-right">
                                    <?php echo form_submit( 'gss_staff' , ($this->uri->segment(4)) ? 'Update Staff':'Add Staff', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>

                            <?php if( isset($gss_staff->ID) ) : ?>
                                <div class="row">
                                    <div class="col-6 text-left">
                                        <?php
                                            switch_to_blog($microsite);
                                            $url = get_the_permalink($gss_staff->ID);
                                            restore_current_blog();
                                        ?>
                                        <a target="_blank" href="<?php echo $url; ?>">View Page</a>
                                    </div>

                                    <div class="col-6 text-right">
                                        <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                        <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header pb-0">
                                                        <div class="modal-title">Are you sure to delete?</div>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                        <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <label class="col-12 text-left" class="thickbox">Status</label>
                                <div class="col-12">
                                    <select name="post_status" class="form-control">
                                        <option value="publish" <?php echo (isset($gss_staff->ID) && isset($gss_staff->post_status) && $gss_staff->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                        <option value="draft" <?php echo (isset($gss_staff->ID) && isset($gss_staff->post_status) && $gss_staff->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                        <option value="private" <?php echo (isset($gss_staff->ID) && isset($gss_staff->post_status) && $gss_staff->post_status === 'private') ? 'selected':''; ?>>Private</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php echo form_close(); ?>
    <?php endif; ?>
