    <?php echo form_open( 'schedules' ); ?>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <div class="alert alert-info h2" role="alert"><strong>Time must be in 24hrs format (e.g 08:00, 17:00, 13:30, 20:00)</strong></div>
                            <h3>Hours of Operations</h3>
                            <div class="row">
                                <?php foreach( $days as $d => $day ) : ?>
                                    <?php $status = isset($schedules['hours_operation'][$day]['active']) ? $schedules['hours_operation'][$day]['active'] : 'off'; ?>
                                    <div class="col hours-operations-day text-center">
                                        <div class="status <?php echo $status !== 'off' ? 'workday-status' : 'restday-status'; ?>">
                                            <input id="ho-<?php echo $d; ?>" type="checkbox" name="hours_operation[<?php echo $day; ?>][active]" value="on" <?php checked($status, 'on'); ?>>
                                        </div>
                                        <label for="ho-<?php echo $d; ?>"><?php echo ucfirst($d); ?></label>
                                        <div class="hours-operations-time">
                                            <?php if ( isset( $schedules['hours_operation'][$day]['hours'] ) ) : $i = 0; ?>
                                                <?php foreach( $schedules['hours_operation'][$day]['hours'] as $key => $hour ) : ?>
                                                    <div>
                                                        <?php if ( $hour ) : ?>
                                                            <div>
                                                                <input type="text" value="<?php echo (isset($hour['start'])) ? $hour['start']:''; ?>" name="hours_operation[<?php echo (isset($day)) ? $day:''; ?>][hours][<?php echo (isset($key)) ? $key:''; ?>][start]">
                                                                <input type="text" value="<?php echo (isset($hour['end'])) ? $hour['end']:''; ?>" name="hours_operation[<?php echo (isset($day)) ? $day:''; ?>][hours][<?php echo (isset($key)) ? $key:''; ?>][end]">
                                                            </div>
                                                            <span class="dashicons dashicons-trash remove"></span>
                                                        <?php endif; $i++; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <span class="dashicons dashicons-plus-alt add" data-day="<?php echo (isset($day)) ? $day:''; ?>" data-rows="<?php echo $i; ?>" data-cat="hours_operation"></span>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <h3>Family Swim Hours</h3>
                            <div class="row">
                                <?php foreach( $days as $d => $day ) : ?>
                                    <?php $status = isset($schedules['family_swim'][$day]['active']) ? $schedules['family_swim'][$day]['active'] : 'off'; ?>
                                    <div class="col hours-operations-day text-center">
                                        <div class="status <?php echo $status !== 'off' ? 'workday-status' : 'restday-status'; ?>">
                                            <input id="ho-<?php echo $d; ?>" type="checkbox" name="family_swim[<?php echo $day; ?>][active]" value="on" <?php checked($status, 'on'); ?>>
                                        </div>
                                        <label for="ho-<?php echo $d; ?>"><?php echo ucfirst($d); ?></label>
                                        <div class="hours-operations-time">
                                            <?php if ( isset($schedules['family_swim'][$day]['hours']) ) : $i = 0; ?>
                                                <?php foreach( $schedules['family_swim'][$day]['hours'] as $key => $hour ) : ?>
                                                    <div>
                                                        <?php if ( $hour ) : ?>
                                                            <div>
                                                                <input type="text" value="<?php echo $hour['start']; ?>" name="family_swim[<?php echo $day; ?>][hours][<?php echo $key; ?>][start]">
                                                                <input type="text" value="<?php echo $hour['end']; ?>" name="family_swim[<?php echo $day; ?>][hours][<?php echo $key; ?>][end]">
                                                            </div>
                                                            <span class="dashicons dashicons-trash remove"></span>
                                                        <?php endif; $i++; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <span class="dashicons dashicons-plus-alt add" data-day="<?php echo $day; ?>" data-rows="<?php echo $i; ?>" data-cat="family_swim"></span>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <h3>Swim Team Hours</h3>
                            <div class="row">
                                <?php foreach( $days as $d => $day ) : ?>
                                    <?php $status = isset($schedules['swim_team'][$day]['active']) ? $schedules['swim_team'][$day]['active'] : 'off'; ?>
                                    <div class="col hours-operations-day text-center">
                                        <div class="status <?php echo $status !== 'off' ? 'workday-status' : 'restday-status'; ?>">
                                            <input id="ho-<?php echo $d; ?>" type="checkbox" name="swim_team[<?php echo $day; ?>][active]" value="on" <?php checked($status, 'on'); ?>>
                                        </div>
                                        <label for="ho-<?php echo $d; ?>"><?php echo ucfirst($d); ?></label>
                                        <div class="hours-operations-time">
                                            <?php if ( isset($schedules['swim_team'][$day]['hours']) ) : $i = 0; ?>
                                                <?php foreach( $schedules['swim_team'][$day]['hours'] as $key => $hour ) : ?>
                                                    <div>
                                                        <?php if ( $hour ) : ?>
                                                            <div>
                                                                <input type="text" value="<?php echo $hour['start']; ?>" name="swim_team[<?php echo $day; ?>][hours][<?php echo $key; ?>][start]">
                                                                <input type="text" value="<?php echo $hour['end']; ?>" name="swim_team[<?php echo $day; ?>][hours][<?php echo $key; ?>][end]">
                                                            </div>
                                                            <span class="dashicons dashicons-trash remove"></span>
                                                        <?php endif; $i++; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <span class="dashicons dashicons-plus-alt add" data-day="<?php echo $day; ?>" data-rows="<?php echo $i; ?>" data-cat="swim_team"></span>
                                    </div>
                                <?php endforeach; ?>
                            </div>

                            <h3>Party Hours</h3>
                            <div class="row">
                                <?php foreach( $days as $d => $day ) : ?>
                                    <?php $status = isset($schedules['party_hour'][$day]['active']) ? $schedules['party_hour'][$day]['active'] : 'off'; ?>
                                    <div class="col hours-operations-day text-center">
                                        <div class="status <?php echo $status !== 'off' ? 'workday-status' : 'restday-status'; ?>">
                                            <input id="ho-<?php echo $d; ?>" type="checkbox" name="party_hour[<?php echo $day; ?>][active]" value="on" <?php checked($status, 'on'); ?>>
                                        </div>
                                        <label for="ho-<?php echo $d; ?>"><?php echo ucfirst($d); ?></label>
                                        <div class="hours-operations-time">
                                            <?php if ( isset($schedules['party_hour'][$day]['hours']) ) : $i = 0; ?>
                                                <?php foreach( $schedules['party_hour'][$day]['hours'] as $key => $hour ) : ?>
                                                    <div>
                                                        <?php if ( $hour ) : ?>
                                                            <div>
                                                                <input type="text" value="<?php echo $hour['start']; ?>" name="party_hour[<?php echo $day; ?>][hours][<?php echo $key; ?>][start]">
                                                                <input type="text" value="<?php echo $hour['end']; ?>" name="party_hour[<?php echo $day; ?>][hours][<?php echo $key; ?>][end]">
                                                            </div>
                                                            <span class="dashicons dashicons-trash remove"></span>
                                                        <?php endif; $i++; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                        <span class="dashicons dashicons-plus-alt add" data-day="<?php echo $day; ?>" data-rows="<?php echo $i; ?>" data-cat="party_hour"></span>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-block">
                            <table class="table table-striped">
                                <tbody>
                                    <?php if( (int)$microsite === 1 ) : ?>
                                        <tr>
                                            <?php $email = gss_get_blog_option($microsite, '_schedule_notif_email'); ?>
                                            <td>Email Franchise Hours Change Notifications To:</td>
                                            <td><input type="text" class="form-control" name="_schedule_notif_email" value="<?php echo isset($email) ? $email:''; ?>" size="100" placeholder="Email Address here"/></td>
                                        </tr>
                                    <?php endif; ?>
                                    <tr>
                                        <td></td>
                                        <td><?php echo form_submit( 'gss_hours' , 'Update Schedule', ['class'=>'btn btn-primary']); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

    <?php echo form_close(); ?>
