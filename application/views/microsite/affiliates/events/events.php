<?php if( $this->router->fetch_class() === 'events' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>

                <div class="row">
                    <div class="col-12">
                        <?php echo form_open(base_url('events/'), ['method'=>'get']); ?>
                            <fieldset>
                                <div class="form-group row">
                                    <div class="col-6"></div>
                                    <div class="col-6">
                                        <div class="row">
                                            <div class="col-6"><input type="text" name="search" class="form-control" value="" placeholder="Search here"></div>
                                            <div class="col-3"><input type="submit" name="" class="btn btn-secondary" value="Search"></div>
                                            <div class="col-3"></div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        <?php echo form_close(); ?>          
                    </div>

                    <div class="col">
                        <div class="card">
                            <div class="card-block">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>URL</th>
                                            <th width="40%"><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/title/'.$order ); ?>">Title</a></th>
                                            <th>Start Date</th>
                                            <th>End Date</th>
                                            <th><a href="<?php echo base_url( $this->router->fetch_class() . '/orderby/date/'.$order ); ?>">Date</a></th>
                                        </tr>
                                    </thead>
                                        <?php if($all_events) : ?>
                                            <tbody>
                                                <?php foreach( $all_events as $event ) : ?>
                                                    <?php $cats = gss_wp_get_post_categories($microsite, $event->ID, 'stellar_event_categories'); ?>
                                                    <tr>
                                                        <td><a target="_blank" href="<?php echo gss_get_blog_details($microsite, 'siteurl') . '/events'; ?>">View</a></td>
                                                        <td><a href="<?php echo base_url('/events/edit/'.$event->ID); ?>"><?php echo $event->post_title; ?> <?php echo ( $event->post_status !== 'publish' ) ? '<strong><em>(' . $event->post_status . ')</em></strong>' : ''; ?></a></td>
                                                        <td>
                                                            <?php $start = isset($event->ID) ? gss_get_post_meta( $microsite, $event->ID, "_stellar_event_startDate" ) : ''; ?>
                                                            <?php echo isset($start) ? date_format(date_create($start),"M d, Y"): ''; ?>
                                                        </td>
                                                        <td>
                                                            <?php $end = (isset($event->ID)) ? gss_get_post_meta( $microsite, $event->ID, "_stellar_event_endDate" ) : ''; ?>
                                                            <?php echo isset($end) ? date_format(date_create($end),"M d, Y"): ''; ?>
                                                        </td>
                                                        <td><?php echo nice_date($event->post_date, 'M d, Y') .' - '. date('h:i:s a', strtotime($event->post_date)); ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        <?php else : ?>
                                            <tbody>
                                                <tr>
                                                    <td><h2>No existing events.</h2></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        <?php endif; ?>
                                    </table>
                                </div>
                                <?php $this->load->view('partials/pagination', $this->data); ?>
                            </div>
                        </div>
                    </div>
<?php endif; ?>
