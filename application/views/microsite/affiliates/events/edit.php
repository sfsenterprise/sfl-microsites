<?= form_open( sprintf( '/events/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'. ($gss_event->ID)?:'' ) ); ?>
    <div class="row">
        <div class="col col-9">
            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Event Title</label>
                            <div class="col-10"><input type="text" name="event_title" class="form-control" value="<?php echo isset($gss_event->post_title) ? $gss_event->post_title : ''; ?>" placeholder="Event Title"/></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Description</label>
                            <div class="col-10">
                                <?php switch_to_blog($microsite); ?>
                                <?php wp_editor( isset($gss_event->ID) ? $gss_event->post_content:'', 'event_content', array('media_buttons' => true, 'editor_class'=>'form-control') ); ?>
                                <?php restore_current_blog(); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Event Settings</h4>
                    <fieldset>
                            <div class="form-group row">
                                <?php $event_startDate = isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, "_stellar_event_startDate" ) : ''; ?>

                                <label class="col-2 col-form-label">Start Date</label>
                                <div class="col-3"><input id="event-start" class="form-control datepicker" name="event_startDate" value="<?php echo isset($event_startDate) ? date_format(date_create($event_startDate),"F j, Y"): ''; ?>"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Start Hour</label>
                                <div class="col-2">
                                    <select name="event_startTime[hour]" class="form-control">
                                        <?php
                                            for ($i=1; $i <= 12 ; $i++) {
                                                $selected = date_format(date_create($event_startDate),"g") == $i ? "selected" : "";
                                                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <?php $s_minute = date_format(date_create($event_startDate),"i"); ?>
                                    <select name="event_startTime[minute]" class="form-control">
                                        <option value="00" <?php echo (isset($event_startDate) && $s_minute == "00") ? "selected" : ""; ?> >00</option>
                                        <option value="05" <?php echo (isset($event_startDate) && $s_minute == "05") ? "selected" : ""; ?>>05</option>
                                        <option value="10" <?php echo (isset($event_startDate) && $s_minute == "10") ? "selected" : ""; ?>>10</option>
                                        <option value="15" <?php echo (isset($event_startDate) && $s_minute == "15") ? "selected" : ""; ?>>15</option>
                                        <option value="20" <?php echo (isset($event_startDate) && $s_minute == "20") ? "selected" : ""; ?>>20</option>
                                        <option value="25" <?php echo (isset($event_startDate) && $s_minute == "25") ? "selected" : ""; ?>>25</option>
                                        <option value="30" <?php echo (isset($event_startDate) && $s_minute == "30") ? "selected" : ""; ?>>30</option>
                                        <option value="35" <?php echo (isset($event_startDate) && $s_minute == "35") ? "selected" : ""; ?>>35</option>
                                        <option value="40" <?php echo (isset($event_startDate) && $s_minute == "40") ? "selected" : ""; ?>>40</option>
                                        <option value="45" <?php echo (isset($event_startDate) && $s_minute == "45") ? "selected" : ""; ?>>45</option>
                                        <option value="50" <?php echo (isset($event_startDate) && $s_minute == "50") ? "selected" : ""; ?>>50</option>
                                        <option value="55" <?php echo (isset($event_startDate) && $s_minute == "55") ? "selected" : ""; ?>>55</option>
                                    </select>
                                </div>
                                 <div class="col-2">
                                    <select name="event_startTime[noon]" class="form-control">
                                        <option value="AM" <?php echo (isset($event_startDate) && date_format(date_create($event_startDate),"A") == "AM") ? "selected" : ""; ?>>AM</option>
                                        <option value="PM" <?php echo (isset($event_startDate) && date_format(date_create($event_startDate),"A") == "PM") ? "selected" : ""; ?>>PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <?php $event_endDate = (isset($gss_event->ID)) ? gss_get_post_meta( $microsite, $gss_event->ID, "_stellar_event_endDate" ) : ''; ?>

                                <label class="col-2 col-form-label">End Hour</label>
                                <div class="col-2">
                                    <select name="event_endTime[hour]" class="form-control">
                                        <?php
                                            for ($i=1; $i <= 12 ; $i++) {
                                                $selected = date_format(date_create($event_endDate),"g") == $i ? "selected" : "";
                                                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <?php $minute = date_format(date_create($event_endDate),"i"); ?>
                                    <select name="event_endTime[minute]" class="form-control">
                                        <option value="00" <?php echo (isset($event_endDate) && $minute == "00") ? "selected" : ""; ?> >00</option>
                                        <option value="05" <?php echo (isset($event_endDate) && $minute == "05") ? "selected" : ""; ?>>05</option>
                                        <option value="10" <?php echo (isset($event_endDate) && $minute == "10") ? "selected" : ""; ?>>10</option>
                                        <option value="15" <?php echo (isset($event_endDate) && $minute == "15") ? "selected" : ""; ?>>15</option>
                                        <option value="20" <?php echo (isset($event_endDate) && $minute == "20") ? "selected" : ""; ?>>20</option>
                                        <option value="25" <?php echo (isset($event_endDate) && $minute == "25") ? "selected" : ""; ?>>25</option>
                                        <option value="30" <?php echo (isset($event_endDate) && $minute == "30") ? "selected" : ""; ?>>30</option>
                                        <option value="35" <?php echo (isset($event_endDate) && $minute == "35") ? "selected" : ""; ?>>35</option>
                                        <option value="40" <?php echo (isset($event_endDate) && $minute == "40") ? "selected" : ""; ?>>40</option>
                                        <option value="45" <?php echo (isset($event_endDate) && $minute == "45") ? "selected" : ""; ?>>45</option>
                                        <option value="50" <?php echo (isset($event_endDate) && $minute == "50") ? "selected" : ""; ?>>50</option>
                                        <option value="55" <?php echo (isset($event_endDate) && $minute == "55") ? "selected" : ""; ?>>55</option>
                                    </select>
                                </div>
                                <div class="col-2">
                                    <select name="event_endTime[noon]" class="form-control">
                                        <option value="AM" <?php echo (isset($event_endDate) && date_format(date_create($event_endDate),"A") == "AM") ? "selected" : ""; ?>>AM</option>
                                        <option value="PM" <?php echo (isset($event_endDate) && date_format(date_create($event_endDate),"A") == "PM") ? "selected" : ""; ?>>PM</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">End Date</label>
                                <div class="col-3"><input id="event-end" class="form-control datepicker" name="event_endDate" value="<?php echo isset($event_endDate) ? date_format(date_create($event_endDate),"F j, Y"): ''; ?>"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Event Location</label>
                                <div class="col-3"><input type="text" name="_stellar_event_streetAddress" class="form-control" value="<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_streetAddress' ) : ''; ?>" placeholder="Street Address" /></div>
                                <div class="col-2"><input type="text" name="_stellar_event_addressLocality" class="form-control" value="<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_addressLocality' ) : ''; ?>" placeholder="City" /></div>
                                <div class="col-2"><input type="text" name="_stellar_event_addressRegion" class="form-control" value="<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_addressRegion' ) : ''; ?>" placeholder="State / Province" /></div>
                                <div class="col-2"><input type="text" name="_stellar_event_postalCode" class="form-control" value="<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_postalCode' ) : ''; ?>" placeholder="Zip Code" /></div>
                            </div>
                            <div class="form-group row">
                                <?php $repeat = isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_repeats' ) : ''; ?>
                                <label class="col-2 col-form-label">Repeating</label>
                                <div class="col-5">
                                    <select name="_stellar_repeating_events[repeats]" class="form-control">
                                        <option value="never" <?php echo $repeat === 'never' ? 'selected':'' ?>>Never</option>
                                        <option data-repeats="days" value="daily" <?php echo $repeat === 'daily' ? 'selected':'' ?>>Daily</option>
                                        <option data-repeats="weeks" value="weekly" <?php echo $repeat === 'weekly' ? 'selected':'' ?>>Weekly</option>
                                        <option data-repeats="months" value="monthly" <?php echo $repeat === 'monthly' ? 'selected':'' ?>>Monthly</option>
                                        <option data-repeats="years" value="yearly" <?php echo $repeat === 'yearly' ? 'selected':'' ?>>Yearly</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group repeat-daily repeat-weekly row hidden-fields">
                                <label class="col-2 col-form-label">Repeat every:</label>
                                <div class="col-10">
                                    <div class="row">
                                        <div class="toggle daily weekly monthly yearly col-2 pr-0">
                                            <?php $repeat_every = isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_repeat_every' ) : ''; ?>
                                            <label>
                                                 <select name="_stellar_repeating_events[repeat_every]" class="form-control">
                                                    <?php for ($i=1; $i <= 31; $i++) : ?>
                                                        <option <?php selected($i, absint($repeat_every)); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </label>
                                        </div>

                                        <div class="repeat-label text-center col-5 text-left pl-0">
                                            <span class="repeat-daily hidden-fields pt-2">days</span>
                                            <span class="repeat-weekly hidden-fields pt-2">weeks</span>
                                            <span class="repeat-monthly hidden-fields pt-2">months</span>
                                            <span class="repeat-yearly hidden-fields pt-2">years</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group repeat-weekly row hidden-fields">
                                <?php $repeating_on = isset($gss_event) ? ( gss_get_post_meta($microsite, $gss_event->ID, '_stellar_event_repeat_on') ?: array() ) : array(); ?>
                                <label class="col-2 col-form-label">Repeat on:</label>
                                <div class="col-10">
                                    <div class="row">
                                        <div class="form-check col-1">
                                            <label for="sunday" class="form-check-label">
                                                <input class="form-check-input" id="sunday" value="Sunday" <?php checked(true, in_array('Sunday', $repeating_on)); ?> type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Sun
                                            </label>
                                        </div>
                                        <div class="form-check col-1">
                                            <label for="monday" class="form-check-label">
                                                <input class="form-check-input" id="monday" value="Monday" <?php checked(true, in_array('Monday', $repeating_on)); ?>  type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Mon
                                            </label>
                                        </div>
                                        <div class="form-check col-1">
                                            <label for="tuesday" class="form-check-label">
                                                <input class="form-check-input" id="tuesday" value="Tuesday" <?php checked(true, in_array('Tuesday', $repeating_on)); ?>  type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Tue
                                            </label>
                                        </div>
                                        <div class="form-check col-1">
                                            <label for="wednesday" class="form-check-label">
                                                <input class="form-check-input" id="wednesday" value="Wednesday" <?php checked(true, in_array('Wednesday', $repeating_on)); ?>  type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Wed
                                            </label>
                                        </div>
                                        <div class="form-check col-1">
                                            <label for="thursday" class="form-check-label">
                                                <input class="form-check-input" id="thursday" value="Thursday" <?php checked(true, in_array('Thursday', $repeating_on)); ?>  type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Thu
                                            </label>
                                        </div>
                                        <div class="form-check col-1">
                                            <label for="friday" class="form-check-label">
                                                <input class="form-check-input" id="friday" value="Friday" <?php checked(true, in_array('Friday', $repeating_on)); ?>  type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Fri
                                            </label>
                                        </div>
                                        <div class="form-check col-1">
                                            <label for="saturday" class="form-check-label">
                                                <input class="form-check-input" id="saturday" value="Saturday" <?php checked(true, in_array('Saturday', $repeating_on)); ?>  type="checkbox" name="_stellar_event_repeat_on[]">
                                                <span class="fa fa-check"></span>
                                                Sat
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group repeat-daily repeat-weekly row hidden-fields">
                                <?php $end_on = isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_stellar_event_repeating_ends_on' ) : ''; ?>
                                <label class="col-2 col-form-label">Ends on:</label>
                                <div class="col-5">
                                    <input id="event-end-on" class="form-control datepicker" name="_stellar_repeating_events[ends_on]" value="<?php echo isset($end_on) ? date_format(date_create($end_on),"F j, Y"): ''; ?>">
                                </div>
                            </div>

                    </fieldset>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                    <h4 class="card-title">Mashsb Settings</h4>
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Mashsb Shares</label>
                            <div class="col-10"><input type="text" name="mashsb_shares" value="<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, 'mashsb_shares' ) : ''; ?>" class="form-control"/></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Mashsb JsonShares</label>
                            <div class="col-10"><input type="text" name="mashsb_jsonshares" value='<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, "mashsb_jsonshares" ) : ""; ?>' class="form-control"/></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Mashsb Timestamp</label>
                            <div class="col-10"><input type="text" name="mashsb_timestamp" value="<?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, 'mashsb_timestamp' ) : ''; ?>" class="form-control"/></div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="col col-3">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-7 text-left">
                            <?php if( isset($gss_event->ID) ) : ?>
                                <div class="form-group">
                                    <strong>Date Created:</strong> <br/><?php echo nice_date($gss_event->post_date, 'M d, Y h:i:s a') ; ?>
                                </div>

                                <div class="form-group">
                                    <strong>Last Modified:</strong> <br/><?php echo nice_date($gss_event->post_modified, 'M d, Y h:i:s a') ; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-5 text-right">
                            <?php echo form_submit( 'gss_events' , ($this->router->fetch_method()==='add') ? 'Add' : 'Update', ['class'=>'btn btn-primary']); ?>
                        </div>
                    </div>

                    <?php if( isset($gss_event->ID) ) : ?>
                        <div class="row">
                            <div class="col-6 text-left">
                                <div class="form-group">
                                    <?php echo form_submit( 'trash_event' , 'Trash', ['class'=>'btn btn-warning']); ?>
                                </div>
                            </div>

                            <div class="col-6 text-right">
                                <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>

                                <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header pb-0">
                                                <div class="modal-title">Are you sure to delete?</div>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label class="col-12 text-left" class="thickbox">Status</label>
                        <div class="col-12">
                            <select name="post_status" class="form-control">
                                <option value="publish" <?php echo (isset($gss_event->ID) && isset($gss_event->post_status) && $gss_event->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                <option value="draft" <?php echo (isset($gss_event->ID) && isset($gss_event->post_status) && $gss_event->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                <option value="private" <?php echo (isset($gss_event->ID) && isset($gss_event->post_status) && $gss_event->post_status === 'private') ? 'selected':''; ?>>Private</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" name="_gss_post_hidden" <?php echo isset($gss_event->ID) ? gss_get_post_meta( $microsite, $gss_event->ID, '_gss_post_hidden' ) === '1' ? 'checked' : '' : ''; ?> />
                                <span class="fa fa-check"></span>
                                Hide this event
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-12 text-left" class="thickbox">Featured Images</label>
                            <?php $_thumbnail = gss_get_post_meta( $microsite, (isset($gss_event->ID)) ? $gss_event->ID:'', '_thumbnail_id', true); ?>
                            <div class="col-12">
                                <a href="" id="set-post-thumbnail" style="display: <?php echo ($_thumbnail) ? 'none':'block'; ?>;">Set featured image</a>
                                <input type="hidden" name="_thumbnail_id" id="_thumbnail_id" value="<?php echo $_thumbnail; ?>"/>
                                <div class="selected-image" style="display: <?php echo ($_thumbnail) ? 'block':'none'; ?>;">
                                    <?php $image = wp_get_attachment_image_src( $_thumbnail, 'thumbnail' ); ?>
                                    <a href="#"><img src="<?php echo isset($image) ? $image[0] : null; ?>" alt="" id="post-featured-src"/></a>
                                    <div><em><span>Click the image to edit or update</span></em></div>
                                </div>
                            </div>
                            <div class="col-12">
                                <a href="#" id="remove-post-thumbnail" style="display: <?php echo ($_thumbnail) ? 'block':'none'; ?>;">Remove featured image</a>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <?php $cats = array(); ?>
                        <label class="col-12 text-left" class="thickbox">Event Categories</label>
                        <div class="col-12">
                            <?php foreach( $categories as $category ) : ?>

                                <?php foreach( $post_cats as $post_cat ) : ?>
                                    <?php if( $category->term_id === $post_cat->term_id ) : ?>
                                        <?php $cats[$post_cat->term_id] = $post_cat->name; ?>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                                <div class="form-check">
                                    <label for="cat-<?php echo $category->term_id; ?>" class="form-check-label">
                                        <input class="form-check-input" id="cat-<?php echo $category->term_id; ?>" value="<?php echo $category->term_id; ?>" type="checkbox" name="tax_input[gss_event_categories][]" <?php echo (array_key_exists($category->term_id, $cats)) ? 'checked':''; ?> />
                                        <span class="fa fa-check"></span>
                                        <?php echo $category->name; ?>
                                    </label>
                                </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>

<script>
    jQuery(document).ready(function($){
        var set_repeats = $('select[name="_stellar_repeating_events[repeats]"]').val();

        $('.repeat-'+set_repeats).css('display', 'flex');

        jQuery('select[name="_stellar_repeating_events[repeats]"]').on('change', function(){
            var repeat_val = $(this).val();

            $('.hidden-fields').css('display', 'none');

            $('.repeat-'+repeat_val).css('display', 'flex');
        });

    });
</script>
