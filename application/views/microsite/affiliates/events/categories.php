<?php if( $this->router->fetch_class() === 'events' && $this->router->fetch_method() === 'categories' ) : ?>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-block">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Slug</th>
                                            <th>Post Count</th>
                                        </tr>

                                        <?php if($all_cats) : ?>
                                            <tbody>
                                                <?php foreach( $all_cats as $cat ) : ?>
                                                    <tr>
                                                        <td><?php echo $cat->term_id; ?></td>
                                                        <td><a href="<?php echo base_url('events/edit_category/'.$cat->term_id); ?>"><?php echo $cat->name; ?></a></td>
                                                        <td><?php echo $cat->description; ?></td>
                                                        <td><?php echo $cat->slug; ?></td>
                                                        <td><?php echo $cat->count; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        <?php else : ?>
                                            <tbody>
                                                <tr>
                                                    <td><h2>No event categories.</h2></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        <?php endif; ?>

                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
<?php endif; ?>
