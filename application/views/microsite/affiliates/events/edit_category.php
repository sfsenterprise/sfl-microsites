<div class="row">
    <div class="col">
        <?php $this->load->view('partials/notification'); ?>
        <div class="card">
            <div class="card-block">
                <?= form_open( sprintf( '/events/%1$s/', ($this->uri->segment(4)) ? 'edit_category/'.$term->term_id : 'add_category' ) ); ?>
                    <fieldset>
                        <div class="form-group row <?php echo form_error('category_name') ? 'has-danger' : ''; ?>">
                            <label class="col-2 col-form-label">Category Name</label>
                            <div class="col-10">
                                <input type="text" name="category_name" class="form-control form-control-danger" value="<?php echo set_value('category_name', $term->name); ?>" />
                                <?php echo form_error('category_name', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Slug</label>
                            <div class="col-10"><input type="text" name="slug" class="form-control" value="<?php echo set_value('slug', $term->slug); ?>" /></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Parents</label>
                            <div class="col-10">
                                <select name="parent" class="form-control">
                                    <option value="">None</option>
                                    <?php foreach( $categories as $category ) : ?>
                                        <option value="<?php echo $category->term_id; ?>" <?php selected($category->term_id, $term->parent); ?>><?php echo $category->name; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Description</label>
                            <div class="col-10"><?php echo form_textarea(['name'=>'description', 'class'=>'form-control', 'value'=> set_value('description', $term->description) ]); ?></div>
                        </div>
                    </fieldset>

                    <fieldset>
                        <div class="form-group row">
                            <div class="col-lg-5 offset-lg-2"><?php echo form_submit('gss_event_category', ($this->uri->segment(4)) ? 'Update Category' : 'Add Category', ['class'=>'btn btn-primary']); ?></div>
                        </div>
                    </fieldset>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
