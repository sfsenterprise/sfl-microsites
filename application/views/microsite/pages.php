<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-block">
				<table class="table table-striped">
				  <thead>
				    <tr>
				      <th>Title</th>
				      <th>Author</th>
				      <th>Date</th>
				    </tr>
				  </thead>
				  <tbody>
					  <?php if ( isset($pages) ) : ?>
						  <?php foreach( $pages as $page ) : ?>
	                          <tr>
	                              <td>
	                                  <strong><a href="<?php echo base_url("/microsites/editpage/{$page->ID}/"); ?>"><?php echo (($page->post_parent) ? '— ' : null) . $page->post_title; ?></a>
									  <?php if ( $page->post_status !== 'publish' ) : ?>
										  —
										  <?php if ($this->uri->segment(4) !== $page->post_status) : ?>
											  <span class="post-state"><?php echo ucfirst($page->post_status); ?></span>
										  <?php endif; ?>
									  <?php endif; ?>
									  </strong>
									  <?php if ( current_user_can('franchisor') && $this->router->fetch_class() === 'microsites' ) : ?>
										  <div class="table-actions">
		                                      <ul>
												  <li>
		                                              <a class="btn btn-success btn-sm" href="<?php echo base_url("microsites/editpage/{$page->ID}/"); ?>" title="Edit user">Edit</a>
		                                          </li>
												  <li>
		                                              <a class="btn btn-danger btn-sm" href="<?php echo base_url("microsites/delete/{$page->ID}/"); ?>" title="Delete user">Delete</a>
		                                          </li>
		                                      </ul>
		                                  </div>
									  <?php endif; ?>
	                              </td>
								  <td><?php echo get_the_author_meta('display_name', $page->post_author); ?></td>
								  <td>Published<br /><abbr title="<?php echo date('Y/m/d g:i:s a', strtotime($page->post_date)); ?>"><?php echo date('Y/m/d', strtotime($page->post_date)); ?></abbr></td>
	                          </tr>
	                      <?php endforeach; ?>
					  <?php else : ?>
						  <tr>
							  <td colspan="5">Nothing found.</td>
						  </tr>
					  <?php endif; ?>
				  </tbody>
				</table>
			</div>
			<?php $this->load->view('partials/pagination', $this->data); ?>
		</div>
	</div>
</div>
