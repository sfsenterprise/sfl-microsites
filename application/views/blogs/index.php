<?php if( $this->router->fetch_class() === 'blogs' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'orderby' ) ) : ?>

            <div class="row">
                <div class="col-12">
                    <?php echo form_open( base_url('blogs/'), ['method'=>'get']); ?>
                        <fieldset>
                            <div class="form-group row">
                                <div class="col-6"></div>
                                <div class="col-6">
                                    <div class="row">
                                        <div class="col-6"><input type="text" name="search" class="form-control" value="<?php echo isset($_GET['search']) ? $_GET['search']:''; ?>" placeholder="Search here" /></div>
                                        <div class="col-3"><input type="submit" name="" class="btn btn-secondary" value="Search"/></div>
                                        <div class="col-3"></div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    <?php echo form_close(); ?>
                </div>

                <div class="col">
                    <div class="card">
                        <div class="card-block">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>URL</th>
                                        <th><a href="<?php echo base_url( $this->router->fetch_class() . '/?orderby=title&order='.$order ); ?>">Title</a></th>
                                        <th>Categories</th>
                                        <th>Author</th>
                                        <th><a href="<?php echo base_url( $this->router->fetch_class() . '/?orderby=date&order='.$order ); ?>">Date</a></th>
                                    </tr>
                                </thead>
                                
                                <?php if($all_blogs) : ?>
                                    <tbody>
                                        <?php foreach( $all_blogs as $blog ) : ?>
                                            <?php $cats = gss_wp_get_post_categories(1, $blog->ID, 'category'); ?>
                                            <tr>
                                                <td><a target="_blank" href="<?php echo get_the_permalink($blog->ID); ?>">View</a></td>
                                                <td><a href="<?php echo base_url('blogs/edit/'.$blog->ID); ?>"><?php echo $blog->post_title; ?></a></td>
                                                <td>
                                                    <?php $comma = count($cats) - 1; ?>
                                                    <?php foreach( $cats as $cat ) : ?>
                                                        <?php echo $cat->name; ?><?php echo ($comma > 0) ? ', ':''; ?>
                                                        <?php $comma--; ?>
                                                    <?php endforeach; ?>
                                                </td>
                                                <td><?php echo the_author_meta( 'user_nicename' , $blog->post_author ); ?></td>
                                                <td><?php echo nice_date($blog->post_date, 'M d, Y') .' - '. date('h:i:s a', strtotime($blog->post_date)); ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    <?php else : ?>
                                        <tbody>
                                            <tr>
                                                <td><h2>No existing blog.</h2></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    <?php endif; ?>
                             </table>
                             
                        </div>
                        <?php $this->load->view('partials/pagination', $this->data); ?>
                    </div>
                </div>
            </div>
<?php endif; ?>

<?php if( $this->router->fetch_class() === 'blogs' && ( $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'add') ) : ?>
    
    <?= form_open( sprintf( '/blogs/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'. $this->uri->segment(4) ) ); ?>
    <input type="hidden" id="post_ID" value="<?php echo $this->uri->segment(4); ?>"/>
    <div class="row">
        <div class="col col-9">
            
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-block">        
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Title</label>
                                    <div class="col-10"><input type="text" name="blog_title" class="form-control" value="<?php echo isset($page->post_title) ? $page->post_title : ''; ?>" /></div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Content</label>
                                    <div class="col-10">
                                        <?php switch_to_blog( $microsite ); ?>
                                        <?php wp_editor( isset($page->post_content) ? $page->post_content : '', 'blog_content', array('textarea_name'=>'blog_content', 'editor_class'=>'form-control') ); ?>
                                        <?php restore_current_blog(); ?>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

            <?php $this->load->view('partials/cards/yoast'); ?>
        </div>
        <div class="col col-3">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-7 text-left">
                            <?php if( isset($page->ID) && $page->ID ) : ?>
                                <div class="form-group">
                                    <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $page->ID ); ?>
                                </div>

                                <div class="form-group">
                                    <strong>Last Modified:</strong> <br/><?php echo  get_the_modified_date( 'M d, Y - h:i:s a', $page->ID ); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        
                        <div class="col-5 text-right">
                            <div class="form-group">
                                <?php echo form_submit('gss_blog', ($this->uri->segment(4)) ? 'Update Blog' : 'Add Blog', ['class'=>'btn btn-primary']); ?>
                            </div>
                        </div>
                    </div>
                    
                    <?php if( $this->uri->segment(4) ) : ?>
                        <div class="row">
                            <div class="col-6 text-left">
                                <a class="h2 text-primary" target="_blank" href="<?php echo get_the_permalink($page->ID); ?>">View Page</a>
                            </div>
                            
                            <div class="col-6 text-right">
                                <a class="btn btn-danger" href="" data-toggle="modal" data-target="#page-delete">Delete</a>
                                
                                <div class="modal fade" id="page-delete" tabindex="-1" role="dialog" aria-labelledby="pageDelete" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header pb-0">
                                                <div class="modal-title">Are you sure to delete?</div>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p>Choosing "Yes" will <strong>permanently</strong> delete this page.</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                <a class="btn btn-primary" href="<?php echo sprintf(base_url() . '?delete=%1s', $this->uri->segment(4) ); ?>">Yes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label class="col-12 text-left" class="thickbox">Status</label>
                        <div class="col-12">
                            <select name="post_status" class="form-control">
                                <option value="publish" <?php echo (isset($page->ID) && isset($page->post_status) && $page->post_status === 'publish') ? 'selected':''; ?>>Publish</option>
                                <option value="draft" <?php echo (isset($page->ID) && isset($page->post_status) && $page->post_status === 'draft') ? 'selected':''; ?>>Draft</option>
                                <option value="private" <?php echo (isset($page->ID) && isset($page->post_status) && $page->post_status === 'private') ? 'selected':''; ?>>Private</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-12 text-left" class="thickbox">Featured Images</label>
                            <?php $_thumbnail = get_the_post_thumbnail_url( isset($page->ID) ? $page->ID : '' ); ?>
                            <div class="col-12">
                                <a href="" id="set-post-thumbnail" onclick="setFeaturedImage()" style="display: <?php echo ($_thumbnail) ? 'none':'block'; ?>;">Set featured image</a>
                                <input type="hidden" name="_thumbnail_id" id="_thumbnail_id" value="<?php echo (get_post_thumbnail_id( isset($page->ID) ? $page->ID : '' )) ?: ''; ?>"/>
                                <div class="selected-image" style="display: <?php echo ($_thumbnail) ? 'block':'none'; ?>;">
                                    <a href="" onclick="setFeaturedImage()"><img src="<?php echo $_thumbnail; ?>" alt="" id="post-featured-src"/></a>
                                    <div><em><span>Click the image to edit or update</span></em></div>
                                </div>
                            </div>
                            <div class="col-12">
                                <a href="#" id="remove-post-thumbnail" style="display: <?php echo ($_thumbnail) ? 'block':'none'; ?>;">Remove featured image</a>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Categories</label>
                            <div class="col-12">
                                <ul class="category-list">
                                    <?php $cats = array(); ?>
                                    <?php foreach( $categories as $category ) : ?>
                                        <?php foreach( $post_cats as $post_cat ) : ?>
                                            <?php if( $category->term_id === $post_cat->term_id ) : ?>
                                                <?php $cats[$post_cat->term_id] = $post_cat->name; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <li>
                                            <label class="form-check-label" for="cat-<?php echo $category->term_id; ?>">
                                                <input class="form-check-input" id="cat-<?php echo $category->term_id; ?>" value="<?php echo $category->term_id; ?>" type="checkbox" name="tax_input[gss_blog_categories][]" <?php echo (array_key_exists($category->term_id, $cats)) ? 'checked':''; ?> />
                                                <span class="fa fa-check"></span>
                                                <?php echo $category->name; ?>
                                            </label>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-12 col-form-label text-left">Tags</label>

                            <div class="col-12">
                                <div class="row">
                                    <div class="col-8">
                                        <input data-wp-taxonomy="post_tag" type="text" id="new-tag-post_tag" name="newtag[post_tag]" class="newtag form-input-tip ui-autocomplete-input form-control" size="16" autocomplete="off" aria-describedby="new-tag-post_tag-desc" value="" role="combobox" aria-autocomplete="list" aria-expanded="false" aria-owns="ui-id-1">
                                        <div id="tags-suggested"></div>
                                    </div>

                                    <div class="col-4">
                                        <input type="button" class="button tagadd btn btn-secondary" value="Add">
                                    </div>

                                    <div class="col-12">
                                        <em><p class="howto" id="new-tag-post_tag-desc">Separate tags with commas</p></em>
                                    </div>

                                    <div class="col-12">
                                        <?php $tags = wp_get_post_tags($page->ID); ?>
                                        <div class="form-check" id="post-tags">
                                            <?php foreach( $tags as $tag ) : ?>
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="post_tags[]" value="<?php echo $tag->name; ?>" checked="checked">
                                                    <span class="fa fa-times post-tag"></span>
                                                    <?php echo $tag->name; ?>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <?php echo form_close(); ?>

<?php endif; ?>

<script type="text/javascript">

    jQuery(document).ready(function($){

        $(document).on('click', 'li.ac_over', function(e){
          alert('click');
        });

        $('#new-tag-post_tag').on('keyup', function(){
            $('ul.ac_results:not(:first)').remove();
            $('#new-tag-post_tag').suggest("<?php echo get_bloginfo('wpurl'); ?>/wp-admin/admin-ajax.php?action=ajax-tag-search&tax=post_tag", {multiple:true, multipleSep: ","});
            $('ul.ac_results').removeAttr('style');
            $('ul.ac_results').appendTo('#tags-suggested');
        });

    });

</script>