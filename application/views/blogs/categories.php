<?php if( $this->router->fetch_class() === 'blogs' && $this->router->fetch_method() === 'categories' ) : ?>
        <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-block">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Slug</th>
                                        <th>Post Count</th>
                                    </tr>
                                </thead>
                                
                                <?php if($all_cats) : ?>
                                    <tbody>
                                        <?php foreach( $all_cats as $cat ) : ?>
                                            <tr>
                                                <td><?php echo $cat->term_id; ?></td>
                                                <td><a href="<?php echo base_url('blogs/edit_category/'.$cat->term_id); ?>"><?php echo $cat->name; ?></a></td>
                                                <td><?php echo $cat->description; ?></td>
                                                <td><?php echo $cat->slug; ?></td>
                                                <td><?php echo $cat->count; ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    <?php else : ?>
                                        <tbody>
                                            <tr>
                                                <td><h2>No existing category.</h2></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    <?php endif; ?>
                             </table>
                        </div>
                        <div class="card-footer">
                            <?php //$this->load->view('partials/pagination', $this->data); ?>
                        </div>
                    </div>
                </div>
            </div>
<?php endif; ?>

<?php if( $this->router->fetch_class() === 'blogs' && ($this->router->fetch_method() === 'add_category' || $this->router->fetch_method() === 'edit_category') ) : ?>
    
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-block">
                    <?= form_open( sprintf( '/blogs/%1$s', ($this->uri->segment(4)) ? 'edit_category/'.$gss_blog_cat->term_id : 'add_category' ) ); ?>
                        
                        <fieldset>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Category Name</label>
                                <div class="col-10"><input type="text" name="category_name" class="form-control" value="<?php echo isset($gss_blog_cat->term_id) ? $gss_blog_cat->name:'' ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Slug</label>
                                <div class="col-10"><input type="text" name="category_slug" class="form-control" value="<?php echo isset($gss_blog_cat->term_id) ? $gss_blog_cat->slug:'' ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Parents</label>
                                <div class="col-10">
                                    <select class="form-control" name="category_parent" id="">
                                        <option value="0">None</option>
                                        <?php foreach( $categories as $category ) : ?>
                                            <option value="<?php echo $category->term_id; ?>" <?php echo isset($gss_blog_cat->term_id) ? ($category->term_id===$gss_blog_cat->parent) ? 'selected' : '' :'' ?> ><?php echo $category->name; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Description</label>
                                <div class="col-10"><?php echo form_textarea(['name'=>'category_description', 'class'=>'form-control', 'value'=>isset($gss_blog_cat->term_id) ? $gss_blog_cat->description:'']); ?></div>
                            </div>
                        </fieldset>

                        <fieldset>
                            <div class="form-group row">
                                <div class="col-lg-5 offset-lg-2"><?php echo form_submit('gss_blog_category', ($this->uri->segment(4)) ? 'Update Category':'Add Category', ['class'=>'btn btn-primary']); ?></div>
                            </div>
                        </fieldset>

                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>