<?php use Surefire\Microsites\Lib\Assets; ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>
<body class="sfmu login">
<div class="wrap">
  <main ng-app="microsites">
      <div class="container-fluid">
          <div class="row justify-content-md-center">
              <div class="col-md-6 col-lg-4 col-md-auto">
                  <div class="card" ng-controller="login">
                      <div class="brand"><img src="<?php echo Assets\asset_path('images/logo.png'); ?>" alt="Goldfish Swim School Franchising Dashboard"></div>
                      <div class="card-block">
                          <h1>Goldfish Swim School Franchising Dashboard</h1>
                          <form ng-submit="submit($event)" method="post" action="<?php echo base_url("/auth/login/");  ?>">
                              <div id="log-form-group" class="form-group">
                                <label for="user_login">Username</label>
                                <input type="text" ng-model="log" focus-if="error" name="log" id="user_login" class="form-control" placeholder="Enter Username" autofocus>
                              </div>
                              <div id="pwd-form-group" class="form-group">
                                <label for="user_pass">Password</label>
                                <input type="password" ng-model="pwd" name="pwd" class="form-control" id="user_pass" placeholder="Password">
                              </div>
                              <p class="text-danger" ng-bind="error"><?php echo isset($error) ? $error : ''; ?></p>
                              <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                          </form>
                      </div>
                  </div>
                  <p id="poweredby" class="text-center"><small>Powered by Surefire Local, Inc.</small></p>
              </div>
          </div>
      </div>
  </main>
</div>
<?php wp_footer(); ?>
</body>
</html>
