<div class="row">
    <div class="col-12">
        <?php echo form_open('logs/filter', ['method'=>'get']); ?>
            <fieldset>
                <div class="form-group row">
                    <div class="col-4"></div>
                    <div class="col-8">
                        <div class="row">
                            <div class="col-3">
                                <select name="filterby" class="form-control">
                                    <optgroup label="Filter by">
                                        <option <?php echo (isset($filterby) && $filterby === 'microsite') ? 'selected':''; ?> value="microsite">Franchise</option>
                                        <option <?php echo (isset($filterby) && $filterby === 'module') ? 'selected':''; ?> value="module">Module</option>
                                        <option <?php echo (isset($filterby) && $filterby === 'date') ? 'selected':''; ?> value="date">Date</option>
                                    </optgroup>
                                </select>
                            </div>
                            <div class="col-6"><input type="text" name="filter" class="form-control" value="<?php echo isset($filter) ? $filter:''; ?>"></div>
                            <div class="col-3"><input type="submit" class="btn btn-secondary" value="Filter"></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        <?php echo form_close(); ?>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-block">
                <table class="table table-striped audit-logs">
                    <thead>
                        <tr>
                            <th>Franchise</th>
                            <th>Module</th>
                            <th>User Name</th>
                            <th>Action</th>
                            <th>Fields</th>
                            <th>Old Value/s</th>
                            <th>New Value/s</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                        <tbody>
                            <?php if($all_logs) : ?>
                                <?php foreach($all_logs as $log) : ?>
                                    <?php 
                                        $fields = maybe_unserialize($log->variables); 
                                        if(is_array($fields))
                                            extract($fields);

                                        $user = get_userdata( $log->user );
                                        $site = get_blog_details($log->microsite);
                                    ?>
                                    <tr>
                                        <td><?php echo ($site) ? $site->blogname:'Might be deleted'; ?></td>
                                        <td><?php echo (isset($log->module)) ? ucwords(str_replace('_', ' ', $log->module)):''; ?></td>
                                        <td><?php echo (isset($user)) ? $user->user_login:''; ?></td>
                                        <td><?php echo (isset($log->action) && $log->action === 'edit') ? 'Update': ucwords(str_replace('_', ' ', $log->action)); ?></td>
                                        <td>
                                            <?php if( is_array($new_values) && ! empty($new_values) ) : ?>
                                                <ol>
                                                    <?php foreach($new_values as $new_key => $new_value) : ?>
                                                        <li>
                                                            <?php if( $log->module === 'sidebars' ) : ?>
                                                                <?php switch_to_blog(1); $title = get_the_title($new_value); restore_current_blog(); ?>
                                                                <?php echo $title; ?>
                                                            <?php elseif( $log->module === 'schedules' ) : ?>
                                                                Multiple Fields
                                                            <?php elseif( gettype($new_key) === 'integer' ) : ?>
                                                                <?php switch_to_blog($log->microsite); $title = get_the_title($new_key); restore_current_blog(); ?>
                                                                <?php echo $title; ?>
                                                            <?php else : ?>
                                                                <?php $remove = array('/_/', '/microsite_/', '/gss_/', '/sfmu/', '/s8/', '/stellar/', '/ff /') ;?>
                                                                <?php echo ucwords( preg_replace($remove, ' ', $new_key) ); ?>
                                                            <?php endif; ?>

                                                            <?php if(is_array($new_value) && ! empty($new_value)) : ?>
                                                                <ul>
                                                                    <?php foreach( $new_value as $segment_key => $segment_val ) : ?>
                                                                        <?php if( is_array($segment_val) && !empty($segment_val) ) : ?>
                                                                            <?php foreach( $segment_val as $field_key => $field_val ) : ?>
                                                                                <li><?php echo $field_key; ?></li>
                                                                            <?php endforeach; ?>
                                                                        <?php else : ?>
                                                                            <li><?php echo ucwords( str_replace('_', ' ', $segment_key) ); ?></li>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            <?php endif; ?>
                                                        </li>
                                                    <?php endforeach; ?>
                                                </ol>
                                            <?php else : ?>
                                                <?php if( strpos($log->action, 'category') ) : ?>
                                                    <ol>
                                                        <?php foreach($old_values as $old_key => $old_value) : ?>
                                                            <li><?php echo $old_key; ?></li>
                                                        <?php endforeach; ?>
                                                    </ol>
                                                <?php else : ?>
                                                    <?php echo ($log->action === 'footer_logos') ? 'Footer Logos':'N/A'; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if( is_array($old_values) && ! empty($old_values) ) : ?>
                                                <ol>
                                                    <?php if( $log->module === 'schedules' ) : ?>
                                                        <a href="" data-toggle="modal" data-target="#schedule-log<?php echo $log->ID; ?>">View Old Schedules</a>
                                                            <div class="modal fade bd-example-modal-lg" id="schedule-log<?php echo $log->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="exampleModalLabel">Old Schedules</h5>
                                                                            <button style="display: block;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            <table class="table table-striped">
                                                                                <thead>
                                                                                    <?php foreach($old_values as $category => $days) : ?>
                                                                                        <th><?php echo ucwords(str_replace('_', ' ', $category)); ?></th>
                                                                                    <?php endforeach; ?>
                                                                                </thead>
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <?php foreach($old_values as $category => $days) : ?>
                                                                                            <td>
                                                                                                <?php foreach($days as $day => $times) : ?>
                                                                                                <ul>
                                                                                                    <li>
                                                                                                        <?php echo ucwords($day); ?>
                                                                                                        <ul>
                                                                                                            <?php foreach($times as $hours) : ?>
                                                                                                                <?php if( ! is_array($hours) ) : ?>
                                                                                                                    <li><?php echo $hours; ?></li>
                                                                                                                <?php else : ?>
                                                                                                                    <?php foreach($hours as $hrs) : ?>
                                                                                                                        <?php foreach($hrs as $hr) : ?>
                                                                                                                            <li><?php echo $hr; ?></li>
                                                                                                                        <?php endforeach; ?>
                                                                                                                    <?php endforeach; ?>
                                                                                                                <?php endif; ?>
                                                                                                            <?php endforeach; ?>
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                </ul>
                                                                                                <?php endforeach; ?>
                                                                                            </td>
                                                                                        <?php endforeach; ?>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <?php endif; ?>
                                                    
                                                    <?php if($log->module !== 'lessons') : ?>
                                                        <?php foreach($old_values as $old_key => $old_value) : ?>
                                                            <?php if( is_array($old_value) ) : ?>
                                                                <?php if( $log->module !== 'schedules' ) : ?>
                                                                    <li>
                                                                        <?php echo ucwords(str_replace('_', ' ', $old_key)); ?>

                                                                        <?php foreach($old_value as $log_key => $log_val) : ?>
                                                                            <?php if($log_val) : ?>
                                                                                <ul>
                                                                                    <li><?php echo $log_val;  ?></li>
                                                                                </ul>
                                                                            <?php else : ?>
                                                                                <ul>
                                                                                    <li>None</li>
                                                                                </ul>
                                                                            <?php endif; ?>
                                                                            
                                                                        <?php endforeach; ?>
                                                                    </li>
                                                                <?php endif; ?>
                                                            <?php else : ?>
                                                                <?php if( $log->module === 'sidebars' ) : ?>
                                                                    <li><?php echo ($old_value) ? '<strong>on</strong>': 'off'; ?></li>
                                                                <?php elseif( $log->module === 'hero') : ?>
                                                                    <?php switch_to_blog($microsite); $attachment = wp_get_attachment_url($old_value); restore_current_blog(); ?>
                                                                    <li><?php echo ($old_value) ? '<a target="_blank" href="'.$attachment.'">'.$old_value.'</a>':'None'; ?></li>
                                                                <?php else :?>
                                                                    <li><?php echo ($old_value) ?: 'None'; ?></li>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    <?php else :?>
                                                        <a href="" data-toggle="modal" data-target="#lesson-log<?php echo $log->ID; ?>">View</a>    
                                                        <div class="modal fade bd-example-modal-lg" id="lesson-log<?php echo $log->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="lessonLog<?php echo $log->ID; ?>" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="lessonLog<?php echo $log->ID; ?>"><?php echo ucwords($log->module); ?> - <?php echo isset($old_values['title']) ? $old_values['title']:''; ?></h5>
                                                                            <button style="display: block;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body alert alert-warning">
                                                                            <div class="row">
                                                                                <div class="col">
                                                                                    <?php unset($old_values['title']); ?>
                                                                                    <?php foreach($old_values as $old_key => $old_value) : ?>
                                                                                        <li>
                                                                                            <?php echo ucwords($old_key); ?>
                                                                                            <ul>
                                                                                                <li><?php echo $old_value; ?></li>
                                                                                            </ul>
                                                                                        </li><br/>
                                                                                    <?php endforeach; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>                                               
                                                    <?php endif; ?>
                                                </ol>
                                            <?php else : ?> 
                                                <?php if($log->action === 'footer_logos') : ?>
                                                    None
                                                <?php else : ?>
                                                    <?php if( is_object($old_values) && get_class($old_values) === 'WP_Post' ) : ?>
                                                        <span><a href="" data-toggle="modal" data-target="#content-log<?php echo $old_values->ID; ?>">View</a></span>
                                                            <div class="modal fade bd-example-modal-lg" id="content-log<?php echo $old_values->ID; ?>" tabindex="-1" role="dialog" aria-labelledby="contentLog<?php echo $old_values->ID; ?>" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="contentLog<?php echo $old_values->ID; ?>"><?php echo ucwords($log->module); ?> - <?php echo $old_values->post_title; ?></h5>
                                                                            <button style="display: block;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body alert alert-warning">
                                                                            <?php echo wpautop($old_values->post_content); ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <?php else :?>
                                                        None
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?php if( is_array($new_values) && ! empty($new_values) ) : ?>
                                                <ol>
                                                    <?php foreach($new_values as $new_key => $new_value) : ?>
                                                        <?php if( is_array($new_value) ) : ?>
                                                            <li>
                                                                <?php echo ucwords(str_replace('_', ' ', $new_key)); ?>

                                                                <?php foreach($new_value as $log_key => $log_val) : ?>
                                                                    <?php if($log_val) : ?>
                                                                        <?php if( is_array($log_val) && ! empty($log_val) ) : ?>
                                                                            <ul>
                                                                                <?php foreach ($log_val as $log_val_new) : ?>
                                                                                    <li><?php echo $log_val_new;  ?></li>
                                                                                <?php endforeach; ?>
                                                                            </ul>
                                                                        <?php else : ?>
                                                                            <ul>
                                                                                <li><?php echo $log_val;  ?></li>
                                                                            </ul>
                                                                        <?php endif; ?>
                                                                    <?php else : ?>
                                                                        <ul>
                                                                            <li>None</li>
                                                                        </ul>
                                                                    <?php endif; ?>
                                                                    
                                                                <?php endforeach; ?>
                                                            </li>

                                                        <?php else : ?>
                                                            <?php if( $log->module === 'sidebars' ) : ?>
                                                                <li><?php echo ($new_value) ? '<strong>on</strong>': 'off'; ?></li>
                                                            <?php elseif( $log->module === 'schedules' && $new_value ) : ?>
                                                                <a target="_blank" href="<?php echo base_url('schedules'); ?>">View Schedule</a>
                                                            <?php elseif( $log->module === 'hero') : ?>
                                                                <?php switch_to_blog($microsite); $attachment = wp_get_attachment_url($new_value); restore_current_blog(); ?>
                                                                <li><?php echo ($new_value) ? '<a target="_blank" href="'.$attachment.'">'.$new_value.'</a>':'None'; ?></li>
                                                            <?php else : ?>
                                                                <li><?php echo ($new_value) ?: 'None'; ?></li>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                </ol>
                                            <?php else : ?>
                                                <?php if( filter_var($new_values, FILTER_VALIDATE_URL) ) : ?>
                                                    <a target="_blank" href="<?php echo $new_values; ?>">View</a>
                                                <?php else : ?>
                                                    <?php if( ! is_array($new_values) ) : ?>
                                                        <?php if(is_integer($new_values)) : ?>
                                                            <?php $post = gss_get_post($microsite, $new_values); ?>
                                                            
                                                            <?php if($post) : ?>
                                                                <span><a href="" data-toggle="modal" data-target="#content-lognew<?php echo $new_values; ?>">View</a></span>
                                                            <?php else : ?>
                                                                <?php if( strpos($log->action, 'category') > 0 ) : ?>
                                                                    <a target="_blank" href="<?php echo base_url('events/edit_category/'.$new_values); ?>">View Category</a>
                                                                <?php else : ?>
                                                                    N/A
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                            <div class="modal fade bd-example-modal-lg" id="content-lognew<?php echo $new_values; ?>" tabindex="-1" role="dialog" aria-labelledby="contentLogNew<?php echo $new_values; ?>" aria-hidden="true">
                                                                <div class="modal-dialog modal-lg" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="contentLogNew<?php echo $new_values; ?>"><?php echo ucwords($log->module); ?> - <?php echo (isset($post->post_title)) ? $post->post_title:''; ?></h5>
                                                                            <button style="display: block;" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body alert alert-success">
                                                                            <?php if($post && get_class($post) === 'WP_Post' && $post->post_content) : ?>
                                                                                <?php echo wpautop($post->post_content); ?>
                                                                            <?php else : ?>
                                                                                <?php $metadata = gss_get_post_meta(1, $new_values); ?>
                                                                                <?php if( $metadata ) : ?>
                                                                                    <ol>
                                                                                        <?php foreach($metadata as $meta => $data ) :?>
                                                                                            <?php if($meta === 'lesson_age' || $meta === 'lesson_details' || $meta === 'lesson_requirements') : ?>
                                                                                            <li>
                                                                                                <?php echo ucwords(str_replace('lesson_', ' ', $meta)); ?>
                                                                                                <ul>
                                                                                                    <li><?php echo $data[0]; ?></li>
                                                                                                </ul>
                                                                                            </li><br/>
                                                                                            <?php endif; ?>
                                                                                        <?php endforeach; ?>
                                                                                    </ol>
                                                                                <?php endif; ?>
                                                                            <?php endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        <?php else : ?>
                                                            <?php echo ucwords($new_values); ?>
                                                        <?php endif; ?>
                                                    <?php else : ?>
                                                        <?php echo ($log->action === 'footer_logos') ? 'None':'N/A'; ?>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo nice_date($log->date, 'M d, Y') .' - '. date('h:i:s a', strtotime($log->date)); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </tbody>
                </table>    
            </div>

            <?php $this->load->view('partials/pagination', $this->data); ?>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){

        if( $('select[name="filterby"]').val() == 'date' ){
            $('input[name="filter"]').addClass('logdate');
            $(".logdate").datepicker();
        }
        else{
            $('.logdate').datepicker("destroy");
        }

        $('select[name="filterby"]').on('change', function(){
            $('input[name="filter"]').val('');
            if( $(this).val() === 'date' ){
                $('input[name="filter"]').addClass('logdate');
                $(".logdate").datepicker();
            }
            else{
                $('.logdate').datepicker("destroy");
            }
        });
    });
</script>