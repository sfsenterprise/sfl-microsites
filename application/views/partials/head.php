<?php use Surefire\Microsites\Lib\Assets; ?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--[if lt IE 9]>
  <script src="<?php echo Assets\asset_path('scripts/html5shiv.js'); ?>"></script>
  <![endif]-->
  <?php wp_head(); ?>
</head>
