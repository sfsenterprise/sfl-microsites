<?php use Surefire\Microsites\Lib\Assets; ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <?php $this->load->view('partials/head', $this->load->data); ?>
    <body
      class="sfmu no-js <?php echo is_admin_bar_showing() ? 'admin-bar' : ''; ?><?php echo $this->router->fetch_class(); ?>"
      ng-app="microsites">
        <script type="text/javascript">
        document.body.className = document.body.className.replace('no-js','js');
        </script>
        <div class="wrap">
            <?php $this->load->view('partials/header', $this->load->data); ?>
            <?php $this->load->view('partials/sidebar', $this->load->data); ?>

            <main class="main">
                <div id="loading-bar-container"></div>
                <?php $this->load->view('partials/title', $this->load->data); ?>

                <div class="main-content container-fluid animated <?php echo ! $this->input->post(NULL) && ! $this->input->get(NULL) ? 'fadeInUp' : ''; ?>">
                    <?php $this->load->view($template, $this->load->data); ?>
                </div>
            </main>
            <?php $this->load->view('partials/footer', $this->load->data); ?>

        </div>

        <!-- Delete Dialog -->
        <div class="modal fade" id="delete-dialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <div class="modal-title">Delete Microsite</div>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                <p>Are you sure you want to delete <span class="text-danger"><?php echo isset($edit_microsite) ? $edit_microsite->microsite_name : null; ?></span>? This will permanently delete all data of microsite.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                <a class="btn btn-primary" href="<?php echo isset($edit_microsite) ? base_url("/microsites/delete/{$edit_microsite->microsite_blog_id}") : null; ?>">Yes</a>
              </div>
            </div>
          </div>
        </div>

      <?php wp_footer(); ?>
    </body>
</html>
