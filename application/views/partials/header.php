<?php use Surefire\Microsites\Lib\Assets; ?>

<header class="topnavbar-wrapper">
    <nav class="topnavbar" role="navigation">
        <div class="topnavbar-header">
            <a class="navbar-brand" href="<?php echo base_url(); ?>">
                <div class="brand-logo">
                    <img src="<?php echo Assets\asset_path('images/goldfish.png'); ?>" alt="Goldfish Swim School" />
                </div>
            </a>
        </div>
        <div class="topnavbar-microsite-select">
            <?php if ( isset($sites) && count($sites) > 0 ) : ?>
                <?= form_open('/microsites/site/'); ?>
                    <select class="microsite-select" name="microsite">
                        <option value="" disabled selected>Select a microsite</option>
                        <?php foreach( $sites as $site ) : ?>
                            <option <?php echo absint($this->microsite) === absint($site->blog_id) ? 'selected' : null; ?> value="<?php echo $site->blog_id?>"><?php echo $site->blogname; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <button type="submit"><i class="fa fa-arrow-right"></i></button>
                <?= form_close(); ?>
            <?php else : ?>
                <div>No site assigned for this user.</div>
            <?php endif; ?>
        </div>
        <div class="topnavbar-userbox">
            <div class="dropdown">
                <a class="userbox-wrapper dropdown-toggle" href="" alt="User Menu" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-circle fa-stack-2x"></i>
                        <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                    </span>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <div class="profile">
                      <?php echo get_avatar($user, 52); ?>
                      <span class="display_name"><?php echo $user->display_name; ?></span>
                  </div>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url(sprintf('users/edit/%1s', get_current_user_id())); ?>">Edit Profile</a>
                  <a class="dropdown-item" href="<?php echo base_url('/auth/logout/'); ?>">Logout</a>
                </div>
            </div>
        </div>
    </nav>
</header>
