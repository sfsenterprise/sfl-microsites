<nav class="sidebar">
    <ul id="navmenu" class="navmenu" ng-controller="micrositeNav">
        <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'dashboard') ? 'navmenu-item-active' : ''; ?>">
            <a href="<?php echo base_url('/dashboard/'); ?>"><i class="fa fa-dashboard"></i>Dashboard</a>
        </li>
        <li class="navmenu-heading">Network Navigation</li>
        <?php $this->load->view('partials/navs/network/microsites'); ?>
        <?php $this->load->view('partials/navs/network/users'); ?>
        <?php $this->load->view('partials/navs/network/locations'); ?>
        <?php $this->load->view('partials/navs/network/pages'); ?>
        <?php $this->load->view('partials/navs/network/hero'); ?>
        <?php $this->load->view('partials/navs/network/blogs'); ?>
        <?php $this->load->view('partials/navs/network/lessons'); ?>
        <?php $this->load->view('partials/navs/network/testimonials'); ?>
        <?php $this->load->view('partials/navs/network/faqs'); ?>
        <?php $this->load->view('partials/navs/network/forms'); ?>
        <?php $this->load->view('partials/navs/network/sidebars'); ?>
        <?php $this->load->view('partials/navs/network/tools'); ?>
        <?php $this->load->view('partials/navs/network/logs'); ?>

        <?php if (  $this->microsite > 0 ) : ?>
            <li class="navmenu-heading">Microsite Navigation</li>
            <?php //$this->load->view('partials/navs/microsite/pages'); ?>
            <?php //$this->load->view('partials/navs/microsite/hero'); ?>
            <?php $this->load->view('partials/navs/microsite/pricingtable'); ?>
            <?php $this->load->view('partials/navs/microsite/schedules'); ?>
            <?php $this->load->view('partials/navs/microsite/events'); ?>
            <?php $this->load->view('partials/navs/microsite/jobs'); ?>
            <?php $this->load->view('partials/navs/microsite/forms'); ?>
            <?php $this->load->view('partials/navs/microsite/announcements'); ?>
            <?php $this->load->view('partials/navs/microsite/promos'); ?>
            <?php $this->load->view('partials/navs/microsite/staff'); ?>
            <?php $this->load->view('partials/navs/microsite/news'); ?>
            <?php $this->load->view('partials/navs/microsite/sidebars'); ?>
            <?php $this->load->view('partials/navs/microsite/social'); ?>
            <?php $this->load->view('partials/navs/microsite/users'); ?>
            <?php $this->load->view('partials/navs/microsite/settings'); ?>
        <?php endif; ?>
    </ul>
</nav>
