<?php if ( isset($messages) ) : ?>
    <div class="notification">
        <div class="notification-container <?php echo isset($messages['error']) ? 'bg-danger' : 'bg-success'; ?>">
            <button class="close">
                <span class="close-x" aria-hidden="true">×</span>
            </button>
            <div class="notification-block">
                <?php if ( isset($messages['added']) ) : ?>
                    <?php echo $messages['added']; ?>
                <?php elseif ( isset($messages['updated']) ) : ?>
                    <?php echo $messages['updated']; ?>
                <?php else : ?>
                    <?php echo isset($messages['error']) ? $messages['error'] : null; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
