<?php use Surefire\Microsites\Lib\Assets; ?>

<h1 class="main-title">
    <div class="row">
        <div class="col-4">
            <?php echo $title; ?> 

            <?php if( isset($view_frontend) ) : ?>
                <a class="h2 text-primary px-2" target="_blank" href="<?php echo $view_frontend; ?>">View in Site</a>
            <?php endif; ?>
        </div>

        <div class="col-8">
            <?php if( isset($form_name) ) : ?>
                <?php echo $form_name; ?> 
            <?php endif; ?>
        </div>
    </div>
</h1>