<?php if ( $this->pagination->create_links() ) : ?>
    <div class="card-footer">
        <div class="table-footer row">
            <div class="col-lg-3"></div>
            <div class="col-lg-5"></div>
            <div class="col-lg-4 text-right">
                <?php echo $this->pagination->create_links(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>
