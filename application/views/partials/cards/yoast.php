<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Yoast SEO</h4>
                </fieldset>
                <fieldset>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">SEO Title</label>
                        <div class="col-10">

                            <input type="text" class="form-control" name="_yoast_wpseo_title" value="<?php echo (isset($yoast['_yoast_wpseo_title'])) ? $yoast['_yoast_wpseo_title']:''; ?>"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Meta Description</label>
                        <div class="col-10">
                            <textarea name="_yoast_wpseo_metadesc" class="form-control" rows="4"><?php echo (isset($yoast['_yoast_wpseo_metadesc']))? $yoast['_yoast_wpseo_metadesc']:''; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-check col-3 offset-2">
                            <label class="form-check-label">
                              <input class="form-check-input" type="checkbox" name="yoast_wpseo_meta-robots-noindex" value="1" <?php checked($yoast['yoast_wpseo_meta-robots-noindex'], '1'); ?>>
                              <span class="fa fa-check"></span>
                               Meta No Index
                            </label>
                        </div>
                        <div class="form-check col-3">
                            <label class="form-check-label">
                              <input class="form-check-input" type="checkbox" name="yoast_wpseo_meta-robots-nofollow" value="1" <?php checked($yoast['yoast_wpseo_meta-robots-nofollow'], '1'); ?>>
                              <span class="fa fa-check"></span>
                               Meta No Follow
                            </label>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Meta robots</label>
                        <div class="col-10">
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="_yoast_wpseo_meta-robots-adv[]" value="-" <?php checked($yoast['_yoast_wpseo_meta-robots-adv'], null); ?>>
                                  <span class="fa fa-check"></span>
                                   Site-wide default: None
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="_yoast_wpseo_meta-robots-adv[]" value="none" <?php checked($yoast['_yoast_wpseo_meta-robots-adv'], 'none'); ?>>
                                  <span class="fa fa-check"></span>
                                   None
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="_yoast_wpseo_meta-robots-adv[]" value="noimageindex" <?php checked(strpos($yoast['_yoast_wpseo_meta-robots-adv'], 'noimageindex') !== false ? 1 : 0, 1); ?>>
                                  <span class="fa fa-check"></span>
                                   No Image Index
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="_yoast_wpseo_meta-robots-adv[]" value="noarchive" <?php checked(strpos($yoast['_yoast_wpseo_meta-robots-adv'], 'noarchive') !== false ? 1 : 0, 1); ?>>
                                  <span class="fa fa-check"></span>
                                   No Archive
                                </label>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="_yoast_wpseo_meta-robots-adv[]" value="nosnippet" <?php checked(strpos($yoast['_yoast_wpseo_meta-robots-adv'], 'nosnippet') !== false ? 1 : 0, 1); ?>>
                                  <span class="fa fa-check"></span>
                                   No Snippet
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label">Canonical URL</label>
                        <div class="col-10"><input type="text" name="canonical_url" class="form-control" id="canonical-url" value="<?php echo isset($page->ID) ? gss_get_post_meta( $microsite, $page->ID, "canonical_url" ) : '' ?>" /></div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
