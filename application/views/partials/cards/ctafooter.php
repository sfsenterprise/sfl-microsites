<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-block">
                <h4 class="card-title">Call to Action Footer</h4>
                <fieldset>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="_call_to_action_footer_corporate">Corporate</label>
                        <div class="col-10">
                            <?php wp_editor( (isset($footer['corporate'])) ? $footer['corporate']: '', '_call_to_action_footer_corporate', ['textarea_name' => '_call_to_action_footer[corporate]', 'textarea_rows' => 4, 'teeny' => true, 'media_buttons' => false]); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-2 col-form-label" for="_call_to_action_footer_local">Local</label>
                        <div class="col-10">
                            <?php wp_editor( (isset($footer['local'])) ? $footer['local']:'', '_call_to_action_footer_local', ['textarea_name' => '_call_to_action_footer[local]', 'textarea_rows' => 4, 'teeny' => true, 'media_buttons' => false]); ?>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
