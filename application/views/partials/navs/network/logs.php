<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'logs' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
        <a href="<?php echo base_url('/logs/'); ?>"><i class="fa fa-eye"></i>Audit Logs</a>
    </li>
<?php endif; ?>