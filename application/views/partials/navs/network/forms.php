<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage' || $this->router->fetch_method() === 'forms' || $this->router->fetch_method() === 'addform' || $this->router->fetch_method() === 'editform' ) ) ? 'navmenu-item-active' : ''; ?>">
        <a href="#site-formpages"
            class="<?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage' || $this->router->fetch_method() === 'forms' || $this->router->fetch_method() === 'addform' || $this->router->fetch_method() === 'editform' ) ) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage' || $this->router->fetch_method() === 'forms' || $this->router->fetch_method() === 'addform' || $this->router->fetch_method() === 'editform' ) ) ? 'true' : 'false'; ?>"
            aria-controls="site-formpages">
            <i class="fa fa-wpforms"></i>Forms</a>
            <ul id="site-formpages"
                class="submenu collapse <?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage' || $this->router->fetch_method() === 'forms' || $this->router->fetch_method() === 'addform' || $this->router->fetch_method() === 'editform' ) ) ? 'show' : ''; ?>"
                aria-expanded="<?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage' || $this->router->fetch_method() === 'forms' || $this->router->fetch_method() === 'addform' || $this->router->fetch_method() === 'editform' ) ) ? 'true' : 'false'; ?>">
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'index' ) ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/formpages/'); ?>">All Forms Page</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'formpages' && ( $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage' ) ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/formpages/addpage'); ?>">Add Form Page</a>
                </li>
                <li class="navmenu-item <?php echo ( $this->router->fetch_class() === 'formpages' && $this->router->fetch_method() === 'forms' ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/formpages/forms'); ?>">All Forms</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'formpages' && ($this->router->fetch_method() === 'addform' || $this->router->fetch_method() === 'editform') ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/formpages/addform'); ?>">Add Form</a>
                </li>
            </ul>
    </li>
<?php endif; ?>