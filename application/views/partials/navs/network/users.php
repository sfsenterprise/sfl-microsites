<?php if ( current_user_can('manage_network_users') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'users' || $this->router->fetch_method() === 'useradd' || $this->router->fetch_method() === 'useredit')) ? 'navmenu-item-active' : ''; ?>">
        <a href="#microsite-users"
            class="<?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'users' || $this->router->fetch_method() === 'useradd' || $this->router->fetch_method() === 'useredit')) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'users' || $this->router->fetch_method() === 'useradd' || $this->router->fetch_method() === 'useredit')) ? 'true' : 'false'; ?>"
            aria-controls="microsite-users">
            <i class="fa fa-id-badge"></i>Microsite Users
        </a>
        <ul id="microsite-users"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'users' || $this->router->fetch_method() === 'useradd' || $this->router->fetch_method() === 'useredit')) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'users' || $this->router->fetch_method() === 'useradd' || $this->router->fetch_method() === 'useredit')) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'users') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/microsites/users/'); ?>">All Users</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'useradd') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/microsites/useradd/'); ?>">Add User</a>
            </li>
        </ul>
    </li>
<?php endif; ?>
