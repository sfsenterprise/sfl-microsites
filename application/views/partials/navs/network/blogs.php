<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'blogs') ? 'navmenu-item-active' : '';?>">
        <a href="#site-blogs"
            class="<?php echo ($this->router->fetch_class() === 'blogs' ) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'blogs') ? 'true' : 'false'; ?>"
            aria-controls="site-blogs">
            <i class="fa fa-book"></i>Blogs</a>
            <ul id="site-blogs"
                class="submenu collapse <?php echo ($this->router->fetch_class() === 'blogs' ) ? 'show' : ''; ?>"
                aria-expanded="<?php echo ($this->router->fetch_class() === 'blogs' ) ? 'true' : 'false'; ?>">
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'blogs' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/blogs/'); ?>">All Blogs</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'blogs' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/blogs/add'); ?>">Add Blog</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'blogs' && ($this->router->fetch_method() === 'categories' || $this->router->fetch_method() === 'edit_category')) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/blogs/categories'); ?>">Categories</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'blogs' && $this->router->fetch_method() === 'add_category') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/blogs/add_category'); ?>">Add Category</a>
                </li>
            </ul>
    </li>
<?php endif; ?>