<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'pages' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage')) ? 'navmenu-item-active' : ''; ?>">
        <a href="#microsite-pages"
            class="<?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'pages' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage')) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'pages' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage')) ? 'true' : 'false'; ?>"
            aria-controls="microsite-pages">
            <i class="fa fa-clone"></i>Global Pages
        </a>
        <ul id="microsite-pages"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'pages' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage')) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'pages' || $this->router->fetch_method() === 'addpage' || $this->router->fetch_method() === 'editpage')) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'pages') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/microsites/pages/'); ?>">All Pages</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'addpage') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/microsites/addpage/'); ?>">Add Page</a>
            </li>
        </ul>
    </li>
<?php endif; ?>
