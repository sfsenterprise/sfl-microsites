<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'testimonials') ? 'navmenu-item-active' : ''; ?>">
        <a href="#site-testimonials"
            class="<?php echo ($this->router->fetch_class() === 'testimonials' ) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'testimonials') ? 'true' : 'false'; ?>"
            aria-controls="site-testimonials">
            <i class="fa fa-comments-o"></i>Testimonials</a>
            <ul id="site-testimonials"
                class="submenu collapse <?php echo ($this->router->fetch_class() === 'testimonials' ) ? 'show' : ''; ?>"
                aria-expanded="<?php echo ($this->router->fetch_class() === 'testimonials' ) ? 'true' : 'false'; ?>">
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'testimonials' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/testimonials/'); ?>">All Testimonials</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'testimonials' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/testimonials/add'); ?>">Add Testimonial</a>
                </li>
            </ul>
    </li>
<?php endif; ?>