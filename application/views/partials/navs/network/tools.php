<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'tools') ? 'navmenu-item-active' : ''; ?>">
        <a href="#site-tools"
            class="<?php echo ($this->router->fetch_class() === 'tools' ) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'tools') ? 'true' : 'false'; ?>"
            aria-controls="site-tools">
            <i class="fa fa-wrench"></i>Tools</a>
            <ul id="site-tools"
                class="submenu collapse <?php echo ($this->router->fetch_class() === 'tools' ) ? 'show' : ''; ?>"
                aria-expanded="<?php echo ($this->router->fetch_class() === 'tools' ) ? 'true' : 'false'; ?>">
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'tools' && $this->router->fetch_method() === 'redirections') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/tools/redirections'); ?>">Redirections</a>
                </li>
            </ul>
    </li>
<?php endif; ?>