<li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && ($this->router->fetch_method() === 'index' || $this->router->fetch_method() === 'edit')) ? 'navmenu-item-active' : ''; ?>">
    <a href="<?php echo base_url('/microsites/'); ?>"><i class="fa fa-sitemap"></i>All Microsites</a>
</li>
<?php if ( $this->microsite ) : ?>
    <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'edit') ? 'navmenu-item-active' : ''; ?>">
        <a href="<?php echo base_url('/microsites/edit/'. $this->microsite); ?>"><i class="fa fa-pencil-square-o"></i>Edit Microsite</a>
    </li>
<?php endif; ?>
<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
        <a href="<?php echo base_url('/microsites/add/'); ?>"><i class="fa fa-desktop"></i>Add Microsite</a>
    </li>
<?php endif; ?>
