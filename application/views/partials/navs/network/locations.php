<?php if ( current_user_can('franchisor') ) : ?>
	<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'locations' && ($this->router->fetch_method() === 'cities' || $this->router->fetch_method() === 'states' || $this->router->fetch_method() === 'edit')) ? 'navmenu-item-active' : ''; ?>">
	    <a href="#site-locations"
	        class="<?php echo ($this->router->fetch_class() === 'locations' && ($this->router->fetch_method() === 'cities' || $this->router->fetch_method() === 'states' || $this->router->fetch_method() === 'edit')) ? '' : 'collapsed'; ?>"
	        data-toggle="collapse"
	        data-parent="#navmenu"
	        aria-expanded="<?php echo ($this->router->fetch_class() === 'locations' && ($this->router->fetch_method() === 'cities' || $this->router->fetch_method() === 'states' || $this->router->fetch_method() === 'edit')) ? 'true' : 'false'; ?>"
	        aria-controls="site-locations">
	        <i class="fa fa-map-marker"></i>Locations</a>
	        <ul id="site-locations"
	            class="submenu collapse <?php echo ($this->router->fetch_class() === 'locations' && ($this->router->fetch_method() === 'cities' || $this->router->fetch_method() === 'states' || $this->router->fetch_method() === 'edit')) ? 'show' : ''; ?>"
	            aria-expanded="<?php echo ($this->router->fetch_class() === 'locations' && ($this->router->fetch_method() === 'cities' || $this->router->fetch_method() === 'states' || $this->router->fetch_method() === 'edit')) ? 'true' : 'false'; ?>">
	            <li class="navmenu-item <?php echo ( $this->router->fetch_class() === 'locations' && $this->router->fetch_method() === 'cities' ) ? 'navmenu-item-active' : ''; ?>">
	            	<a href="<?php echo base_url('/locations/cities'); ?>">Cities</a>
	            </li>
	            <li class="navmenu-item <?php echo ( $this->router->fetch_class() === 'locations' && $this->router->fetch_method() === 'states' ) ? 'navmenu-item-active' : ''; ?>">
	                <a href="<?php echo base_url('/locations/states'); ?>">States/Provinces</a>
	            </li>
	        </ul>
	</li>
<?php endif; ?>