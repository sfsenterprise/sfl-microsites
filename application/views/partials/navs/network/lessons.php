<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'lessons') ? 'navmenu-item-active' : '';?>">
        <a href="#site-lessons" class="<?php echo ($this->router->fetch_class() === 'lessons' ) ? '' : 'collapsed'; ?>"
                data-toggle="collapse"
                data-parent="#navmenu"
                aria-expanded="<?php echo ($this->router->fetch_class() === 'lessons') ? 'true' : 'false'; ?>"
                aria-controls="site-lessons">
                    <i class="fa fa-television"></i>Lessons</a>
                        <ul id="site-lessons" class="submenu collapse <?php echo ($this->router->fetch_class() === 'lessons' ) ? 'show' : ''; ?>" aria-expanded="<?php echo ($this->router->fetch_class() === 'lessons' ) ? 'true' : 'false'; ?>">
                            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'lessons' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                                <a href="<?php echo base_url('/lessons/'); ?>">All Lessons</a>
                            </li>
                            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'lessons' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                                <a href="<?php echo base_url('/lessons/add'); ?>">Add Lesson</a>
                            </li>
                            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'lessons' && ($this->router->fetch_method() === 'group' || $this->router->fetch_method() === 'edit_group')) ? 'navmenu-item-active' : ''; ?>">
                                <a href="<?php echo base_url('/lessons/group'); ?>">Lessons Group</a>
                            </li>
                            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'lessons' && $this->router->fetch_method() === 'add_group') ? 'navmenu-item-active' : ''; ?>">
                                <a href="<?php echo base_url('/lessons/add_group'); ?>">Add Group</a>
                            </li>
                        </ul>
    </li>
<?php endif; ?>