<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'hero') ? 'navmenu-item-active' : ''; ?>"><a href="<?php echo base_url('/hero/'); ?>"><i class="fa fa-image"></i>Hero Banners</a></li>
<?php endif; ?>
