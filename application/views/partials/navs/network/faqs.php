<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'faqs') ? 'navmenu-item-active' : ''; ?>">
        <a href="#site-faqs"
            class="<?php echo ($this->router->fetch_class() === 'faqs' ) ? '' : 'collapsed'; ?>"
            data-toggle="collapse"
            data-parent="#navmenu"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'faqs') ? 'true' : 'false'; ?>"
            aria-controls="site-faqs">
            <i class="fa fa-question-circle"></i>FAQs</a>
            <ul id="site-faqs"
                class="submenu collapse <?php echo ($this->router->fetch_class() === 'faqs' ) ? 'show' : ''; ?>"
                aria-expanded="<?php echo ($this->router->fetch_class() === 'faqs' ) ? 'true' : 'false'; ?>">
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'faqs' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/faqs/'); ?>">All FAQs</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'faqs' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/faqs/add'); ?>">Add FAQ</a>
                </li>
            </ul>
    </li>
<?php endif; ?>