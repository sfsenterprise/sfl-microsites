<?php if ( current_user_can('franchisor') ) : ?>
<li class="navmenu-item card <?php echo ( ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'sidebars') || ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'add') ) ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-sidebars-network"
        class="<?php echo ( ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'sidebars') || ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'add') ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ( ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'sidebars') || ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'add') ) ? 'true' : 'false'; ?>"
        aria-controls="site-sidebars-network">
        <i class="fa fa-window-restore"></i>Sidebars</a>
        <ul id="site-sidebars-network"
            class="submenu collapse <?php echo ( ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'sidebars') || ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'add') ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ( ($this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'sidebars') || ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'add') ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'microsites') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/microsites/sidebars/'); ?>">All Sidebars</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/sidebars/add/'); ?>">Add New</a>
            </li>
        </ul>
</li>
<?php endif; ?>