<?php if ( current_user_can('franchisor') ) : ?>
    <li class="navmenu-item card">
    <a href="#site-general"
        class="<?php echo ($this->router->fetch_class() === 'settings' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'settings') ? 'true' : 'false'; ?>"
        aria-controls="site-redirection">
        <i class="fa fa-cog"></i>General Settings</a>
        <ul id="site-general"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'settings' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'settings' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'settings' && $this->router->fetch_method() === 'redirection') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('settings/redirections/'); ?>">Redirections</a>
            </li>
        </ul>
</li>
<?php endif; ?>