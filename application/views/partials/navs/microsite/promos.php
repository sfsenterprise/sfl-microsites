<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'promos') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-promos"
        class="<?php echo ($this->router->fetch_class() === 'promos' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'promos') ? 'true' : 'false'; ?>"
        aria-controls="site-promos">
        <i class="fa fa-coffee"></i>Offers</a>
        <ul id="site-promos"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'promos' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'promos' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'promos' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/promos/'); ?>">All Offers</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'promos' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/promos/add'); ?>">Add Offer</a>
            </li>
        </ul>
</li>
