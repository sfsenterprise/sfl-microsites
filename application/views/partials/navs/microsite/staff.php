<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'staff') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-staff"
        class="<?php echo ($this->router->fetch_class() === 'staff' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'staff') ? 'true' : 'false'; ?>"
        aria-controls="site-staff">
        <i class="fa fa-users"></i>Staff</a>
        <ul id="site-staff"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'staff' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'staff' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'staff' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/staff/'); ?>">All Staff</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'staff' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/staff/add'); ?>">Add Staff</a>
            </li>
        </ul>
</li>
