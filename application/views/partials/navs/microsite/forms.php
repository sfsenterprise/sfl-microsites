<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'microsites' || $this->router->fetch_method() === 'leads' || $this->router->fetch_method() === 'leaddetails' || $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'settings' ) ) ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-form"
        class="<?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'microsites' || $this->router->fetch_method() === 'leads' || $this->router->fetch_method() === 'leaddetails' || $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'settings' ) ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'microsites' || $this->router->fetch_method() === 'leads' || $this->router->fetch_method() === 'leaddetails' || $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'settings' ) ) ? 'true' : 'false'; ?>"
        aria-controls="site-form">
        <i class="fa fa-wpforms"></i>Forms</a>
        <ul id="site-form"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'microsites' || $this->router->fetch_method() === 'leads' || $this->router->fetch_method() === 'leaddetails' || $this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit' || $this->router->fetch_method() === 'settings' ) ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'microsites' || $this->router->fetch_method() === 'leads' || $this->router->fetch_method() === 'micrositeform' || $this->router->fetch_method() === 'settings' ) ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'leads' || $this->router->fetch_method() === 'leaddetails' ) ) ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/form/leads'); ?>">Form Fills</a>
            </li>
            <?php if ( current_user_can('franchisor') ) : ?>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'form' && ( $this->router->fetch_method() === 'microsites' ) ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/form/microsites'); ?>">Notification Settings</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'form' && ($this->router->fetch_method() === 'add' || $this->router->fetch_method() === 'edit') ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/form/add'); ?>">Add Form Page</a>
                </li>
                <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'form' && $this->router->fetch_method() === 'settings' ) ? 'navmenu-item-active' : ''; ?>">
                    <a href="<?php echo base_url('/form/settings'); ?>">Settings</a>
                </li>
            <?php endif; ?>
        </ul>
</li>
