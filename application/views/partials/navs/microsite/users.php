<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'users') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-users"
        class="<?php echo ($this->router->fetch_class() === 'users' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'users') ? 'true' : 'false'; ?>"
        aria-controls="site-users">
        <i class="fa fa-users"></i>Users</a>
        <ul id="site-users"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'users' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'users' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'users' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/users/'); ?>">All Users</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'add' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/users/add'); ?>">Add User</a>
            </li>
        </ul>
</li>
