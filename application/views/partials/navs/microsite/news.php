<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'news') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-news"
        class="<?php echo ($this->router->fetch_class() === 'news' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'news') ? 'true' : 'false'; ?>"
        aria-controls="site-news">
        <i class="fa fa-newspaper-o"></i>News</a>
        <ul id="site-news"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'news' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'news' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'news' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/news/'); ?>">All News</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'news' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/news/add'); ?>">Add News</a>
            </li>
        </ul>
</li>
