<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'events') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-events"
        class="<?php echo ($this->router->fetch_class() === 'events' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'events') ? 'true' : 'false'; ?>"
        aria-controls="site-events">
        <i class="fa fa-calendar-o"></i>Events</a>
        <ul id="site-events"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'events' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'events' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'events' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/events/'); ?>">All Events</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'events' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/events/add'); ?>">Add Event</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'events' && ($this->router->fetch_method() === 'categories' || $this->router->fetch_method() === 'edit_category')) ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/events/categories'); ?>">Categories</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'events' && $this->router->fetch_method() === 'add_category') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/events/add_category'); ?>">Add Category</a>
            </li>
        </ul>
</li>
