<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'announcements') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-announcements"
        class="<?php echo ($this->router->fetch_class() === 'announcements' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'announcements') ? 'true' : 'false'; ?>"
        aria-controls="site-announcements">
        <i class="fa fa-thumb-tack"></i>Announcements</a>
        <ul id="site-announcements"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'announcements' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'announcements' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'announcements' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/announcements/'); ?>">All Announcements</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'announcements' && $this->router->fetch_method() === 'add') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/announcements/add'); ?>">Add Announcement</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'announcements' && $this->router->fetch_method() === 'settings') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/announcements/settings'); ?>">Settings</a>
            </li>
        </ul>
</li>
