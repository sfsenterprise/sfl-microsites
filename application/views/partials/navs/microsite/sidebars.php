<li class="navmenu-item card <?php echo ( $this->router->fetch_class() === 'sidebars' && ($this->router->fetch_method() === 'homepage' || $this->router->fetch_method() === 'innerpage') ) ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-sidebars"
        class="<?php echo ($this->router->fetch_class() === 'sidebars' && ($this->router->fetch_method() === 'homepage' || $this->router->fetch_method() === 'innerpage')) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'sidebars' && ($this->router->fetch_method() === 'homepage' || $this->router->fetch_method() === 'innerpage')) ? 'true' : 'false'; ?>"
        aria-controls="site-sidebars">
        <i class="fa fa-window-restore"></i>Sidebars</a>
        <ul id="site-sidebars"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'sidebars' && ($this->router->fetch_method() === 'homepage' || $this->router->fetch_method() === 'innerpage')) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'sidebars' && ($this->router->fetch_method() !== 'homepage' || $this->router->fetch_method() !== 'innerpage')) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'homepage' ) ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/sidebars/homepage'); ?>">Home Page</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'sidebars' && $this->router->fetch_method() === 'innerpage') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/sidebars/innerpage'); ?>">Inner Pages</a>
            </li>
        </ul>
</li>
