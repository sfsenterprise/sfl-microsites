<li class="navmenu-item card <?php echo ($this->router->fetch_class() === 'jobs') ? 'navmenu-item-active' : ''; ?>">
    <a href="#site-jobs"
        class="<?php echo ($this->router->fetch_class() === 'jobs' ) ? '' : 'collapsed'; ?>"
        data-toggle="collapse"
        data-parent="#navmenu"
        aria-expanded="<?php echo ($this->router->fetch_class() === 'jobs') ? 'true' : 'false'; ?>"
        aria-controls="site-jobs">
        <i class="fa fa-briefcase"></i>Jobs</a>
        <ul id="site-jobs"
            class="submenu collapse <?php echo ($this->router->fetch_class() === 'jobs' ) ? 'show' : ''; ?>"
            aria-expanded="<?php echo ($this->router->fetch_class() === 'jobs' ) ? 'true' : 'false'; ?>">
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'jobs' && $this->router->fetch_method() === 'index') ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/jobs/'); ?>">All Jobs</a>
            </li>
            <li class="navmenu-item <?php echo ($this->router->fetch_class() === 'jobs' && ($this->router->fetch_method() === 'add')) ? 'navmenu-item-active' : ''; ?>">
                <a href="<?php echo base_url('/jobs/add'); ?>">Add Job</a>
            </li>
        </ul>
</li>
