<div class="row">
    <div class="col">
        <form method="post" action="<?php echo base_url('/'.$this->router->fetch_class().'/'); ?>">
            <div class="card">
                <div class="card-block">
                    <div class="alert alert-warning">
                        If you want to change Hero banner for <strong>pages</strong>, that can be replaced from </strong><a href="<?php echo ($this->router->fetch_class() === 'hero' ) ? base_url('/microsites/pages/') : base_url('/pages/'); ?>">pages</a></strong>.
                    </div>
                    <h2 class="font-weight-bold">Archives</h2>
                    <div class="row">
                        <?php foreach( $posttypes as $posttype => $level ) : ?>
                            <?php if ( !in_array($posttype, ['sidebars', 'swim_lessons', 'forms', 'testimonials', 'announcements']) ) : ?>
                                <div class="col-md-4">
                                    <div class="hero-image-<?php echo $posttype; ?>-wrap">
                                        <div class="hero-image-container">
                                            <div class="hero-action-btn">
                                                <button class="btn btn-primary btn-sm media-hndl">Replace</button>
                                                <button class="btn btn-danger btn-sm media-del-hndl">Remove <i class="fa fa-trash"></i></button>
                                            </div>
                                            <h3><?php echo $posttype; ?></h3>
                                            <div class="hero-image">
                                                <?php $hero = $this->router->fetch_class() === 'hero' ? get_option('_sfmu_' . $posttype . '_hero_image_id', '') : get_blog_option($this->microsite, '_sfmu_' . $posttype . '_hero_image_id', ''); ?>
                                                <img class="hero-image" src="<?php echo wp_get_attachment_image_src($hero, 'medium')[0]; ?>" alt="">
                                            </div>
                                            <input class="hero-image-id" type="hidden" name="_sfmu_<?php echo $posttype ?>_hero_image_id" value="<?php echo $hero; ?>" />
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                    <?php if ( $this->router->fetch_class() === 'hero' ) : ?>
                        <h2 class="font-weight-bold">Location</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="hero-image-location-wrap">
                                    <div class="hero-image-container">
                                        <div class="hero-action-btn">
                                            <button class="btn btn-primary btn-sm media-hndl">Replace</button>
                                            <button class="btn btn-danger btn-sm media-del-hndl">Remove <i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3>Location</h3>
                                        <div class="hero-image">
                                            <?php $hero = get_option('_sfmu_location_hero_image_id', ''); ?>
                                            <img class="hero-image" src="<?php echo wp_get_attachment_image_src($hero, 'medium')[0]; ?>" alt="">
                                        </div>
                                        <input class="hero-image-id" type="hidden" name="_sfmu_location_hero_image_id" value="<?php echo $hero; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="hero-image-state-wrap">
                                    <div class="hero-image-container">
                                        <div class="hero-action-btn">
                                            <button class="btn btn-primary btn-sm media-hndl">Replace</button>
                                            <button class="btn btn-danger btn-sm media-del-hndl">Remove <i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3>State</h3>
                                        <div class="hero-image">
                                            <?php $hero = get_option('_sfmu_state_hero_image_id', ''); ?>
                                            <img class="hero-image" src="<?php echo wp_get_attachment_image_src($hero, 'medium')[0]; ?>" alt="">
                                        </div>
                                        <input class="hero-image-id" type="hidden" name="_sfmu_state_hero_image_id" value="<?php echo $hero; ?>" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="hero-image-city-wrap">
                                    <div class="hero-image-container">
                                        <div class="hero-action-btn">
                                            <button class="btn btn-primary btn-sm media-hndl">Replace</button>
                                            <button class="btn btn-danger btn-sm media-del-hndl">Remove <i class="fa fa-trash"></i></button>
                                        </div>
                                        <h3>City</h3>
                                        <div class="hero-image">
                                            <?php $hero = get_option('_sfmu_city_hero_image_id', ''); ?>
                                            <img class="hero-image" src="<?php echo wp_get_attachment_image_src($hero, 'medium')[0]; ?>" alt="">
                                        </div>
                                        <input class="hero-image-id" type="hidden" name="_sfmu_city_hero_image_id" value="<?php echo $hero; ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <h2 class="font-weight-bold">404 &amp; Search Page</h2>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="hero-image-404-wrap">
                                <div class="hero-image-container">
                                    <div class="hero-action-btn">
                                        <button class="btn btn-primary btn-sm media-hndl">Replace</button>
                                        <button class="btn btn-danger btn-sm media-del-hndl">Remove <i class="fa fa-trash"></i></button>
                                    </div>
                                    <h3>404 Page</h3>
                                    <div class="hero-image">
                                        <?php $hero = $this->router->fetch_class() === 'hero' ? get_option('_sfmu_404_hero_image_id', '') : get_blog_option($this->microsite, '_sfmu_404_hero_image_id', ''); ?>
                                        <img class="hero-image" src="<?php echo wp_get_attachment_image_src($hero, 'medium')[0]; ?>" alt="">
                                    </div>
                                    <input class="hero-image-id" type="hidden" name="_sfmu_404_hero_image_id" value="<?php echo $hero; ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="hero-image-search-wrap">
                                <div class="hero-image-container">
                                    <div class="hero-action-btn">
                                        <button class="btn btn-primary btn-sm media-hndl">Replace</button>
                                        <button class="btn btn-danger btn-sm media-del-hndl">Remove <i class="fa fa-trash"></i></button>
                                    </div>
                                    <h3>Search Page</h3>
                                    <div class="hero-image">
                                        <?php $hero = $this->router->fetch_class() === 'hero' ? get_option('_sfmu_search_hero_image_id', '') : get_blog_option($this->microsite, '_sfmu_search_hero_image_id', ''); ?>
                                        <img class="hero-image" src="<?php echo wp_get_attachment_image_src($hero, 'medium')[0]; ?>" alt="">
                                    </div>
                                    <input class="hero-image-id" type="hidden" name="_sfmu_search_hero_image_id" value="<?php echo $hero; ?>" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col text-right">
                            <input class="btn btn-primary" type="submit" name="update" value="Update Banners" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
