    <div class="row">
        <?php if( $all_leads ) : ?>
            <div class="col-4">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Form Name</th>
                                    <th>Lead Count</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach( $all_leads as $lead_key => $lead_val ) : ?>
                                    <?php $post = get_post($lead_key); ?>
                                    <tr>
                                        <td><a href="<?php echo base_url('form/leads/'.$lead_key); ?>"><?php echo $post->post_title; ?></a></td>
                                        <td><a href="<?php echo base_url('form/leads/'.$lead_key); ?>"><?php echo $lead_val; ?></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-block text-right pt-0">
                        <a href="<?php echo base_url('form/leads'); ?>" class="btn btn-primary">All Form Leads</a>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>