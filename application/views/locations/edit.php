<?php echo form_open('locations/edit/'.$this->uri->segment(4)); ?>
<div class="row">
    <div class="col col-9">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <fieldset>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Title</label>
                                <div class="col-10"><input class="form-control" readonly type="text" name="title" value="<?php echo isset($location) ? $location->post_title:''; ?>" /></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Content</label>
                                <div class="col-10">
                                    <?php wp_editor(isset($location) ? $location->post_content:'', 'content', array('media_buttons' => true) ); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col col-3">
        <div class="card">
            <div class="card-block">
                <?php if( $location ) : ?>
                    <div class="form-group row">
                        <div class="col-12">
                            <strong>Last Modified:</strong> <br/><?php echo  get_the_modified_date( 'F d, Y - h:i:s a', $location->ID ); ?>
                        </div>
                    </div>
                <?php endif; ?>
                    
                <div class="form-group row">
                    <div class="col-6 text-left">
                        <?php if( $location ) : ?>
                            <a class="h2 text-primary" target="_blank" href="<?php echo get_the_permalink($location->ID); ?>">View Page</a>
                        <?php endif; ?>
                    </div>
                        
                    <div class="col-6 text-right">
                        <?php echo form_submit('page_update', ($location->ID) ? 'Update':'Publish', ['class'=>'btn btn-primary']); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $curr_location = get_post($this->uri->segment(4)); ?>

        <?php if( (int)$curr_location->post_parent !== 0 ) : ?>
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-12"><strong>Page Attribute</strong></div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <strong>Parent</strong>
                            <?php echo $location_dropdown; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php echo form_close(); ?>