<div class="row">
    <div class="col-12">
        <?php echo form_open( sprintf(base_url('locations/%1s/'), $this->router->fetch_method()), ['method'=>'get']); ?>
            <fieldset>
                <div class="form-group row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6"><input type="text" name="search" class="form-control" value="<?php echo isset($_GET['search']) ? $_GET['search']:''; ?>" placeholder="Search here" /></div>
                            <div class="col-3">
                                <select name="searchby" class="form-control">
                                    <?php if($this->router->fetch_method() === 'cities') : ?>
                                        <option <?php echo (isset($_GET['searchby']) && $_GET['searchby'] === 'city') ? 'selected':''; ?> value="city">City</option>
                                    <?php endif; ?>
                                    <option <?php echo (isset($_GET['searchby']) && $_GET['searchby'] === 'state_province') ? 'selected':''; ?> value="state_province">State</option>
                                    <option <?php echo (isset($_GET['searchby']) && $_GET['searchby'] === 'zip_postal') ? 'selected':''; ?> value="zip_postal">Zip</option>
                                </select>
                            </div>
                            <div class="col-3"><input type="submit" name="" class="btn btn-secondary" value="Search"/></div>
                        </div>
                    </div>
                </div>
            </fieldset>
        <?php echo form_close(); ?>
    </div>

    <div class="col-12">
        <div class="card">
            <div class="card-block">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th><?php echo ($this->router->fetch_method() === 'cities') ? 'City' : 'State' ?></th>
                            <?php if( $this->router->fetch_method() === 'cities' ) : ?>
                                <th>State</th>
                            <?php endif; ?>
                            <th>Zip/Postal</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php if( $locations && get_class($locations[0]) === 'WP_Post' ) : ?>
                            <?php $this->load->helper('locations_helper'); ?>
                            <?php foreach($locations as $location) : ?>
                                <tr>
                                    <?php if( $this->router->fetch_method() === 'cities' ) : ?>
                                        <?php $parent = get_post($location->post_parent); ?>
                                        <?php $zip = get_location_zip('city', $location->post_title); ?>

                                        <td><a href="<?php echo base_url('locations/edit/'.$location->ID); ?>"><?php echo $location->post_title; ?></a></td>
                                        <td><?php echo $parent->post_title; ?></td>
                                        <td><?php echo $zip; ?></td>

                                    <?php else : ?>
                                        <?php $zip = get_location_zip('state', $location->post_title); ?>
                                        <td><a href="<?php echo base_url('locations/edit/'.$location->ID); ?>"><?php echo $location->post_title; ?></a></td>
                                        <td><?php echo $zip; ?></td>

                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>

                        <?php else : ?>
                            <?php if( $locations ) : ?>
                                <?php foreach( $locations as $location ) : ?>
                                    <?php if($location->microsite_state_province) : ?>
                                        <tr>
                                            <?php if($this->router->fetch_method() === 'cities') : ?>
                                                <td><a href="<?php echo base_url('locations/edit/') . get_page_by_title( $location->microsite_city, OBJECT, 'location' )->ID; ?>"><?php echo isset($location) ? $location->microsite_city : '' ; ?></a></td>
                                            <?php endif; ?>
                                            <td>
                                                <?php if($this->router->fetch_method() === 'states') : ?>
                                                    <?php $loc_page = get_page_by_title( $location->microsite_state_province, OBJECT, 'location' ); ?>
                                                    <a href="<?php echo base_url('locations/edit/') . $loc_page->ID; ?>">
                                                        <?php echo isset($location) ? $location->microsite_state_province : ''; ?>
                                                    </a>
                                                <?php else : ?>
                                                    <?php echo isset($location) ? $location->microsite_state_province : ''; ?>
                                                <?php endif; ?>
                                            </td>
                                            <td><?php echo isset($location) ? $location->microsite_zip_postal : ''; ?></td>
                                        </tr>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <td>No result found</td>
                                <td></td>
                                <td></td>
                            <?php endif; ?>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
            <?php $this->load->view('partials/pagination', $this->data); ?>
        </div>
    </div>
</div>