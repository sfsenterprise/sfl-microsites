<?php if ( $this->router->fetch_method() === 'addpage' ) : ?>
    <?php echo form_open(base_url("/pages/add/")); ?>
<?php else : ?>
    <?php echo form_open(base_url("/pages/edit/{$page->ID}/")); ?>
<?php endif; ?>
    <div class="row">
        <div class="col col-9">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Title</label>
                                    <div class="col-10">
                                        <?php
                                            $post_title = ['name' => 'page_title', 'type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter title here', 'value' => $page->post_title];
                                            if ( isset($page->post_title) ) {
                                                if ( !current_user_can('franchisor') ) {
                                                    $post_title['readonly'] = true;
                                                }
                                            }
                                        ?>
                                        <?= form_input($post_title); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Content</label>
                                    <div class="col-10">
                                        <?php wp_editor(isset($page->post_content) ? $page->post_content : '', 'content', array('media_buttons' => true) ); ?>
                                    </div>
                                </div>
                                <input type="hidden" name="post_type" value="page" />
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
            <?php $this->load->view('partials/cards/ctafooter', $this->data); ?>
            <?php $this->load->view('partials/cards/yoast', $this->data); ?>
        </div>

        <div class="col col-3">
            <div class="card">
                <div class="card-block">                    
                    <div class="form-group row">
                        <div class="col-8 text-left">
                            <?php if( isset($page->ID) && $page->ID ) : ?>
                                <div class="form-group">
                                    <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $page->ID ); ?>
                                </div>

                                <div class="form-group">
                                    <strong>Last Modified:</strong> <br/><?php echo  get_the_modified_date( 'M d, Y - h:i:s a', $page->ID ); ?>
                                </div>

                                <a class="h2 text-primary" target="_blank" href="<?php echo get_the_permalink($page->ID); ?>">View Page</a>
                            <?php endif; ?>
                        </div>
                        
                        <div class="col-4 text-right">
                            <?php echo form_submit('page_update', ($page->ID) ? 'Update':'Publish', ['class'=>'btn btn-primary']); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-12">
                            <strong>Page Attributes</strong>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12"><strong>Parent</strong></div>
                        <div class="col-12">
                            <?php wp_dropdown_pages(['selected'=>(isset($page->post_parent))?$page->post_parent:0, 'name'=>'parent_id', 'id'=>'parent_id', 'class'=>'form-control', 'show_option_none'=>'(no parent)']);  ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-12"><strong>Hero Image Override</strong></div>
                        <div class="col-12">
                            <a href="" id="set-hero-image" aria-describedby="set-hero-image-desc">Set hero image</a>
                            <img id="page-hero-img" style="display: <?php echo ($hero_image_id) ? 'block':'none'; ?>" src="<?php echo ($hero_image_id)?:''; ?>" alt="Page Hero Image" />
                            <input type="hidden" name="_hero_image_id" value="<?php echo get_post_meta($this->data['page']->ID, '_hero_image_id', true); ?>"/>
                            <div class="remove-img" style="display: <?php echo ($hero_image_id) ? 'block':'none'; ?>;">
                                <p>Click the image to edit or update</p>
                                <a href="#" id="remove-hero-image">Remove hero image</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-12"><strong>Featured Image</strong></div>
                        <div class="col-12">
                            <a href="" id="set-post-thumbnail">Set featured image</a>
                            <img id="page-featured-img" style="display: <?php echo (get_the_post_thumbnail_url($page->ID)) ? 'block':'none'; ?>" src="<?php echo get_the_post_thumbnail_url($page->ID); ?>" alt="Page Featured Image" />
                            <input type="hidden" id="_thumbnail_id" name="_thumbnail_id" value="<?php echo get_post_meta($page->ID, '_thumbnail_id', true); ?>" />
                            <div class="remove-img" style="display: <?php echo (get_the_post_thumbnail_url($page->ID)) ? 'block':'none'; ?>;">
                                <p>Click the image to edit or update</p>
                                <a href="#" id="remove-featured-image">Remove featured image</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label class="col-12 col-form-label text-left">Global Page</label>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-check">
                                <label class="form-check-label text-left">
                                    <input type="checkbox" name="_sfmu_default_page" class="form-check-input" value="yes" <?php echo ($page->ID && gss_get_post_meta(1, $page->ID, '_sfmu_default_page') === 'yes') ? 'checked' : ''; ?> />
                                    <span class="fa fa-check"></span>
                                    Set as global
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php echo form_close(); ?>
