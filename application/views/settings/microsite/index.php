<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item">
        <a class="nav-link micro-settings active" href="#locationinfo" role="tab" data-toggle="tab">Location Information</a>
    </li>
    <li class="nav-item">
        <a class="nav-link micro-settings" href="#creative-requestform" role="tab" data-toggle="tab">Creative Request Form</a>
    </li>
    <li class="nav-item">
        <a class="nav-link micro-settings" href="#homepage-settings" role="tab" data-toggle="tab">Homepage Settings</a>
    </li>
    <li class="nav-item">
        <a class="nav-link micro-settings" href="#header-image" role="tab" data-toggle="tab">Header Image</a>
    </li>
    <li class="nav-item">
        <a class="nav-link micro-settings" href="#footer-logos" role="tab" data-toggle="tab">Footer Logos</a>
    </li>
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active show" id="locationinfo">

        <?php echo form_open('settings/location_information'); ?>
            <div class="row">
                <div class="col col-9">
                        <?php if( (int)$microsite === 1 ) : ?>
                            <div class="card">
                                <div class="card-block">
                                    <fieldset>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Default search distance</label>
                                            <div class="col-10">
                                                <?php echo form_input(['name'=>'gss_default_search_distance', 'type'=>'text', 'class'=>'form-control', 'value'=>(get_blog_option($microsite, 'gss_default_search_distance')) ?: '']); ?>
                                                <em>Default search distance in miles</em>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Search distances</label>
                                            <div class="col-10">
                                                <?php echo form_input(['name'=>'gss_search_distances', 'type'=>'text', 'class'=>'form-control', 'value'=>(get_blog_option($microsite, 'gss_search_distances')) ?: '']); ?>
                                                <em>Comma separated list of distances in miles (e.g. 5, 10, 20, 25)</em>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Geo location</label>
                                            <div class="col-10">
                                                <div class="form-check">
                                                    <label class="form-check-label">    
                                                        <input class="form-check-input" type="checkbox" name="gss_geo_location" id="gss_geo_location" value="1" <?php checked( get_blog_option($microsite, 'gss_geo_location'), 1 ); ?>/> 
                                                        <span class="fa fa-check"></span>
                                                        <label for="gss_geo_location">Enable geo location</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">"Get Directions" link</label>
                                            <div class="col-10">
                                                <div class="form-check">
                                                    <label class="form-check-label">  
                                                        <input class="form-check-input" type="checkbox" name="gss_direction_link" id="gss_direction_link" value="1" <?php checked( get_blog_option($microsite, 'gss_direction_link'), 1 ); ?>/> 
                                                        <span class="fa fa-check"></span>
                                                        <label for="gss_direction_link">Don't show "Get Directions" link</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!--div class="form-group row">
                                            <label class="col-2 col-form-label">Map view</label>
                                            <div class="col-10">
                                                <div class="form-check">
                                                    <label class="form-check-label"> 
                                                        <input class="form-check-input" type="checkbox" name="gss_map_view" id="gss_map_view" value="1" <?php //checked( get_blog_option($microsite, 'gss_map_view'), 1 ); ?> /> 
                                                        <span class="fa fa-check"></span>
                                                        <label for="gss_map_view">Map view</label>
                                                    </label>
                                                </div>
                                            </div>
                                        </div-->
                                        <!--div class="form-group row">
                                            <label class="col-2 col-form-label">Search result order</label>
                                            <div class="col-10">
                                                <div class="form-check">
                                                    <label class="form-check-label text-left"> 
                                                        <input class="form-check-input" type="checkbox" name="gss_search_result_order" id="gss_search_result_order" value="1" <?php //checked( get_blog_option($microsite, 'gss_search_result_order'), 1 ); ?> /> 
                                                        <span class="fa fa-check"></span>
                                                        <label for="gss_search_result_order">Always show search results in same zip code first</label>
                                                    </label>
                                                    <div><em>Only applies to zip code searches, will place all results in the searched zip code first even if another location is considered closer.</em></div>
                                                </div>
                                            </div>
                                        </div-->
                                        <!--div class="form-group row">
                                            <label class="col-2 col-form-label" for="gss_search_result_page">Search results page</label>
                                            <div class="col-10">
                                                <?php /*$results_page = get_blog_option($microsite, 'gss_search_result_page'); ?>
                                                <select name="gss_search_result_page" id="gss_search_result_page" class="form-control">
                                                    <option value="0">Select a page</option>
                                                    <?php foreach($pages as $page) : ?>
                                                        <option value="<?php echo $page->ID; ?>" <?php echo ( isset($results_page) && (int)$results_page === $page->ID ) ? 'selected':''; ?> ><?php echo $page->post_title; ?></option>
                                                    <?php endforeach;*/ ?>
                                                </select>
                                                <div><em>This is the page where all search queries are directed, this page should contain the map and/or search results shortcodes.</em></div>
                                            </div>
                                        </div-->
                                    </fieldset>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="card">
                            <div class="card-block">
                                <fieldset>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Hide Location</label>
                                        <div class="col-10">
                                            <div class="form-check">
                                                <label class="form-check-label text-left"> 
                                                    <input class="form-check-input" type="checkbox" name="gss_hide_location" id="gss-hide-location" value="1" <?php checked( get_blog_option($microsite, 'gss_hide_location'), 1 ); ?> /> 
                                                    <span class="fa fa-check"></span>
                                                    <label for="gss-hide-location">Don't show location in search results</label>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <?php $micros_settings = get_blog_option($microsite, 's8_ff_location_settings', true); ?>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Location ID</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'s8_ff_location_settings[location_id]', 'type'=>'text', 'class'=>'form-control', 'value'=>($micros_settings['location_id']) ?: '', 'placeholder'=>'Location ID']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">POS ID</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'s8_ff_location_settings[pos_id]', 'type'=>'text', 'class'=>'form-control', 'value'=>($micros_settings['pos_id']) ?: '', 'placeholder'=>'POS ID']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Member Login URL</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'s8_ff_location_settings[member_login_url]', 'type'=>'text', 'class'=>'form-control', 'value'=>($micros_settings['member_login_url']) ?: '', 'placeholder'=>'Member Login URL']); ?></div>
                                    </div>
                                    <!--div class="form-group row">
                                        <label class="col-2 col-form-label">Admin Email</label>
                                        <div class="col-10"><?php /*echo form_input(['name'=>'admin_email', 'type'=>'text', 'class'=>'form-control', 'value'=>(get_blog_option($microsite, 'admin_email')) ?: '', 'placeholder'=>'Admin Email']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Owner Name</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'gss_owner_name', 'type'=>'text', 'class'=>'form-control', 'value'=>(get_blog_option($microsite, 'gss_owner_name')) ?: '', 'placeholder'=>'Owner Name']);*/ ?></div>
                                    </div-->
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Areas Served 1</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'s8_ff_location_settings[area_served_1]', 'type'=>'text', 'class'=>'form-control', 'value'=>($micros_settings['area_served_1']) ?: '', 'placeholder'=>'Areas Served 1']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Areas Served 2</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'s8_ff_location_settings[area_served_2]', 'type'=>'text', 'class'=>'form-control', 'value'=>($micros_settings['area_served_2']) ?: '', 'placeholder'=>'Areas Served 2']); ?></div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Footer Scripts</label>
                                        <div class="col-10"><?php echo form_textarea(['name'=>'s8_ff_location_settings[footer_scripts]', 'cols'=>'35', 'rows'=>'8', 'value'=>($micros_settings['footer_scripts']) ?: '', 'class'=>'form-control']); ?> <em>Here you can paste in any scripts that need to be put in before the closing body tag</em></div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                </div>

                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-12">
                                    <?php echo form_submit('gss_settings', 'Save Settings', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        <?php echo form_close(); ?>
    </div><!-- .tabpanel -->

    <div role="tabpanel" class="tab-pane fade" id="creative-requestform">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <?php echo form_open('settings/creative_request', ['name'=>'gss_creativeform']); ?>
                            <fieldset>
                                <div class="form-group row <?php echo form_error('gss_creativeform_name') ? 'has-danger' : '' ?>">
                                    <label class="col-2 col-form-label">Your Name *</label>
                                    <div class="col-10">
                                        <?php echo form_input(['name'=>'gss_creativeform_name', 'class'=>'form-control']); ?>
                                        <?php echo form_error('gss_creativeform_name', '<div class="form-control-feedback">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group row <?php echo form_error('gss_creativeform_email') ? 'has-danger' : '' ?>">
                                    <label class="col-2 col-form-label">Your Email *</label>
                                    <div class="col-10">
                                        <?php echo form_input(['name'=>'gss_creativeform_email', 'class'=>'form-control']); ?>
                                        <?php echo form_error('gss_creativeform_email', '<div class="form-control-feedback">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group row <?php echo form_error('gss_creativeform_subject') ? 'has-danger' : '' ?>">
                                    <label class="col-2 col-form-label">Subject *</label>
                                    <div class="col-10">
                                        <?php echo form_input(['name'=>'gss_creativeform_subject', 'class'=>'form-control']); ?>
                                        <?php echo form_error('gss_creativeform_subject', '<div class="form-control-feedback">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group row <?php echo form_error('gss_creativeform_message') ? 'has-danger' : '' ?>">
                                    <label class="col-2 col-form-label">Message *</label>
                                    <div class="col-10">
                                        <?php echo form_textarea(['name'=>'gss_creativeform_message', 'cols'=>'35', 'rows'=>'6', 'class'=>'form-control']); ?>
                                        <?php echo form_error('gss_creativeform_message', '<div class="form-control-feedback">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label"></label>
                                    <div class="col-10">
                                        <?php echo form_submit('gss_creative_form', 'Contact Us', ['class'=>'btn btn-primary']); ?>
                                        <div class="<?php echo form_error('gss_creativeform_message') ? 'has-danger' : '' ?>">
                                            <div class="form-control-feedback"><?php echo isset($email_message) ? $email_message : ''; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div> 
    </div><!-- .tabpanel -->

    <div role="tabpanel" class="tab-pane fade" id="homepage-settings">

        <?php echo form_open('settings/homepage_settings'); ?>
            <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">

                            <?php if( is_main_site( $microsite ) ) : ?>
                                <fieldset>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Front Page Image</label>
                                        <div class="col-10">
                                            <div class="select-image btn btn-secondary" id="gss-frontpage-image">Select Image</div>
                                            <?php echo form_input(['name'=>'gss_frontpage_image', 'type'=>'hidden', 'value'=>(get_blog_option($microsite, 'gss_frontpage_image')) ?: '']); ?>
                                            <img src="<?php echo wp_get_attachment_url( get_blog_option($microsite, 'gss_frontpage_image') ); ?>" alt="" style="max-width: 250px;"/>
                                            <span class="dashicons dashicons-trash remove" style="display: <?php echo (get_blog_option($microsite, 'gss_frontpage_image')) ? 'inline-block':'none'; ?>;"></span>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Front Page Image URL</label>
                                        <div class="col-10"><?php echo form_input(['name'=>'gss_frontpage_image_url', 'type'=>'text', 'class'=>'form-control', 'value'=>(get_blog_option($microsite, 'gss_frontpage_image_url')) ?: '']); ?></div>
                                    </div>
                                </fieldset>
                            <?php else : ?>

                                
                                <fieldset>
                                    <?php if( is_super_admin() ) : ?>
                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Header Image</label>
                                            <div class="col-10">
                                                <div class="select-image btn btn-secondary" id="gss-header-image" onclick="imageUpload(event)">Select Image</div>
                                                <?php echo form_input(['name'=>'gss_header_image', 'type'=>'hidden', 'value'=>(gss_get_post_meta($microsite, get_blog_option($microsite, 'page_on_front', true), '_hero_image_id')) ?: '']); ?>
                                                <img src="<?php echo $_hero_image; ?>" alt="" style="max-width: 250px;"/>
                                                <span class="dashicons dashicons-trash remove" style="display: <?php echo ($_hero_image) ? 'inline-block':'none'; ?>;"></span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Upcoming Events Image</label>
                                            <div class="col-10">
                                                <div class="select-image btn btn-secondary" id="gss-upcoming-events" onclick="imageUpload(event)">Select Image</div>
                                                <?php echo form_input(['name'=>'gss_upcoming_events', 'type'=>'hidden', 'value'=>(get_blog_option($microsite, 'gss_upcoming_events')) ?: '']); ?>
                                                <img src="<?php echo wp_get_attachment_url( get_blog_option($microsite, 'gss_upcoming_events') ); ?>" alt="" style="max-width: 250px;"/>
                                                <span class="dashicons dashicons-trash remove" style="display: <?php echo (get_blog_option($microsite, 'gss_upcoming_events')) ? 'inline-block':'none'; ?>;"></span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Swim Lesson Fees Image</label>
                                            <div class="col-10">
                                                <div class="select-image btn btn-secondary" id="gss-swimming-lesson-fees" onclick="imageUpload(event)">Select Image</div>
                                                <?php echo form_input(['name'=>'gss_swimming_lesson_fees', 'type'=>'hidden', 'value'=>(get_blog_option($microsite, 'gss_swimming_lesson_fees')) ?: '']); ?>
                                                <img src="<?php echo wp_get_attachment_url( get_blog_option($microsite, 'gss_swimming_lesson_fees') ); ?>" alt="" style="max-width: 250px;"/>
                                                <span class="dashicons dashicons-trash remove" style="display: <?php echo (get_blog_option($microsite, 'gss_swimming_lesson_fees')) ? 'inline-block':'none'; ?>;"></span>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-2 col-form-label">Swim Lesson Options Image</label>
                                            <div class="col-10">
                                                <div class="select-image btn btn-secondary" id="gss-swimming-lesson-options" onclick="imageUpload(event)">Select Image</div>
                                                <?php echo form_input(['name'=>'gss_swimming_lesson_options', 'type'=>'hidden', 'value'=>(get_blog_option($microsite, 'gss_swimming_lesson_options')) ?: '']); ?>
                                                <img src="<?php echo wp_get_attachment_url( get_blog_option($microsite, 'gss_swimming_lesson_options') ); ?>" alt="" style="max-width: 250px;"/>
                                                <span class="dashicons dashicons-trash remove" style="display: <?php echo (get_blog_option($microsite, 'gss_swimming_lesson_options')) ? 'inline-block':'none'; ?>;"></span>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Discover Content</label>
                                        <div class="col-10">
                                            <?php echo form_textarea(['name'=>'gss_discover_content', 'cols'=>'25', 'rows'=>'8', 'class'=>'form-control', 'value'=>(get_blog_option($microsite, 'gss_discover_content')) ?: '']); ?>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label" for="gss-hide-taketour">Hide Take A Tour Icon</label>
                                        <div class="col-10">
                                            <div class="class="form-check"">
                                                <?php 
                                                    $take_tour = get_blog_option($microsite, 'theme_mods_goldfish-swim-school-franchisee'); 
                                                    $take_tour_new = get_blog_option($microsite, 'gss_hide_take_tour');
                                                ?>
                                                <label class="form-check-label text-left">
                                                    <input class="form-check-input" type="checkbox" name="gss_hide_take_tour" id="gss-hide-taketour" value="yes" <?php echo ( strlen($take_tour_new) > 0 ) ? ( $take_tour_new === 'yes' ) ? '':'checked' : ($take_tour['hide_take_tour_image'] !== 'checked') ? '':'checked'; ?>/>
                                                    <span class="fa fa-check"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-2 col-form-label">Take a Tour iFrame</label>
                                        <div class="col-10">
                                            <input class="form-control" type="text" name="tour_iframe" value="<?php echo get_blog_option($microsite, 'tour_iframe'); ?>" />
                                        </div>
                                    </div>
                                </fieldset>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>

                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-12"><?php echo form_submit('gss_homepage_settings', 'Save Settings', ['class'=>'btn btn-primary']); ?></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div> 
        <?php echo form_close(); ?>
    </div><!-- .tabpanel -->

    <div role="tabpanel" class="tab-pane fade" id="header-image">
        <?php echo form_open('settings/header_image'); ?>
            <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Header Image</label>
                                    <div class="col-10">
                                        <div class="btn btn-secondary" id="header-image-btn">Upload Header Image <span class="dashicons dashicons-plus-alt"></span></div>
                                        <div class="btn btn-secondary remove-header-image">Remove <span class="dashicons dashicons-trash remove"></span></div>
                                        <input type="hidden" name="site_header_image" id="header-image-id" value="<?php echo (get_blog_option($main_site_id, 'gss_header_image_push_sites')==='1') ? (get_blog_option($main_site_id, 'site_header_image')) ?: '' : get_blog_option($microsite, 'site_header_image'); ?>" class="form-control" />
                                        <img id="site_header_image_src" src="<?php echo (get_blog_option($main_site_id, 'gss_header_image_push_sites')==='1') ? wp_get_attachment_url(get_blog_option($main_site_id, 'site_header_image')) : wp_get_attachment_url(get_blog_option($microsite, 'site_header_image')); ?>" style="max-width: 250px;">
                                    </div>
                                </div>

                                <?php if ( is_main_site($microsite) ) : ?>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label" for="gss_header_image_push_sites">Push to All Sites</label>
                                    <div class="col-10">
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" id="gss_header_image_push_sites" name="gss_header_image_push_sites" value="1" <?php checked( get_blog_option($microsite, 'gss_header_image_push_sites'), 1 ); ?> />
                                                <span class="fa fa-check"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php endif; ?>
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-12">
                                    <?php echo form_submit('gss_header_image', 'Save Settings', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        <?php echo form_close(); ?>   
    </div><!-- .tabpanel -->

    <div role="tabpanel" class="tab-pane fade" id="footer-logos">
        <?php echo form_open('settings/footer_logos'); ?>
            <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-2 col-form-label">Footer Logos</label>
                                    <div class="col-10" id="gss-footer-logos">
                                        <?php if ($footer_logos) : ?>
                                            <?php foreach( $footer_logos as $key => $value ) : ?>
                                                <?php 
                                                    switch_to_blog( $microsite );
                                                        $logo_url = wp_get_attachment_url($value['id']);
                                                    restore_current_blog();
                                                    ?>
                                                
                                                <?php if( $logo_url ) : ?>
                                                    <div class="footer-logo row">
                                                        <div class="logo col-3">
                                                            <img src="<?php echo $logo_url; ?>" style="max-width: 175px;"> <span class="dashicons dashicons-trash remove-logo"></span>
                                                        </div>
                                                        <div class="col-7"><input type="text" name="footer_logos[<?php echo $key; ?>][url]" value="<?php echo $value['url']; ?>" class="form-control" placeholder="Link here"/><em>Logo Link (URL)</em></div>
                                                        <input type="hidden" name="footer_logos[<?php echo $key; ?>][id]" value="<?php echo $value['id']; ?>">
                                                    </div>
                                                    <?php  ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-2 col-form-label"><div class="btn btn-secondary add-logo" id="add-logo">Add Logo <span class="dashicons dashicons-plus-alt"></span></div></label>
                                    <div class="col-10"></div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>

                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-12"><?php echo form_submit('gss_footer_logos', 'Save Changes', ['class'=>'btn btn-primary']); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        <?php echo form_close(); ?>  
    </div><!-- .tabpanel -->

</div>