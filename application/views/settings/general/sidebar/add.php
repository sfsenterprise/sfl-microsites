<?php echo form_open( sprintf( '/sidebars/%1$s', ($this->router->fetch_method() === 'add') ? 'add' : 'edit/'.$this->uri->segment(4) ) ); ?>
    <div class="row">
        <div class="col col-9">
            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Sidebar Name</label>
                            <div class="col-10"><?php echo form_input(['name'=>'sidebar_name', 'type'=>'text', 'class'=>'form-control', 'value'=>isset($gss_sidebar->ID) ? $gss_sidebar->post_title : '']); ?></div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Sidebar Content</label>
                            <div class="col-10">
                                <?php wp_editor( isset($gss_sidebar->ID) ? $gss_sidebar->post_content : '', 'sidebar_content', array('media_buttons' => true) ); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="col col-3">
            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-7 text-left">
                            <?php if( isset($gss_sidebar->ID) ) : ?>
                                <div class="form-group">
                                    <strong>Date Created:</strong> <br/><?php echo  get_the_date( 'M d, Y - h:i:s a', $gss_sidebar->ID ); ?>
                                </div>

                                <div class="form-group">
                                    <strong>Last Modified:</strong> <br/><?php echo get_the_modified_date( 'M d, Y - h:i:s a', $gss_sidebar->ID ); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-5 text-right">
                             <?php echo form_submit('gss_sidebar', ($this->uri->segment(4)) ? 'Update Sidebar' : 'Add Sidebar', ['class'=>'btn btn-primary']); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="form-group row">
                                <label class="col-12 text-left" for="sidebar-hide-title">Hide Title</label>
                                <div class="col-12">
                                    <div class="form-check">
                                        <label for="sidebar-hide-title" class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="sidebar_hidetitle" id="sidebar-hide-title" <?php echo isset($gss_sidebar->ID) ? (gss_get_post_meta( 1, $gss_sidebar->ID, 'sidebar_hidetitle' )) ? 'checked': '':''; ?>/>
                                            <span class="fa fa-check"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-12 text-left" for="sidebar-hide-widget">Hide Widget</label>
                                <div class="col-10">
                                    <div class="form-check">
                                        <label for="sidebar-hide-widget" class="form-check-label">
                                            <input type="checkbox" class="form-check-input" name="sidebar_hidewidget" id="sidebar-hide-widget" <?php echo isset($gss_sidebar->ID) ? (gss_get_post_meta( 1, $gss_sidebar->ID, 'sidebar_hidewidget' )) ? 'checked': '':''; ?>/>
                                            <span class="fa fa-check"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-block">
                    <div class="form-group row">
                        <label class="col-12 text-left" for="hide_inner_page_settings">Hide in Microsite Nav > Sidebars > Inner Pages</label>
                        <div class="col-10">
                            <div class="form-check">
                                <label for="hide_inner_page_settings" class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="hide_inner_page_settings" id="hide_inner_page_settings" <?php echo isset($gss_sidebar->ID) ? (gss_get_post_meta( 1, $gss_sidebar->ID, 'hide_inner_page_settings' )) ? 'checked': '':''; ?>/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo form_close(); ?>