    <?php echo form_open("sidebars/{$this->router->fetch_method()}/"); ?>
        <div class="row">
            <div class="col col-9">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-block">
                                <?php if($sidebar_included_microsite) : ?>
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sidebar Name</th>
                                                <th class="text-center"><?php echo $this->router->fetch_method() === 'homepage' ? 'Show in Homepage' : 'Show in Innerpage' ?></th>
                                            </tr>
                                        </thead>
                                        <tbody id="sortable">
                                           <?php foreach( $sidebars as $key => $widget ) : ?>
                                               <?php $sidebar = get_post($widget); ?>
                                               <?php $hide = gss_get_post_meta( 1, $widget, 'hide_inner_page_settings' ); ?>
                                                
                                                <?php if( $this->router->fetch_method() === 'innerpage' ) : ?>
                                                  <?php if( !$hide ) : ?>
                                                    <tr class="ui-state-default">
                                                       <td><i class="fa fa-arrows-v" aria-hidden="true"></i> <label for="sidebar-<?php echo $sidebar->ID; ?>"><?php echo $sidebar->post_title; ?>(<?php echo $sidebar->post_name; ?>)</label></td>
                                                       <td class="text-center">
                                                           <div class="form-check">
                                                               <label class="form-check-label">
                                                                   <input class="form-check-input" type="checkbox" name="gss_sidebar_<?php echo $this->router->fetch_method() === 'homepage' ? 'homepage' : 'frontend'; ?>[]" value="<?php echo $sidebar->ID; ?>" id="sidebar-<?php echo $sidebar->ID; ?>" <?php checked(true, in_array($sidebar->ID, $sidebars_included)); ?> />
                                                                   <span class="fa fa-check"></span>
                                                               </label>
                                                           </div>
                                                       </td>
                                                    </tr>
                                                  <?php endif; ?>
                                                <?php else :?>
                                                  <tr class="ui-state-default">
                                                     <td><i class="fa fa-arrows-v" aria-hidden="true"></i> <label for="sidebar-<?php echo $sidebar->ID; ?>"><?php echo $sidebar->post_title; ?>(<?php echo $sidebar->post_name; ?>)</label></td>
                                                     <td class="text-center">
                                                         <div class="form-check">
                                                             <label class="form-check-label">
                                                                 <input class="form-check-input" type="checkbox" name="gss_sidebar_<?php echo $this->router->fetch_method() === 'homepage' ? 'homepage' : 'frontend'; ?>[]" value="<?php echo $sidebar->ID; ?>" id="sidebar-<?php echo $sidebar->ID; ?>" <?php checked(true, in_array($sidebar->ID, $sidebars_included)); ?> />
                                                                 <span class="fa fa-check"></span>
                                                             </label>
                                                         </div>
                                                     </td>
                                                  </tr>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                <?php else : ?>
                                    <h2>Request from Franchisor to set sidebar.</h2>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col col-3">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-block">
                                <div class="form-group row">
                                    <div class="col-12 text-right"><?php echo form_submit('gss_sidebars_update', 'Save Settings', ['class'=>'btn btn-primary']); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>
