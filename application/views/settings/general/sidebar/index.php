<?php if( $this->router->fetch_class() === 'microsites' && $this->router->fetch_method() === 'sidebars' ) : ?>
    <?php echo form_open('sidebars/settings'); ?>

        <div class="row">
                <div class="col col-9">
                    <div class="card">
                        <div class="card-block">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Sidebar Name</th>
                                            <th class="text-center">Added in Microsites</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach( $all_sidebars as $sidebar ) : ?>
                                            <tr>
                                                <td><a href="<?php echo base_url('sidebars/edit/'.$sidebar->ID); ?>"><?php echo $sidebar->post_title; ?></a></td>
                                                <td class="text-center">
                                                    <div class="form-check">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="checkbox" name="gss_sidebar_included_microsite[]" value="<?php echo $sidebar->ID; ?>" <?php echo (in_array($sidebar->ID, $sidebar_included_microsite)) ? 'checked':''; ?>/>
                                                            <span class="fa fa-check"></span>
                                                            <input type="hidden" name="gss_sidebar[]" value="<?php echo $sidebar->ID; ?>" />
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>

                <div class="col col-3">
                    <div class="card">
                        <div class="card-block">
                            <div class="form-group row">
                                <div class="col-12 text-right">
                                    <?php echo form_submit('gss_sidebars_update', 'Save Settings', ['class'=>'btn btn-primary']); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
        </div>

    <?php echo form_close(); ?>

<?php endif; ?>