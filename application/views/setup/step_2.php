<h2>Step 2: Generate Default Global Pages</h2>
<p>Please choose all the global pages that a microsite will inherit from the main site.</p>
<?php if ( ! $pages ) : ?>
    <p class="text-danger">No pages found in the main site.</p>
    <p class="text-danger">Please create pages in the main site inside the WP Dashboard.</p>
<?php else : ?>
    <form method="post" action="<?php echo base_url('/setup/step_2/'); ?>">
        <table>
            <thead>
                <tr>
                    <th style="min-width: 50px;">Global</th>
                    <th>Page Title (slug)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pages as $page) : ?>
                    <?php if ( $page->post_name !== 'surefire' ) : ?>
                        <tr>
                            <td>
                                <?php $selected = in_array($page->post_name, $defaults) ? true : false; ?>
                                <?php if ( $selected ) : ?>
                                    <input type="hidden" name="defaults[]" value="<?php echo $page->ID; ?>" />
                                <?php endif; ?>
                                <div class="form-check">
                                    <label class="form-check-label">
                                      <input id="page-<?php echo $page->ID; ?>" class="form-check-input" type="checkbox" <?php checked(true, $selected); ?> name="pages[]" value="<?php echo $page->ID; ?>">
                                      <span class="fa fa-check"></span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                <label style="cursor:pointer;" for="page-<?php echo $page->ID; ?>"><?php echo ($page->post_parent > 0) ? '&mdash; ' : null;  ?><?php echo $page->post_title; ?> (<?php echo $page->post_name; ?>)</label>
                            </td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="text-right">
            <input class="btn btn-primary" type="submit" name="next_3" value="Next Step">
        </div>
    </form>
<?php endif; ?>
