<p>Hi, surefire-admin!</p>
<p>Please set a new password for the USERNAME: <strong>surefire-admin</strong>.</p>
<form method="post" action="<?php echo base_url("/setup/");  ?>">
    <div id="log-form-group" class="form-group row <?php echo form_error('password') ? 'has-danger' : null; ?>">
      <label class="col-2 text-right" for="password">New Password</label>
      <div class="col-4">
          <input type="password" value="<?php echo set_value('password'); ?>" name="password" id="password" class="form-control form-control-danger" placeholder="Enter New Password">
          <?php echo form_error('password', '<div class="form-control-feedback">', '</div>'); ?>
      </div>
    </div>
    <div id="log-form-group" class="form-group row <?php echo form_error('cpassword') ? 'has-danger' : null; ?>">
      <label class="col-2 text-right" for="cpassword">Confirm Password</label>
      <div class="col-4">
          <input type="password" value="<?php echo set_value('cpassword'); ?>" name="cpassword" id="cpassword" class="form-control form-control-danger" placeholder="Confirm Password">
          <?php echo form_error('cpassword', '<div class="form-control-feedback">', '</div>'); ?>
      </div>
    </div>
    <div class="text-right">
        <button type="submit" name="submit" class="btn btn-primary">Next Step</button>
    </div>
</form>
