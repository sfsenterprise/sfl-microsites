<h2>Step 3: Generate Default Global Form Pages</h2>
<p>Please choose all the global form pages that a microsite will inherit from the main site.</p>
<p><strong>THESE ARE NOT THE FORMS. THESE ARE JUST THE PAGE WHERE THE FORMS WILL SHOW.</strong></p>
<p class="text-danger"><strong>Note:</strong> Selecting a draft or trashed form page will automatically turn it into a published form page.</p>
<?php if ( ! $pages ) : ?>
    <p class="text-danger">No form pages found in the main site.</p>
    <p class="text-danger">Please create form pages in the main site inside the WP Dashboard.</p>
<?php else : ?>
    <form method="post" action="<?php echo base_url('/setup/step_3/'); ?>">
        <table>
            <thead>
                <tr>
                    <th style="min-width: 50px;">Global</th>
                    <th>Form Page Title (slug)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($pages as $page) : ?>
                    <tr>
                        <td>
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input id="page-<?php echo $page->ID; ?>" class="form-check-input" type="checkbox" <?php checked(true, true); ?> name="pages[]" value="<?php echo $page->ID; ?>">
                                  <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </td>
                        <td>
                            <label style="cursor:pointer;" for="page-<?php echo $page->ID; ?>"><?php echo $page->post_title; ?> (<?php echo $page->post_name; ?>)<?php echo ($page->post_status !== 'publish') ? '<strong> &mdash; ' . $page->post_status . '</strong>': null;  ?></label>
                            <input type="hidden" name="post_status[]" value="<?php echo $page->post_status; ?>" />
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="text-right">
            <input class="btn btn-primary" type="submit" name="next_4" value="Next Step">
        </div>
    </form>
<?php endif; ?>
