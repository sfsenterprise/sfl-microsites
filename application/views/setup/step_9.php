<h2>Step 9: Update Microsite Form Settings.</h2>
<p>You are about to update microsite form settings.</p>
<form method="post" action="<?php echo base_url('/setup/step_9/'); ?>">
<div class="text-right">
    <input class="btn btn-primary" type="submit" name="next_10" value="Update">
</div>
</form>
