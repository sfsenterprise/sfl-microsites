<h2>Step 6: Convert main blog to original post format.</h2>
<p>You are about to convert OLD GSS blogs to the standard WP post (blog).</p>
<form method="post" action="<?php echo base_url('/setup/step_6/'); ?>">
<div class="text-right">
    <input class="btn btn-primary" type="submit" name="next_7" value="Update Blog">
</div>
</form>
