<h2>Step 10: Update Microsite Global Pages.</h2>
<p>You are about to update microsite global pages. This may take a while.</p>
<form method="post" action="<?php echo base_url('/setup/step_10/'); ?>">
<div class="text-right">
    <input class="btn btn-primary" type="submit" name="next_11" value="Update">
</div>
</form>
