<h2>Step 7: Generate sidebars.</h2>
<p>You are about to generate the sidebars for the main site and the micro sites.</p>
<form method="post" action="<?php echo base_url('/setup/step_7/'); ?>">
<div class="text-right">
    <input class="btn btn-primary" type="submit" name="next_8" value="Generate Sidebars">
</div>
</form>
