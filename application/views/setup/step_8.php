<h2>Step 8: Convert Testimonials.</h2>
<p>You are about to convert testimonial images to text so it can be used on SEO.</p>
<form method="post" action="<?php echo base_url('/setup/step_8/'); ?>">
<div class="text-right">
    <input class="btn btn-primary" type="submit" name="next_9" value="Convert Testimonials">
</div>
</form>
