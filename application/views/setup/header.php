<?php use Surefire\Microsites\Lib\Assets; ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>
<body class="sfmu login setup step-1">
<div class="wrap">
  <main ng-app="microsites">
      <div class="container">
          <div class="row">
              <div class="col">
                  <div class="card" ng-controller="login">
                      <div class="brand"><img src="<?php echo Assets\asset_path('images/logo.png'); ?>" alt="Goldfish Swim School Franchising Dashboard"></div>
                      <h1 style="margin-top: 30px;">Goldfish Swim School Franchising Setup</h1>
                      <div class="card-block">
