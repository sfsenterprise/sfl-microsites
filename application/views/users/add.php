<div class="row microsite-users">
    <div class="col-md-6 offset-md-3">
        <?php $this->load->view('partials/notification'); ?>
        <?php if ( $this->router->fetch_method() === 'add') : ?>
            <form method="post" action="<?php echo base_url("/users/add/"); ?>" autocomplete="off">
        <?php else : ?>
            <form method="post" action="<?php echo base_url("/users/edit/{$edit_user->ID}/"); ?>" autocomplete="off">
        <?php endif; ?>
            <div class="card">
                <div class="card-block">
                    <fieldset>
                        <div class="form-group row <?php echo form_error('user[username]') ? 'has-danger' : ''; ?>">
                            <label for="username" class="col-4 col-form-label">Username</label>
                            <div class="col-8">
                                <input id="username" class="form-control form-control-danger" <?php echo ( $this->router->fetch_method() === 'edit' ) ? 'readonly' : ''; ?> type="text" name="user[username]" value="<?php echo set_value('user[username]', isset($edit_user) ? $edit_user->user_login : null); ?>" />
                                <?php echo form_error('user[username]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row <?php echo form_error('user[email]') ? 'has-danger' : ''; ?>">
                            <label for="email" class="col-4 col-form-label">Email</label>
                            <div class="col-8">
                                <input id="email" class="form-control form-control-danger" type="text" name="user[email]" value="<?php echo set_value('user[email]', isset($edit_user) ? $edit_user->user_email : null); ?>" />
                                <?php echo form_error('user[email]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="role" class="col-4 col-form-label">Role</label>
                            <div class="col-8">
                                <select id="role" name="user[role]" class="form-control form-control-warning">
                                    <?php if ( array_key_exists('franchisee', $select) ) : ?>
                                        <option value="franchisee" <?php echo set_select('user[role]', 'franchisee', $select['franchisee']); ?>>Franchisee</option>
                                    <?php endif; ?>
                                    <?php if ( array_key_exists('administrator', $select)) : ?>
                                        <option value="administrator" <?php echo set_select('user[role]', 'administrator', $select['administrator']); ?>>Administrator</option>
                                    <?php endif; ?>
                                    <?php if ( array_key_exists('editor', $select)) : ?>
                                        <option value="editor" <?php echo set_select('user[role]', 'editor', $select['editor']); ?>>Editor</option>
                                    <?php endif; ?>
                                    <?php if ( array_key_exists('author', $select)) : ?>
                                        <option value="author" <?php echo set_select('user[role]', 'author', $select['author']); ?>>Author</option>
                                    <?php endif; ?>
                                    <?php if ( array_key_exists('contributor', $select)) : ?>
                                        <option value="contributor" <?php echo set_select('user[role]', 'contributor', $select['contributor']); ?>>Contributor</option>
                                    <?php endif; ?>
                                    <?php if ( array_key_exists('subscriber', $select)) : ?>
                                        <option value="subscriber" <?php echo set_select('user[role]', 'subscriber', $select['subscriber']); ?>>Subscriber</option>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row <?php echo form_error('user[password]') ? 'has-danger' : ''; ?>">
                            <label for="password" class="col-4 col-form-label">Password</label>
                            <div class="col-8">
                                <input id="password" class="form-control form-control-danger" type="password" name="user[password]" value="<?php echo set_value('user[password]'); ?>" />
                                <?php echo form_error('user[password]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group row <?php echo form_error('user[cpassword]') ? 'has-danger' : ''; ?>">
                            <label for="cpassword" class="col-4 col-form-label">Confirm Password</label>
                            <div class="col-8">
                                <input id="cpassword" class="form-control form-control-danger" type="password" name="user[cpassword]" value="<?php echo set_value('user[cpassword]'); ?>" />
                                <?php echo form_error('user[cpassword]', '<div class="form-control-feedback">', '</div>'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="form-check">
                                <label class="form-check-label">
                                  <input class="form-check-input" type="checkbox" name="sendmail" value="1" <?php echo set_checkbox('sendmail', 1); ?>>
                                  <span class="fa fa-check"></span>
                                   Send an email of account details to the new user.
                                </label>
                            </div>
                        </div>
                        <div class="col text-right">
                            <input class="btn btn-primary" type="submit" value="<?php echo $this->router->fetch_method() === 'add' ? 'Add User' : 'Update User' ; ?>" />
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
