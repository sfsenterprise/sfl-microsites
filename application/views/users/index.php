        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-block">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>User Registered</th>
                                </tr>
                            </thead>

                            <?php if($users) : ?>
                                <tbody>
                                    <?php foreach( $users as $user ) : ?>
                                        <tr>
                                            <td><a href="<?php echo base_url('/users/edit/'.$user->ID); ?>"><?php echo $user->user_login; ?></a></td>
                                            <td><?php echo $user->display_name; ?></td>
                                            <td><?php echo $user->user_email; ?></td>
                                            <td><?php echo (! empty($user->roles) ) ? ucwords($user->roles[ count($user->roles) - 1 ]):''; ?></td>
                                            <td><?php echo $user->user_registered; ?></td>
                                        </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                <?php else : ?>
                                    <tbody>
                                        <tr>
                                            <td><h2>No existing events.</h2></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                <?php endif; ?>
                         </table>
                         
                    </div>
                    <div class="card-footer">
                        <?php //$this->load->view('partials/pagination', $this->data); ?>
                    </div>
                </div>
            </div>
        </div>
