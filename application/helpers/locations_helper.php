<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
* Get zip by title of either city or state
* @param string, string - city/state, title
*/
function get_location_zip($address, $title){

	$ci =& get_instance(); //get main CodeIgniter object
    $ci->load->database();

    $ci->db->select('microsite_zip_postal');
	$ci->db->from('sfl_microsites');

	if($address === 'city')
		$ci->db->where('microsite_city = ', $title);

	if($address === 'state')
		$ci->db->where('microsite_state_province = ', $title);

	$ci->db->limit(1);
	$zip = $ci->db->get();

	if( $zip->num_rows() > 0 ){
		$result = $zip->result();
		return $result[0]->microsite_zip_postal;
	}
	else{
		return false;
	}
}