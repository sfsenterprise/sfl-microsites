<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('memory_limit', '5120M');
set_time_limit ( 0 );

function remove_comments(&$output)
{
   $lines = explode("\n", $output);
   $output = "";

   // try to keep mem. use down
   $linecount = count($lines);

   $in_comment = false;
   for($i = 0; $i < $linecount; $i++)
   {
      if( preg_match("/^\/\*/", preg_quote($lines[$i])) )
      {
         $in_comment = true;
      }

      if( !$in_comment )
      {
         $output .= $lines[$i] . "\n";
      }

      if( preg_match("/\*\/$/", preg_quote($lines[$i])) )
      {
         $in_comment = false;
      }
   }

   unset($lines);
   return $output;
}

//
// remove_remarks will strip the sql comment lines out of an uploaded sql file
//
function remove_remarks($sql)
{
   $lines = explode("\n", $sql);

   // try to keep mem. use down
   $sql = "";

   $linecount = count($lines);
   $output = "";

   for ($i = 0; $i < $linecount; $i++)
   {
      if (($i != ($linecount - 1)) || (strlen($lines[$i]) > 0))
      {
         if (isset($lines[$i][0]) && $lines[$i][0] != "#")
         {
            $output .= $lines[$i] . "\n";
         }
         else
         {
            $output .= "\n";
         }
         // Trading a bit of speed for lower mem. use here.
         $lines[$i] = "";
      }
   }

   return $output;

}

//
// split_sql_file will split an uploaded sql file into single sql statements.
// Note: expects trim() to have already been run on $sql.
//
function split_sql_file($sql, $delimiter)
{
   // Split up our string into "possible" SQL statements.
   $tokens = explode($delimiter, $sql);

   // try to save mem.
   $sql = "";
   $output = array();

   // we don't actually care about the matches preg gives us.
   $matches = array();

   // this is faster than calling count($oktens) every time thru the loop.
   $token_count = count($tokens);
   for ($i = 0; $i < $token_count; $i++)
   {
      // Don't wanna add an empty string as the last thing in the array.
      if (($i != ($token_count - 1)) || (strlen($tokens[$i] > 0)))
      {
         // This is the total number of single quotes in the token.
         $total_quotes = preg_match_all("/'/", $tokens[$i], $matches);
         // Counts single quotes that are preceded by an odd number of backslashes,
         // which means they're escaped quotes.
         $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$i], $matches);

         $unescaped_quotes = $total_quotes - $escaped_quotes;

         // If the number of unescaped quotes is even, then the delimiter did NOT occur inside a string literal.
         if (($unescaped_quotes % 2) == 0)
         {
            // It's a complete sql statement.
            $output[] = $tokens[$i];
            // save memory.
            $tokens[$i] = "";
         }
         else
         {
            // incomplete sql statement. keep adding tokens until we have a complete one.
            // $temp will hold what we have so far.
            $temp = $tokens[$i] . $delimiter;
            // save memory..
            $tokens[$i] = "";

            // Do we have a complete statement yet?
            $complete_stmt = false;

            for ($j = $i + 1; (!$complete_stmt && ($j < $token_count)); $j++)
            {
               // This is the total number of single quotes in the token.
               $total_quotes = preg_match_all("/'/", $tokens[$j], $matches);
               // Counts single quotes that are preceded by an odd number of backslashes,
               // which means they're escaped quotes.
               $escaped_quotes = preg_match_all("/(?<!\\\\)(\\\\\\\\)*\\\\'/", $tokens[$j], $matches);

               $unescaped_quotes = $total_quotes - $escaped_quotes;

               if (($unescaped_quotes % 2) == 1)
               {
                  // odd number of unescaped quotes. In combination with the previous incomplete
                  // statement(s), we now have a complete statement. (2 odds always make an even)
                  $output[] = $temp . $tokens[$j];

                  // save memory.
                  $tokens[$j] = "";
                  $temp = "";

                  // exit the loop.
                  $complete_stmt = true;
                  // make sure the outer loop continues at the right point.
                  $i = $j;
               }
               else
               {
                  // even number of unescaped quotes. We still don't have a complete statement.
                  // (1 odd and 1 even always make an odd)
                  $temp .= $tokens[$j] . $delimiter;
                  // save memory.
                  $tokens[$j] = "";
               }

            } // for..
         } // else
      }
   }

   return $output;
}

function sf_parseCSV($filename = '', $header = null, $delimiter = ',') {
    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $data = array(); $fields = array(); $i = 0; $prereg = false;

    if (($handle = fopen($filename, 'r')) !== false) {
        if ( $header ) {
            $row = fgetcsv($handle, 0, $delimiter);
            $fields = array();

            if ( strpos($filename, 'pre-registration-form-full-') !== FALSE ) {
                $prereg = true;
            }

            if ( ! $row ) return $data;

            foreach ( $row as $fieldkey => $fieldname ) {
                $label = str_replace('"', '', $fieldname);
                if ( in_array($label, $fields) ) {
                    if ( $prereg ) {
                        if ( $label === 'Gender' ) {
                            if ( ! in_array("Second Child's Gender", $fields) ) {
                                $fields[$fieldkey] = "Second Child's " . $label;
                            } else if ( ! in_array("Third Child's Gender", $fields) ) {
                                $fields[$fieldkey] = "Third Child's " . $label;
                            } else if ( ! in_array("Forth Child's Gender", $fields) ) {
                                $fields[$fieldkey] = "Forth Child's " . $label;
                            } else if ( ! in_array("Fifth Child's Gender", $fields) ) {
                                $fields[$fieldkey] = "Fifth Child's " . $label;
                            }
                        } else if ( $label === 'T-Shirt Size' ) {
                            if ( ! in_array("Second Child's T-Shirt Size", $fields) ) {
                                $fields[$fieldkey] = "Second Child's " . $label;
                            } else if ( ! in_array("Third Child's T-Shirt Size", $fields) ) {
                                $fields[$fieldkey] = "Third Child's " . $label;
                            } else if ( ! in_array("Forth Child's T-Shirt Size", $fields) ) {
                                $fields[$fieldkey] = "Forth Child's " . $label;
                            } else if ( ! in_array("Fifth Child's T-Shirt Size", $fields) ) {
                                $fields[$fieldkey] = "Fifth Child's " . $label;
                            }
                        } else if ( $label === "Child's Swimming Experience Level" ) {
                            if ( ! in_array("Second Child's Child's Swimming Experience Level", $fields) ) {
                                $fields[$fieldkey] = "Second Child's " . $label;
                            } else if ( ! in_array("Third Child's Child's Swimming Experience Level", $fields) ) {
                                $fields[$fieldkey] = "Third Child's " . $label;
                            } else if ( ! in_array("Forth Child's Child's Swimming Experience Level", $fields) ) {
                                $fields[$fieldkey] = "Forth Child's " . $label;
                            } else if ( ! in_array("Fifth Child's Child's Swimming Experience Level", $fields) ) {
                                $fields[$fieldkey] = "Fifth Child's " . $label;
                            } else {
                                $fields[$fieldkey] = "Extra " . $label;
                            }
                        }
                    } else {
                        // for other forms
                    }
                } else {
                    if ( $prereg && $label === 'Yes' ) {
                        if ( ! in_array("Second Child Yes", $fields) ) {
                            $fields[$fieldkey] = "Second Child " . $label;
                        } else if ( ! in_array("Third Child Yes", $fields) ) {
                            $fields[$fieldkey] = "Third Child " . $label;
                        } else if ( ! in_array("Forth Child Yes", $fields) ) {
                            $fields[$fieldkey] = "Forth Child " . $label;
                        } else if ( ! in_array("Fifth Child Yes", $fields) ) {
                            $fields[$fieldkey] = "Fifth Child " . $label;
                        }
                    } else {
                        $fields[$fieldkey] = $label;
                    }
                }
            }
        }

        while (($row = fgetcsv($handle, 0, $delimiter)) !== false) {
            if ($header === null) {
                $data[] = $row;
            } else {
                foreach($row as $key => $value) {
                    $data[$i][$fields[$key]] = $value;
                }
            }
            $i++;
        }
        fclose($handle);
    }

    return $data;
}

function sf_getcsv_otherforms($filename = '', $delimiter = ',') {
    if (!file_exists($filename) || !is_readable($filename))
        return false;

    $data = array(); $fields = array(); $i = 0;
    $fields = array();

    if (($handle = fopen($filename, 'r')) !== false) {
        $header = fgetcsv($handle, 0, $delimiter);

        foreach( $header as $label ) {
            $label = str_replace('"', '', $label);
            $fields[] = str_replace(':', '', $label);
        }

        while (($row = fgetcsv($handle, 0, $delimiter)) !== false) {
            foreach($row as $key => $value) {
                $data[$i][$fields[$key]] = $value;
            }
            $i++;
        }
        fclose($handle);
    }

    return $data;
}
