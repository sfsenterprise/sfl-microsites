<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('sf_clear_cloud_cache') ) :
    function sf_clear_cloud_cache()
    {
        if ( ENVIRONMENT !== 'development' ) {
            $domain = str_replace(array('http://', 'https://'), array('', ''), rtrim(home_url(), '/'));
            $servers = array(
                'nginx.c1.e.node.sfs.io',
                'nginx.c1.w.node.sfs.io',
                'nginx.c1.tokyo.node.sfs.io',
            );

            foreach($servers as $server) {
                $response = wp_remote_get('http://' . $server . '/cache_clear?domain=' . $domain, [
                    'headers' => array(
                        'Authorization' => 'Basic ' . base64_encode('spapidev:rE3q8uAeIadcMGtVMknizqdO3uVL5Pcz'),
                    ),
                ]);

                if ( is_wp_error( $response ) ) {
                    $message = 'Failed to clear the cache.';
                    if ( isset(array_shift($response->errors)[0]) ) {
                        $message = array_shift($response->errors)[0];
                    }
                    error_log($message, 0);
    				return;
    			}

                $data = wp_remote_retrieve_body( $response );

    			if ( is_wp_error( $data ) ) {
    				return;
    			}

                return $data;
            }

        }
    }
endif;
