<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists( 'sf_exports_data' ) ) :
    function sf_exports_data($data, $headings = array(), $filename) {
        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"{$filename}".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

        $handle = fopen('php://output', 'w');

        if ( $headings ) {
            fputcsv($handle, $headings);
        }

        foreach ($data as $data) {
            fputcsv($handle, $data);
        }
            fclose($handle);
        exit;
    }
endif;
