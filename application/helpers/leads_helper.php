<?php

defined('BASEPATH') OR exit('No direct script access allowed');

function get_form_meta($form_id=null, $microsite=null){

    $ci =& get_instance(); //get main CodeIgniter object
    $ci->load->database();

    global $wpdb;
    
    switch_to_blog($microsite);
        if( $ci->db->table_exists("{$wpdb->prefix}rg_form") && $ci->db->table_exists("{$wpdb->prefix}rg_form_meta") ){
            $ci->db->select('*');
            $ci->db->from("{$wpdb->prefix}rg_form as f");
            $ci->db->join("{$wpdb->prefix}rg_form_meta as m", 'f.id = m.form_id');
            $ci->db->where('f.id = ', $form_id);
            $details = $ci->db->get();
        }
    restore_current_blog();

    if( isset($details) && $details->num_rows() > 0 ){
        return $details->result();
    }
    else{
        switch_to_blog(1);

            $cf7_form = get_post($ci->uri->segment(4));
            $rg_form = $ci->db->get_where("{$wpdb->prefix}rg_form", array('title' => $cf7_form->post_title));
            $rg_form = $rg_form->result();

            if( $ci->db->table_exists("{$wpdb->prefix}rg_form") ){
                $ci->db->select('*');
                $ci->db->from("{$wpdb->prefix}rg_form as f");
                $ci->db->join("{$wpdb->prefix}rg_form_meta as m", 'f.id = m.form_id');
                $ci->db->where('f.id = ', $rg_form[0]->id);
                $details_main = $ci->db->get();
            }
        restore_current_blog();

        if( isset($details_main) && $details_main->num_rows() > 0 ){
            return $details_main->result();
        }
        else{
            return false;
        }
        
    }
}

function get_lead_details($lead_id=null, $microsite=null){

    $ci =& get_instance(); //get main CodeIgniter object
    $ci->load->database();

    global $wpdb;

    switch_to_blog($microsite);

    if( $ci->db->table_exists("{$wpdb->prefix}rg_lead_detail") ){
        $ci->db->select('lead_id, field_number, value');
        $ci->db->from("{$wpdb->prefix}rg_lead_detail");
        $ci->db->where('lead_id = ', $lead_id);
        $details = $ci->db->get();
    }
        
    if( $ci->db->table_exists("{$wpdb->prefix}sfl_lead_detail") ){
        $ci->db->select('lead_id, field_name, value');
        $ci->db->from("{$wpdb->prefix}sfl_lead_detail");
        $ci->db->where('lead_id = ', $lead_id);
        $sfl_details = $ci->db->get();
    }
        
    restore_current_blog();

    if( isset($details) && $details->num_rows() > 0 ){
        return $details->result();
    }
    elseif( isset($sfl_details) && $sfl_details->num_rows() > 0 ){
        return $sfl_details->result();
    }
    else{
        return false;
    }

}

function get_long_detail($lead_detail_id, $microsite){

    $ci =& get_instance(); //get main CodeIgniter object
    $ci->load->database();

    global $wpdb;

    switch_to_blog($microsite);

        if( $ci->db->table_exists("{$wpdb->prefix}rg_lead_detail_long") ){
            $ci->db->select('value');
            $ci->db->from("{$wpdb->prefix}rg_lead_detail_long");
            $ci->db->where('lead_detail_id = ', $lead_detail_id);
            $long_detail = $ci->db->get();
        }
        
    restore_current_blog();

    if( isset($long_detail) && $long_detail->num_rows() > 0 ){
        return $long_detail->result();
    }
    else{
        return false;
    }
}

function get_fieldname_col($fields, $field_name, $field_number=false){
    foreach ($fields as $key => $value) {
        foreach ($value as $field_key => $field_val) {
            if( ! in_array(trim($field_name), $field_val) )
                continue;

            if($field_number){
                return $key;
            }
            else{
                return $field_key;
            }
            
        }
    }

    return false;
}

function get_lead_date_created($lead_id, $microsite){

    $ci =& get_instance(); //get main CodeIgniter object
    $ci->load->database();

    global $wpdb;

    switch_to_blog($microsite);

        if( $ci->db->table_exists("{$wpdb->prefix}rg_lead") ){
            $ci->db->select('date_created');
            $ci->db->from("{$wpdb->prefix}rg_lead");
            $ci->db->where('id = ', $lead_id);
            $date = $ci->db->get();
        }
        
    restore_current_blog();

    if( $date->num_rows() > 0 ){
        $date_created = $date->result();
        return $date_created[0]->date_created;
    }
    else{
        return false;
    }
}

function get_microsite_by_email($email){

    if( !$email || !is_email($email) ) return false;

    $ci =& get_instance(); //get main CodeIgniter object
    $ci->load->database();

    $sites = get_sites(['site__not_in'=>array('1')]);

    foreach ($sites as $site) {

        if( get_blog_option($site->blog_id, 'admin_email') !== $email ){

            $ci->db->select('microsite_fields');
            $ci->db->from("sfl_microsites");
            $ci->db->where('microsite_blog_id = ', $site->blog_id);
            $micro_fields = $ci->db->get();

            $microsite_fields = $micro_fields->result();
            $microsite_fields = maybe_unserialize( $microsite_fields[0]->microsite_fields );

            if( isset($microsite_fields['admin_email_alias']) && $microsite_fields['admin_email_alias'] == $email ){
                return $site->blogname;
            }
            else{
                continue;
            }

        }
        else{
            return $site->blogname;
        }

    }

}