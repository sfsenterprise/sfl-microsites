<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends SFL_Controller {

    function __construct(){
        parent::__construct();
         $this->load->model('Microsite_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All FAQs';

        $this->load->library('pagination');

        $this->data['orderby'] = (isset($_GET['orderby'])) ? $_GET['orderby']:'';
        $this->data['order'] = (isset($_GET['order']) && $_GET['order'] === 'desc') ? 'asc':'desc';

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => ( isset($_GET['pagenum']) ) ? ( ($_GET['pagenum'] - 1) * 10) : 0
        );

        if( isset($_GET['search']) && $_GET['search'] !== '' ){
            $offset = isset($_GET['pagenum']) ? $_GET['pagenum']:1;
            $search = $this->Microsite_Model->search( $_GET['search'], $offset );
            $this->data['all_faqs'] = $search;
        }
        else{
            $this->data['all_faqs'] = gss_get_posts( 1, ['post_type'=>'ff_faqs', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order']]);
        }

        $config['total_rows'] = (isset($_GET['search']) && $_GET['search']) ? count($search):count( gss_get_posts( 1, ['post_type'=>'ff_faqs', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 4;
        $config['base_url'] = base_url("/faqs/index/{$filter}");
        $config['reuse_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'pagenum';
        $this->pagination->initialize($config);

        $this->load->template('microsite/affiliates/faqs', $this->data);
    }

    public function add()
    {
        $this->data['title'] = 'Add FAQ';

        if( ! empty($this->input->post(NULL, true)) ){
            $gss_faqs = $this->Microsite_Model->gss_faqs($this->input->post(NULL), 1, $update = false);

            if( $gss_faqs ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_faqs]);
                redirect( base_url('faqs/edit/'.$gss_faqs) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }

        $this->load->template('microsite/affiliates/faqs', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit FAQ';

        $old_values = gss_get_post( 1, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL, true)) && $this->uri->segment(4) ){
            $gss_faq = $this->Microsite_Model->gss_faqs($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $gss_faq ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_faq]);
                redirect( base_url('faqs/edit/'.$gss_faq) );
            } else {
                $this->data['erroe_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {
            $this->data['gss_faq'] = gss_get_post( 1, $this->uri->segment(4) );
            $this->load->template('microsite/affiliates/faqs', $this->data);
        }

    }

    public function orderby()
    {
        $this->index();
    }

}
