<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedules extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Schedules_Model');
        $this->data['title'] = 'Hours';
        $this->data['view_frontend'] = home_url( gss_get_blog_details($this->microsite, 'path') );
    }

    public function index()
    {
        if ( $this->input->post('gss_hours') ) {

            $old_values = get_blog_option($this->microsite, 'microsite_hours_operations');

            $hour_operations = $this->Schedules_Model->save_schedules($this->microsite);

            $this->notification_email();

            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>'Update', 'old_values'=>$old_values]);

        }

        $this->data['days'] = array(
            'mon' => 'monday',
            'tue' => 'tuesday',
            'wed' => 'wednesday',
            'thu' => 'thursday',
            'fri' => 'friday',
            'sat' => 'saturday',
            'sun' => 'sunday'
        );

        $this->data['schedules'] = $this->Schedules_Model->get_schedules();

        $this->load->template('microsite/affiliates/schedules', $this->data);

    }

    public function notification_email()
    {
        $email = get_blog_option( 1, '_schedule_notif_email');
        $notif_emails =  ($email) ? explode(',', $email) : array('ginger@webpunch12.com');
        $site_name = gss_get_blog_details($this->microsite, 'blogname');

        foreach ($notif_emails as $notif_email) {
            $to = $notif_email;
            $subject = $site_name . ' Hours Changed';
            $body  = "Hi,<br><br>This is an automated notice that {$site_name} has updated the hours information on their website.<br/><br/>Thank You!";
            $headers[] = "MIME-Version: 1.0\n";
            $headers[] = 'Content-Type: text/html; charset=UTF-8';
            $headers[] = 'From: WordPress <no-reply@goldfishswimschool.com>';

            wp_mail( $to, $subject, $body, $headers );
        }

        return;
    }
}
