<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebars extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Settings_Model');
    }

    public function index()
    {
        global $current_site;

        $this->data['title'] = 'Sidebar Management';

        // get all added sidebars
        $this->data['all_sidebars'] = gss_get_posts( $this->microsite, ['post_type'=>'gss_sidebars', 'post_status'=>array('publish', 'draft')]);

        //get all included sidebars
        $this->data['sidebar_included_microsite'] = get_option('gss_sidebar_included_microsite', array());

        $this->data['main_site'] = $current_site;

    }

    public function add()
    {
        $this->data['title'] = 'Add New Sidebar';
        if( isset($_POST['gss_sidebar']) && ! empty($this->input->post(NULL)) ){
            $add_sidebar = $this->Settings_Model->add_sidebar($this->input->post(NULL), 1, $update = false);

            if( $add_sidebar ){
                wp_redirect( base_url(sprintf('sidebars/edit/%1$s', $add_sidebar)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                $this->data;
            }

        }

        $this->load->template('settings/general/sidebar/add', $this->data);

    }

    public function edit()
    {
        $this->data['title'] = 'Edit Sidebar';

        $old_values =  gss_get_post(1, $this->uri->segment(4));

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){
            $add_sidebar = $this->Settings_Model->add_sidebar($this->input->post(NULL), 1, $this->uri->segment(4));

            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$add_sidebar]);
        }

        $this->data['gss_sidebar'] = gss_get_post( 1, $this->uri->segment(4) );
        $this->load->template('settings/general/sidebar/add', $this->data);
    }

    public function homepage()
    {

        if ( $this->input->post('gss_sidebars_update') ) {
            $posts = $this->input->post('gss_sidebar_homepage');

            $old_values = get_blog_option( $this->microsite, 'gss_sidebar_homepage' );

            update_blog_option( $this->microsite, 'gss_sidebar_homepage', $posts);

            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>sprintf('Update (%1s)', ucwords($this->router->fetch_method())), 'old_values'=>(!empty($old_values)?$old_values:array()), 'new_values'=>$posts ]);
        }

        $this->index();

        $this->data['title'] = 'Homepage Sidebars Management';

        // if no saved sidebar, save the included sidebars from main sidebar settings
        if( empty(get_blog_option( $this->microsite, 'gss_sidebar_homepage' )) ){
            update_blog_option( $this->microsite, 'gss_sidebar_homepage', $this->data['sidebar_included_microsite'] );
        }

        // added sidebars for homepage
        $this->data['sidebars_included'] = get_blog_option( $this->microsite, 'gss_sidebar_homepage' );
        $this->data['sidebars'] =  $this->data['sidebars_included'];

        foreach ( $this->data['sidebar_included_microsite'] as $include ) {
            if ( !in_array($include, $this->data['sidebars']) ) array_push($this->data['sidebars'], $include);
        }

        $this->load->template('settings/general/sidebar/homepage', $this->data);
    }

    public function innerpage()
    {
        if ( $this->input->post('gss_sidebars_update') ) {
            $posts = $this->input->post('gss_sidebar_frontend', true);

            $old_values = get_blog_option( $this->microsite, 'gss_sidebar_frontend' );

            update_blog_option( $this->microsite, 'gss_sidebar_frontend', $posts);

            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>sprintf('Update (%1s)', ucwords($this->router->fetch_method())), 'old_values'=>(!empty($old_values)?$old_values:array()), 'new_values'=>$posts ]);
        }

        $this->index();

        $this->data['title'] = 'Inner Page Sidebars Management';

        // if no saved sidebar, save the included sidebars from main sidebar settings
        if( empty(get_blog_option( $this->microsite, 'gss_sidebar_frontend' )) ){
            update_blog_option( $this->microsite, 'gss_sidebar_frontend', $this->data['sidebar_included_microsite'] );
        }

        // added sidebars for homepage
        $this->data['sidebars_included'] = get_blog_option( $this->microsite, 'gss_sidebar_frontend' );
        $this->data['sidebars'] =  $this->data['sidebars_included'];

        foreach ( $this->data['sidebar_included_microsite'] as $include ) {
            if ( !in_array($include, $this->data['sidebars']) ) array_push($this->data['sidebars'], $include);
        }

        $this->load->template('settings/general/sidebar/homepage', $this->data);
    }

    public function settings()
    {
        global $current_site;

        $posts = $this->input->post(NULL);
        unset($posts['gss_sidebars_update']);

        if( ! empty($posts['gss_sidebar']) || ! empty($posts['gss_sidebar_included_microsite']) ){
            $deactivate = array_diff($posts['gss_sidebar'], ($posts['gss_sidebar_included_microsite'])?:$posts['gss_sidebar_included_microsite'] = array());
            foreach ($deactivate as $sidebar) {
                $post = gss_get_post(1, $sidebar);
                $post->post_status = 'draft';

                gss_update_post(1, $post);
            }
        }

        if( ! empty($posts['gss_sidebar']) || ! empty($posts['gss_sidebar_included_microsite']) ){
            $activate = array_intersect($posts['gss_sidebar'], ($posts['gss_sidebar_included_microsite'])?:$posts['gss_sidebar_included_microsite'] = array());
            foreach ($activate as $sidebar) {
                $post = gss_get_post(1, $sidebar);
                $post->post_status = 'publish';

                gss_update_post(1, $post);
            }
        }

        if( ! array_key_exists('gss_sidebar_included_microsite', $posts) )
            $posts['gss_sidebar_included_microsite'] = array();

        foreach ($posts as $key => $value) {
            update_blog_option( 1, $key, $value);

            if( $key === 'gss_sidebar_included_microsite' ){
                if( empty(get_blog_option( $this->microsite, 'gss_sidebar_homepage' )) ){
                    update_blog_option( $this->microsite, 'gss_sidebar_homepage', $value);
                }

                if( empty(get_blog_option( $this->microsite, 'gss_sidebar_frontend' )) ){
                    update_blog_option( $this->microsite, 'gss_sidebar_frontend', $value);
                }
            }
        }

        redirect( base_url('microsites/sidebars') );
    }

}
