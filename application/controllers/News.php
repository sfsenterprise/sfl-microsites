<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
        $this->data['categories'] = gss_get_categories( $this->microsite, [ 'type' => 'ff_news', 'taxonomy' => 'ff_news_taxonomy' ] );
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'News';

        if( isset($this->microsite) ){

            $args = array(
                'number'        => 10,
                'orderby'       => ($this->uri->segment(4)) ?: 'date',
                'order'         => ($this->uri->segment(5)) ?: 'desc',
                'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
            );

            $this->load->library('pagination');
            $config['total_rows'] = count( gss_get_posts( $this->microsite, ['post_type'=>'ff_news', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
            $config['per_page'] = $args['number'];
            $config['uri_segment'] = 5;
            $config['base_url'] = base_url("/news/index/{$filter}");
            $this->pagination->initialize($config);

            $this->data['all_news'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_news', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
            $this->load->template('microsite/affiliates/news', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }

    }

    public function add()
    {
        $this->data['title'] = 'Add News';
        if( ! empty($this->input->post(NULL)) ){
            $gss_news = $this->Microsite_Model->gss_news($this->input->post(NULL), $this->microsite, $update = false);

            if( $gss_news ){
                redirect( base_url(sprintf('news/edit/%1$s', $gss_news)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }

        $this->data['page'] = (object) array('ID' => '', 'post_title' => '', 'post_content' => '');
        $this->data['categories'] = gss_get_categories( $this->microsite, [ 'type' => 'ff_news', 'taxonomy' => 'ff_news_taxonomy' ] );
        $this->data['yoast']['yoast_wpseo_title'] = '';
        $this->data['yoast']['yoast_wpseo_metadesc'] = '';
        $this->data['yoast']['yoast_wpseo_meta-robots-noindex'] = '';
        $this->data['yoast']['yoast_wpseo_meta-robots-nofollow'] = '';
        $this->data['yoast']['_yoast_wpseo_meta-robots-adv'] = '';
        $this->load->template('microsite/affiliates/news', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit News';

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){
            $gss_news = $this->Microsite_Model->gss_news($this->input->post(NULL), $this->microsite, $update = $this->uri->segment(4));

            if( $gss_news ){
                redirect( base_url(sprintf('news/edit/%1$s', $gss_news)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {

            $this->data['page'] = gss_get_post( $this->microsite, $this->uri->segment(4) );
            $this->data['post_cats'] = gss_get_post_categories(['blog_id'=>$this->microsite, 'post_id'=>$this->uri->segment(4), 'taxonomy'=>'ff_news_taxonomy']);
            $this->data['yoast']['yoast_wpseo_title'] = '';
            $this->data['yoast']['yoast_wpseo_metadesc'] = '';
            $this->data['yoast']['yoast_wpseo_meta-robots-noindex'] = '';
            $this->data['yoast']['yoast_wpseo_meta-robots-nofollow'] = '';
            $this->data['yoast']['_yoast_wpseo_meta-robots-adv'] = '';
            $this->load->template('microsite/affiliates/news', $this->data);
        }

    }

}
