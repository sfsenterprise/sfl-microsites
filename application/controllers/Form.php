<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Forms_Model');
    }

    public function index()
    {
        redirect(base_url('form/microsites/'));
    }

    public function add()
    {
        $this->data['title'] = 'Add Form Page';
        $this->data['title'] .= '<small class="desc">Add a page where you can add contents and manage some settings</small>';
        $this->data['form_fields'] = gss_get_posts(1, ['posts_per_page'=>-1, 'post_type'=>'wpcf7_contact_form']);
        $this->data['form_position'] = 0;
        $this->data['default'] = '';
        $this->data['form_shortcode'] = 0;

        if( ! empty($this->input->post(NULL)) ){
            $gss_forms = $this->Forms_Model->gss_forms($this->input->post(NULL), $this->microsite, $update = false);

            if( $gss_forms ){
                redirect( base_url( 'form/edit/'.$gss_forms) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
            }
        }

        $this->load->template('microsite/affiliates/forms/add-form', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Form Page';
        $this->data['title'] .= '<small class="desc">Edit a page where you can modify a contents and manage some settings</small>';

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $gss_forms = $this->Forms_Model->gss_forms($this->input->post(NULL), $this->microsite, $this->uri->segment(4));

            if( $gss_forms ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_forms]);
                redirect( base_url('form/edit/'.$gss_forms) );
            } else {
                $this->data['erroe_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {

            $this->data['form_position'] = 0;
            $this->data['_messages'] = gss_get_post_meta($this->microsite, $this->uri->segment(4), '_messages');
            $this->data['form_shortcode'] = gss_get_post_meta( $this->microsite, $this->uri->segment(4), 'form_shortcode' );
            $this->data['gss_forms'] = gss_get_post( $this->microsite, $this->uri->segment(4) );

            if( isset($this->data['gss_forms']->ID) && $this->data['gss_forms'] ){
                $this->data['form_position'] = gss_get_post_meta($this->microsite, $this->data['gss_forms']->ID, 'form_position') ?: 0;
            }

            $this->data['parent_id'] = gss_get_post_meta( $this->microsite, $this->uri->segment(4), '_sfmu_parent_id' );
            $this->data['parent_form'] = gss_get_post_meta( 1, $this->data['parent_id'], 'form_shortcode' );
            $this->data['form_fields'] = gss_get_posts(1, ['posts_per_page'=>-1, 'post_type'=>'wpcf7_contact_form']);
            $this->data['redirect_page'] = gss_get_post_meta( $this->microsite, $this->uri->segment(4), 'redirect_page' );
            $this->data['parent_redirect'] = gss_get_post_meta( 1, $this->data['form_shortcode'], 'redirect_page' );

            $microsite_notifs = gss_get_post_meta(1, $this->data['form_shortcode'], '_sfmu_mail_'.$this->microsite)?:gss_get_post_meta(1, $this->uri->segment(4), '_sfmu_mail_'.$this->microsite);
            $main_notifs = gss_get_post_meta(1, $this->data['form_shortcode'], '_sfmu_mail');
            $this->data['_mail'] = ((int)$this->microsite === 1) ? $main_notifs : $microsite_notifs;

            if( $this->data['gss_forms'] && isset($this->data['gss_forms']->post_type) && $this->data['gss_forms']->post_type === 'ff_forms' ){
                $this->load->template('microsite/affiliates/forms/add-form', $this->data);
            }
            else{
                redirect( base_url('form/microsites/') );
            }


        }
    }

    public function settings()
    {
        $this->data['title'] = 'Settings';
        $this->data['title'] .= '<small class="desc">Forms additional settings and configurations</small>';

        if( isset($_POST['form_settings']) ){
            $this->Forms_Model->form_settings($this->microsite, $this->input->post(null));
        }

        $emma_api = gss_get_blog_option($this->microsite, 'gravityformsaddon_gravityformsemma_settings');

        $this->data['emma_accnt_id'] = $emma_api['account_id'];
        $this->data['public_api_key'] = $emma_api['public_api_key'];
        $this->data['private_api_key'] = $emma_api['private_api_key'];

        $this->data['group_ids'] = '';

        if(isset($emma_api['group_ids'])){
        $this->data['group_ids'] = $emma_api['group_ids'];
        }

        if($emma_api['account_id']!='' && $emma_api['public_api_key'] !='' && $emma_api['private_api_key']!=''){

            $public_api_key  =  $emma_api['public_api_key'];
            $private_api_key =  $emma_api['private_api_key'];
            $account_id      =  $emma_api['account_id'];
            // Set URL
            $url = "https://api.e2ma.net/".$account_id."/groups";

            // Open connection
            $ch = curl_init();

            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_USERPWD, $public_api_key . ":" . $private_api_key);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $head = curl_exec($ch);
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

           //execute post
            if($http_code > 200) {
                $this->data['emma_groups'] = '';
            } else {
                $this->data['emma_groups'] = json_decode($head);
            }
        }
        
        $this->load->template('microsite/affiliates/forms/settings', $this->data);
    }

    public function microsites($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'Microsites Form Settings';

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count(gss_get_posts(1, ['posts_per_page'=>-1, 'post_status'=>array('draft', 'publish'), 'post_type'=>'wpcf7_contact_form']));
        $config['per_page'] =  $args['number'];
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/form/microsites/{$filter}");
        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $this->data['microsite_forms'] = gss_get_posts( $this->microsite, ['offset'=>$args['paged'], 'posts_per_page'=>-1, 'post_type'=>'ff_forms', 'post_status'=>array('publish', 'draft'), 'orderby'=>'title', 'order'=>'ASC'] );
        $this->load->template('microsite/affiliates/forms/microsites');
    }

    public function micrositestatus()
    {
        $form_settings = array();
        $old_values = array();
        $forms = $this->input->post();
        unset($forms['update_status']);

        $gss_forms = gss_get_posts($this->microsite, ['posts_per_page'=>-1, 'post_type'=>'ff_forms', 'post_status'=>array('publish', 'draft')]);

        switch_to_blog($this->microsite);
            foreach ($gss_forms as $gss_form) {

                $old_values[$gss_form->ID] = ($gss_form->post_status === 'publish') ? 'Active':'Inactive';

                if( in_array($gss_form->ID, $forms['forms_status']) ){
                    $gss_form->post_status = 'publish';
                }
                else{
                    $gss_form->post_status = 'draft';
                }

                $update_status = wp_update_post( $gss_form );

                $form_settings[$gss_form->ID] = ($gss_form->post_status === 'publish') ? 'Active':'Inactive';
            }
        restore_current_blog();

        $this->logs->add(['franchise'=>$this->microsite, 'module'=>'Microsite Form', 'action'=>'Settings', 'old_values'=>$old_values, 'new_values'=>$form_settings]);

        redirect( base_url('/form/microsites/') );

    }

    public function leads($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'Form Leads';
        $this->data['title'] .= '<small class="desc">Shows the list of leads, click "View" link to open the whole details of specific lead.</small>';

        if( $this->uri->segment(4) && $this->uri->segment(4) !== 'all' ){
            $form = get_post($this->uri->segment(4));
            $this->data['form_name'] = $form->post_title;
        }
        else{
            $this->data['title'] = 'Form Leads';
        }

        $args = array(
            'number'        => 10,
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 5;
        $config['reuse_query_string'] = TRUE;
        $config['base_url'] = base_url("form/leads/{$filter}");

        if( $this->uri->segment(4) && $this->uri->segment(4) !== 'all' ){

            $config['total_rows'] = $this->Forms_Model->form_leads($this->uri->segment(4), $this->microsite, true);
            $this->pagination->initialize($config);

            $this->data['rg_form'] = $this->Forms_Model->get_rg_form($this->uri->segment(4), $this->microsite);
            $this->data['leads'] = $this->Forms_Model->form_leads($this->uri->segment(4), $this->microsite, false, $limit=$args['number'], $offset=$args['paged']);

            $this->load->template('microsite/affiliates/forms/leads', $this->data);
        }
        else{

            $config['total_rows'] = count( $this->Forms_Model->get_forms($this->data['microsite']) );
            $this->pagination->initialize($config);

            $this->data['micro_forms'] = $this->Forms_Model->get_forms($this->microsite, $limit=$args['number'], $this->uri->segment(5)?:1);
            $this->load->template('microsite/affiliates/forms/form-fills', $this->data);
        }

    }

    public function export()
    {
        global $gender, $shirt, $swim_exp;

        $blog_details = get_blog_details($this->microsite);
        $filename = str_replace(' ', '-', strtolower($blog_details->blogname)) . '-' . strtolower(str_replace('---', '-', str_replace(' ', '-', $_POST['form_name']))) . '-leads.csv';

        /*$ipaddress = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ipaddress = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ipaddress = $_SERVER['REMOTE_ADDR'];
            else
                $ipaddress = 'UNKNOWN';
    
        if( filter_var($ipaddress, FILTER_VALIDATE_IP) ){
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ipaddress)); 
            $timezone = geoip_time_zone_by_country_and_region($ipdat->geoplugin_countryCode);
        }*/
        
        header("Content-Type: text/csv");
        header("Content-disposition: attachment; filename=$filename");

        $fp = fopen('php://output', 'w');

        $form_meta = $this->get_form_meta();
        $exp_reviews = $this->Forms_Model->export_leads($this->uri->segment(4), $form_meta['form_id']);

        if( is_array($form_meta) && ! empty($form_meta) ){
            unset($form_meta['form_id']);
            $form_meta['Date Created'] = 'Date Created';

            if( (int)$this->microsite !== 1 ){
                foreach ($form_meta as $key => $value) {
                    if( stripos($value, 'location') > 0 ){
                        unset($form_meta[$key]);
                    }
                }
            }

            fputcsv($fp, $form_meta);
        }
        else{
            $form = str_replace(' ', '_', strtolower($_POST['form_name']));
            $fields = $this->$form();

            fputcsv($fp, $fields);
        }

        if (function_exists("set_time_limit") == TRUE AND ini_get("safe_mode") == 0) {
            set_time_limit(0);

            foreach ($exp_reviews[0]['all_leads'] as $lead_id) {
                $lead_details = $this->Forms_Model->lead_details($lead_id['id'], $this->microsite, true);

                /*$timestamp = $lead_id['date_created'];
                $date = new DateTime();
                $date->setTimezone(new DateTimeZone( $timezone ));   
                $date->setTimestamp( strtotime($timestamp) );                           
                $local = $date->format('M d, Y h:i:sa');*/

                $lead_details[] = array(
                        'field_name'    => 'Date Created',
                        'value'         => $lead_id['date_created']
                    );

                $lead_formatted = array();

                if( is_array($form_meta) && !empty($form_meta) ){
                    unset($form_meta['form_id']);
                    foreach ($form_meta as $meta_key => $meta_value) {
                        if( isset($lead_details[0]['field_number']) ){
                            $lead_formatted[] = $this->in_array_r($meta_key, $lead_details) ?: ' ';
                        }
                        else{
                            $lead_formatted[] = $this->in_array_r($meta_value, $lead_details) ?: ' ';
                        }
                    }
                }
                else{
                    foreach ($fields as $field) {
                        $lead_formatted[] = $this->in_array_r($field, $lead_details) ?: ' '; //$lead_detail['value'] ?: ' ';
                    }
                }

                fputcsv($fp, $lead_formatted);

                $gender = array();
                $shirt = array();
                $swim_exp = array();

            }
        }

        fclose($fp);

    }

    private function in_array_r($needle, $haystack)
    {

        global $gender, $shirt, $swim_exp;

        if( $needle === 'Gender' ){
            $gender[] = $needle;
        }

        if( $needle === 'T-Shirt Size' ){
            $shirt[] = $needle;
        }

        if( $needle === "Child's Swimming Experience Level" ){
            $swim_exp[] = $needle;
        }

        $location_labels = array(
                'What location is this for?',
                'Choose a location',
                'Location',
            );

        $days_pref = array(
                'Day Preference',
                'Time Preference',
                'Days Available',
                'days-available'
            );

        $date_fields = array(
                '3'     => 'Child\'s Date of Birth',
                '26'    => 'Second Child\'s Date of Birth',
                '30'    => 'Third Child\'s Date of Birth',
                '37'    => 'Fourth Child\'s Date of Birth',
                '44'    => 'Fifth Child\'s Date of Birth',
            );

        foreach ($haystack as $item) {

            if ( array_key_exists('field_number', $item) && $item['field_number'] == $needle ) {
                if( strlen($item['value']) >= 200 ){
                    $long_detail = $this->check_detail_long($item['id']);

                    if( !$long_detail )
                        return str_replace('\\', '', mb_convert_encoding($item['value'], "HTML-ENTITIES", "UTF-8"));

                    return mb_convert_encoding(stripslashes_deep($long_detail[0]->value), "HTML-ENTITIES", "UTF-8");
                }
                else{

                    if( array_key_exists($item['field_number'], $date_fields) ){

                        $date = date_create($item['value']);

                        return date_format($date,"d-M-y");

                    }
                    else{
                        return mb_convert_encoding($item['value'], "HTML-ENTITIES", "UTF-8");
                    }

                }
            }
            elseif( array_key_exists('field_name', $item) && $item['field_name'] !== 'location' ){

                $bom = pack('H*','EFBBBF');
                $fieldname = preg_replace("/^$bom/", '', $item['field_name']);

                if( strtolower(str_replace('_', ' ', $fieldname)) === strtolower(str_replace('_', ' ', $needle)) ){

                    if( strlen($item['value']) >= 200 ){
                        $long_detail = $this->check_detail_long($item['id']);

                        if( !$long_detail )
                            return str_replace('\\', '', mb_convert_encoding($item['value'], "HTML-ENTITIES", "UTF-8"));

                        return mb_convert_encoding(stripslashes_deep($long_detail[0]->value), "HTML-ENTITIES", "UTF-8");
                    }
                    else{

                        if( $fieldname === 'Gender' ){
                            $returned_gender[] = $fieldname;

                            if( count($returned_gender) == count($gender) ){
                                $returned_gender = array();

                                return $item['value'];
                            }
                        }
                        elseif( $fieldname === 'T-Shirt Size' ){
                            $returned_shirt[] = $fieldname;

                            if( count($returned_shirt) == count($shirt) ){
                                $returned_shirt = array();

                                return (strpos($item['value'], '/')) ? $item['value']."'":$item['value'];
                            }
                        }
                        elseif( $fieldname === "Child's Swimming Experience Level" ){
                            $returned_swim_exp[] = $fieldname;

                            if( count($returned_swim_exp) == count($swim_exp) ){
                                $returned_swim_exp = array();

                                return $item['value'];
                            }
                        }
                        elseif( in_array($fieldname, $date_fields) ){

                            $date = date_create(str_replace('%', '/', $item['value']));

                            return date_format($date,"d-M-y");

                        }
                        else{
                            return $item['value'];
                        }

                    }
                }
                elseif( in_array($needle, $location_labels) && $fieldname === '_loc_address' ){
                    return $item['value'];
                }
                elseif( in_array($fieldname, $days_pref) ){
                    $prefs = explode(',', $item['value']);

                    foreach ($prefs as $pref) {
                        if( $needle !== trim($pref) )
                            continue;

                        return $pref;
                    }
                }
                elseif( $fieldname === 'embed_referrer' && $needle === 'Referer URL' ){
                    return $item['value'];
                }
                else{

                    if( stripos(strtolower(trim($fieldname)), 'preference') > 0 || stripos(strtolower(trim($fieldname)), 'days-available') > 0 || trim($fieldname) === 'date-of-cancellation' ){

                        if( strpos($item['value'], ',') > 0 ){
                            $prefs = explode(',', $item['value']);

                            // convert value and check if date
                            $m_d = explode(' ', $prefs[0]);
                            if( count($m_d) > 1 ){
                                $month = date('m', strtotime( $m_d[0] ));
                                $day = date('d', strtotime( $m_d[1] ));
                                $year = date('Y', strtotime( $prefs[1] ));
                                $is_date = checkdate($month, $day, $year);

                                if( ! $is_date ){
                                    foreach ($prefs as $pref) {
                                        if( $needle !== trim($pref) )
                                            continue;

                                        return $pref;
                                    }
                                }
                            }
                            elseif( trim($fieldname) === trim($needle) ){
                                return $item['value'];
                            }

                        }
                        elseif( $item['value'] === $needle ){
                            return $item['value'];
                        }

                    }
                    else{
                        $form = $_POST['form_name'];

                        switch ($form) {
                            case 'General Contact Form':
                                $prefs = $this->general_contact_form_meta($fieldname);
                                break;
                            
                            case 'Pre-Registration Form - Full (with additional kids)':
                                $prefs = $this->pre_reg_form_meta($fieldname);
                                break;

                            case 'Cancel Form':
                                $prefs = $this->cancel_form_meta($fieldname);
                                break;

                            case 'Pre-Employment Application':
                                $prefs = $this->pre_employment_form_meta($fieldname);
                                break;

                            case 'Holiday Form':
                                $prefs = $this->holiday_form_meta($fieldname);
                                break;

                            case 'Refer-a-Friend Form':
                                $prefs = $this->refer_form_meta($fieldname);
                                break;
                                
                            default:
                                $prefs = $item['field_name'];
                                break;
                        }

                        if( trim($prefs) === trim($needle) ){
                            if( strlen($item['value']) >= 200 ){
                                $long_detail = $this->check_detail_long($item['id']);

                                if( $long_detail )
                                    return mb_convert_encoding(stripslashes_deep($long_detail[0]->value), "HTML-ENTITIES", "UTF-8");

                                return str_replace('\\', '', mb_convert_encoding($item['value'], "HTML-ENTITIES", "UTF-8"));
                            }
                            else{

                                if( trim($prefs) === 'Gender' ){
                                    $returned_gender[] = $fieldname;

                                    if( count($returned_gender) == count($gender) ){
                                        $returned_gender = array();

                                        return $item['value'];
                                    }
                                }
                                elseif( trim($prefs) === 'T-Shirt Size' ){
                                    $returned_shirt[] = $fieldname;

                                    if( count($returned_shirt) == count($shirt) ){
                                        $returned_shirt = array();

                                        return (strpos($item['value'], '/')) ? $item['value']."'":$item['value'];
                                    }
                                }
                                elseif( trim($prefs) === "Child's Swimming Experience Level" ){
                                    $returned_swim_exp[] = $fieldname;

                                    if( count($returned_swim_exp) == count($swim_exp) ){
                                        $returned_swim_exp = array();

                                        return $item['value'];
                                    }
                                }
                                elseif( in_array(trim($prefs), $date_fields) ){

                                    $date = date_create($item['value']);

                                    return date_format($date,"d-M-y");

                                }
                                else{
                                    return mb_convert_encoding($item['value'], "HTML-ENTITIES", "UTF-8");
                                }
                                
                            }
                        }
                    }
                        
                }

            }
            elseif( $_POST['form_name'] == 'Holiday Gift Card Form' && $needle === 'Location' ){
                
                if( is_email( $item['value'] ) && $item['field_name'] === 'location' ){
                    $this->load->helper('leads_helper');
                    $location = get_microsite_by_email($item['value']);
                    
                    if( $location )
                        return $location;

                }
                elseif( $item['field_name'] === '_loc_address' ){
                    return $item['value'];
                }

            }

        }

        return false;
    }

    private function check_detail_long($detail_id)
    {
        global $wpdb;

        switch_to_blog($this->microsite);

        if( $this->db->table_exists("{$wpdb->prefix}rg_lead_detail_long") ){

            $this->db->select('value');
            $this->db->where('lead_detail_id', $detail_id);
            $result = $this->db->get("{$wpdb->prefix}rg_lead_detail_long");

            if( $result->num_rows() > 0 ){
                return $result->result();
            }
            else{
                return false;
            }

        }
        else{
            return false;
        }

        restore_current_blog();
    }

    private function get_form_meta()
    {
        global $wpdb;

        
        $table_name = 'wp_'.$this->microsite.'_rg_form';
        $form_fields = array();


        switch_to_blog($this->microsite);
            if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") == $table_name) {
                $get_rg_from = $this->microsite;
            }
            else{
                $get_rg_from = 1;
            }
        restore_current_blog();

        switch_to_blog($get_rg_from);    
                $rg_form = $this->db->get_where("{$wpdb->prefix}rg_form", array('title' => $_POST['form_name']));
                $rg_form = $rg_form->result();

                if( $rg_form ){
                    $this->db->select('f.id, m.display_meta');
                    $this->db->from("{$wpdb->prefix}rg_form as f");
                    $this->db->join("{$wpdb->prefix}rg_form_meta as m", 'f.id = m.form_id');
                    $this->db->where('f.id = ', $rg_form[0]->id);
                    $form = $this->db->get();

                    $form = $form->result();

                    if( isset($form[0]->display_meta) ){
                        $form_meta = json_decode($form[0]->display_meta);
                    }

                }
        restore_current_blog();

        if( empty($rg_form) && !isset($form_meta)){
            switch_to_blog(1);    
                $rg_form = $this->db->get_where("{$wpdb->prefix}rg_form", array('title' => $_POST['form_name']));
                $rg_form = $rg_form->result();

                if( $rg_form ){
                    $this->db->select('f.id, m.display_meta');
                    $this->db->from("{$wpdb->prefix}rg_form as f");
                    $this->db->join("{$wpdb->prefix}rg_form_meta as m", 'f.id = m.form_id');
                    $this->db->where('f.id = ', $rg_form[0]->id);
                    $form = $this->db->get();

                    $form = $form->result();

                    if( isset($form[0]->display_meta) ){
                        $form_meta = json_decode($form[0]->display_meta);
                    }

                }
            restore_current_blog();
        }

        if( isset($form_meta) ){
            foreach ($form_meta->fields as $field){
                if( ( isset($field->displayOnly) && !$field->displayOnly ) || is_array($field->inputs) ){
                    if( ! empty($field->inputs) ){
                        foreach($field->inputs as $input){
                            if( ! isset($input->isHidden) ){
                                if( $input->label !== 'Yes' ){
                                    if( $field->type === 'address' ){
                                        $address = explode('.', $input->id);
                                        $form_fields[$address[0] . '.' . $address[1]] = str_replace(':', '', $input->label);
                                    }
                                    else{
                                        $form_fields[$input->id] = str_replace(':', '', $input->label);
                                    }
                                }
                                else{
                                    $form_fields[$input->id] = str_replace(':', '', $field->label);
                                }
                            }
                        }
                    }
                    else{
                        $form_fields[$field->id] = str_replace(':', '', $field->label);
                    }
                }
                elseif( $field->type !== 'section' && $field->type !== 'page' && $field->type !== 'html' ){
                    $form_fields[$field->id] = str_replace(':', '', $field->label);
                }
            }
        }

        if( isset($form) && $form ){
            $form_fields['form_id'] = $form[0]->id;

            return $form_fields;
        }
        else{
            return false;
        }

    }

    public function leaddetails()
    {

        $this->data['details'] = $this->Forms_Model->lead_details($this->uri->segment(4), $this->microsite);

        $this->data['title'] = 'Lead Details';

        $form_name = '';

        if( isset($this->data['details'][0]->title) ){
            $form_name = $this->data['details'][0]->title;
        }
        else{
            if( isset($this->data['details'][0]) ){
                $form = (object)$this->data['details'][0];
                $form_page = get_post($form->form_id);
                $form_name = $form_page->post_title;
            }
        }

        $this->data['title'] .= '<small class="desc">'. $form_name .' Lead</small>';

        $this->load->template('microsite/affiliates/forms/leads', $this->data);
    }

    private function pre_reg_form_meta($field_name)
    {

        $fields = array(
            '1' => array(
                    "Child's First Name" => array(
                            'child-first-name', 'child-first-name-1'
                        )
                ),
            '24' => array(
                    "Child's Last Name" => array(
                            'child-last-name', 'child-last-name-24'
                        )
                ),   
            '3' => array(
                    "Child's Date of Birth" => array(
                            'child-dob', 'child-dob-3'
                        )
                ),
            '4' => array(
                    "Gender" => array(
                            'child-gender', 'child-gender-4'
                        )
                ),
            '5' => array(
                    "T-Shirt Size" => array(
                            'child-tshirt-size', 'child-tshirt-size-5'
                        )
                ),
            '47' => array(
                    "Child's Swimming Experience Level" => array(
                            'child-swimming-exp', 'child-swimming-exp-47'
                        )
                ),
            '23' => array(
                    "Add a Second Child?" => array(
                            'add-second-child', 'Second Child Yes', 'add-second-child-23'
                        )
                ),
            '25' => array(
                    "Second Child's First Name" => array(
                            'second-child-first-name', 'second-child-first-name-25'
                        )
                ),
            '2' => array(
                    "Second Child's Last Name" => array(
                            'second-child-last-name', 'second-child-last-name-2'
                        )
                ),
            '26' => array(
                    "Second Child's Date of Birth" => array(
                            'second-child-dob', 'second-child-dob-26'
                        )
                ),
            '31' => array(
                    "Gender" => array(
                            'second-child-gender', "Second Child's Gender", 'second-child-gender-31'
                        )
                ),
            '32' => array(
                    "T-Shirt Size" => array(
                            'second-child-tshirt-size', "Second Child's T-Shirt Size", 'second-child-tshirt-size-32'
                        )
                ),
            '48' => array(
                    "Child's Swimming Experience Level" => array(
                            'second-child-swimming-exp', "Second Child's Child's Swimming Experience Level", '  second-child-tshirt-size-32'
                        )
                ),
            '27' => array(
                    "Add a Third Child?" => array(
                            'add-third-child', 'Third Child Yes', 'add-third-child-27'
                        )
                ),
            '28' => array(
                    "Third Child's First Name" => array(
                            'third-child-first-name', 'third-child-first-name-28'
                        )
                ),
            '29' => array(
                    "Third Child's Last Name" => array(
                            'third-child-last-name', 'third-child-last-name-29'
                        )
                ),
            '30' => array(
                    "Third Child's Date of Birth" => array(
                            'third-child-dob', 'third-child-dob-30'
                        )
                ),
            '33' => array(
                    "Gender" => array(
                            'third-child-gender', 'third-child-gender-33'
                        )
                ),
            '34' => array(
                    "T-Shirt Size" => array(
                            'third-child-tshirt-size', 'third-child-tshirt-size-34'
                        )
                ),
            '49' => array(
                    "Child's Swimming Experience Level" => array(
                            'third-child-swimming-exp', 'third-child-swimming-exp-49'
                        )
                ),
            '35' => array(
                    "Add a Fourth Child?" => array(
                            'add-fourth-child', 'Fourth Child Yes', 'add-fourth-child-35'
                        )
                ),
            '36' => array(
                    "Fourth Child's First Name" => array(
                            'fourth-child-first-name', 'fourth-child-first-name-36'
                        )
                ),
            '38' => array(
                    "Fourth Child's Last Name" => array(
                            'fourth-child-last-name', 'fourth-child-last-name-38'
                        )
                ),
            '37' => array(
                    "Fourth Child's Date of Birth" => array(
                            'fourth-child-dob', 'fourth-child-dob-37'
                        )
                ),
            '39' => array(
                    "Gender" => array(
                            'fourth-child-gender', 'fourth-child-gender-39'
                        )
                ),
            '40' => array(
                    "T-Shirt Size" => array(
                            'fourth-child-tshirt-size', 'fourth-child-tshirt-size-40'
                        )
                ),
            '50' => array(
                    "Child's Swimming Experience Level" => array(
                            'fourth-child-swimming-exp', 'fourth-child-swimming-exp-50'
                        )
                ),
            '41' => array(
                    "Add a Fifth Child?" => array(
                            'add-fifth-child', 'Fifth Child Yes', 'add-fifth-child-41'
                        )
                ),
            '42' => array(
                    "Fifth Child's First Name" => array(
                            'fifth-child-first-name', 'fifth-child-first-name-42'
                        )
                ),
            '43' => array(
                    "Fifth Child's Last Name" => array(
                            'fifth-child-last-name', 'fifth-child-last-name-43'
                        )
                ),
            '44' => array(
                    "Fifth Child's Date of Birth" => array(
                            'fifth-child-dob', 'fifth-child-dob-44'
                        )
                ),
            '45' => array(
                    "Gender" => array(
                            'fifth-child-gender', 'fifth-child-gender-45'
                        )
                ),
            '46' => array(
                    "T-Shirt Size" => array(
                            'fifth-child-tshirt-size', 'fifth-child-tshirt-size-46'
                        )
                ),
            '51' => array(
                    "Child's Swimming Experience Level" => array(
                            'fifth-child-swimming-exp', 'fifth-child-swimming-exp-51'
                        )
                ),
            '8' => array(
                    "Program or Event" => array(
                            'program-event', 'program-event-8'
                        )
                ),
            '14.3' => array(
                    "First" => array(
                            'parent-first-name', 'Name (First)', 'parent-first-name-14_3'
                        )
                ),
            '14.6' => array(
                    "Last" => array(
                            'parent-last-name', 'Name (Last)', 'parent-last-name-14_6'
                        )
                ),
            '22.1' => array(
                    "Street Address" => array(
                            'parent-street-address', 'Address (Street Address)', 'parent-street-address-22_1'
                        )
                ),
            '22.2' => array(
                    "Address Line 2" => array(
                        'Address (Address Line 2)', 'parent-street-address2-22_2'
                    )
                ),
            '22.3' => array(
                    "City" => array(
                            'parent-city', 'Address (City)', 'parent-city-22_3'
                        )
                ),
            '22.4' => array(
                    "State / Province" => array(
                            'parent-state-province', 'Address (State / Province)', 'parent-state-province-22_4'
                        )
                ),
            '22.5' => array(
                    "ZIP / Postal Code" => array(
                            'parent-zip', 'Address (ZIP / Postal Code)', 'parent-zip-22_5'
                        )
                ),
            '16' => array(
                    "Primary Phone" => array(
                            'parent-primary-phone', 'parent-primary-phone-16'
                        )
                ),
            '17' => array(
                    "Email" => array(
                            'parent-email', 'parent-email-17'
                        )
                ),
            '18' => array(
                    "How did you hear about us?" => array(
                            'how-did-you-hear-about-us', 'how-did-you-hear-about-us-18'
                        )
                ),
            '19' => array(
                    "Embed URL" => array(
                            'hidden_url', 'hidden_url-19'
                        )
                ),
            '20' => array(
                    "Referer URL" => array(
                            'hidden_referrer', 'hidden_referrer-20'
                        )
                ),
        );

        return $this->get_col_name($fields, $field_name);

    }

    private function general_contact_form_meta($field_name)
    {
        $fields = array(
            '1.3' => array(
                    "First" => array(
                            'contact-first-name', 'Name (First)'
                        )
                ),
            '1.6' => array(
                    "Last" => array(
                            'contact-last-name', 'Name (Last)'
                        )
                ),
            '2' => array(
                    "Email" => array(
                            'contact-email', 'Email'
                        )
                ),
            '3' => array(
                    "Phone" => array(
                            'contact-phone', 'Phone'
                        )
                ),
            '4' => array(
                    "Message/Comment" => array(
                            'contact-message', 'Message/Comment'
                        )
                ),
            '5' => array(
                    "Embed URL" => array( 'Embed URL' )
                ),
        );
        
        return $this->get_col_name($fields, $field_name);
    }

    private function refer_form_meta($field_name)
    {
        $fields = array(
            '2.3' => array(
                    "First" => array(
                            'Your Name (First)', 'your-first-name'
                        )
                ),
            '2.6' => array(
                    "Last" => array(
                            'Your Name (Last)', 'your-last-name'
                        )
                ),
            '3' => array(
                    "Your Email" => array(
                            'Your Email', 'your-email'
                        )
                ),
            '5.3' => array(
                    "First" => array(
                            'Referral Name (First)', 'referral-first-name'
                        )
                ),
            '5.6' => array(
                    "Last" => array(
                            'Referral Name (Last)', 'referral-last-name'
                        )
                ),
            '6' => array(
                    "Referral Email" => array(
                            'Referral Email', 'referral-email'
                        )
                ),
            '8' => array(
                    "Referral Phone (optional)" => array( 'referral-phone' )
                ),
            '7' => array(
                    "Tell us a little about your referral!" => array('referral-description')    
                ),  
            '9' => array(
                    "Embed URL" => array('hidden_url')
                )
        );

        return $this->get_col_name($fields, $field_name);
    }

    private function cancel_form_meta($field_name)
    {

        $fields = array(
            '2' => array(
                    "Student's First Name" => array(
                            'student-first-name'
                        )
                ),
            '3' => array(
                    "Student's Last Name" => array(
                            'student-last-name'
                        )
                ),
            '4' => array(
                    "Lesson Day and Time" => array(
                            'lesson-day-time'
                        )
                ),
            '5' => array(
                    "Date you will be cancelling for" => array(
                            'date-of-cancellation'
                        )
                ),
            '6' => array(
                    "Your email" => array(
                            'student-email'
                        )
                ),
            '7' => array(
                    "Your Phone #" => array(
                            'student-phone'
                        )
                ),
        );

        return $this->get_col_name($fields, $field_name);
    }

    private function pre_employment_form_meta($field_name)
    {
        $fields = array(
            '15.3' => array(
                    "First" => array(
                            'applicant-first-name'
                        )
                ),
            '15.6' => array(
                    "Last" => array(
                            'applicant-last-name'
                        )
                ),
            '14' => array(
                    "Phone" => array(
                            'applicant-phone'
                        )
                ),
            '13' => array(
                    "Email" => array(
                            'applicant-email'
                        )
                ),
            '4' => array(
                    "Are you under 18 years of age?" => array(
                            'under-age'
                        )
                ),
            '120' => array(
                    "Are you legally eligible to work in the United States?" => array(
                            'legally-eligible'
                        )
                ),
            '12.1' => array(
                    "Street Address" => array(
                            'applicant-street-address'
                        )
                ),
            '12.2' => array(
                    "Address Line 2" => array(
                            'applicant-address-line2'
                        )
                ),
            '12.3' => array(
                    "City" => array(
                            'applicant-city'
                        )
                ),
            '12.4' => array(
                    "State / Province" => array(
                            'applicant-state-province'
                        )
                ),
            '12.5' => array(
                    "ZIP / Postal Code" => array(
                            'applicant-zip'
                        )
                ),
            '16' => array(
                    "Date you can start" => array(
                            'applicant-start-date'
                        )
                ),
            '17' => array(
                    "Availability" => array(
                            'applicant-availability'
                        )
                ),
            '123' => array(
                    "Days Available" => array(
                            'days-available'
                        )
                ),
            '18' => array(
                    "Position Desired" => array(
                            'position-desired'
                        )
                ),
            '20' => array(
                    "Referred by" => array(
                            'applicant-referrer'
                        )
                ),
            '29' => array(
                    "Have you been employed at one of our Goldfish Swim School locations before?" => array(
                            'employed-once'
                        )
                ),
            '30' => array(
                    "Which location did you work at?" => array(
                            '_loc_address'
                        )
                ),
            '33' => array(
                    "Are you physically able to perform the skills necessary to complete the duties of the job for which you are applying?" => array(
                            'physically-able', 'Are you physically able to perform the skills necessary to complete the duties of the job for which'
                        )
                ),
            '34' => array(
                    "Please explain" => array(
                            'physically-enable-details'
                        )
                ),
            '124' => array(
                    "Relevant Job Experience" => array(
                            'job-experience'
                        )
                ),
            '35' => array(
                    "Have you ever been fired from a job or asked to resign?" => array(
                            'fired-or-resigned'
                        )
                ),
            '36' => array(
                    "Please give details" => array(
                            'fired-or-resigned-details'
                        )
                ),
            '121' => array(
                    "Graduated High School" => array(
                            'high-school-graduate'
                        )
                ),
            '122' => array(
                    "Graduated College" => array(
                            'college-graduate'
                        )
                ),
            '82' => array(
                    "Degree Obtained" => array(
                            'degree-obtained'
                        )
                ),
            '92' => array(
                    "Special Activities (Civic, Athletics, Academics, etc. - exclude labor organizations, 
                    and memberships which reveal race, color, religion, national origin, sex, age, 
                    disability or other protected status)" => array(
                        'special-activities'
                    )
                ),
            '119' => array(
                    "Embed URL" => array()
                ),
            '112.1' => array(
                    "I Agree" => array(
                            'I have read, understand, and consent to these statements', 'authorization'
                        )
                ),
            '00000' => array(
                    'Authorization' => array(
                            'I have read, understand, and consent to these statements'
                        )
                ),

        );

        return $this->get_col_name($fields, $field_name);
    }

    private function holiday_form_meta($field_name)
    {
        $fields = array(
            '1.3' => array(
                    "First" => array(
                            'first-name'
                        )
                ),
            '1.6' => array(
                    "Last" => array(
                            'last-name'
                        )
                ),
            '2' => array(
                    "Phone" => array(
                            'phone'
                        )
                ),
            '3' => array(
                    "Email" => array(
                            'email'
                        )
                ),
            '4' => array(
                    "How Did You Hear About Us?" => array()
                ),
        );

        return $this->get_col_name($fields, $field_name);
    }

    private function pre_reg_form_meta_old($field_name)
    {
        $fields = array(
            '2' => array(
                    '24' => array('2', 'Child\'s Last Name')
                ),
            '12' => array(
                    '47' => array('12', '')
                ),
            '21.1' => array(
                    '22.1' => array('21.1', 'Street Address')
                ),
            '21.2' => array(
                    '22.2' => array('21.2', 'Address Line 2')
                ),
            '21.3' => array(
                    '22.3' => array('21.3', 'City')
                ),
            '21.4' => array(
                    '22.4' => array('21.4', 'State / Province')
                ),
            '21.5' => array(
                    '22.5' => array('21.5', 'ZIP / Postal Code')
                ),    
        );

        return $this->get_col_name($fields, $field_name);
    }

    private function holiday_gift_card_form()
    {
        $fields = array(
            'Location',
            'Reserve',
            'First Name',
            'Last Name',
            'Address1',
            'Address2',
            'City',
            'State',
            'Zip',
            'Country',
            'Phone',
            'Email',
            'Heard On',
            'First Name Child',
            'Last Name Child',
            'Birth Child',
            'Gender Child',
            'Recipient Phone',
            'Email Recipient',
            'Delivery Method',
            'F Name Ship',
            'L Name Ship',
            'Address1 Ship',
            'Address2 Ship',
            'City Ship',
            'State Ship',
            'Zip Ship',
            'Country Ship',
            'Embed URL',
            'Embed Referrer',
            'Date Created'
        );

        return $fields;
    }

    private function get_col_name($fields, $field_name)
    {
        foreach ($fields as $key => $value) {
            foreach ($value as $field_key => $field_val) {
                if( ! in_array(trim($field_name), $field_val) )
                    continue;

                return $field_key;
            }
        }
    }

}
