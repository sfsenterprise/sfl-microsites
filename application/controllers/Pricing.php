<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pricing extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
        $this->data['title'] = 'Pricing Table';
        $this->data['view_frontend'] = home_url( gss_get_blog_details($this->microsite, 'path') );
    }

    public function index()
    {
        if( isset($this->microsite) ){
            $this->data['gss_pricing'] = maybe_unserialize(get_blog_option($this->microsite, 'gss_pricing'));
            $this->load->template('microsite/affiliates/pricing-table', $this->data);
        }
        else {
            $this->load->template('microsite/index', $this->data);
        }
        
    }

    public function edit()
    {
        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/pricing-table');

        if( ! empty($this->input->post()) ){

            $old_values = maybe_unserialize(get_blog_option($this->microsite, 'gss_pricing'));
            $new_values = $this->input->post();
            unset($new_values['gss_pricing']);

            $gss_pricing = $this->Microsite_Model->update_pricing($this->input->post(NULL), $this->microsite);

            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values]);

            if( is_array($old_values) && ! empty($old_values) ){
                foreach ($old_values as $key => $value) {
                    if( is_array($old_values[$key]) && is_array($new_values[$key]) ){
                        $has_diff = array_diff_assoc($old_values[$key], $new_values[$key]);

                        if( $has_diff && ! empty($has_diff) ){
                            $this->notification_email();
                            break;
                        }
                    }
                }
            }
            
            $this->data['gss_pricing'] = maybe_unserialize(get_blog_option($this->microsite, 'gss_pricing'));
            $this->load->template('microsite/affiliates/pricing-table', $this->data);

        } else {
            $this->data['gss_pricing'] = maybe_unserialize(get_blog_option($this->microsite, 'gss_pricing'));
            $this->load->template('microsite/affiliates/pricing-table', $this->data);
        }
    }

    public function notification_email()
    {

        $pricing = maybe_unserialize(get_blog_option( 1, 'gss_pricing'));
        $notif_emails =  ($pricing['email']) ? explode(',', $pricing['email']) : array('bryan@goldfishfranchise.com', 'dan@goldfishfranchise.com');
        $site_name = gss_get_blog_details($this->microsite, 'blogname');

        foreach ($notif_emails as $notif_email) {
            $to = $notif_email;
            $subject = $site_name . ' Pricing Changed';
            $body  = "Hi,<br><br>This is an automated notice that {$site_name} has updated the pricing and fee information on their website.<br><br>You can view the changes that were made by logging into the Control Panel at: http://goldfishswimschool.com/surefire<br><br>Thank you!";
            $headers[] = "MIME-Version: 1.0\n";
            $headers[] = 'Content-Type: text/html; charset=UTF-8';
            $headers[] = 'From: Goldfish Swim School <no-reply@goldfishswimschool.com>';
                         
            wp_mail( $to, $subject, $body, $headers );
        }

        return;
    }

}