<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Tools_Model', 'tools');
    }

    public function redirections($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'Redirections';
        $this->data['title'] .= '<small class="desc">Manage site redirections</small>';

        if( $this->input->post('url') && $this->input->post('action_data') ){
            $add_redirect = $this->tools->add_redirection($this->data['microsite'], $this->input->post(null));
        }
        elseif( $this->input->post('item') && $this->input->post('action') ){
            $action = $this->tools->action($this->data['microsite'], $this->input->post('item'), $this->input->post('action'));
        }

        if( $search = $this->input->post('search') ){
            $url = base_url( $this->router->fetch_class() . '/' . $this->router->fetch_method() );            
            redirect( sprintf($url . '/?search=%1s', $search) );
        }
        
        $args = array(
            'number'        => 10,
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count( $this->tools->get_redirections(['microsite'=>$this->data['microsite'], 'search'=>$this->input->get('search') ?: '']) );
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/tools/redirections/{$filter}");

        $config['reuse_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'pagenum';

        $this->pagination->initialize($config);

        $this->data['orderby'] = (isset($_GET['orderby'])) ? $_GET['orderby']:'';
        $this->data['order'] = (isset($_GET['order']) && $_GET['order'] === 'desc') ? 'asc':'desc';

        $params = array(
            'microsite' => $this->data['microsite'] ?: 1,
            'limit'     => $args['number'],
            'offset'    => $args['paged'],
            'orderby'   => $this->data['orderby'],
            'order'     => $this->data['order'],
            'search'    => $this->input->get('search') ?: ''
        );

        $this->data['redirects'] = $this->tools->get_redirections($params);
        $this->load->template('tools/redirections/index', $this->data);
        
    }

}