<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formpages extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Forms_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Form Pages';
        $this->data['title'] .= '<small class="desc">List of pages with form in content</small>';
        $this->data['last_updated_by']  = get_userdata( get_blog_option( 1, '_forms_updated_by' ) );

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count(gss_get_posts( 1, ['post_type'=>'ff_forms', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1, 'orderby'=>'ID', 'order'=>'ASC']));
        $config['per_page'] =  $args['number'];
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/formpages/index/{$filter}");
        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $this->data['gss_forms'] = gss_get_posts( 1, ['post_type'=>'ff_forms', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1, 'orderby'=>'title', 'order'=>'ASC']);
        $this->load->template('microsite/affiliates/forms/index', $this->data);
    }

    public function addpage()
    {

        $this->data['title'] = 'Add Form Page';
        $this->data['title'] .= '<small class="desc">Add a page where you can add contents and manage some settings</small>';
        $this->data['form_fields'] = gss_get_posts(1, ['posts_per_page'=>-1, 'post_type'=>'wpcf7_contact_form']);
        $this->data['form_position'] = 0;
        $this->data['default'] = '';
        $this->data['form_shortcode'] = 0;

        if( ! empty($this->input->post(NULL)) ){
            $gss_forms = $this->Forms_Model->gss_forms($this->input->post(NULL), 1, $update = false);

            if( $gss_forms ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_forms]);
                redirect( base_url( 'formpages/editpage/'.$gss_forms) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
            }
        }

        $this->load->template('microsite/affiliates/forms/add-form', $this->data);

    }

    public function editpage()
    {
        $this->data['title'] = 'Edit Form Page';
        $this->data['title'] .= '<small class="desc">Edit a page where you can modify a contents and manage some settings</small>';

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

            $gss_forms = $this->Forms_Model->gss_forms($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $gss_forms ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_forms]);
                redirect( base_url('formpages/editpage/'.$gss_forms) );
            } else {
                $this->data['erroe_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {

            $this->data['gss_forms'] = gss_get_post( 1, $this->uri->segment(4) );
            $this->data['form_position'] = gss_get_post_meta(1, $this->data['gss_forms']->ID, 'form_position') ?: 0;
            $this->data['form_shortcode'] = gss_get_post_meta( 1, $this->uri->segment(4), 'form_shortcode' );
            $this->data['default'] = gss_get_post_meta( 1, $this->uri->segment(4), '_sfmu_default_page' ) ?: '';
            $this->data['form_fields'] = gss_get_posts(1, ['posts_per_page'=>-1, 'post_type'=>'wpcf7_contact_form']);
            $this->load->template('microsite/affiliates/forms/add-form', $this->data);

        }
    }

    public function forms($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Forms';
        $this->data['title'] .= '<small class="desc">List of forms with shortcodes</small>';

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count(gss_get_posts(1, ['posts_per_page'=>-1, 'post_status'=>array('draft', 'publish'), 'post_type'=>'wpcf7_contact_form']));
        $config['per_page'] =  $args['number'];
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/formpages/forms/{$filter}");
        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $this->data['form_fields'] = gss_get_posts(1, ['posts_per_page'=>$args['number'], 'offset'=>$args['paged'], 'post_status'=>array('draft', 'publish'), 'post_type'=>'wpcf7_contact_form']);
        $this->load->template('microsite/affiliates/forms/forms-list', $this->data);
    }

    public function addform()
    {
        $this->data['title'] = 'Add Form';
        $this->data['title'] .= '<small class="desc">Add a form that can be added to form pages</small>';

        if( ! empty($this->input->post(NULL)) ){
            $cf7_form = $this->Forms_Model->add_form($this->input->post(NULL), 1, $update = false);

            if( $cf7_form ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$cf7_form]);
                redirect( base_url( 'formpages/editform/'.$cf7_form) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
            }
        }

        $this->load->template('microsite/affiliates/forms/add-form', $this->data);
    }

    public function editform()
    {
        $this->data['title'] = 'Edit Form';
        $this->data['title'] .= '<small class="desc">Edit a form and modify some settings</small>';

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $cf7_form = $this->Forms_Model->add_form($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $cf7_form ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$cf7_form]);
                redirect( base_url( 'formpages/editform/'.$cf7_form) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
            }

        } else {

            $this->data['_mail'] = gss_get_post_meta(1, $this->uri->segment(4), '_sfmu_mail');
            $this->data['_messages'] = gss_get_post_meta(1, $this->uri->segment(4), '_messages');
            $this->data['_form'] = gss_get_post_meta( 1, $this->uri->segment(4), '_form' );
            $this->data['redirect_page'] = gss_get_post_meta( 1, $this->uri->segment(4), 'redirect_page' );
            $this->data['gss_form'] = gss_get_post(1, $this->uri->segment(4));
            $this->load->template('microsite/affiliates/forms/add-form', $this->data);

        }

    }

    public function status()
    {
        $form_settings = array();
        $old_values = array();

        $forms = $this->input->post();
        $form_id = isset($forms['form_id']) ? $forms['form_id']:array();
        $publish = isset($forms['forms_status']) ? $forms['forms_status']:array();
        unset($forms['update_status']);

        $draft = array_diff($form_id, $publish);

        if( isset($forms['microsite_forms']) ){
            foreach ($forms['microsite_forms'] as $form) {
                $this->add_to_microsite($form);
            }
        }

        switch_to_blog(1);

            if( is_array($publish) && ! empty($publish) ){
                foreach ($publish as $form) {
                    $publish_form = get_post($form);

                    $old_values[$form] = ($publish_form->post_status === 'publish') ? 'Active':'Inactive';

                    if( $publish_form->post_status !== 'publish' ){
                        $publish_form->post_status = 'publish';

                        $update_status = wp_update_post( $publish_form );
                        $form_settings[$publish_form->ID] = 'Active';
                    }
                }
            }

            if( is_array($draft) && ! empty($draft) ){
                foreach ($draft as $form) {
                    $draft_form = get_post($form);

                    $old_values[$form] = ($draft_form->post_status === 'publish') ? 'Active':'Inactive';

                    if( $draft_form->post_status !== 'draft' ){
                        $draft_form->post_status = 'draft';

                        $update_status = wp_update_post( $draft_form );

                        $form_settings[$draft_form->ID] = 'Inactive';
                    }
                }
            }
        restore_current_blog();

        $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>'Settings', 'old_values'=>array(), 'new_values'=>$form_settings]);

        redirect( base_url('formpages') );

    }

    private function add_to_microsite($id=null, $status=false)
    {
        switch_to_blog(1);
            $cpt_form = get_post($id);
            $shortcode = get_post_meta($id, 'form_shortcode', true);
            $position = get_post_meta($id, 'form_position', true);
        restore_current_blog();

        //$this->data['sites']
        $microsites = get_sites(['site__not_in'=>array(1), 'site__in'=>array(3)]);

        foreach ($microsites as $microsite) {
            switch_to_blog($microsite->blog_id);

                $exist = get_page_by_title( $cpt_form->post_title, ARRAY_A, 'gss_forms' );

                if( ! $exist ){
                    $add_cpt_form = wp_insert_post([
                            'post_title'    => $cpt_form->post_title,
                            'post_status'   => 'publish',
                            'post_type'     => $cpt_form->post_type,
                            'post_name'     => sanitize_title($cpt_form->post_title),
                            'post_content'  => $cpt_form->post_content,
                        ]);

                    update_post_meta( $add_cpt_form, 'form_shortcode', $shortcode );
                    update_post_meta( $add_cpt_form, 'form_position', $position );
                }

            restore_current_blog();
        }

        return;

    }

    public function micrositeform()
    {
        $this->data['title'] = 'Microsite Form Settings';

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $gss_forms = $this->Forms_Model->gss_forms($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $gss_forms ){
                redirect( base_url('form/micrositeform/'.$gss_forms) );
            } else {
                $this->data['erroe_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {

            $this->data['_messages'] = gss_get_post_meta( 1, $this->uri->segment(4), '_messages' );
            $this->data['gss_forms'] = gss_get_post( 1, $this->uri->segment(4) );
            $this->data['form_id'] = gss_get_post_meta( 1, $this->uri->segment(4), 'cf7_form_id' );
            $this->data['form_settings'] = gss_get_post_meta( 1, $this->uri->segment(4), '_mail' );
            $this->load->template('microsite/affiliates/forms/add-form', $this->data);

        }
    }

    public function delete($form_id)
    {
        wp_delete_post($form_id);
        redirect(base_url('/formpages/'));
        exit;
    }

}
