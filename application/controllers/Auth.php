<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct(){
        parent::__construct();
    }

	public function index()
	{
        if ( is_user_logged_in() ) {
             redirect(base_url());
        }

        redirect(base_url("/auth/login/"));
	}

    public function getUser()
    {
        $user = array();

        if ( is_user_logged_in() ) {
            $user = wp_get_current_user();
            header('Content-type: application/json');
            echo json_encode($user);
        } else {
            http_response_code(401);
        }
        exit;
    }

    public function login()
    {
        if ( is_user_logged_in() ) {
             redirect(base_url());
        }
        $data = array();

        if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
            $user = wp_signon( [
                'user_login'    => @$this->input->post('log'),
                'user_password' => @$this->input->post('pwd'),
                'remember'      => true,
            ], false );

            if ( is_wp_error( $user ) ) {
                if (  $user->get_error_message() === "" ) {
                    $data['error'] = '<strong>ERROR</strong>: The username and password field are empty.';
                } else {
                    $data['error'] = $user->get_error_message();
                }

                http_response_code(401);

            } else {
                wp_set_current_user( $user->ID, $this->input->post('log'));
                wp_set_auth_cookie( $user->ID, true );

                redirect(base_url());
            }

            if ( $this->input->post('remote') === true ) {
                return $data;
            }
        }

        $this->load->view('auth/login', $data);

    }

    public function logout()
    {
        delete_cookie('selected_microsite');
        wp_logout();
        redirect(base_url("/auth/login/"));
    }
}
