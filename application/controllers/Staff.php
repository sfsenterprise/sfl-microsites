<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Staff';

        if( $this->uri->segment(5) ){
            $this->data['order'] = ($this->uri->segment(5) === 'desc') ? 'asc':'desc';
        } else {
            $this->data['order'] = 'desc';
        }

        if( $this->microsite ){

            $args = array(
                'number'        => 10,
                'orderby'       => ($this->uri->segment(4)) ?: 'date',
                'order'         => ($this->uri->segment(5)) ?: 'desc',
                'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
            );

            $this->load->library('pagination');
            $config['total_rows'] = count( gss_get_posts( $this->microsite, ['post_type'=>'s8_staff_member', 'post_status'=>array('publish', 'draft', 'private'), 'posts_per_page'=>-1 ] ) );
            $config['per_page'] = $args['number'];
            $config['uri_segment'] = 5;
            $config['base_url'] = base_url("/staff/index/{$filter}");
            $this->pagination->initialize($config);

            $this->data['all_staff'] = gss_get_posts( $this->microsite, ['post_type'=>'s8_staff_member', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft', 'private'), 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
            $this->load->template('microsite/affiliates/staff', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }
        
    }

    public function add()
    {
        $this->data['title'] = 'Add Staff';
        if( ! empty($this->input->post(NULL)) ){
            $add_staff = $this->Microsite_Model->add_staff($this->input->post(NULL), $this->microsite, $update = false);
        
            if( $add_staff ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$add_staff]);
                redirect( base_url(sprintf('staff/edit/%1$s', $add_staff)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }
        
        $this->load->template('microsite/affiliates/staff', $this->data);

    }

    public function edit()
    {
        $this->data['title'] = 'Edit Staff';

        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/staff');

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){
            $edit_staff = $this->Microsite_Model->add_staff($this->input->post(NULL), $this->microsite, $update = $this->uri->segment(4));
            
            if( $edit_staff ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$edit_staff]);
                redirect( base_url(sprintf('staff/edit/%1$s', $edit_staff)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        } else {
         
            $this->data['gss_staff'] = gss_get_post( $this->microsite, $this->uri->segment(4) );
            $this->data['_thumbnail'] = gss_wp_get_attachment_image( 1, gss_get_post_meta( $this->microsite, $this->uri->segment(4), '_thumbnail_id' ) );
            $this->load->template('microsite/affiliates/staff', $this->data);
        }
    }

    public function orderby()
    {
        $this->index();
    }

}