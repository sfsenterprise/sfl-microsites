<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends SFL_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('Forms_Model');
	}

	public function index()
	{
        $this->data['all_leads'] = $this->Forms_Model->get_forms($this->data['microsite'], 10);

		$this->load->template('dashboard/form-fills/index', $this->data);
	}

}
