<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promos extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Offers';

        if( $this->uri->segment(5) ){
            $this->data['order'] = ($this->uri->segment(5) === 'desc') ? 'asc':'desc';
        } else {
            $this->data['order'] = 'desc';
        }

        if( $this->microsite ){

            $args = array(
                'number'        => 10,
                'orderby'       => ($this->uri->segment(4)) ?: 'date',
                'order'         => ($this->uri->segment(5)) ?: 'desc',
                'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 10) * 1) : 0
            );

            $this->load->library('pagination');
            $config['total_rows'] = count( gss_get_posts( $this->microsite, ['post_type'=>'ff_promos', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
            $config['per_page'] = $args['number'];
            $config['uri_segment'] = 5;
            $config['base_url'] = base_url("/promos/index/{$filter}");
            $this->pagination->initialize($config);

            $this->data['all_promos'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_promos', 'offset'=>$args['paged'], 'post_status'=>'publish', 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
            $this->load->template('microsite/affiliates/promos', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }

    }

    public function add()
    {
        $this->data['title'] = 'Add Offer';
        if( ! empty($this->input->post(NULL)) ){
            $gss_promos = $this->Microsite_Model->gss_promos($this->input->post(NULL), $this->microsite, $update = false);

            if( $gss_promos ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_promos]);
                redirect( base_url(sprintf('promos/edit/%1$s', $gss_promos)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        }

        $this->load->template('microsite/affiliates/promos', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Offer';

        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/promos');

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){
            $gss_promos = $this->Microsite_Model->gss_promos($this->input->post(NULL), $this->microsite, $this->uri->segment(4));

            if( $gss_promos ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_promos]);
                redirect( base_url(sprintf('promos/edit/%1$s', $gss_promos)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        } else {

            $this->data['gss_promo'] = gss_get_post( $this->microsite, $this->uri->segment(4) );
            $this->load->template('microsite/affiliates/promos', $this->data);
        }
    }

    public function orderby()
    {
        $this->index();
    }

}
