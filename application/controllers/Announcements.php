<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Announcements extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');

    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Announcements';

        if( $this->uri->segment(5) ){
            $this->data['order'] = ($this->uri->segment(5) === 'desc') ? 'asc':'desc';
        } else {
            $this->data['order'] = 'desc';
        }

        if( $this->microsite ){

            $args = array(
                'number'        => 10,
                'orderby'       => ($this->uri->segment(4)) ?: 'date',
                'order'         => ($this->uri->segment(5)) ?: 'desc',
                'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
            );

            $this->load->library('pagination');
            $config['total_rows'] = count( gss_get_posts( $this->microsite, ['post_type'=>'ff_announcements', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
            $config['per_page'] = $args['number'];
            $config['uri_segment'] = 5;
            $config['base_url'] = base_url("/announcements/index/{$filter}");
            $this->pagination->initialize($config);

            $this->data['all_announcements'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_announcements', 'offset'=>$args['paged'], 'post_status'=>'publish', 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
            $this->load->template('microsite/affiliates/announcements', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }
        
    }

    public function add()
    {
        $this->data['title'] = 'Add Announcement';
        if( ! empty($this->input->post(NULL)) ){
            $gss_announcements = $this->Microsite_Model->gss_announcements($this->input->post(NULL), $this->microsite, $update = false);

            if( $gss_announcements ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_announcements]);
                redirect( base_url(sprintf('announcements/edit/%1$s', $gss_announcements)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
            
        }
        
        $this->load->template('microsite/affiliates/announcements', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Announcement';
        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/announcements');

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){
            
            $gss_announcements = $this->Microsite_Model->gss_announcements($this->input->post(NULL), $this->microsite, $this->uri->segment(4));

            if( $gss_announcements ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_announcements]);
                redirect( base_url(sprintf('announcements/edit/%1$s', $gss_announcements)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {
            $this->data['gss_announcement'] = gss_get_post( $this->microsite, $this->uri->segment(4) );
            $this->load->template('microsite/affiliates/announcements', $this->data);
        }
    }

    public function settings()
    {
        $this->data['title'] = 'Announcement Settings';
        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/announcements');

        if( ! empty($this->input->post(NULL, true)) ){
            $announcement_settings = $this->Microsite_Model->announcement_settings($this->input->post(NULL, true), $this->microsite);
            
            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>'Update', 'old_values'=>array(), 'new_values'=>'Settings']);

            redirect( base_url(sprintf('announcements/settings')) );
        } else {

            $this->data['all_announcements'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_announcements', 'post_status'=>'publish', 'posts_per_page'=>-1]);
            $this->data['announcement_to_display'] = get_blog_option($this->microsite, 'announcement_to_display');
            $this->load->template('microsite/affiliates/announcements', $this->data);
        }
    }

    public function orderby()
    {
        $this->index();
    }

}