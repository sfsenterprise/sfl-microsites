<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hero extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Hero_Model');
    }

    public function index()
    {

        if( ! current_user_can('franchisor') ) redirect( base_url() );

        if( isset($_POST['update']) ){

            $old_values = array();
            unset($_POST['update']);
            foreach ($_POST as $key => $value) {
                $old_values[$key] = get_blog_option(($this->microsite)?:1, $key);
            }
            
            $blog_id = (isset($this->microsite) && $this->microsite)? $this->microsite:1;

            $this->Hero_Model->save($blog_id);

            $this->logs->add(['franchise'=>($this->microsite)?:1, 'module'=>$this->router->fetch_class(), 'action'=>'edit', 'old_values'=>$old_values, 'new_values'=>$this->input->post(null)]);
        
        }
        
        $this->data['title'] = 'Hero Banners';
        $this->data['title'] .= '<small class="desc">Modify hero banners setting.</small>';

        $this->data['posttypes'] = include SFL_MICROSITES_DIR . 'lib/config/posttypes.php';
        $this->load->template('hero/index', $this->data);

    }
}
