<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Events_Model');
        $this->data['categories'] = gss_get_categories( $this->microsite, [ 'type' => 'stellar_event', 'taxonomy' => 'stellar_event_categories' ] );
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'Events';

        $this->data['orderby'] = (isset($_GET['orderby'])) ? $_GET['orderby']:'';
        $this->data['order'] = (isset($_GET['order']) && $_GET['order'] === 'desc') ? 'asc':'desc';

        $this->load->library('pagination');

        if( isset($this->microsite) ){

            $args = array(
                'number'        => 10,
                'orderby'       => ($this->uri->segment(4)) ?: 'date',
                'order'         => ($this->uri->segment(5)) ?: 'desc',
                'paged'         => ( isset($_GET['pagenum']) ) ? ( ($_GET['pagenum'] - 1) * 10) : 0
            );

            if( isset($_GET['search']) && $_GET['search'] !== '' ){
                $offset = isset($_GET['pagenum']) ? $_GET['pagenum']:1;
                $search = $this->Events_Model->search( $_GET['search'], $offset, $this->microsite );
                $this->data['all_events'] = $search;
            }
            else{
                $this->data['all_events'] = gss_get_posts( $this->microsite, ['post_type'=>'stellar_event', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft', 'private', 'trash'), 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
            }

            $config['total_rows'] = (isset($_GET['search']) && $_GET['search'] !== '') ? count($search) : count( gss_get_posts( $this->microsite, ['post_type'=>'stellar_event', 'post_status'=>array('publish', 'draft', 'private', 'trash'), 'posts_per_page'=>-1 ] ) );
            $config['per_page'] = $args['number'];
            $config['uri_segment'] = 4;
            $config['base_url'] = base_url("/events/index/");
            $config['reuse_query_string'] = TRUE;
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $config['query_string_segment'] = 'pagenum';
            $this->pagination->initialize($config);

            $this->load->template('microsite/affiliates/events/events', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }

    }

    public function add()
    {
        $this->data['title'] = 'Add Event';

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) ){
            $gss_events = $this->Events_Model->gss_events($this->input->post(NULL), $this->microsite, $update = false);
            //$event = $this->Events_Model->save($this->input->post(NULL, NULL), $this->microsite, $update = false);

            if( $gss_events ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_events]);
                redirect( base_url( 'events/edit/'.$gss_events) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        }

        $this->data['post_cats'] = array();
        switch_to_blog($this->microsite);
        $this->data['categories'] = get_terms(['taxonomy' => 'stellar_event_categories', 'hide_empty' => false]);
        restore_current_blog();
        $this->load->template('microsite/affiliates/events/edit', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Event';

        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/events/events');

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && isset($_POST['gss_events']) ){

            $gss_events = $this->Events_Model->gss_events($this->input->post(NULL), $this->microsite, $this->uri->segment(4));

            $this->audit_log( $old_values, $gss_events );
        } 
        elseif( isset($_POST['trash_event']) ) {
            $post = $this->input->post(NULL);
            $post['post_status'] = 'trash';

            $gss_events = $this->Events_Model->gss_events($post, $this->microsite, $this->uri->segment(4));

            $this->audit_log( $old_values, $gss_events );
        }
        else {

            $this->data['post_cats'] = gss_wp_get_post_categories($this->microsite, $this->uri->segment(4), 'stellar_event_categories');
            $this->data['gss_event'] = gss_get_post( $this->microsite, $this->uri->segment(4) );

            $this->load->template('microsite/affiliates/events/edit', $this->data);
        }

    }

    private function audit_log($old_values, $new_values)
    {
        $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$new_values]);
        redirect( base_url('events/edit/'.$new_values) );
    }

    public function categories()
    {
        $this->data['title'] = 'All Categories';
        $this->data['all_cats'] = gss_get_categories( $this->microsite, [ 'type' => 'stellar_event', 'taxonomy' => 'stellar_event_categories' ] );
        $this->load->template('microsite/affiliates/events/categories', $this->data);
    }

    public function add_category()
    {
        $this->data['title'] = 'Add Category';
        $this->form_validation->set_rules(
            'category_name',
            'Category Name',
            array(
                'required',
                array(
                    'term_exists',
                    function($value) {
                        switch_to_blog($this->microsite);
                        $term = term_exists($value, 'stellar_event_categories', $this->input->post('parent', NULL ));
                        restore_current_blog();

                        if ( $term !== 0 && $term !== null ) {
                            $this->form_validation->set_message('term_exists', 'That category is already taken.');
                            return FALSE;
                        }

                        return TRUE;
                    }
                )
            ),
            array('required' => 'You must provide a %s.')
        );

        if ($this->form_validation->run() !== false) {
            $term = $this->Events_Model->save_category($this->input->post('category_name', NULL), $this->input->post(NULL), $this->microsite, $update = false);

            if ( $term ) {
                redirect(base_url("/events/edit_category/{$term['term_id']}"));
            }
        }

        switch_to_blog($this->microsite);
        $this->data['categories'] = get_terms(['taxonomy' => 'stellar_event_categories', 'hide_empty' => false]);
        restore_current_blog();
        $this->data['term'] = (object) [
            'term_id' => '',
            'name' => '',
            'slug' => '',
            'taxonomy' => 'stellar_event_categories',
            'description' => '',
            'parent' => '',
        ];

        $this->load->template('microsite/affiliates/events/edit_category', $this->data);

    }

    public function edit_category()
    {
        $this->data['title'] = 'Edit Category';
        $this->form_validation->set_rules(
            'category_name',
            'Category Name',
            array(
                'required',
                array(
                    'term_exists',
                    function($value) {
                        switch_to_blog($this->microsite);
                        $term = term_exists($value, 'stellar_event_categories', $this->input->post('parent'));
                        restore_current_blog();

                        if ( $term !== 0 && $term !== null ) {
                            if ( absint($term['term_id']) !== absint($this->uri->segment(4)) ) {
                                $this->form_validation->set_message('term_exists', 'That category is already taken.');
                                return FALSE;
                            } else {
                                return TRUE;
                            }
                        }

                        return TRUE;
                    }
                )
            ),
            array('required' => 'You must provide a %s.')
        );

        if ($this->form_validation->run() !== false) {

            $old_values = array();
            switch_to_blog($this->microsite);
                $cat_metadata = get_term(absint($this->uri->segment(4)), 'stellar_event_categories');
            restore_current_blog();

            foreach( $cat_metadata as $key => $value ){
                if( $key === 'slug' || $key === 'parent' || $key === 'description' ){
                    $old_values[$key] = $value;
                }
            }

            $term = $this->Events_Model->save_category($this->input->post('category_name', NULL), $this->input->post(NULL), $this->microsite, $update = absint($this->uri->segment(4)));
            
            if ( $term ) {
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$term['term_id']]);
                redirect(base_url("/events/edit_category/{$term['term_id']}"));
            }
        }

        switch_to_blog($this->microsite);
        $this->data['categories'] = get_terms(['taxonomy' => 'stellar_event_categories', 'hide_empty' => false, 'exclude' => $this->uri->segment(4)]);
        restore_current_blog();

        switch_to_blog($this->microsite);
        $this->data['term'] = get_term(absint($this->uri->segment(4)), 'stellar_event_categories');
        restore_current_blog();
        $this->load->template('microsite/affiliates/events/edit_category', $this->data);
    }

    public function orderby()
    {
        $this->index();
    }

}
