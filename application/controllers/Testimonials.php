<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonials extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {

        $this->data['title'] = 'Testimonials';

        if( $this->uri->segment(5) ){
            $this->data['order'] = ($this->uri->segment(5) === 'desc') ? 'asc':'desc';
        } else {
            $this->data['order'] = 'desc';
        }

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count( gss_get_posts( 1, ['post_type'=>'s8_testimonials', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/testimonials/index/{$filter}");
        $this->pagination->initialize($config);

        $this->data['all_testimonials'] = gss_get_posts( 1, ['post_type'=>'s8_testimonials', 'offset'=>$args['paged'], 'post_status'=>'publish', 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
        $this->load->template('microsite/affiliates/testimonials', $this->data);
    }

    public function add()
    {
        $this->data['title'] = 'Add Testimonial';

        if( ! empty($this->input->post(NULL)) ){
            $gss_testimonials = $this->Microsite_Model->gss_testimonials($this->input->post(NULL), 1, $update = false);

            if( $gss_testimonials ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_testimonials]);
                redirect( base_url('testimonials/edit/'.$gss_testimonials) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }

        $this->load->template('microsite/affiliates/testimonials', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Testimonial';

        $old_values = gss_get_post( 1, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){
            $gss_testimonials = $this->Microsite_Model->gss_testimonials($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $gss_testimonials ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_testimonials]);
                redirect( base_url('testimonials/edit/'.$gss_testimonials) );
            } else {
                $this->data['erroe_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {

            $this->data['gss_testimonials'] = gss_get_post( 1, $this->uri->segment(4) );
            $this->load->template('microsite/affiliates/testimonials', $this->data);
        }

    }

    public function orderby()
    {
        $this->index();
    }

}
