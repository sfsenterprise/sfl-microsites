<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Microsites extends SFL_Controller {

	function __construct(){
        parent::__construct();
		require_once ABSPATH . 'wp-admin/includes/ms.php';

        $this->load->model('Microsite_Model');
    }

	public function index($filter = 'all', $paged = 1)
	{
		if ( ( !current_user_can('administrator') || !is_super_admin() ) && (current_user_can('subscriber') || current_user_can('contributor') || current_user_can('author') || current_user_can('editor')) ) {
			//show_403('microsites/index');
		}

		$this->data['title'] = 'Microsites';

		$this->load->library('pagination');

		$config['total_rows'] = count($this->data['sites']);
		$config['per_page'] = 10;
		$config['uri_segment'] = 5;
		$config['base_url'] = base_url("/microsites/index/{$filter}/");
		$this->pagination->initialize($config);

		$microsites = array_chunk($this->data['sites'], $config['per_page']);
		$this->load->template('microsite/index', ['microsites' => $microsites[($paged - 1)]]);
	}

	private function attach_nap($sites)
	{
		if ( ! is_array($sites) ) {
			return;
		}

		foreach($sites as $site) {
			$nap = $this->Microsite_Model->get_microsite( $site->userblog_id );
		}

		return $sites;
	}

	public function add()
	{
		if ( ! current_user_can('level_9') ) {
			show_403('microsites/add');
		}

		$this->data['title'] = 'Add New Microsite';
		
		if ( $this->Microsite_Model->validate_microsite_arr() !== false ) {
			$new_microsite = $this->Microsite_Model->save_microsite();

			update_blog_option($new_microsite['microsite_blog_id'], 'default_pingback_flag', 0);
			update_blog_option($new_microsite['microsite_blog_id'], 'default_ping_status', 0);

			// $this->logs->add(['franchise'=>'', 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array()]);

			if( ! $new_microsite ){
				$this->data['messages']['error'] = 'Something went wrong, microsite not created.';
			} else {
				redirect(base_url("/microsites/edit/{$new_microsite['microsite_blog_id']}/?new=1"));
			}
		}

		$this->data['users'] = get_users( ['blog_id'=>'', 'orderby'=>'email', 'order'=>'ASC', 'fields'=>'all'] );
		$this->load->template('microsite/add', $this->data);
	}

	public function edit($microsite_blog_id = false)
	{
		
		if ( ( !current_user_can('administrator') || !is_super_admin() ) && (current_user_can('subscriber') || current_user_can('contributor') || current_user_can('author') || current_user_can('editor')) ) {
			if ( ! is_user_member_of_blog( get_current_user_id(), $microsite_blog_id ) ) {
			   show_403('microsites/edit');
		   }
		}

		$this->data['title'] = "Edit Microsite";
		$added = $this->input->get('new');
		if ( $added !== null && $added === '1' ) {
			$this->data['messages']['added'] = 'Microsite successfully created.';
		}

		if ( ! $microsite_blog_id ) {
			redirect( base_url('microsites') );
		}

		$microsite = $microsite_blog_id ?: $this->microsite;
		$microsite = get_blog_details($microsite);

		$this->data['title'] .= '<small class="desc">You are editing <strong class="text-primary">' . $microsite->blogname . '</strong></small>';

		if ( $this->Microsite_Model->validate_microsite_arr($microsite_blog_id) !== false ) {
			if ( ( is_super_admin() || is_user_member_of_blog( absint($this->data['user']->ID), absint($microsite_blog_id))) ) {
				if ( !is_super_admin() && !current_user_can('franchisor') && current_user_can('franchisee') && absint($microsite_blog_id) === 1 ) {
					$this->data['error'] = 'You don\'t have sufficient access to update the main site.';
				} else {

					$old_values = $this->Microsite_Model->get_microsite( $microsite_blog_id );
					$this->logs->add(['franchise'=>$microsite_blog_id, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values]);

					$edit_microsite = $this->Microsite_Model->save_microsite($microsite_blog_id);
					$this->data['messages']['updated'] = 'Microsite successfully updated.';

				}
			} else {
				$this->data['messages']['error'] = 'You don\'t have sufficient access to update this microsite.';
			}
		}

		$this->data['edit_microsite'] = $this->Microsite_Model->get_microsite( $microsite_blog_id )?:$this->microsite;
		$this->data['microsite_fields'] = isset($this->data['edit_microsite']->microsite_fields) ? maybe_unserialize($this->data['edit_microsite']->microsite_fields):'';
		$this->data['users'] = get_users( ['blog_id'=>'', 'orderby'=>'email', 'order'=>'ASC', 'fields'=>'all'] );
		$this->load->template('microsite/add', $this->data);
	}

	public function delete($microsite_blog_id)
	{
		if ( ! current_user_can('delete_sites') ) {
			show_403('microsites/delete');
		}

		if ( current_user_can('delete_sites') && absint($microsite_blog_id) === 1) {
			$this->data['error'] = 'You cannot delete the main site.';
			redirect(base_url("/microsites/edit/{$microsite_blog_id}/"));
		} else {

			$old_values = $this->Microsite_Model->get_microsite( $microsite_blog_id );
			$this->logs->add(['franchise'=>$microsite_blog_id, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values]);

			$this->Microsite_Model->delete_microsite($microsite_blog_id);
			redirect(base_url("/microsites/"));
		}
	}

	public function archive()
	{
		$this->db->where('blog_id', $this->uri->segment(4));
        $update = $this->db->update('wp_blogs', ['archived'=>1, 'public'=>0]);

        redirect( base_url("/microsites/edit/".$this->uri->segment(4)) );
	}

	public function unarchive()
	{
		$this->db->where('blog_id', $this->uri->segment(4));
        $update = $this->db->update('wp_blogs', ['archived'=>0, 'public'=>1]);

        redirect( base_url("/microsites/edit/".$this->uri->segment(4)) );
	}

	public function users($filter = null, $paged = 1)
	{
		if ( ! current_user_can('manage_network_users') ) {
			show_403('microsites/users');
		}

		$assignees = array();

		if ( ! empty($this->microsite) ) {
			if ( $filter === 'unassigned' ) {
				// Use to exlude assigned users to microsite if microsite is selected
				$assignees = get_users(['blog_id' => $this->microsite, 'fields' => 'ID']);
			}
		}

		$this->data['title'] = 'Microsite Users';

		if ( isset($this->microsite) ) {
			$this->data['title'] .= '<small class="desc">Assign or remove user to microsite <span class="text-primary"><strong>' . get_blog_details($this->microsite)->blogname . '</strong></span></small>';
		}

		$args = array(
			'blog_id'=> $filter === 'assigned' ? absint($this->microsite) : null,
			'paged' => $paged,
			'number' => 10,
			'exclude' => $assignees,
			'orderby'=>'email',
			'order'=>'ASC',
			'fields'=>'all',
			'count_total' => true
		);

		if ( $search = $this->input->get('search', true) ) {
			$args['search'] = $search;
		}

		$users = new \WP_User_Query( $args );

		$this->load->library('pagination');
		$config['total_rows'] = $users->get_total();
		$config['per_page'] = $args['number'];
		$filter = $filter ?: 'all';
		$config['uri_segment'] = 5;
		$config['base_url'] = base_url("microsites/users/{$filter}/");
		$this->pagination->initialize($config);

		$this->data['users'] = $users->get_results();
		foreach($this->data['users'] as $row => $user) {
			if ( $user->user_login !== 'surefire-admin' ) {
				$roles = $this->Microsite_Model->get_roles($user->ID);
				$this->data['users'][$row]->roles = array_merge($this->data['users'][$row]->roles, $roles);
			}
		}

		$this->load->template('microsite/users', $this->data);

	}

	public function pages($filter = 'all', $paged = 1)
	{
		if ( ! current_user_can('franchisor') ) {
			show_403('microsites/pages');
		}

		$this->data['title'] = 'All Pages';
		$posts_per_page = 10;
		$offset = isset($_GET['pagenum']) ? $_GET['pagenum']:1;

		$this->load->model('microsites/Pages_Model', 'pages');

		if( isset($_GET['search']) && $_GET['search'] !== '' ){
			$search = $this->pages->search($_GET['search'], $offset);
			$this->data['pages'] = $search;
		}
		else{
			$display_pages = $this->pages->get_pages($posts_per_page, $offset, true);
			$this->data['pages'] = $display_pages['pages'];
		}

        $this->load->library('pagination');

        $config['total_rows'] = (isset($search) && $search) ? count($search):$display_pages['rows'];
        $config['per_page'] = $posts_per_page;
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/microsites/pages/");
        $config['reuse_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'pagenum';
        $this->pagination->initialize($config);

        $this->load->template('pages/index', $this->data);
	}

	public function search()
	{
		redirect(base_url('microsites/pages/?search='.$_GET['search']));
	}

	public function editpage($page_id) {
		$this->data['title'] = 'Edit Page';
		$this->load->model('microsites/Pages_Model', 'pages');

		$this->pages->save_page($this->input->post(NULL), $page_id);

		$this->data['page'] = get_post($page_id);
		$this->data['yoast']['_yoast_wpseo_title'] = get_post_meta($page_id, '_yoast_wpseo_title', true);
		$this->data['yoast']['_yoast_wpseo_metadesc'] = get_post_meta($page_id, '_yoast_wpseo_metadesc', true);
		$this->data['yoast']['yoast_wpseo_meta-robots-noindex'] = get_post_meta($page_id, 'yoast_wpseo_meta-robots-noindex', true);
		$this->data['yoast']['yoast_wpseo_meta-robots-nofollow'] = get_post_meta($page_id, 'yoast_wpseo_meta-robots-nofollow', true);
		$this->data['yoast']['_yoast_wpseo_meta-robots-adv'] = get_post_meta($page_id, '_yoast_wpseo_meta-robots-adv', true);
		$this->data['footer'] = get_post_meta( $page_id, '_call_to_action_footer', true );
		$this->data['hero_image_id'] = wp_get_attachment_url( get_post_meta($this->data['page']->ID, '_hero_image_id', true) );

		$this->load->template('pages/edit', $this->data);
	}

	public function addpage()
	{
		$this->data['title'] = 'Add Page';
		$this->data['page'] = (object) array('ID' => '', 'post_title' => '', 'post_content' => '');
		$this->data['footer'] =  array('local' => '', 'corporate' => '');
		$this->data['yoast']['yoast_wpseo_title'] = '';
		$this->data['yoast']['yoast_wpseo_metadesc'] = '';
		$this->data['yoast']['yoast_wpseo_meta-robots-noindex'] = '';
		$this->data['yoast']['yoast_wpseo_meta-robots-nofollow'] = '';
		$this->data['yoast']['_yoast_wpseo_meta-robots-adv'] = '';
		$this->data['hero_image_id'] = wp_get_attachment_url( get_post_meta($this->data['page']->ID, '_hero_image_id', true) );

		$this->load->template('pages/edit', $this->data);
	}

	public function deletepage($page_id)
	{
		wp_delete_post($page_id);
		redirect(base_url("/microsites/pages/"));
	}

	public function userdelete($user_id)
	{
		if ( ! current_user_can('manage_network_users') ) {
			show_403('microsites/users');
		}

		wpmu_delete_user($user_id);
		redirect(base_url("/microsites/users/"));
	}

	public function useradd()
	{
		if ( ! current_user_can('manage_network_users') ) {
			show_403('microsites/users');
		}

		global $wpdb;

		$this->data['title'] = 'Add a microsite user';

		$this->form_validation->set_rules(
			'user[username]',
			'Username',
			'trim|required|is_unique['.$wpdb->prefix.'users.user_login]|alpha_dash',
			array(
				'required' => 'You must provide a %s.',
				'is_unique' => 'The %s is already taken.',
				'alpha_dash' => '%s can only contain lowercase letters (a-z) and numbers.'
			)
		);

		$this->form_validation->set_rules(
			'user[email]',
			'Email',
			'required|is_unique['.$wpdb->prefix.'users.user_email]|valid_email',
			array(
				'required' => 'You must provide a %s.',
				'is_unique' => 'The %s is already registered.',
				'valid_email' => 'The %s you provided is invalid.'
			)
		);

		$this->form_validation->set_rules(
			'user[password]',
			'Password',
			'required|trim',
			array(
				'required' => '%s is required.'
			)
		);
		$this->form_validation->set_rules(
			'user[cpassword]',
			'Confirm password',
			'trim|matches[user[password]]',
			array(
				'matches' => '%s did not match the password.'
			)
		);

		if ($this->form_validation->run() !== false) {
			$this->load->model('microsites/Users_Model', 'micrositesUsers');
			if ( $user_id = $this->micrositesUsers->save_user() ) {
				redirect(base_url("/microsites/useredit/{$user_id}"));
			}
		}

		$this->data['select'] = array(
			'su' => '',
			'none' => '',
			'franchisor' => '',
			'franchisee' => ''
		);


		$this->load->template('microsite/useradd', $this->data);
	}

	public function useredit($user_id)
	{
		if ( ! current_user_can('manage_network_users') ) {
			show_403('microsites/users');
		}

		if ( ! $user_id ) {
			redirect(base_url("/microsites/users/"));
		}

		if ( ! $this->data['edit_user'] = get_userdata($user_id) ) {
			redirect(base_url("/microsites/users/"));
		}

		global $wpdb;
		$this->load->model('microsites/Users_Model', 'micrositesUsers');

		$this->form_validation->set_rules(
			'user[email]',
			'Email',
			array(
				'required',
				'valid_email',
				array('is_unique_email', array($this->micrositesUsers, 'valid_email'))
			),
			array(
				'required' => 'You must provide a %s.',
				'valid_email' => 'The %s you provided is invalid.',
				'is_unique_email' => 'The %s is already registered.'
			)
		);

		if ( $this->input->post('user[password]') || $this->input->post('user[cpassword]') ) {
			$this->form_validation->set_rules(
				'user[password]',
				'Password',
				'required|trim',
				array(
					'required' => '%s is required.'
				)
			);
			$this->form_validation->set_rules(
				'user[cpassword]',
				'Confirm password',
				'trim|matches[user[password]]',
				array(
					'matches' => '%s did not match the password.'
				)
			);
		}

		if ($this->form_validation->run() !== false) {
			if ( $user_id = $this->micrositesUsers->save_user($user_id) ) {
				redirect(base_url("/microsites/useredit/{$user_id}/"));
			}
		}

		$this->data['title'] = "Microsite user edit.";
		$this->data['title'] .= '<span class="desc">You are editing <strong class="text-primary">' . $this->data['edit_user']->user_login . '</strong></span>';

		$this->data['select'] = array(
			'su' => '',
			'none' => '',
			'franchisor' => '',
			'franchisee' => ''
		);

		if ( is_super_admin($user_id) ) {
			$this->data['select']['su'] = true;
		} elseif ( user_can($user_id, 'franchisee') ) {
			$this->data['select']['franchisee'] = true;
		} elseif ( user_can($user_id, 'franchisor') ) {
			$this->data['select']['franchisor'] = true;
		} else {
			$this->data['select']['none'] = true;
		}

		$this->load->template('microsite/useradd', $this->data);
	}

	public function assignuser($blog_id, $user_id)
	{
		if ( user_can($user_id, 'subcriber') ) {
			$role = 'editor';
		} elseif ( user_can($user_id, 'franchisee') ) {
			$role = 'franchisee';
		} elseif ( user_can($user_id, 'franchisor') ) {
			$role = 'franchisor';
		} elseif ( is_super_admin($user_id) ) {
			$role = 'administrator';
		} else {
			$role = 'subscriber';
		}

		add_user_to_blog($blog_id, $user_id, $role);
		redirect($this->agent->referrer());
	}

	public function unassignuser($blog_id, $user_id) {
		remove_user_from_blog($user_id, $blog_id);
		redirect($this->agent->referrer());
	}

	public function user_edit()
	{
		if( ! $this->microsite )
            $this->load->view('microsite/affiliates/users');

        if( ! empty($this->input->post(NULL, true)) ){
            $add_users = $this->Microsite_Model->add_users($this->input->post(NULL, true), $this->microsite);
            $this->microsite = $this->microsite;
            //$this->load->template('microsite/affiliates/users', $data);

        } else {
            $this->microsite = $this->microsite;
            $this->load->template('microsite/affiliates/users', $this->data);
            redirect( base_url('microsites/users') );
        }
	}

	public function settings()
	{

		// microsite settings
		$this->data['title'] = 'Microsite Settings';

		$get_footer_logos = get_blog_option( $this->microsite, 'footer_logos');

		switch_to_blog( $this->microsite );
			$this->data['_hero_image'] = wp_get_attachment_url( gss_get_post_meta($this->microsite, get_blog_option($this->microsite, 'page_on_front', true), '_hero_image_id') );
		restore_current_blog();

		$cpanel_page = get_page_by_path('surefire', OBJECT, 'page');
		$this->data['footer_logos'] = maybe_unserialize($get_footer_logos);
		$this->data['pages'] = gss_get_pages(1, ['post_type'=>'page', 'post_status'=>array('publish', 'draft'), 'exclude'=>array($cpanel_page->ID)]);

		$this->load->template('settings/microsite/index', $this->data);
	}

	public function sidebars()
	{
		global $current_site;

        if( $this->microsite && is_super_admin() === FALSE && ! current_user_can('franchisor') )
            redirect( base_url('microsites/sidebars') );

        $this->data['title'] = 'Sidebar Management';

        $this->data['all_sidebars'] = gss_get_posts( 1, ['post_type'=>'gss_sidebars', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1]);

        // for franchisor settings
        $this->data['sidebar_included_microsite'] = get_option('gss_sidebar_included_microsite', array());

        $this->data['main_site'] = $current_site;

        $this->load->template('settings/general/sidebar/index', $this->data);
	}

	public function site()
	{
		$microsite = $this->input->post('microsite');
		$this->input->set_cookie('selected_microsite', absint($microsite), time() + ( 14 * DAY_IN_SECONDS ) + ( 12 * HOUR_IN_SECONDS ));
		redirect( base_url() );
		exit;
	}

}
