<?php

use Surefire\Microsites\Lib\Bootstrap;

defined('BASEPATH') OR exit('No direct script access allowed');

class Setup extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // surefireadmin
        // 2mn8Y4Zsg

        $this->load->model('Setup_Model', 'setup');

        if ( $theme = get_stylesheet() !== 'surepress' ) {
            switch_theme('surepress');
        }

        if ( ! current_user_can('manage_options') ) {
            wp_die(
                '<h1>Surefire Local &rsaquo; Surepress &rsaquo; Setup</h1>
                <p>Site is currently under the setup process.</p>
                <p>Please come back later.</p>
                <p>Thank you!</p>
                <p><a href="https://surefirelocal.com/">Surefire Local Marketing Cloud&trade;</a></p>',
                __('sfl-microsites plugin required.', 'surepress')
            );
        }

        $this->setup->which_setup_page();
    }

    public function index()
    {
        $this->form_validation->set_rules(
            'password',
            'New Password',
            'trim|required',
            array(
                'required' => 'Please provide a password.'
            )
        );
        $this->form_validation->set_rules(
            'cpassword',
            'Confirm Password',
            'trim|required|matches[password]',
            array(
                'required' => 'Please provide a password.',
            )
        );

        if ( $this->form_validation->run() !== FALSE ) {
            $this->setup->modify_super_admin(1);

            $this->update_socialmedia();

            $this->setup->microsites();

            $blog = get_page_by_path('blog');
            update_option('page_for_posts', $blog->ID);
            $home = get_page_by_path('home');
            update_option('page_on_front', $home->ID);
            update_option('show_on_front', 'page');
            update_post_meta($home->ID, '_wp_page_template', 'templates/template-home.php');

            update_option('permalink_structure', '/blog/%postname%/');
            update_option('_sfmu_setup', 2);

            flush_rewrite_rules();

            redirect(base_url('/setup/step_2/'));
            exit;
        }

        $this->load->view('setup/header');
        $this->load->view('setup/index');
        $this->load->view('setup/footer');
    }

    public function step_2() {
        if ( $this->input->post('next_3') ) {

            $pages = $this->input->post('pages') ?: array();

            foreach( $this->input->post('defaults') as $default ) {
                if ( !in_array($default, $pages) ) $pages[] = $default;
            }

            foreach( $this->input->post('pages') as $page_id ) {
                update_post_meta($page_id, '_sfmu_default_page', 'yes');
            }

            update_option('_sfmu_setup', 3);

            redirect(base_url('/setup/step_3/'));
            exit;
        }

        $this->load->view('setup/header');
        $this->load->view('setup/step_2', ['pages' =>  get_pages(), 'defaults' => array(
            'home',
            'about',
            'contact',
            'our-facility',
            'our-story',
            'fees',
            'gift-certificates',
            'pre-reg-thank-you',
            'privacy',
            'programs',
            'additional-programs',
            'family-swim',
            'jump-start-clinics',
            'parties',
            'perpetual-lessons',
            'playdate-at-the-pool',
            'swim-force',
            'swim-lesson-levels',
            'technique-clinics',
            'water-safety',
            'thank-you',
        )]);
        $this->load->view('setup/footer');
    }

    public function step_3()
    {
        if ( $this->input->post('next_4') ) {

            $pages = $this->input->post('pages') ?: array();
            $post_status = $this->input->post('post_status') ?: array();

            if ( $pages ) {
                foreach( $pages as $row => $page_id ) {
                    if ( $post_status[$row] !== 'publish' )
                        wp_update_post(['ID' => $page_id, 'post_status' => 'publish']);

                    update_post_meta($page_id, '_sfmu_default_page', 'yes');
                }
            }

            $cpt_forms = get_posts(['post_type' => 'ff_forms', 'post_status' => 'any', 'posts_per_page' => -1]);

            foreach( $cpt_forms as $cpt_form ) {
                if ( strpos($cpt_form->post_content, '[gravityform') !== FALSE ) {
                    $content = preg_replace('/(^\[gravityform.*?\])/', '', $cpt_form->post_content);
                    wp_update_post([
                        'ID' => $cpt_form->ID,
                        'post_content' => $content,
                    ]);
                }
            }

            update_option('_sfmu_setup', 4);

            redirect(base_url('/setup/step_4/'));
            exit;
        }

        switch_to_blog(2);
        $siteforms = get_posts(['post_type' => 'ff_forms', 'post_status' => 'any', 'posts_per_page' => -1]);
        restore_current_blog();

        $add_to_global_forms = array(
            'inclement-weather-policy' => 'Inclement Weather Policy',
            'party-waiver' => 'Party Waiver',
            'family-swim-waiver' => 'Family Swim Waiver',
            'policies-document' => 'Policies Document'
        );

        foreach( $siteforms as $siteform ) {
            if ( array_key_exists($siteform->post_name, $add_to_global_forms) ) {
                $new = get_page_by_path($siteform->post_name, OBJECT, 'ff_forms');
                if ( ! $new ) {
                    wp_insert_post(['post_type' => $siteform->post_type, 'post_content' => $siteform->post_content, 'post_title' => $siteform->post_title, 'post_name' => $siteform->post_name]);
                }
            }
        }

        $this->load->view('setup/header');
        $this->load->view('setup/step_3', ['pages' =>  get_posts(['post_type' => 'ff_forms', 'post_status' => 'any', 'posts_per_page' => -1])]);
        $this->load->view('setup/footer');
    }

    public function step_4()
    {

        if ( $this->input->post('next_5') ) {
            $forms = $this->db->get_where('wp_rg_form', ['is_trash' => 0])->result();

            $form_map = array(
                1 => 'newsletter',
                2 => 'general_contact_form',
                6 => 'refer_a_friend_form',
                8 => 'cancel_form',
                11 => 'register_form',
                12 => 'pre_registration_form',
                13 => 'pre_employment_application_form',
                14 => 'holiday_form',
            );

            $new_forms = array();
            foreach( $forms as $form ) {
                if ( !in_array(absint($form->id), [3,4,5,7]) ) {
                    if ( ! get_page_by_title($form->title, OBJECT, 'wpcf7_contact_form') ) {
                        $args = ['post_type' => 'wpcf7_contact_form', 'post_title' => $form->title, 'post_status' => 'publish'];
                        if ( absint($form->is_trash) > 0 ) $args['post_status'] = 'draft';

                        if ( file_exists( $file = SFL_MICROSITES_DIR . "lib/forms/{$form_map[absint($form->id)]}/{$form_map[absint($form->id)]}.php" ) ) {
                            ob_start();
                            include $file;
                            $fields = ob_get_clean();
                            $json_file = json_decode(file_get_contents(SFL_MICROSITES_DIR . "lib/forms/{$form_map[absint($form->id)]}/{$form_map[absint($form->id)]}.json"), true);
                            $json_file['_form'] = $fields;
                            $args['meta_input'] = $json_file;
                        }
                        $new_forms[absint($form->id)] = wp_insert_post($args);
                    }
                }
            }

            if ( ! get_page_by_title('Contest Form', OBJECT, 'wpcf7_contact_form') ) {
                $contest_args = ['post_type' => 'wpcf7_contact_form', 'post_title' => 'Contest Form', 'post_status' => 'publish'];
                if ( file_exists( $file = SFL_MICROSITES_DIR . "lib/forms/contest_form/contest_form.php" ) ) {

                    ob_start();
                    include $file;
                    $fields = ob_get_clean();

                    $json_file = json_decode(file_get_contents(SFL_MICROSITES_DIR . "lib/forms/contest_form/contest_form.json"), true);
                    $json_file['_form'] = $fields;
                    $contest_args['meta_input'] = $json_file;

                }
                wp_insert_post($contest_args);
            }

            if ( ! get_page_by_title('Creative Request Form', OBJECT, 'wpcf7_contact_form') ) {
                $creative_args = ['post_type' => 'wpcf7_contact_form', 'post_title' => 'Creative Request Form', 'post_status' => 'publish'];
                if ( file_exists( $file = SFL_MICROSITES_DIR . "lib/forms/creative_request_form/creative_request_form.php" ) ) {

                    ob_start();
                    include $file;
                    $fields = ob_get_clean();

                    $json_file = json_decode(file_get_contents(SFL_MICROSITES_DIR . "lib/forms/creative_request_form/creative_request_form.json"), true);
                    $json_file['_form'] = $fields;
                    $creative_args['meta_input'] = $json_file;
                }
                wp_insert_post($creative_args);

            }

            $form_to_site = array(
                3354 => 14,
                3157 => 13,
                3012 => 11,
                2893 => 8,
                246 => 6,
                245 => 12,
                239 => 2,
            );
            $siteforms = get_posts(['post_type' => 'ff_forms', 'post_status' => 'any', 'posts_per_page' => -1, 'include' => array(3354, 3157, 3012, 2893, 246, 245, 239)]);

            foreach( $siteforms as $siteform ) {

                update_post_meta($siteform->ID, 'form_shortcode', $new_forms[$form_to_site[$siteform->ID]]);

                if ( $siteform->ID === 3354 ) {
                    update_post_meta($siteform->ID, 'form_position', '1');
                } else {
                    update_post_meta($siteform->ID, 'form_position', '2');
                }
            }

            update_option('_sfmu_setup', 5);
            redirect(base_url('/setup/step_5/'));
            exit;
        }
        $this->load->view('setup/header');
        $this->load->view('setup/step_4');
        $this->load->view('setup/footer');
    }

    public function step_5()
    {
        if ( $this->input->post('next_6') ) {
            $primarynav = get_term_by('name', 'Primary Menu', 'nav_menu');
            $footernav = get_term_by('name', 'Footer Menu', 'nav_menu');
            $locations = array();
            $navmenu = get_registered_nav_menus();

            foreach( $navmenu as $nav_id => $nav_name) {
                if ( $nav_id === 'primary_navigation' ) {
                    $locations[$nav_id] = $primarynav->term_id;
                } else if ( $nav_id === 'footer_navigation' ) {
                    $locations[$nav_id] = $footernav->term_id;
                }
            }

            set_theme_mod('nav_menu_locations', $locations);

            $nav_menu_object = wp_get_nav_menu_object('primary-menu');
            $nav_menu_items = wp_get_nav_menu_items('primary-menu');

            $about_id = null;
            foreach( $nav_menu_items as $item ) {
                if ( $item->post_name === 'about') $about_id = $item->ID;
            }

            $nav_menu_object = wp_get_nav_menu_object('primary-menu');
            $tour_id = wp_update_nav_menu_item($nav_menu_object->term_id, 0, array(
                'menu-item-title' =>  __('Take a Virtual Tour'),
                'menu-item-url' => home_url( '/tour' ),
                'menu-item-type' => 'custom',
                'menu-item-position' => 14,
                'menu-item-parent-id' => $about_id,
                'menu-item-status' => 'publish')
            );

            $nav_menu_items = wp_get_nav_menu_items('primary-menu');
            foreach($nav_menu_items as $item) {
                if ( $item->post_name === 'testimonials' || $item->ID === $tour_id ) {
                    $defaults = array(
                        'menu-item-db-id' => $item->db_id,
                        'menu-item-object-id' => $item->object_id,
                        'menu-item-object' => $item->object,
                        'menu-item-parent-id' => $item->menu_item_parent,
                        'menu-item-position' => 14,
                        'menu-item-type' => 'custom',
                        'menu-item-title' => $item->title,
                        'menu-item-url' => $item->url,
                        'menu-item-description' => $item->description,
                        'menu-item-attr-title' => $item->attr_title,
                        'menu-item-target' => $item->target,
                        'menu-item-classes' => implode(' ', $item->classes),
                        'menu-item-xfn' => $item->xfn,
                        'menu-item-status' => $item->post_status,
                    );

                    if ( $item->post_name === 'testimonials' ) $defaults['menu-item-position'] = 15;

                    wp_update_nav_menu_item($nav_menu_object->term_id, $item->db_id, $defaults);
                }
            }

            wp_update_nav_menu_item($nav_menu_object->term_id, 0, array(
                'menu-item-title' =>  __('Our Team'),
                'menu-item-url' => home_url( '/staff' ),
                'menu-item-type' => 'custom',
                'menu-item-position' => 13,
                'menu-item-parent-id' => $about_id,
                'menu-item-status' => 'publish')
            );

            wp_update_nav_menu_item($nav_menu_object->term_id, 0, array(
                'menu-item-title' =>  __('Events'),
                'menu-item-url' => home_url( '/events' ),
                'menu-item-position' => 9,
                'menu-item-status' => 'publish')
            );

            update_option('_sfmu_setup', 6);
            redirect(base_url('/setup/step_6/'));
            exit;
        }

        $this->load->view('setup/header');
        $this->load->view('setup/step_5');
        $this->load->view('setup/footer');
    }

    public function step_6()
    {
        if ( $this->input->post('next_7') ) {
            global $wpdb;

            $ff_news_taxonomy = $this->db->where('taxonomy', 'ff_news_taxonomy')->get($wpdb->term_taxonomy);
            if ( ! $ff_news_taxonomy->num_rows() ) return;

            set_time_limit(0);

            $categories = get_categories(['hide_empty' => false, 'exclude' => 1]);
            foreach ( $categories as $cat ) {
                wp_delete_term($cat->term_id, $cat->taxonomy);
            }

            $post_tags = get_terms(['taxonomy' => 'post_tag', 'hide_empty' => false, 'count' => true]);
            foreach( $post_tags as $tag ) {
                wp_delete_term($tag->term_id, $tag->taxonomy);
            }

            wp_delete_term(175, 'ff_news_taxonomy');

            $this->db->delete($wpdb->term_relationships, ['term_taxonomy_id' => 175]);
            $this->db->where('taxonomy', 'ff_news_taxonomy')->update($wpdb->term_taxonomy, ['taxonomy' => 'category']);
            $this->db->where('taxonomy', 'ff_news_tag')->update($wpdb->term_taxonomy, ['taxonomy' => 'post_tag']);
            $this->db->where('post_type', 'ff_news')->update($wpdb->posts, ['post_type' => 'post']);

            update_option('_sfmu_setup', 7);
            redirect(base_url('/setup/step_7/'));
            exit;
        }

        $this->load->view('setup/header');
        $this->load->view('setup/step_6');
        $this->load->view('setup/footer');
    }

    public function step_7()
    {
        if ( $this->input->post('next_8') ) {
            global $wpdb;
            $this->db->where('post_type', 's8_dp_ads')->update($wpdb->posts, ['post_type' => 'gss_sidebars']);

            if ( get_post(259) )  wp_delete_post(259, true);
            if ( get_post(153) )  wp_delete_post(153, true);
            if ( get_post(119) )  wp_delete_post(119, true);

            $sidebars = get_posts(['post_type' => 'gss_sidebars', 'posts_per_page' => -1]);

            foreach( $sidebars as $sidebar ) {
                if ( $sidebar->post_name === 'testimonials-circle' ) {
                    $content = '<a href="/testimonials"><img src="/wp-content/uploads/2015/09/RightColumnTestimonials-300x300.jpg" alt="" width="300" height="300" class="alignnone size-medium wp-image-2858" /></a>';
                    wp_update_post(['ID' => $sidebar->ID, 'post_content' => $content]);
                    update_post_meta($sidebar->ID, 'sidebar_hidetitle', '1');
                } elseif ( $sidebar->post_name === 'pre-register-circle' ) {
                    $content = '<a href="[microsite homeurl]/forms/pre-registration-form/"><img class="alignnone size-medium wp-image-2859 img-fluid" src="/wp-content/uploads/2015/09/RightColumn_PreReg-300x300.jpg" alt="" width="300" height="300" /></a>';
                    wp_update_post(['ID' => $sidebar->ID, 'post_content' => $content]);
                    update_post_meta($sidebar->ID, 'sidebar_hidetitle', '1');
                } elseif ( $sidebar->post_name === 'find-a-location-circle' ) {
                    $content = '<a href="/locations"><img class="alignnone size-medium wp-image-2856" src="/wp-content/uploads/2015/09/RightColumnFindLocation-300x300.jpg" alt="" width="300" height="300" /></a>';
                    wp_update_post(['ID' => $sidebar->ID, 'post_content' => $content]);
                    update_post_meta($sidebar->ID, 'sidebar_hidetitle', '1');
                } elseif ( $sidebar->post_name === 'refer-a-friend-circle' ) {
                    $content = '<a href="[microsite homeurl]/forms/refer-a-friend"><img class="alignnone size-medium wp-image-2860 img-fluid" src="/wp-content/uploads/2015/09/RightColumnRefer-300x300.jpg" alt="" width="300" height="300" /></a>';
                    wp_update_post(['ID' => $sidebar->ID, 'post_content' => $content]);
                    update_post_meta($sidebar->ID, 'sidebar_hidetitle', '1');
                } elseif ( $sidebar->post_name === 'join-our-crew' ) {
                    update_post_meta($sidebar->ID, 'sidebar_hidetitle', '1');
                } elseif ( $sidebar->post_name === 'contact-information' ) {
                    $content = '<dl class="contact-information">'.
                        '<dt>PHONE</dt><dd><a href="tel:[microsite phone]" class="location_phone_number">[microsite phone]</a></dd>'.
                        '<dt>EMAIL</dt><dd><a class="email" href="mailto:[microsite email]">[microsite email]</a></dd>'.
                        '<dt>ADDRESS</dt><dd><a class="address" href="https://maps.google.com/?daddr=[microsite street] [microsite city] [microsite state] [microsite zip]" target="_blank"><span>[microsite address]</span></a></dd>'.
                        '<dt>GENERAL MANAGER</dt><dd>[microsite manager]</dd>'.
                        '</dl>';
                    wp_update_post(['ID' => $sidebar->ID, 'post_content' => $content]);

                }
            }

            if ( ! get_page_by_title('Get Started at Goldfish!', OBJECT, 'gss_sidebars') ) {
                $content = '<p>Ready to book?</p>'.
                    '<a class="button orange" href="[microsite homeurl]/forms/registration-form/">Register Now</a>'.
                    '<p>Need more information?</p>'.
                    '<a class="button orange" href="[microsite homeurl]/forms/pre-registration-form">Pre-register Now</a>'.
                    '<hr>'.
                    '<a href="#microsite-newsletter" class="button blue" data-toggle="modal" >Newsletter Sign Up</a>'.
                    '<a class="button blue" href="[microsite homeurl]/forms/single-class-cancellation/">Single Class Cancellation</a>'.
                    '<a class="button light-blue" href="[microsite homeurl]/jobs">Join Our Crew</a> <a class="button orange" href="#">Member Login</a>';
                wp_insert_post(['post_type' => 'gss_sidebars', 'post_title' => 'Get Started at Goldfish!', 'post_content' => $content, 'post_status' => 'publish']);
            }

            if ( ! get_page_by_title('Our Map', OBJECT, 'gss_sidebars') ) {
                $content = '[sidebar map]';
                wp_insert_post(['post_type' => 'gss_sidebars', 'post_title' => 'Our Map', 'post_content' => $content, 'post_status' => 'publish']);
            }
            $include_to_microsite = array();
            $sidebars = $this->db->select(['ID', 'post_name'])->where('post_type', 'gss_sidebars')->get($wpdb->posts);

            $sidebarmain = array();
            $homepage = array();
            $frontend = array();
            foreach( $sidebars->result() as $sidebar ) {
                $include_to_microsite[] = $sidebar->ID;

                if ( in_array($sidebar->post_name, ['get-started-at-goldfish', 'our-map', 'hours-of-operation', 'contact-information']) ) {

                    if ( $sidebar->post_name === 'get-started-at-goldfish' ) {
                        $homepage[0] = $sidebar->ID;
                    } else if ( $sidebar->post_name === 'our-map' ) {
                        $homepage[1] = $sidebar->ID;
                    } else if ( $sidebar->post_name === 'hours-of-operation' ) {
                        $homepage[2] = $sidebar->ID;
                    } else if ( $sidebar->post_name === 'contact-information' ) {
                        $homepage[3] = $sidebar->ID;
                    }
                }

                if ( in_array($sidebar->post_name, ['pre-register-circle', 'refer-a-friend-circle', 'hours-of-operation', 'location-contact-info']) ) {

                    if ( $sidebar->post_name === 'pre-register-circle' ) {
                        $frontend[0] = $sidebar->ID;
                    } else if ( $sidebar->post_name === 'refer-a-friend-circle' ) {
                        $frontend[1] = $sidebar->ID;
                    } else if ( $sidebar->post_name === 'hours-of-operation' ) {
                        $frontend[2] = $sidebar->ID;
                    } else if ( $sidebar->post_name === 'contact-information' ) {
                        $frontend[3] = $sidebar->ID;
                    }
                }

                if ( $sidebar->post_name === 'find-a-location-circle' ) {
                    $sidebarmain[0] = $sidebar->ID;
                } else if ( $sidebar->post_name === 'testimonials-circle' ) {
                    $sidebarmain[1] = $sidebar->ID;
                }

            }
            ksort($sidebarmain);
            ksort($homepage);
            ksort($frontend);

            update_option('gss_sidebar_included_microsite', $include_to_microsite);
            update_option('gss_sidebar', $include_to_microsite);
            update_option('gss_sidebar_homepage', $sidebarmain);
            update_option('gss_sidebar_frontend', $sidebarmain);

            foreach( get_sites(['site__not_in' => 1]) as $site ) {
                update_blog_option($site->blog_id, 'gss_sidebar_homepage', $homepage);
                update_blog_option($site->blog_id, 'gss_sidebar_frontend', $frontend);
            }

            update_option('_sfmu_setup', 8);
            redirect(base_url('/setup/step_8/'));
            exit;
        }

        $this->load->view('setup/header');
        $this->load->view('setup/step_7');
        $this->load->view('setup/footer');
    }

    /**
    * Testimonials convert from images to text
    */
    public function step_8()
    {
        if ( $this->input->post('next_9') ) {
            $this->setup->convert_testimonials();


            update_option('_sfmu_setup', 9);
            redirect(base_url('/setup/step_9/'));
            exit;
        }

        $this->load->view('setup/header');
        $this->load->view('setup/step_8');
        $this->load->view('setup/footer');
    }

    /**
    * Microsite Form Notification Settings
    */
    public function step_9()
    {
        if ( $this->input->post('next_10') ) {
            remove_action('pre_get_posts',  [Bootstrap::class, '_sfmu_update_global_page']);
            $this->setup->microsite_form_settings();
            add_action('pre_get_posts',  [Bootstrap::class, '_sfmu_update_global_page'], 99, 1);

            update_option('_sfmu_setup', 10);
            redirect(base_url('/setup/step_10/'));
            exit;
        }
        $this->load->view('setup/header');
        $this->load->view('setup/step_9');
        $this->load->view('setup/footer');
    }

    /**
    * Microsite Global Pages
    */
    public function step_10()
    {
        if ( $this->input->post('next_11') ) {
            // remove_action('pre_get_posts',  [Bootstrap::class, '_sfmu_update_global_page']);
            $this->setup->microsite_global_pages();
            // add_action('pre_get_posts',  [Bootstrap::class, '_sfmu_update_global_page'], 99, 1);

            update_option('_sfmu_setup', 0);
            redirect(base_url('/setup/done/'));
            // redirect(base_url('/setup/step_11/'));
            exit;
        }
        $this->load->view('setup/header');
        $this->load->view('setup/step_10');
        $this->load->view('setup/footer');
    }

    public function done()
    {
        $this->load->view('setup/header');
        $this->load->view('setup/done');
        $this->load->view('setup/footer');
    }

    public function special_socialmedia()
    {
        $this->update_socialmedia();
        $this->load->view('setup/done');
    }

    private function update_socialmedia()
    {
        $new_value = array(
            'facebook' => get_site_option('s8theme_facebook_url', ''),
            'twitter' => get_site_option('s8theme_twitter_url', ''),
            'yelp' => get_site_option('s8theme_yelp_url', ''),
            'instagram' => get_site_option('s8theme_instagram_url', ''),
            'youtube' => get_site_option('s8theme_youtube_url', ''),
            'google-plus' => get_site_option('s8theme_google_url', ''),
            'pinterest' => get_site_option('s8theme_pinterest_url', ''),
            'linkedin' => get_site_option('s8theme_linkedin_url', ''),
            'tumblr' => '',
        );

        foreach( $new_value as $media => $value ) {
            if ( strpos($value, 'http') !== FALSE ) {
                $new_value[$media] = str_replace('http://', 'https://', $value);
            } else {
                $new_value[$media] = $value ? 'https://' . (str_replace('//', '', $value)) : '';
            }
        }

        update_option('microsite_socialmedia', $new_value);
    }
}
