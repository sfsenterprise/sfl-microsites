<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends SFL_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Pages_Local_Model', 'localpages');
        $this->load->model('microsites/Pages_Model', 'pages');

        $this->localpages->_recheck_globals();
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Pages';
        switch_to_blog($this->microsite);
        $display_pages = $this->pages->get_pages($posts_per_page = 10, $paged);
        restore_current_blog();

        $this->data['pages'] = $display_pages['pages'];

        $this->load->library('pagination');

        $config['total_rows'] = $display_pages['rows'];

        $config['per_page'] = $posts_per_page;
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/pages/index/{$filter}/");
        $this->pagination->initialize($config);

        $this->load->template('pages/index', $this->data);
    }

    public function add()
    {
        if( ! empty($this->input->post(NULL)) ){
            $page = $this->pages->save_page($this->input->post(NULL));

            if( $page ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>'New', 'new_values'=>$page]);
                redirect( base_url( 'microsites/editpage/'.$page) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }
    }

    public function edit()
    {

        $old_values = gss_get_post( 1, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) ){
            $page = $this->pages->save_page($this->input->post(NULL), $this->uri->segment(4));

            if( $page ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$page]);
                redirect( base_url( 'microsites/editpage/'.$page) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }
    }


}
