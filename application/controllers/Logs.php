<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logs extends SFL_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index($filter = 'all', $paged = 1)
    {
        if ( ! is_super_admin() ) {
            show_403('logs/index');
        }
        
        $this->data['title'] = 'Audit Logs';

        $args = array(
            'number'        => 10,
            'paged'         => isset($_GET['pagenum']) ? ( ($_GET['pagenum'] - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count( $this->logs->all_logs() );
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 4;
        $config['base_url'] = base_url("/logs/{$this->uri->segment(3)}/");
        
        $config['reuse_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'pagenum';
        $this->pagination->initialize($config);

        $this->data['all_logs'] = $this->logs->all_logs($args['number'], $args['paged']);

        $this->load->template('logs/index', $this->data);
    }

    public function filter()
    {
        $this->data['filterby'] = $this->input->get('filterby');
        $this->data['filter'] = $this->input->get('filter');

        $this->index();
    }

}