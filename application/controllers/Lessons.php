<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lessons extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Lessons_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'Swim School Lessons';

        if( $this->uri->segment(5) ){
            $this->data['order'] = ($this->uri->segment(5) === 'desc') ? 'asc':'desc';
        } else {
            $this->data['order'] = 'desc';
        }

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
        );

        $this->load->library('pagination');
        $config['total_rows'] = count( gss_get_posts(1, ['post_type'=>'gss_swim_lessons', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/lessons/index/{$filter}");
        $this->pagination->initialize($config);

        $this->data['all_lessons'] = gss_get_posts(1, ['post_type'=>'gss_swim_lessons', 'offset'=>$args['paged'], 'post_status'=>'publish', 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
        $this->data['all_groups'] = gss_get_categories(1, [ 'type' => 'gss_swim_lessons', 'taxonomy' => 'gss_swim_lesson_groups' ] );
        $this->load->template('microsite/affiliates/lessons/lessons', $this->data);
    }

    public function add()
    {
        $this->data['title'] = 'Add New Lesson';
        if( ! empty($this->input->post(NULL)) ){
            $gss_lessons = $this->Lessons_Model->gss_lessons($this->input->post(NULL), 1, $update = false);

            if( $gss_lessons )
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_lessons]);
                wp_redirect( base_url(sprintf('lessons/edit/%1$s', $gss_lessons)) );
        }

        $this->data['post_cats'] = gss_wp_get_post_categories(1, $this->uri->segment(4), 'gss_swim_lesson_groups');
        $this->data['categories'] = gss_get_categories( 1, [ 'type' => 'gss_swim_lessons', 'taxonomy' => 'gss_swim_lesson_groups' ] );
        $this->load->template('microsite/affiliates/lessons/lessons', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Lesson';

        $old_values = array();
        $post = gss_get_post(1, $this->uri->segment(4));
        $old_values['age'] = gss_get_post_meta( 1, $this->uri->segment(4), 'lesson_age' );
        $old_values['details'] = gss_get_post_meta( 1, $this->uri->segment(4), 'lesson_details' );
        $old_values['requirements'] = gss_get_post_meta( 1, $this->uri->segment(4), 'lesson_requirements' );
        $old_values['title'] = $post->post_title;

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $gss_lessons = $this->Lessons_Model->gss_lessons($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $gss_lessons ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_lessons]);
                redirect( base_url('lessons/edit/'.$gss_lessons) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {
            $this->data['post_cats'] = gss_wp_get_post_categories(1, $this->uri->segment(4), 'gss_swim_lesson_groups');
            $this->data['gss_lesson'] = gss_get_post( 1, $this->uri->segment(4) );
            $this->data['categories'] = gss_get_categories( 1, [ 'type' => 'gss_swim_lessons', 'taxonomy' => 'gss_swim_lesson_groups' ] );
            $this->load->template('microsite/affiliates/lessons/lessons', $this->data);
        }
    }

    public function group()
    {
        $this->data['title'] = 'All Lessons Group';
        $this->data['all_groups'] = gss_get_categories( 1, [ 'type' => 'gss_swim_lessons', 'taxonomy' => 'gss_swim_lesson_groups' ] );
        $this->load->template('microsite/affiliates/lessons/lessons-group', $this->data);
    }

    public function add_group()
    {
        $this->data['title'] = 'Add Group';
        if( ! empty( $new_group = $this->input->post(NULL)) ){
            unset($new_group['gss_lesson_group']);

            $gss_lesson_group = $this->Lessons_Model->gss_lesson_group($new_group, 1, $update = false);

            if( $gss_lesson_group ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_lesson_group['term_id'] ]);
                redirect( base_url('lessons/edit_group/'.$gss_lesson_group['term_id']) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        }

        $this->data['categories'] = gss_get_categories( 1, [ 'type' => 'gss_swim_lessons', 'taxonomy' => 'gss_swim_lesson_groups' ] );
        $this->load->template('microsite/affiliates/lessons/lessons-group', $this->data);
    }

    public function edit_group()
    {

        $this->data['title'] = 'Edit Group';

        if( ! empty( $new_group = $this->input->post(NULL)) && $this->uri->segment(4) ){
            unset($new_group['gss_lesson_group']);

            $old_values = gss_get_term( 1, $this->uri->segment(4) );

            $gss_lesson_group = $this->Lessons_Model->gss_lesson_group($new_group, 1, $this->uri->segment(4));

            if( $gss_lesson_group ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_lesson_group['term_id'] ]);
                redirect( base_url('lessons/edit_group/'.$gss_lesson_group['term_id']) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }

        $this->data['term_order'] = $this->Lessons_Model->get_term_order($this->uri->segment(4));
        $this->data['categories'] = gss_get_categories( 1, [ 'type' => 'gss_swim_lessons', 'taxonomy' => 'gss_swim_lesson_groups' ] );
        $this->data['gss_lesson_group'] = gss_get_term( 1, $this->uri->segment(4) );
        $this->load->template('microsite/affiliates/lessons/lessons-group', $this->data);
    }

    public function orderby()
    {
        $this->index();
    }

}
