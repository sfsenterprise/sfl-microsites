<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migrate extends CI_Controller
{
    public function index()
    {
        if ( is_super_admin() === FALSE ) {
            $heading = 'Unauthorized';
            $message = "Only Super Administrator can migrate.";

            $this->load->view('errors/html/error_general', array('heading' => $heading, 'message' => $message));
        } else {

            $this->load->library('migration');

            if ($this->migration->current() === FALSE)
            {
                show_error($this->migration->error_string());
            }

            $this->load->model('Microsite_Model');

            if ( $this->Microsite_Model->has_mainsite_nap() === FALSE ) {
                $this->Microsite_Model->create_default_nap();
            }

            echo "Successfully migrated! <br />";
        }

    }

    public function wp()
    {
        set_time_limit(0);
        $this->index();
        $this->unzip();
    }

    private function unzip()
    {
        $zipfile = APPPATH.'migrations/wp_gss.sql.zip';
        $zip = new \ZipArchive;
        $path = wp_upload_dir()['basedir'];
        if ( $zip->open($zipfile) !== FALSE ) {
            $zip->extractTo( $path );
            $zip->close();

            $this->load->helper('file');
            $sql = read_file( wp_upload_dir()['basedir'] . '/wp_gss.sql' );

            foreach ( $this->db->list_tables() as $table ) {
                $this->dbforge->drop_table($table, TRUE);
            }

            $this->load->helper('import');

            $queries = remove_comments($sql);
            $queries = remove_remarks($queries);
            $queries = split_sql_file($queries, ';');
            unlink($path . '/wp_gss.sql');

            foreach ( $queries as $query ) {
                $this->db->query($query);
            }

            echo "Please download the uploads folder from ent-dev1! <br />";
        }
    }

}
