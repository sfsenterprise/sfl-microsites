<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitehero extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Hero_Model');
    }

    public function index()
    {
        $old_values = array();
        unset($_POST['update']);
        foreach ($_POST as $key => $value) {
            $old_values[$key] = get_blog_option(($this->microsite)?:1, $key);
        }

        $this->Hero_Model->save($this->microsite);

        $this->logs->add(['franchise'=>($this->microsite)?:1, 'module'=>$this->router->fetch_class(), 'action'=>'edit', 'old_values'=>$old_values, 'new_values'=>$this->input->post(null)]);

        $this->data['title'] = 'Hero Banners';
        $this->data['title'] .= '<small class="desc">Modify hero banners setting for ' . get_blog_details($this->microsite)->blogname . '.</small>';

        $this->data['posttypes'] = include SFL_MICROSITES_DIR . 'lib/config/posttypes.php';

        $this->load->template('hero/index', $this->data);
    }
}
