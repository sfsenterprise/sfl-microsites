<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends SFL_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index()
    {
        // microsite settings
        $this->data['title'] = 'Microsite General Settings';

        $get_footer_logos = get_blog_option( $this->microsite, 'footer_logos');

        switch_to_blog( $this->microsite );
            $this->data['_hero_image'] = wp_get_attachment_url( gss_get_post_meta($this->microsite, get_blog_option($this->microsite, 'page_on_front', true), '_hero_image_id') );
        restore_current_blog();

        $cpanel_page = get_page_by_path('surefire', OBJECT, 'page');
        $this->data['footer_logos'] = maybe_unserialize($get_footer_logos);
        $this->data['pages'] = gss_get_pages(1, ['post_type'=>'page', 'post_status'=>array('publish', 'draft'), 'exclude'=>array($cpanel_page->ID)]);

        $this->load->template('settings/microsite/index', $this->data);
    }

    public function location_information()
    {
        
        unset($_POST['gss_settings']);

        $settings = $this->input->post();

        if( isset($settings['gss_hide_location']) ){
            update_blog_details( $this->microsite, ['archived'=>1, 'public'=>0 ] );
        }
        else{
            update_blog_details( $this->microsite, ['archived'=>0, 'public'=>1 ] );
        }

        $checkboxes = array('gss_hide_location', 'gss_geo_location', 'gss_direction_link', 'gss_map_view', 'gss_search_result_order');
        foreach ($checkboxes as $checkbox) {
            if( ! array_key_exists($checkbox, $settings) ){
                $settings[$checkbox] = '';
            }
        }

        $old_values = array();

        foreach ($settings as $setting_key => $setting_value) {
            $old_values[$setting_key] = (get_blog_option($this->microsite, $setting_key)) ?: '';
            update_blog_option( $this->microsite, $setting_key, stripslashes_deep( $setting_value ) );
        }

        $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$settings]);
        
        redirect( base_url('microsites/settings#locationinfo') );
    }

    public function creative_request()
    {
        $this->form_validation->set_rules('gss_creativeform_name', 'Name', 'required',
            array('required' => 'You must provide a %s.'));
        $this->form_validation->set_rules('gss_creativeform_email', 'Email', 'required',
            array('required' => 'You must provide an %s.'));
        $this->form_validation->set_rules('gss_creativeform_subject', 'Subject', 'required',
            array('required' => 'You must provide a %s.'));
        $this->form_validation->set_rules('gss_creativeform_message', 'Message', 'required',
            array('required' => 'You must provide a %s.'));

        if ($this->form_validation->run() !== false) {
            $email = $this->creative_form();

            if( $email ){
                $this->data['email_message'] = 'Successfully Sent!';
            } else {
                $this->data['email_message'] = 'Something went wrong, please try again.';
            }
        }

        redirect( base_url('microsites/settings#creative-requestform') );
    }

    private function creative_form()
    {

        $current_inc_num = gss_get_blog_option( $this->microsite, 'gss_crf_incrementer');
        $emailTrackNum = '#' . ($current_inc_num) ?: 1;

        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
        $this->email->initialize( $config );

        $body  = '<p>Tracking: ' . $emailTrackNum . '<br/>From Name: ' . $this->input->post('gss_creativeform_name');
        $body .= '<br/>From Email: ' . $this->input->post('gss_creativeform_email') . '<br/>Message:<br>' . $this->input->post('gss_creativeform_message') . '</p>';

        $to = array( 'creative@goldfishss.com' );
        $this->email->cc( array_filter( array() ) );

        $this->email->from($this->input->post('gss_creativeform_email'), $this->input->post('gss_creativeform_name'));
        $this->email->to(implode( ',', $to ));
        $this->email->subject( $emailTrackNum.': '. $this->input->post('gss_creativeform_subject'));
        $this->email->message($body);
        $email = $this->email->send();

        if( $email ){
            update_site_option( 'gss_crf_incrementer', ( $current_inc_num + 1 ) );
            return true;
        } else {
            return false;
        }
    }

    public function homepage_settings()
    {
        unset($_POST['gss_homepage_settings']);

        $settings = $this->input->post();
        $old_values = array();

        if( ! array_key_exists('gss_hide_take_tour', $settings) ){
            $settings['gss_hide_take_tour'] = 'no';
        }

        foreach ($settings as $setting_key => $setting_value) {
            $old_values[$setting_key] = (get_blog_option($this->microsite, $setting_key)) ?: '';

            // save this setting to homepage Hero image 
            if( $setting_key === 'gss_header_image' ){
                $homepage = get_blog_option($this->microsite, 'page_on_front', true);

                switch_to_blog($this->microsite);
                    update_post_meta($homepage, '_hero_image_id', $setting_value);
                restore_current_blog();
            }

            update_blog_option( $this->microsite, $setting_key, stripslashes_deep( $setting_value ) );
        }

        if( $settings['gss_discover_content'] ){
            switch_to_blog($this->microsite);
                $home_id = get_option('page_on_front');

                $home_content = wp_update_post([
                        'ID'            => $home_id,
                        'post_content'  => $settings['gss_discover_content']
                ]);
            restore_current_blog();
        }

        $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$settings]);

        redirect( base_url('microsites/settings/#homepage-settings') );
    }

    public function header_image()
    {
        unset($_POST['gss_header_image']);

        $header_image_settings = $this->input->post();
        $old_values = array();

        if( ! array_key_exists('gss_header_image_push_sites', $header_image_settings) )
            $header_image_settings['gss_header_image_push_sites'] = '';

        foreach ($header_image_settings as $key => $value) {
            $old_values[$key] = (get_blog_option($this->microsite, $key)) ?: '';
            update_blog_option( $this->microsite, $key, $value );
        }

        $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$header_image_settings]);

        redirect( base_url('microsites/settings/#header-image') );
    }

    public function footer_logos()
    {
        unset($_POST['gss_footer_logos']);

        $footer_logos = $this->input->post();
        $old_values = array();

        $old_values['footer_logos'] = (get_blog_option($this->microsite, 'footer_logos')) ?: '';

        if( empty($footer_logos) ){
            update_blog_option( $this->microsite, 'footer_logos', '' );
        } else {
            foreach ($footer_logos as $key => $value) {
                $old_values[$key] = (get_blog_option($this->microsite, $key)) ?: '';
                update_blog_option( $this->microsite, $key, $value );
            }
        }

        $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$footer_logos]);
        redirect( base_url('microsites/settings/#footer-logos') );
    }

}