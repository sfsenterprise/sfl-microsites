<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller
{
    public function index()
    {
        #
    }
    public function in_between_leads()
    {
        $this->load->model('Json_Export_Model', 'export');

        $leads = $this->export->in_between_leads();
        $this->export->download_in_between_leads($leads);
    }

    public function import_in_between_leads()
    {
        $this->load->model('Json_Export_Model', 'export');

        $leads = $this->export->in_between_leads();
        $this->export->import_in_between_leads($leads);
    }
}
