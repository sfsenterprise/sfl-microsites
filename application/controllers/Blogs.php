<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Blogs_Model');

        $this->data['categories'] = gss_get_categories( 1, [ 'type' => 'post', 'taxonomy' => 'category' ] );
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'All Blogs';

        $this->data['orderby'] = (isset($_GET['orderby'])) ? $_GET['orderby']:'';
        $this->data['order'] = (isset($_GET['order']) && $_GET['order'] === 'desc') ? 'asc':'desc';

        $this->load->library('pagination');

        $args = array(
            'number'        => 10,
            'orderby'       => ($this->uri->segment(4)) ?: 'date',
            'order'         => ($this->uri->segment(5)) ?: 'desc',
            'paged'         => isset($_GET['pagenum']) ? ( ($_GET['pagenum'] - 1) * 10) + 1 : 0
        );

        if( isset($_GET['search']) && $_GET['search'] !== '' ){
            $this->data['all_blogs'] = $this->Blogs_Model->search( $_GET['search'], $args['paged'], $args['number'] );
            $config['total_rows'] =  count( $this->Blogs_Model->search( $_GET['search'] ) );
        }
        else{
            $this->data['all_blogs'] = gss_get_posts( 1, ['post_type'=>'post', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>$args['number'], 'orderby'=>$this->data['orderby'], 'order'=>$this->data['order'] ]);
            $config['total_rows'] =  count( gss_get_posts( 1, ['post_type'=>'post', 'offset'=>$args['paged'], 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1, 'orderby'=>$this->data['orderby'], 'order'=>$this->data['order'] ]) );
        }
        
        $config['per_page'] = $args['number'];
        $config['uri_segment'] = 4;
        $config['base_url'] = base_url("/blogs/index/");
        $config['base_url'] = base_url("/blogs/index/{$filter}");
        $config['reuse_query_string'] = TRUE;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['query_string_segment'] = 'pagenum';
        $this->pagination->initialize($config);

        $this->load->template('blogs/index', $this->data);

    }

    public function add()
    {
        $this->data['title'] = 'Add Blog';

        if( ! empty($this->input->post(NULL)) && isset($_POST['gss_blog']) ){
            $gss_blogs = $this->Blogs_Model->gss_blogs($this->input->post(NULL), 1, $update = false);

            if( $gss_blogs )
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_blogs]);
                wp_redirect( base_url('blogs/edit/'.$gss_blogs) );
        }

        $this->data['page'] = (object) array('ID' => '', 'post_title' => '', 'post_content' => '');
        $this->data['post_cats'] = gss_wp_get_post_categories(1, $this->uri->segment(4), 'category');
        $this->data['yoast']['yoast_wpseo_title'] = '';
        $this->data['yoast']['yoast_wpseo_metadesc'] = '';
        $this->data['yoast']['yoast_wpseo_meta-robots-noindex'] = '';
        $this->data['yoast']['yoast_wpseo_meta-robots-nofollow'] = '';
        $this->data['yoast']['_yoast_wpseo_meta-robots-adv'] = '';

        $this->load->template('blogs/index', $this->data);
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Blog';

        $old_values = gss_get_post( 1, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $gss_blogs = $this->Blogs_Model->gss_blogs($this->input->post(NULL), 1, $this->uri->segment(4));

            if( $gss_blogs )
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_blogs]);
                redirect( base_url('blogs/edit/'.$gss_blogs) );

        } else {
            $this->data['meta_robot'] = maybe_unserialize(gss_get_post_meta( 1, $this->uri->segment(4), "yoast_wpseo_meta-robots-adv" ));
            $this->data['post_cats'] = gss_wp_get_post_categories(1, $this->uri->segment(4), 'category');
            $this->data['page'] = gss_get_post( 1, $this->uri->segment(4) );
            $this->data['yoast']['yoast_wpseo_title'] = '';
            $this->data['yoast']['yoast_wpseo_metadesc'] = '';
            $this->data['yoast']['yoast_wpseo_meta-robots-noindex'] = '';
            $this->data['yoast']['yoast_wpseo_meta-robots-nofollow'] = '';
            $this->data['yoast']['_yoast_wpseo_meta-robots-adv'] = '';

            $this->load->template('blogs/index', $this->data);
        }
    }

    public function categories()
    {
        $this->data['all_cats'] = gss_get_categories( 1, [ 'type' => 'post', 'taxonomy' => 'category' ] );
        $this->load->template('blogs/categories', $this->data);
    }

    public function add_category()
    {
        $this->data['title'] = 'Add Category';
        if( ! empty( $new_cat = $this->input->post(NULL)) && isset($_POST['gss_blog_category']) ){
            unset($new_cat['gss_blog_category']);

            $gss_blog_category = $this->Blogs_Model->gss_blog_category($new_cat, 1, $update = false);

            if( $gss_blog_category ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$gss_blog_category['term_id'] ]);
                redirect(base_url("/blogs/edit_category/{$gss_blog_category['term_id']}"));
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
            }

        }

        $this->load->template('blogs/categories', $this->data);
    }

    public function edit_category()
    {
        $this->data['title'] = 'Edit Category';

        if( ! empty( $new_cat = $this->input->post(NULL)) && isset($_POST['gss_blog_category']) ){
            unset($new_cat['gss_blog_category']);

            $old_values = gss_get_term( 1, $this->uri->segment(4) );

            $gss_blog_category = $this->Blogs_Model->gss_blog_category($new_cat, 1, $this->uri->segment(4));

            if( $gss_blog_category ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$gss_blog_category['term_id'] ]);
                redirect(base_url("/blogs/edit_category/{$gss_blog_category['term_id']}"));
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
            }
                
        }

        $this->data['gss_blog_cat'] = gss_get_term( 1, $this->uri->segment(4) );
        $this->load->template('blogs/categories', $this->data);
    }

    public function orderby()
    {
        $this->index();
    }

}
