<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Socialmedia extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
        $this->data['title'] = 'Social Media';
        $this->data['view_frontend'] = home_url( gss_get_blog_details($this->microsite, 'path') );
    }

    public function index()
    {
        if( $this->microsite ){
            $this->data['socialmedia'] = maybe_unserialize(get_blog_option($this->microsite, 'microsite_socialmedia'));
            $this->load->template('microsite/affiliates/social', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }

    }

    public function edit()
    {
        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/social');

        $old_values = maybe_unserialize(get_blog_option($this->microsite, 'microsite_socialmedia'));

        if( ! empty($this->input->post(NULL, true)) ){

            $socialmedia = $this->Microsite_Model->add_socialmedia($this->input->post(NULL, true), $this->microsite);

            $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>(!empty($old_values)?$old_values:array()) ]);

            if( $socialmedia ){
                redirect( base_url('socialmedia/') );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                $this->data;
            }

        } else {
            $this->data['socialmedia'] = maybe_unserialize(get_blog_option($this->microsite, 'microsite_socialmedia'));
            $this->load->template('microsite/affiliates/social', $this->data);
        }

    }

}
