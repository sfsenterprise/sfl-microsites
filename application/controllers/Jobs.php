<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Microsite_Model');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = 'Jobs';

        if( $this->uri->segment(5) ){
            $this->data['order'] = ($this->uri->segment(5) === 'desc') ? 'asc':'desc';
        } else {
            $this->data['order'] = 'desc';
        }

        if( $this->microsite ){

            $args = array(
                'number'        => 10,
                'orderby'       => ($this->uri->segment(4)) ?: 'date',
                'order'         => ($this->uri->segment(5)) ?: 'desc',
                'paged'         => ($this->uri->segment(5) && is_numeric($this->uri->segment(5))) ? ( ($this->uri->segment(5) - 1) * 10) : 0
            );

            $this->load->library('pagination');
            $config['total_rows'] = count( gss_get_posts( $this->microsite, ['post_type'=>'ff_job_listings', 'post_status'=>array('publish', 'draft'), 'posts_per_page'=>-1 ] ) );
            $config['per_page'] = $args['number'];
            $config['uri_segment'] = 5;
            $config['base_url'] = base_url("/jobs/index/{$filter}");
            $this->pagination->initialize($config);

            $this->data['all_jobs'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_job_listings', 'offset'=>$args['paged'], 'post_status'=>'publish,draft,private', 'posts_per_page'=>$args['number'], 'orderby'=>$args['orderby'], 'order'=>$args['order'] ]);
            $this->load->template('microsite/affiliates/job-listings', $this->data);
        }
        else {
            $this->load->template('microsite/index');
        }

    }

    public function add()
    {
        $this->data['title'] = 'Add Job';
        if( ! empty($this->input->post(NULL)) ){
            $job_listings = $this->Microsite_Model->job_listings($this->input->post(NULL), $this->microsite, $update = false);

            if( $job_listings ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$job_listings]);
                redirect( base_url( 'jobs/edit/'.$job_listings ) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }

        $this->data['forms'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_forms', 'post_status'=>'publish', 'posts_per_page'=>-1]);
        $this->load->template('microsite/affiliates/job-listings', $this->data);
    }

    public function edit()
    {

        $this->data['title'] = 'Edit Job';
        if( ! $this->microsite )
            $this->load->template('microsite/affiliates/job-listings');

        $old_values = gss_get_post( $this->microsite, $this->uri->segment(4) );

        if( ! empty($this->input->post(NULL)) && $this->uri->segment(4) ){

            $job_listings = $this->Microsite_Model->job_listings($this->input->post(NULL), $this->microsite, $update = $this->uri->segment(4));

            if( $job_listings ){
                $this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$job_listings]);
                redirect( base_url( 'jobs/edit/'.$job_listings ) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }

        } else {
            $this->data['job_form'] = gss_get_post_meta($this->microsite, $this->uri->segment(4), '_s8_ff_jl_position_form');
            $this->data['gss_job'] = gss_get_post( $this->microsite, $this->uri->segment(4) );
            $this->data['forms'] = gss_get_posts( $this->microsite, ['post_type'=>'ff_forms', 'post_status'=>'publish', 'posts_per_page'=>-1]);
            $this->load->template('microsite/affiliates/job-listings', $this->data);
        }

    }

    public function orderby()
    {
        $this->index();
    }

}
