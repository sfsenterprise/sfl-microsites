<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends SFL_Controller{

	public $loggedin;

    function __construct(){
        parent::__construct();
        $this->loggedin = wp_get_current_user();
        $this->load->model('Users_Model');
    }

    public function index()
    {
        $this->data['title'] = 'All User';
        $this->data['users'] = get_users( ['blog_id'=>$this->microsite, 'orderby'=>'email', 'order'=>'ASC', 'fields'=>'all'] );
    	$this->load->template('users/index', $this->data);
    }

    public function add()
    {
		switch_to_blog($this->microsite);
		if ( ! current_user_can('administrator') && ! current_user_can('franchisee') ) {
			show_403('users/edit');
		}
		restore_current_blog();

		global $wpdb;

		$this->data['title'] = 'Users';
		$this->data['title'] .= '<small class="desc">Add user to microsite <span class="text-primary"><strong>' . get_blog_details($this->microsite)->blogname . '</strong></span></small>';

		$this->form_validation->set_rules(
			'user[username]',
			'Username',
			'trim|required|is_unique['.$wpdb->prefix.'users.user_login]|alpha_dash',
			array(
				'required' => 'You must provide a %s.',
				'is_unique' => 'The %s is already taken.',
				'alpha_dash' => '%s can only contain lowercase letters (a-z) and numbers.'
			)
		);

		$this->form_validation->set_rules(
			'user[email]',
			'Email',
			'required|is_unique['.$wpdb->prefix.'users.user_email]|valid_email',
			array(
				'required' => 'You must provide a %s.',
				'is_unique' => 'The %s is already registered.',
				'valid_email' => 'The %s you provided is invalid.'
			)
		);

		$this->form_validation->set_rules(
			'user[password]',
			'Password',
			'required|trim',
			array(
				'required' => '%s is required.'
			)
		);
		$this->form_validation->set_rules(
			'user[cpassword]',
			'Confirm password',
			'trim|matches[user[password]]',
			array(
				'matches' => '%s did not match the password.'
			)
		);

		if ($this->form_validation->run() !== false) {
			if ( $user_id = $this->Users_Model->save_user() ) {
				$this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>array(), 'new_values'=>$user_id]);
				redirect(base_url("/users/edit/{$user_id}"));
			}
		}

		$this->data['select']['franchisee'] = '';

		global $wp_roles;
		foreach( $wp_roles->roles as $role ) {
			if ( $role['name'] !== 'Franchisor' && $role['name'] !== 'Franchisee' ) {
				if ( $role['name'] === 'Subscriber') {
					$this->data['select'][strtolower($role['name'])] = TRUE;
				} else {
					$this->data['select'][strtolower($role['name'])] = '';
				}
			}
		}

    	$this->load->template('users/add');
    }

    public function edit($user_id)
    {
		switch_to_blog($this->microsite);
		if ( ! current_user_can('administrator') && ! current_user_can('franchisee') ) {
			show_403('users/edit');
		}
		restore_current_blog();

		if ( ! $user_id ) {
			redirect(base_url("/users/"));
		}

		switch_to_blog($this->microsite);
		if ( ! $this->data['edit_user'] = get_userdata($user_id) ) {
			redirect(base_url("/users/"));
		}
		restore_current_blog();
		global $wpdb;

		$this->form_validation->set_rules(
			'user[email]',
			'Email',
			array(
				'required',
				'valid_email',
				array('is_unique_email', array($this->Users_Model, 'valid_email'))
			),
			array(
				'required' => 'You must provide a %s.',
				'valid_email' => 'The %s you provided is invalid.',
				'is_unique_email' => 'The %s is already registered.'
			)
		);

		if ( $this->input->post('user[password]') || $this->input->post('user[cpassword]') ) {
			$this->form_validation->set_rules(
				'user[password]',
				'Password',
				'required|trim',
				array(
					'required' => '%s is required.'
				)
			);
			$this->form_validation->set_rules(
				'user[cpassword]',
				'Confirm password',
				'trim|matches[user[password]]',
				array(
					'matches' => '%s did not match the password.'
				)
			);
		}

		if ($this->form_validation->run() !== false) {
			if ( $user_id = $this->Users_Model->save_user($user_id) ) {
				$this->logs->add(['franchise'=>$this->microsite, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>(!empty($this->data['edit_user'])?$this->data['edit_user']:array()), 'new_values'=>$user_id ]);
				redirect(base_url("/users/edit/{$user_id}/"));
			}
		}

		$this->data['title'] = "Users";
		$this->data['title'] .= '<span class="desc">You are editing <strong class="text-primary">' . $this->data['edit_user']->user_login . '</strong></span>';

		$this->data['select']['franchisee'] = '';

		global $wp_roles;
		foreach( $wp_roles->roles as $role ) {
			if ( $role['name'] !== 'Franchisor' && $role['name'] !== 'Franchisee' ) {
				$this->data['select'][strtolower($role['name'])] = '';
			}
		}

		switch_to_blog($this->microsite);
		if ( user_can($user_id, 'franchisee') ) {
			$this->data['select']['franchisee'] = true;
		} elseif ( user_can($user_id, 'administrator') ) {
			$this->data['select']['administrator'] = true;
		} elseif ( user_can($user_id, 'editor') ) {
			$this->data['select']['editor'] = true;
		} elseif ( user_can($user_id, 'author') ) {
			$this->data['select']['author'] = true;
		} elseif ( user_can($user_id, 'contributor') ) {
			$this->data['select']['contributor'] = true;
		} else {
			$this->data['select']['subscriber'] = true;
		}
		restore_current_blog();

    	$this->load->template('users/add');
    }

}
