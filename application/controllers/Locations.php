<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Locations extends SFL_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Locations_Model', 'locations');
    }

    public function index($filter = 'all', $paged = 1)
    {
        $this->data['title'] = ucwords($this->uri->segment(3));
        $this->data['title'] .= "<small class='desc'>View all the Franchise Location " . ucwords($this->uri->segment(3)) . "</small>";

        $this->load->library('pagination');
        $config['total_rows'] = count($this->locations->get_locations($this->uri->segment(3)));
        $config['per_page'] = 15;
        $config['uri_segment'] = 5;
        $config['base_url'] = base_url("/locations/{$this->uri->segment(3)}/{$filter}");
        $config['reuse_query_string'] = TRUE;
        $this->pagination->initialize($config);

        $locations = array_chunk($this->locations->get_locations($this->uri->segment(3)), 15);

        if( $this->uri->segment(5) ){
            $this->data['locations'] = $locations[$this->uri->segment(5) - 1];
        }
        else{
            $this->data['locations'] = ($locations) ? $locations[0]:'';
        }
        
        $this->load->template('locations/index');
    }

    public function edit()
    {
        $this->data['title'] = 'Edit Location Contents';

        $old_values = gss_get_post( 1, $this->uri->segment(4) );

        if( ! empty( $this->input->post() ) ){
            $location = $this->locations->update($this->input->post(), 1, $this->uri->segment(4));

            if( $location ){
                $this->logs->add(['franchise'=>1, 'module'=>$this->router->fetch_class(), 'action'=>$this->router->fetch_method(), 'old_values'=>$old_values, 'new_values'=>$location]);
                redirect( base_url(sprintf('locations/edit/%1$s', $location)) );
            } else {
                $this->data['error_message'] = 'Something went wrong, please try again.';
                return $this->data;
            }
        }
        else{
            $this->data['location_dropdown'] = $this->location_dropdown();
            $this->data['location'] = gss_get_post(1, $this->uri->segment(4));
            $this->load->template('locations/edit');
        }
    }

    public function cities()
    {
        $this->index();
    }

    public function states()
    {
        $this->index();
    }

    private function location_dropdown()
    {
        $post_type_object = get_post_type_object('location');
        $locations = get_pages(array('post_type'=> 'location', 'post_status'=> 'publish', 'posts_per_page'=>-1));
        $curr_loc = get_post($this->uri->segment(4));

        $output = '<select name="post_parent" class="form-control" >';

            $output .= '<option value="">All '.$post_type_object->label.'</option>';

            foreach ($locations as $location) {
                $has_parent = (isset($location->post_parent) && $location->post_parent !== 0 ) ? '&nbsp;&nbsp;&nbsp;':'';
                $parent_parent = wp_get_post_parent_id($location->post_parent);
                $selected = ((int)$curr_loc->post_parent === (int)$location->ID) ? 'selected':'';

                if( (int)$location->post_parent === 0 || (int)$parent_parent === 0 ){
                    $output .= '<option value="'.$location->ID.'"' . $selected . '>'. $has_parent . $location->post_title.'</option>';
                }
                
            }

        $output .= '</select>';

        return $output;
    }

}