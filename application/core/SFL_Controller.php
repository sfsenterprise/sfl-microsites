<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SFL_Controller extends CI_Controller {

	public $data = array();
	public $microsite;
	public $exclude_media_override;

	function __construct()
	{
		parent::__construct();
		$this->load->model('Logs_Model', 'logs');

		if ( ! is_user_logged_in() ) {
			redirect(base_url("/auth/login/"));
		} else {

			if ( $sfmu_setup = absint(get_option('_sfmu_setup', 1)) !== 0 ) {
				if ( $this->fetch_class() !== 'setup' ) {
					redirect(base_url('/setup/'));
					exit;
				}
			}

			$this->data['user'] = wp_get_current_user();
			$blogs = get_blogs_of_user( $this->data['user']->ID);
			if ( ! $blogs && ! current_user_can('franchisor') ) {
	            show_403('auth');
	        }
		}

		$args = array('');
		if ( !current_user_can('franchisor') && is_super_admin() === FALSE && ! current_user_can('franchisor') ) {

			$args['network__in'] = array();
			foreach($blogs as $blog) {
				$args['site__in'][] = $blog->userblog_id;
			}
        }

        // if ZEE
		if ( ! current_user_can('franchisor') ) {
			$args['site__not_in'][] = 1;
			$args['archived'] = 0;
		}

		$this->data['sites'] = get_sites($args);

		$microsite = $this->input->cookie('selected_microsite');
		$this->microsite =& $microsite;
		$this->data['microsite'] =& $microsite;

		$this->data['main_site_id'] = 1;

		$this->data['title'] = 'Hi, '. $this->data['user']->display_name . '<small class="desc">Welcome to Goldfish Swim School Control Panel</small>';

		$exclude = array(
			'microsites',
			'dashboard',
			'sidebars',
			'hero',
			'formpages',
			'blogs',
			'lessons',
			'testimonials',
			'faqs',
			'tools',
			'logs',
			'locations'
		);

		if ( !isset($this->microsite) && !in_array($this->router->fetch_class(), $exclude) ) {
			redirect(base_url());
		}


		$this->include_media_override = array(
			'pages' => true,
			'sitehero' => true,
			'events' => true,
			'jobs' => true,
			'announcements' => true,
			'promos' => true,
			'staff' => true,
			'news' => true,
			'settings' => true,
		);

		if ( isset($this->microsite) ) {
			$this->data['current_microsite'] = get_blog_details($this->microsite);
		}

		if ( isset($_GET['delete']) ){
			$delete = $this->delete( $this->input->get('delete') );

			if( $delete ){
				$url = explode(base_url(), $this->agent->referrer());
				$module = explode('/', $url[1]);

				redirect( base_url($module[0]) );
			}
		}
	}

	private function delete($postid)
	{
		switch_to_blog($this->microsite);
			$delete = wp_delete_post( $postid, true );
		restore_current_blog();

		if( $delete ){
			return true;
		}
		else{
			return false;
		}
	}
}
