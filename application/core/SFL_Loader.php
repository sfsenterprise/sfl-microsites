<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SFL_Loader extends CI_Loader {
    function __construct()
    {
        parent::__construct();
    }

    public function template($template, $vars = array(), $return = FALSE)
    {
        $vars['template'] = $template;

        if ( $return ) {
            $content  = $this->view('partials/main-template', $vars, $return);

            return $content;
        } else {
            $this->view('partials/main-template', $vars, $return);
        }
    }
}
