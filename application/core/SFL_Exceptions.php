<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SFL_Exceptions extends CI_Exceptions {

    // --------------------------------------------------------------------

	/**
	 * 403 Error Handler
	 *
	 * @uses	CI_Exceptions::show_error()
	 *
	 * @param	string	$page		Page URI
	 * @param 	bool	$log_error	Whether to log the error
	 * @return	void
	 */
	public function show_403($page = '', $log_error = TRUE)
	{
        $heading = '403 Forbidden';
        $message = 'Only Franchisors could access this page.';

		// By default we log this, but allow a dev to skip it
		if ($log_error)
		{
			log_message('error', $heading.': '.$page);
		}

		echo $this->show_error($heading, $message, 'error_403', 403);
		exit(0);
	}
}
