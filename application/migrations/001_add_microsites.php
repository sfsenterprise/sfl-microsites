<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Microsites extends CI_Migration {

    public function up()
    {
        $this->dbforge->add_field(array(
            'ID' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'microsite_blog_id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'microsite_post_id' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'null' => FALSE,
            ),
            'microsite_date' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'microsite_updated' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
            'microsite_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => FALSE,
            ),
            'microsite_slug' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => FALSE
            ),
            'microsite_vanity_url' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => FALSE
            ),
            'microsite_street' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => FALSE
            ),
            'microsite_street2' => array(
                'type' => 'VARCHAR',
                'constraint' => '250',
                'null' => TRUE
            ),
            'microsite_city' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => FALSE
            ),
            'microsite_state_province' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => FALSE
            ),
            'microsite_zip_postal' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
                'null' => FALSE
            ),
            'microsite_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'microsite_general_manager' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'microsite_lat' => array(
                'type' => 'FLOAT',
                'constraint' => '10,6',
                'null' => FALSE,
                'defualt' => '0.000000'
            ),
            'microsite_lng' => array(
                'type' => 'FLOAT',
                'constraint' => '10,6',
                'null' => FALSE,
                'defualt' => '0.000000'
            ),
            'microsite_gtm_ga' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE
            ),
            'microsite_fields' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE
            ),
        ));
        $this->dbforge->add_key('ID', TRUE);
        $this->dbforge->add_key('microsite_blog_id');
        $this->dbforge->add_key('microsite_post_id');
        $this->dbforge->add_key(array('microsite_name', 'microsite_slug'));
        $this->dbforge->add_key(array('microsite_city', 'microsite_state_province', 'microsite_zip_postal'));
        $this->dbforge->create_table('sfl_microsites');
    }

    public function down()
    {
        $this->dbforge->drop_table('sfl_microsites');
    }
}
