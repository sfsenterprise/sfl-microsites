<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Audit_Logs extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
            'ID' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'microsite' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'null' => FALSE,
            ),
            'module' => array(
                'type' => 'varchar',
                'constraint' => 100,
                'null' => FALSE,
            ),
            'user' => array(
                'type' => 'BIGINT',
                'constraint' => 20,
                'null' => FALSE,
            ),
            'action' => array(
                'type' => 'varchar',
                'constraint' => 50,
                'null' => FALSE,
            ),
            'variables' => array(
                'type' => 'LONGTEXT',
                'null' => TRUE
            ),
            'date' => array(
                'type' => 'DATETIME',
                'null' => FALSE,
                'default' => '0000-00-00 00:00:00'
            ),
        ));
        $this->dbforge->add_key('ID', TRUE);
        $this->dbforge->add_key('date');
        $this->dbforge->create_table('sfl_logs');
	}

	public function down()
    {
        $this->dbforge->drop_table('sfl_logs');
    }

}
