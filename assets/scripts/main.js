/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  var Function = {
    getTagName: function() {

      // insert tag button
      $('.insert-tag').on('click', function(){
        var tag = $('#TB_ajaxContent form').attr('data-id');
        var get_tag = $('#tag-'+tag).val();
        var curr_form = $('#formtab textarea').val();

        $('#formtab textarea').val(curr_form + get_tag);
        $('.tb-close-icon').trigger('click');
        $('#TB_ajaxContent form').find("input, textarea").val("");
        $('#TB_ajaxContent form').find("input[type='radio'], input[type='checkbox']").prop('checked', false);
      });

      // open popup box ang assign initial shortcode
      $('.thickbox.button').on('click', function(){
        var tag = $('#TB_ajaxContent form').attr('data-id');
        var this_tb = $(this).attr('data-id');
        $('.insert-tag').val('Insert Tag');
      });

      // all checkboxes
      $('input[name="required"], input[name="exclusive"], input[name="use_label_element"], input[name="placeholder"], input[name="include_blank"], input[name="multiple"], input[name="label_first"]').on('click', function(){
        Function.createTag();
      });

      // all input fields and textarea
      $('textarea, input[name="filetypes"], input[name="limit"], input[name="class"], input[name="id"], input[name="values"], input[name="name"]').on('change', function(){
        Function.createTag();
      });

    },

    createTag: function() {
        var toStr = [];
        var tag = $('#TB_ajaxContent form').attr('data-id');
        var st = Function.getFields();

        $.each(st, function(i, val) {
          if( val ){

            if( $.type(val) === 'array' ){
              for(var arr_i=0; arr_i < val.length; arr_i++){
                toStr.push(val[arr_i]);
              }
            } else {
              toStr.push(val);
            }

          }
        });

        if( tag === 'radio' || tag === 'submit' ){
          tag = tag + ' ';
        }

        $('#tag-'+tag).val( '[' +tag+ toStr.join(" ") +']' );
    },

    getFields: function() {
          var fields = {};
          var tag = $('#TB_ajaxContent form').attr('data-id');

          $('#TB_ajaxContent form#form-'+tag+' input').each(function(){
            var field_val = $(this).val();
            var field_name = $(this).attr('name');

            if( field_val !== 'Insert Tag' && field_name !== '['+tag+']' && field_name !== tag ){

                fields[field_name] = field_val;

                if( field_name === 'id' && field_val ){
                  fields[field_name] = 'id:'+ field_val;
                }

                if( field_name === 'class' && field_val ){
                  fields[field_name] = 'class:'+ field_val;
                }

                if( field_name === 'values' && field_val ){
                  fields[field_name] = '"'+ field_val + '"';
                }

                if( field_name === 'limit' && field_val ){
                  fields[field_name] = 'limit:'+ field_val;
                }

                if( field_name === 'filetypes' && field_val ){
                  fields[field_name] = 'filetypes:'+ field_val;
                }

            }

          });

          var required_c = $('input[name="required"]').is(':checked');
          var placeholder_c = $('input[name="placeholder"]').is(':checked');
          var include_blank_c = $('input[name="include_blank"]').is(':checked');
          var multiple_c = $('input[name="multiple"]').is(':checked');
          var label_first_c = $('input[name="label_first"]').is(':checked');
          var use_label_element_c = $('input[name="use_label_element"]').is(':checked');
          var exclusive_c = $('input[name="exclusive"]').is(':checked');

            if( required_c ){
              fields.required = '* ';
            } else {
              fields.required = ' ';
            }

            var lines_val = [];

            if( $('#TB_ajaxContent form#form-'+tag+' textarea').is(':visible') ){
              var lines = $('#TB_ajaxContent form#form-'+tag+' textarea').val().split(/\n/);

              for (var i=0; i < lines.length; i++) {
                if (/\S/.test(lines[i])) {
                  lines_val.push( "'"+ $.trim(lines[i]) + "'");
                }
              }

              console.log(lines_val);

              if( lines_val.length > 0 ){
                fields.values = lines_val;
              }
            }

            if( include_blank_c ){
              fields.include_blank = 'include_blank';
            } else {
              fields.include_blank = '';
            }

            if( multiple_c ){
              fields.multiple = 'multiple';
            } else {
              fields.multiple = '';
            }

            if( placeholder_c ){
              fields.placeholder = 'placeholder';
            } else {
              fields.placeholder = '';
            }

            if( label_first_c ){
              fields.label_first = 'label_first';
            } else {
              fields.label_first = '';
            }

            if( use_label_element_c ){
              fields.use_label_element = 'use_label_element';
            } else {
              fields.use_label_element = '';
            }

            if( exclusive_c ){
              fields.exclusive = 'exclusive';
            } else {
              fields.exclusive = '';
            }

          return fields;
    },

    staffPhoto: function() {

      $('#upload-staff-img').on('click', function(){
        var mediaUploader;

        if (mediaUploader) {
            mediaUploader.open();
            return;
        }

        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Photo',
            button: {
                text: 'Choose An Image'
            }, multiple: false
        });

        mediaUploader.on('select', function() {
            var attachment = mediaUploader.state().get('selection').first().toJSON();
            $('.staff-photo').fadeIn();
            $('#_thumbnail_id').val(attachment.id).change();
            $('.staff-image').attr('src', attachment.url).fadeIn();
        });

        mediaUploader.open();
      });
    },

    setFeaturedImage: function() {
      $('#set-post-thumbnail, #post-featured-src').on('click', function(){
        var mediaUploader;
        var post_id = parseInt($(this).data('post_id')) || 0;

        if (mediaUploader) {
          if ( post_id > 0 ) {
            mediaUploader.uploader.uploader.param('post_id', post_id);
          }

            mediaUploader.open();
            return;
        }

        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Featured Image',
            button: {
                text: 'Choose An Image'
            }, multiple: false
        });

        if ( post_id > 0 ) {
          wp.media.model.settings.post.id = post_id;
        }

        mediaUploader.on('select', function() {
            var attachment = mediaUploader.state().get('selection').first().toJSON();
            $('#set-post-thumbnail').css('display', 'none');
            $('#_thumbnail_id').val(attachment.id).change();
            $('#post-featured-src').attr('src', attachment.url);
            $('.selected-image, #remove-post-thumbnail').fadeIn();
        });

        mediaUploader.open();
      });
    },

    imageUpload: function(e) {

        var mediaUploader;
        var img_id = e.target.id;

        if (mediaUploader) {
          mediaUploader.open();
          return;
        }

        mediaUploader = wp.media.frames.file_frame = wp.media({
          title: 'Select Image',
          button: {
          text: 'Choose An Image'
            }, multiple: false
        });

        mediaUploader.on('select', function() {
          var attachment = mediaUploader.state().get('selection').first().toJSON();
          $('#'+img_id).parent().children('input').val(attachment.id).change();
          $('#'+img_id).parent().children('img').attr('src', attachment.url).fadeIn('fast');
          $('#'+img_id).parent().children('.remove').fadeIn('fast');
          $('#'+img_id).parent().children('.remove-img').fadeIn('fast');
        });

        mediaUploader.open();
    },

    addLogo: function(e) {
      var mediaUploader;

            if (mediaUploader) {
                mediaUploader.open();
                return;
            }

            mediaUploader = wp.media.frames.file_frame = wp.media({
                title: 'Select Image',
                button: {
                    text: 'Choose An Image'
                }, multiple: false
            });

            mediaUploader.on('select', function() {
                var attachment = mediaUploader.state().get('selection').first().toJSON();
                jQuery('#gss-footer-logos').append('<div class="footer-logo row"><div class="logo col-3">'+
                                                    '<img src="'+attachment.url+'" style="max-width: 175px;">'+
                                                    ' <span class="dashicons dashicons-trash remove-logo"></span></div>'+
                                                    '<div class="col-7"><input type="text" name="footer_logos['+attachment.id+'][url]" value="" class="form-control" placeholder="Link here"/><em>Logo Link (URL)</em></div>'+
                                                    '<input type="hidden" name="footer_logos['+attachment.id+'][id]" value="'+attachment.id+'">'+
                                                    '</div>');
            });

            mediaUploader.open();
    },

    groupImage: function() {
        var mediaUploader;

        if (mediaUploader) {
            mediaUploader.open();
            return;
        }

        mediaUploader = wp.media.frames.file_frame = wp.media({
            title: 'Select Image',
            button: {
                text: 'Choose An Image'
            }, multiple: false
        });

        mediaUploader.on('select', function() {
            var attachment = mediaUploader.state().get('selection').first().toJSON();
            $('input#group-image').val(attachment.id).change();
            $('#group-featured-img').attr('src', attachment.url).fadeIn();
        });

        mediaUploader.open();
    }
  };

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var SFL_Microsites = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        if ( $('body').hasClass('login') === false && jQuery('li.navmenu-item-active').length > 0 ) {
          $('.sidebar').scrollTop($('li.navmenu-item-active').offset().top - 100);
        }

        $("#event-start, #promo_startDate, #promo_endDate, #event-end-on").datepicker();

        $("#event-end").datepicker({
            onSelect: function (selectedDate) {
              //console.log(selectedDate);
              var start = $('#event-start').val();

              if( new Date(selectedDate) < new Date(start) ){
                $("#event-end").val( start );
              }
            }
        });

        $('a[href^="#"]').on('click', function(event) {
          event.preventDefault();
        });

        $('.notification').find('.close').on('click', function() {
          $(this).parents('.notification').fadeOut(function () {
            $(this).remove();
          });
        });

        $(".microsite-select").select2({
          placeholder: "Select a microsite",
          allowClear: true
        });

        $(".microsite-users-select").select2({
          placeholder: "",
          tags: true,
          selectOnClose: true,
          minimumResultsForSearch: 1,
          createTag: function (params) {
            var term = $.trim(params.term);

            if (term === '') {
              return null;
            }

            return {
              id: term,
              text: term,
              newTag: true // add additional parameters
            };
          }
        });
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Dashboard page
    'dashboard': {
      init: function() {
        // JavaScript to be fired on the home page
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    // Forms page
    'formpages': {
      init: function() {

        $('#fields-form').on('click', function(){
            $('.form-settings').fadeIn();
        });

        $('#downloadable-form').on('click', function(){
            $('.form-settings').fadeOut();
        });

        $('#check-all').on('click', function(){
          $('.add-to-franchise').trigger('click');
        });

        $('#add-notif').click(function(){
          var new_arr = $('#notiftab fieldset').length + 1;
          var final_arr = new_arr-1;
          var newaddress = $("#notiftab fieldset").eq(0).clone();

          newaddress.find('input, textarea').each(function() {
            this.name= this.name.replace('[0]', '['+final_arr+']');
          });

          newaddress.insertBefore("#add-notif");
        });

        $('body .tab-pane').on('click', '.remove', function(){
          $(this).parent().parent().parent().remove();
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS

        Function.getTagName();
      }
    },
    // microsite forms
    'form': {
      init: function() {
        $('#add-notif').click(function(){
          var new_arr = $('#notiftab fieldset').length + 1;
          var final_arr = new_arr-1;
          var newaddress = $("#notiftab fieldset").eq(0).clone();

          newaddress.find('input, textarea').each(function() {
            this.name= this.name.replace('[0]', '['+final_arr+']');
          });

          newaddress.insertBefore("#add-notif");
        });

        $('body .tab-pane').on('click', '.remove', function(){
          $(this).parent().parent().parent().remove();
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS

        Function.getTagName();
      }

    },
    // Pricing page
    'pricing': {
      init: function() {
        var price = $('.price-wrapper input[type="text"]');

        $(price).on('change', function(){
          if( $.isNumeric($(this).val()) ){
            $(this).val( parseFloat( $(this).val() ).toFixed(2) );
          }
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Schedules page
    'schedules': {
      init: function() {

        $('span.add').on('click', function(){
            var day = $(this).data('day');
            var category = $(this).data('cat');
            var key = parseInt($(this).data('rows'));
            $(this).prev('div.hours-operations-time').append('<div> <div><input type="text" name="'+category+'['+day+'][hours]['+key+'][start]" /><input type="text" name="'+category+'['+day+'][hours]['+key+'][end]" /></div> <span class="dashicons dashicons-trash remove"></span></div>');
            $(this).data('rows', (key + 1));
        });

        $('div').on('click', '.remove', function(){
            $(this).parent('div').remove();
        });

        $('div.status').on('click', function(){
            if( $(this).children('input').is(':checked') ){
                $(this).children('input').prop('checked', false);
                $(this).addClass('restday-status').removeClass('workday-status');
            } else {
                $(this).children('input').prop('checked', true);
                $(this).addClass('workday-status').removeClass('restday-status');
            }
        });

        $('.hours-operations-time input').delegate('input[type="text"]', "change", function(){
          alert($(this).val());
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Events page
    'events': {
      init: function() {
        Function.setFeaturedImage();

        $('#remove-post-thumbnail').on('click', function(){
            $('#_thumbnail_id').val('');
            $('#post-featured-src').attr('src', '');
            $('.selected-image').fadeOut();
            $(this).fadeOut();
            $('#set-post-thumbnail').fadeIn();
        });


      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Staff page
    'staff': {
      init: function() {
        Function.staffPhoto();

        $('.dashicons-trash.remove').on('click', function(){
          $('.staff-photo').fadeOut();
          $('#_thumbnail_id').val('');
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // blogs page
    'blogs': {
      init: function() {
        Function.setFeaturedImage();

        $('#remove-post-thumbnail').on('click', function(){
            $('#_thumbnail_id').val('');
            $('#post-featured-src').attr('src', '');
            $('.selected-image').fadeOut();
            $(this).fadeOut();
            $('#set-post-thumbnail').fadeIn();
        });

        $(document).on('click', '.post-tag', function(e) {
          $(this).parent().remove();
        });

        $(document).on('click', '#post-tags .form-check-label', function(e) {
          $(this).remove();
        });

        $(document).on('change', '#new-tag-post_tag', function(e) {
          setTimeout(function(){
            $('#tags-suggested').empty();
          }, 500);
        });

        $('.btn.tagadd').on('click', function(){
          var new_tag = $('#new-tag-post_tag').val();
          var tag = new_tag.split(',');
          if( tag.length > 0 ){
            for( var t = 0; t < tag.length; t++ ){
              if( tag[t].length > 2 ){
                $('#post-tags').append(
                        '<label class="form-check-label">' +
                        '<input class="form-check-input" type="checkbox" name="post_tags[]" value="'+tag[t]+'" checked="checked">' +
                        '<span class="fa fa-times post-tag"></span> ' +
                        tag[t] +
                        '</label>'
                );
              }
            }
          }

          $('#new-tag-post_tag').val('');

        });

        $('#new-tag-post_tag').on('keyup keypress', function(event){
          if (event.keyCode == 13) {
            event.preventDefault();
            return false;
          }
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // microsites settings page
    'microsites': {
      init: function() {
        $('#gss-frontpage-image').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#gss-header-image').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#gss-upcoming-events').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#gss-swimming-lesson-fees').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#gss-swimming-lesson-options').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#header-image-btn').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#add-logo').on('click', function(e){
          Function.addLogo(e);
        });

        $('#set-hero-image').on('click', function(e){
          Function.imageUpload(e);
        });

        $('#set-post-thumbnail').on('click', function(e){
          Function.imageUpload(e);
        });

        $('.micro-settings').on('click', function(e){
          var tab = e.target.hash;
          window.history.pushState({},"", tab);
        });

        $('#page-hero-img').on('click', function(){
          $('#set-hero-image').trigger('click');
        });

        $('#page-featured-img').on('click', function(){
          $('#set-post-thumbnail').trigger('click');
        });

        $('.remove, .remove-img').click(function(){
            var id = $(this).parent().children('div').attr('id');

            $('#'+id).parent().children('input').val('');
            $('#'+id).parent().children('img').attr('src', '').fadeOut();
            $(this).fadeOut();
        });

        $('.remove-logo').click(function(){
            var remove_logo = $(this).parent().parent();

            remove_logo.remove();
        });

        $('.remove-header-image').click(function(){
            $('#header-image-id').val('');
            $('#gss_header_image_push_sites').prop('checked', false);
            $('#site_header_image_src').attr('src', '');
        });

        $('#remove-hero-image, #remove-featured-image').on('click', function(){
          $(this).parent().parent().children('img').attr('src', '').fadeOut('fast');
          $(this).parent().parent().children('input').val('');
          $(this).parent().fadeOut('fast');
        });

        if( $('.nav-tabs').length ){
          var tab_hash = window.location.hash;
          tab_hash = tab_hash.replace(/#/g, '').replace(/!/g, '');
          $('a[href="#'+tab_hash+'"]').click();
        }

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },

    // settings page
        'settings': {
          init: function() {
            $('#gss-frontpage-image').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#gss-header-image').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#gss-upcoming-events').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#gss-swimming-lesson-fees').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#gss-swimming-lesson-options').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#header-image-btn').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#add-logo').on('click', function(e){
              Function.addLogo(e);
            });

            $('#set-hero-image').on('click', function(e){
              Function.imageUpload(e);
            });

            $('#set-post-thumbnail').on('click', function(e){
              Function.imageUpload(e);
            });

            $('.micro-settings').on('click', function(e){
              var tab = e.target.hash;
              window.history.pushState({},"", tab);
            });

            $('#page-hero-img').on('click', function(){
              $('#set-hero-image').trigger('click');
            });

            $('#page-featured-img').on('click', function(){
              $('#set-post-thumbnail').trigger('click');
            });

            $('.remove, .remove-img').click(function(){
                var id = $(this).parent().children('div').attr('id');

                $('#'+id).parent().children('input').val('');
                $('#'+id).parent().children('img').attr('src', '').fadeOut();
                $(this).fadeOut();
            });

            $('.remove-logo').click(function(){
                var remove_logo = $(this).parent().parent();

                remove_logo.remove();
            });

            $('.remove-header-image').click(function(){
                $('#header-image-id').val('');
                $('#gss_header_image_push_sites').prop('checked', false);
                $('#site_header_image_src').attr('src', '');
            });

            $('#remove-hero-image, #remove-featured-image').on('click', function(){
              $(this).parent().parent().children('img').attr('src', '').fadeOut('fast');
              $(this).parent().parent().children('input').val('');
              $(this).parent().fadeOut('fast');
            });

            if( $('.nav-tabs').length ){
              var tab_hash = window.location.hash;
              tab_hash = tab_hash.replace(/#/g, '').replace(/!/g, '');
              $('a[href="#'+tab_hash+'"]').click();
            }

          },
          finalize: function() {
            // JavaScript to be fired on the home page, after the init JS
          }
        },

    // lessons settings page
    'lessons': {
      init: function() {
        $('#add-group').on('click', function(){
          Function.groupImage();
        });

        $('#remove-group').on('click', function(){
          $('#group-featured-img').fadeOut();
          $('#group-image').val('');
        });

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },

    // sidebars page
    'sidebars': {
      init: function() {

        $( "#sortable" ).sortable();
        $( "#sortable" ).disableSelection();

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    'hero': {
      init: function() {
        _sfmu.Hero();
      }
    },
    'sitehero': {
      init: function() {
        _sfmu.Hero();
      }
    },
    'news': {
      init: function() {
        Function.setFeaturedImage();
        $('#remove-post-thumbnail').on('click', function() {
            $('#_thumbnail_id').val('');
            $('#post-featured-src').attr('src', '');
            $('.selected-image').fadeOut();
            $(this).fadeOut();
            $(this).prev('p').fadeOut();
            $('#set-post-thumbnail').fadeIn();
        });
      }
    }

  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = SFL_Microsites;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
