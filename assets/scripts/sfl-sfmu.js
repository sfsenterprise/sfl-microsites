var _sfmu = {
  Hero: function() {
    var frame;
    var input;
    var container;
    var $ = jQuery.noConflict();
    $('.media-hndl').on('click', function(event) {
      event.preventDefault();
      var parent = $(this).parents('.hero-image-container');
      container = parent.find('.hero-image');
      input = parent.find('.hero-image-id');

      if ( frame ) {
        frame.open();
        return;
      }

      frame = wp.media({
        title: 'Select Hero Image',
        button: {
          text: 'Choose Hero'
        },
        multiple: false
      });

      frame.on('select', function() {
        var attachment = frame.state().get('selection').first().toJSON();
        container.html('<img src="'+attachment.url+'" alt="">');
        input.val(attachment.id);
      });

      frame.open();
    });

    $('.media-del-hndl').on('click', function(event) {
      event.preventDefault();
      var parent = $(this).parents('.hero-image-container');

      parent.find('.hero-image').html('<img src="" alt="">');
      parent.find('.hero-image-id').val('');
    });
  }
};
