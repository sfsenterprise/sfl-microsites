/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {
  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.

  var user = {
    lat: 0,
    lng: 0,
  };

  var sites = [];

  var fn = {
    adaptiveHeight: function () {
      if ( $('#sfl-locator-map-wrapper').height() < 604 ) {
        $('#sfl-locator-map-wrapper').height($(window).height() - $('.banner').height());
      }
      $('.location-search .location-search-results').height( ($('#sfl-locator-map-wrapper').height() - 44 ) - $('.location-search form').outerHeight());
    },
    map: function(microsites, zoom, pin, autosearch) {

      if ( ! microsites ) return;
      var map = null;
      var options = {
          center: new google.maps.LatLng(microsites[0].lat, microsites[0].lng),
          zoom: zoom,
          scrollwheel: false,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          scaleControl: false,
          panControl: false,
          streetViewControl: false
      };

      map = new google.maps.Map(document.getElementById("map"), options);

      var markers = fn.setMarkers(microsites, map, pin);

      if ( autosearch === true ) {
        // Create the search box and link it to the UI element.
        var input = document.getElementById('search-location');
        var searchBox = new google.maps.places.SearchBox(input);
        // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        var itemsloaded = google.maps.event.addDomListener(map, 'idle', function(e) {
          if (input.value !== null) {
            //remove the listener
            google.maps.event.removeListener(itemsloaded);
            google.maps.event.trigger( input, 'keydown', {keyCode:40});
            google.maps.event.trigger( input, 'keydown', {keyCode:13});
            google.maps.event.trigger( input, 'focus');
            google.maps.event.trigger( input, 'keydown', {keyCode:13});
          }
        });

        if ( input !== null ) {
          // Bias the SearchBox results towards current map's viewport.
           map.addListener('bounds_changed', function() {
             searchBox.setBounds(map.getBounds());
           });

           $('#search-location').on('blur', function(event) {
             if ( $(this).val().length === 0 ) {
              //  $('body').removeClass('search-results');
              //  $('.location-search .location-search-results').height( ($('#sfl-locator-map-wrapper').height() - 44 ) - $('.location-search form').outerHeight());

               var center = map.getCenter();
               google.maps.event.trigger(map, "resize");
               map.setCenter(center);
               map.setZoom(5);
             }
           });

          // Listen for the event fired when the user selects a prediction and retrieve
          // more details for that place.
          google.maps.event.addListener(searchBox, 'places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
              return;
            }

            map.setZoom(10);
            ///$('body').addClass('search-results');
            ///$('.location-search .location-search-results').height( ($('#sfl-locator-map-wrapper').height() - 44 ) - $('.location-search form').outerHeight());

            var place = places[0];
            if ( !place.geometry ) {
              console.log("Returned place contains no geometry");
              return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
              marker.setMap(null);
            });
            markers = [];

            var search = {
              lat: place.geometry.location.lat(),
              lng: place.geometry.location.lng(),
            };

            var re_microsites = [];
            $.each(microsites, function( i, microsite) {
              microsite.distance = fn.getDistance(search, microsite);
              microsite.li.find('.location-distance a').text(microsite.distance + 'mi');
              re_microsites.push(microsite);
            });
            microsites = re_microsites;

            microsites.sort(function ( a, b ) {
              return a.distance - b.distance;
            });

            $.each(microsites, function(i, microsite) {
              $('#locations-list').append(microsite.li);
            });

            markers = fn.setMarkers(microsites, map, pin);
          });
        }
      }
    },
    setMarkers: function(microsites, map, pin) {
      var bounds = new google.maps.LatLngBounds(), i;
      var locations = [];

      var infoWindow = new google.maps.InfoWindow(), marker, position, active = null;
      $.each(microsites, function(index, microsite) {
          position = new google.maps.LatLng(microsite.lat, microsite.lng);

          marker = new google.maps.Marker({
              position: position,
              map: map,
              icon: {
                url: pin || null,
                scaledSize: new google.maps.Size(45, 28),
                // The origin for this image is 0,0.
                origin: new google.maps.Point(0,0),
                // The anchor for this image is the base of the flagpole at 0,32.
                anchor: new google.maps.Point(21, 11)
              },
              // animation: google.maps.Animation.DROP,
              title: microsite.name,
              zIndex: index
          });

          bounds.extend(position);

          google.maps.event.addListener(marker, 'click', (function(marker, index) {
              return function() {
                  infoWindow.setContent('<a href="' + microsite.url +
                  '" target="_blank">' + microsite.name + '</a><br />'+
                  microsite.address + '<br />' + microsite.phone + '<br />' +
                  '<a href="' + microsite.url + '" target="_blank">Visit Site</a> | <a href="' + microsite.directions + '" target="_blank"> Get Directions</a>');
                  infoWindow.open(map, marker);

                  if ( active !== null ) {
                    locations[active].setIcon({
                      url: pin,
                      scaledSize: new google.maps.Size(45, 28),
                      // The origin for this image is 0,0.
                      origin: new google.maps.Point(0,0),
                      // The anchor for this image is the base of the flagpole at 0,32.
                      anchor: new google.maps.Point(21, 11)
                    });
                  }

                  locations[index].setIcon({
                    url: pin,
                    size: new google.maps.Size(118, 68),
                    // The origin for this image is 0,0.
                    origin: new google.maps.Point(0,0),
                    // The anchor for this image is the base of the flagpole at 0,32.
                    anchor: new google.maps.Point(59, 34)
                  });

                  active = index;

                  if ( ! $('body').hasClass('microsite') && ! $('body').hasClass('single-location') ) {
                    var targetScroll = $('#locations-list li:eq(' + index + ')').position().top + $('#locations-list li').height();
                    $('.location-search-results').animate({ scrollTop: targetScroll }, 1000);
                    $('#locations-list li').removeClass('active');
                    $('#locations-list li:eq(' + index + ')' ).addClass('active');
                  }
              };
          })(marker, index, active));

          google.maps.event.addListener(infoWindow, 'closeclick', function() {
            if ( active !== null ) {
              locations[active].setIcon({
                url: pin,
                scaledSize: new google.maps.Size(45, 28),
                // The origin for this image is 0,0.
                origin: new google.maps.Point(0,0),
                // The anchor for this image is the base of the flagpole at 0,32.
                anchor: new google.maps.Point(21, 11)
              });

              $('#locations-list li').removeClass('active');
            }

          });

          locations.push(marker);
          if ( $('body').hasClass('single-location') ) {
            if ( locations.length >= 2 ) {
              map.fitBounds(bounds);
            }
          }
          map.setCenter(locations[0].position);
      });

      google.maps.event.addDomListener(window, 'resize', function() {
          center = map.getCenter();
          //center = locations[0].position;
          google.maps.event.trigger(map, "resize");
          map.setCenter(center);
          map.fitBounds(bounds);
      });

      return locations;
    },
    deg2rad: function(deg) {
      return deg * (Math.PI/180);
    },
    getDistance: function (from, to) {
      var R = 3959; // Radius of the earth in miles
      var dLat = fn.deg2rad(to.lat-from.lat);
      var dLng = fn.deg2rad(to.lng-from.lng);
      var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(fn.deg2rad(from.lat)) * Math.cos(fn.deg2rad(to.lat)) *
        Math.sin(dLng/2) * Math.sin(dLng/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c; // Distance in mi

      return d.toFixed(2);
    },
    generateMap: function(zoom) {
      fn.adaptiveHeight();
      $(window).resize(fn.adaptiveHeight());
      var microsites = [];

      $('#locations-list li').each(function(i) {
        microsite = $(this).data('microsite');
        $(this).removeAttr('data-microsite');
        microsite.li = $(this);
        microsite.distance = fn.getDistance(user, microsite);
        microsite.li.find('.location-distance a').text(microsite.distance + 'mi');
        microsites.push(microsite);
        $(this).remove();
      });
      microsites.sort(function ( a, b ) {
        return a.distance - b.distance;
      });

      $.each(microsites, function(i, microsite) {
        $('#locations-list').append(microsite.li);
      });
      $('.loading-spinner-maps').hide();
      $('#locations-list').show();

      var pin = $('#map').data('pin') || '';

      fn.map(microsites, zoom, pin, true);
    },
    generateMapSingle: function(zoom) {
      var pin = $('#map').data('pin') || '';
      var microsites = [];
      microsites.push($('.sfl-locator-map-wrapper').data('microsite'));
      $('.sfl-locator-map-wrapper').removeAttr('data-microsite');
      if ( ! microsites ) return;

      fn.map(microsites, zoom, pin, false);
    },
    generateMapStateCity: function(zoom) {
      var pin = $('#map').data('pin') || '';

      var  microsites = $('.sfl-locator-map-wrapper').data('microsites') || [];
      $('.sfl-locator-map-wrapper').removeAttr('data-microsites');

      if ( ! microsites ) return;

      if ( microsites.length < 2) {
        zoom = 15;
      } else if (microsites.length < 3) {
        zoom = 8;
      }

      fn.map(microsites, zoom, pin, false);
    },
    closestLocation: function(user) {
      var closest = [];
      if ( typeof sites !== 'undefined' ) {
        $.each(sites, function( i, site ) {
          site.distance = fn.getDistance(user, site);
          closest.push(site);
        });

        closest.sort(function ( a, b ) {
          return a.distance - b.distance;
        });

        if ( typeof closest[0] !== 'undefined' ) {
          $('.address address #address-city-state').text(closest[0].city + ',' + closest[0].stateabbr);
          $('.address address #address-city-state').prop('href', closest[0].url);
          $('.address address #address-phone').text(closest[0].phone);
          $('.address address #address-phone').prop('href', 'tel:' + closest[0].phone);

          $('.loading-nearest-location').remove();
          $('.address address').show();
        }
      }
    },
    initPosition: function() {
      if ( navigator.geolocation ) {
        if ( user.lat === 0 && user.lng === 0 ) {
          navigator.geolocation.getCurrentPosition(function(position) {
            user.lat = position.coords.latitude;
            user.lng = position.coords.longitude;

            fn.closestLocation(user);

            if ( $('body').hasClass('post-type-archive-location') ) {
              fn.generateMap(5);
            }

            return user;
          }, function ( error ) {
            fn.closestLocation(user);
            console.warn('Geolocation failed.');
          },
          { timeout: 30000 });
        }
      } else {
        fn.closestLocation(user);
        console.warn('Your browser doesn\' support or doesn\'t allow geolocation.');
      }
    },
  };

  var SFL_Location = {
    // All pages
    'common': {
      init: function() {
        //39.130956399999995
        //-77.7020745

        $('#search-location-form').submit(function(event) { event.preventDefault(); });
        $('#locations-list').hide();

        if ( $('.address address').data('sites') ) {
          $('.address address').hide();
          sites = $('.address address').data('sites');
          $('.address address').removeAttr('data-sites');
        }

        if ( ! $('body').hasClass('sfmu') && ! $('body').hasClass('microsite') ) {
          fn.initPosition();
        }

        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired

      }
    },
    'post_type_archive_location': {
      init: function() {
        //
      },
      finalize: function() {
        // $(window).load(function() {
        //   if ( $('#locations-list li').data('microsite').length ) {
        //     if ( navigator.geolocation && (user.lat > 0 && user.lng > 0) ) {
        //       fn.generateMap(5);
        //     }
        //   }
        // });
      }
    },
    'single_location': {
      init: function() {
      },
      finalize: function() {
        fn.generateMapStateCity(7);
      }
    },
    'microsite': {
      init: function() {
        //
        if ( $('.sfl-locator-map-wrapper').length ) {
          fn.generateMapSingle(15);
        }
      }
    },
    'page_template_template_location_optional': {
      init: function() {
        fn.generateMap(5);
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = SFL_Location;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
