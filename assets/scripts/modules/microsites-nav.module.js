angular.module("microsites-nav", []).
  config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
  }]).
  controller("micrositeNav", ['$scope', '$http', '$location', function($scope, $http, $window) {
    $scope.error = "";

    $scope.move = function(e) {
      e.preventDefault();
      var link = e.target.href;
      $scope.error = "";
      $http.get(link).
      then(function(response) {
        if ( response.status === 200 ) {
          window.location.href = link;
        }
      }).
      catch(function(error) {
        console.error('Failed to get the page');
      });
    };

  }]);
