angular.module("microsites-login", ["angular-loading-bar"]).
  controller("login", ['$scope', '$http', '$location', function($scope, $http, $window) {
    $scope.error = "";
    $scope.log = "";
    $scope.pwd = "";

    $scope.submit = function(e) {
      e.preventDefault();
      $scope.error = "";
      $http.post("/surefire/auth/login/", { "remote" : true, "log" : $scope.log, "pwd" : $scope.pwd }).
      then(function(response) {
        if ( response.status !== 401 ) {
          window.location.replace("/surefire/");
        }
      }).
      catch(function(error) {
        $scope.error = "Invalid Username or Password";
        angular.element(document.querySelector('#log-form-group')).addClass('has-danger');
        angular.element(document.querySelector('#pwd-form-group')).addClass('has-danger');
      });
    };

  }]);
